#!/bin/bash


[ -f .env.development ] && . .env.development
[ -f .env.development.local ] && . .env.development.local

export REACT_APP_GRAPHQL_URI
export REACT_APP_GRAPHQL_PASS

npm run build

echo "to serve it, run : ./node_modules/.bin/serve -p $PORT -s build"
