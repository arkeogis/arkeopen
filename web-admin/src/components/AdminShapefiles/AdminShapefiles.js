import React from 'react'
import PropTypes from 'prop-types'

import AdminAkgShapefiles from '../AdminAkgShapefiles/AdminAkgShapefiles'
import AdminAkoShapefiles from '../AdminAkoShapefiles/AdminAkoShapefiles'

const AdminShapefiles = props => {
  return (
    <div className="AdminShapefiles RightComponent">
      <h1>Administration des shapefiles</h1>
      <div className="AdminShapefilesPanel AdminPanel">
        <AdminAkgShapefiles/>
        <AdminAkoShapefiles/>
      </div>
    </div>
  )
}

AdminShapefiles.propTypes = {}

export default AdminShapefiles
