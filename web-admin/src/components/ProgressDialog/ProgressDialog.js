// react
import React, { useState } from 'react'
import PropTypes from 'prop-types'

// bootstrap
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import ProgressBar from 'react-bootstrap/ProgressBar';

import { BiError, BiQuestionMark, BiCheck } from 'react-icons/bi';
import { BsGearFill } from 'react-icons/bs';

const ProgressDialog = props => {
  const { title, body, percent, error, onClose, onConfirm } = props;
  const [ show, setShow ] = useState(true);
  const [ askingConfirm, setAskingConfirm ] = useState(true);

  const onPrimaryButtonClick = () => {
    if (askingConfirm) {
      setAskingConfirm(false);
      onConfirm();
    } else {
      setShow(false);
    }
  }

  let errstr=null;
  if (error) {
    try {
      errstr = error.graphQLErrors[0].extensions.internal.error.description;
    } catch (err2) {
      if (error.message) {
        if (error.description)
          errstr = `${error.message}; ${error.description}`;
        else
          errstr = error.message;
      } else if (error) {
        errstr = error;
      }
    }
  }

  return (
    <Modal show={show} backdrop="static" onExited={() => { if (onClose) onClose() }}>
      <Modal.Header>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        {error ?
          (<div className="ModalError"><BiError className="ModalErrorIcon" /><p>{errstr}</p></div>) : ''
        }
        {!error && askingConfirm ?
          (<div className="ModalConfirm"><BiQuestionMark className="ModalConfirmIcon" /><p>{body} ?</p></div>) : ''
        }
        {!error && !askingConfirm && percent < 100 ?
          (<div className="ModalWorking"><BsGearFill className="ModalWorkingIcon animatedrotation" /><p>{body}...</p></div>) : ''
        }
        {!error && !askingConfirm && percent >= 100 ?
          (<div className="ModalDone"><BiCheck className="ModalDoneIcon" /><p>{body}.</p></div>) : ''
        }
        {!error && !askingConfirm ?
          (<ProgressBar animated={percent < 100} now={percent} />) : ''
        }
      </Modal.Body>

      <Modal.Footer>
        { askingConfirm ? (
          <Button
            onClick={() => setShow(false)}
            variant="secondary">
              Annuler
          </Button>
        ) : ''}
        <Button
          disabled={!askingConfirm && percent < 100}
          onClick={() => onPrimaryButtonClick()}
          variant="primary">
            { askingConfirm ? "Confirmer" : "Ok" }
        </Button>
      </Modal.Footer>
    </Modal>
  );

}

ProgressDialog.propTypes = {
  title: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  percent: PropTypes.number.isRequired,
  onConfirm: PropTypes.func,
  onClose: PropTypes.func,
  error: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
}

export default ProgressDialog
