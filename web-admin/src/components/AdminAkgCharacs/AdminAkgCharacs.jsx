// React
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

// GraphQL
import { useQuery, useApolloClient } from '@apollo/client'

// Components
import ProgressDialog from '../ProgressDialog/ProgressDialog'

// libs
import { transfertCharac, downloadAkgCharac, uploadAkgCharac } from '../../lib/transfertCharac'
import { AKGGetCharacsRoot } from '../../lib/queries/charac'
import { getInLang } from '../../lib/translate'

// store
import useStore from '../../lib/store'
import {shallow} from 'zustand/shallow'

import { FaLongArrowAltRight } from 'react-icons/fa';
import SimpleDate from '../SimpleDate/SimpleDate'

const AdminAkgCharacs = props => {
  const apolloClient = useApolloClient();
  const { loading, error, data, refetch } = useQuery(AKGGetCharacsRoot, {
    //variables: { public: true }
  })
  const [ transfering, setTransfering ] = useState(null);
  const [ akgCharacsValid, setAkgCharacsValid, setAkoCharacsValid ] = useStore(state => [
    state.akgCharacsValid, state.setAkgCharacsValid, state.setAkoCharacsValid
  ], shallow);

  useEffect(() => {
    if (!akgCharacsValid)
      refetch();
  }, [ akgCharacsValid, refetch ])

  useEffect(() => {
    if (!loading && data)
      setAkgCharacsValid(true);
  }, [ data, loading, setAkgCharacsValid ]);

  const myUploadAkgCharac = (e) => {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0], "UTF-8");
    fileReader.onload = e => {
      console.log("e.target.result", e.target.result);
      //uploadAkgCharac(apolloClient, JSON.parse(e.target.result));
    };
  }

  const doTransfert = () => {
    transfertCharac(apolloClient, transfering.id_charac).then(res => {
      setTransfering({
        ...transfering,
        percent: 100,
      })
      setAkoCharacsValid(false);
    }).catch(err => {
      setTransfering({
        ...transfering,
        percent: 100,
        error: err
      })
    })
  }

  const transfert = (id_charac, characTitle) => {
    setTransfering({
      percent: 0,
      characTitle,
      id_charac,
    })
  }

  let content;

  if (loading) {
    content=<div>Loading...</div>
  } else if (error) {
    content=<div className="error">Error: {error.message}</div>
  } else {
    console.log("data", data);
    content=(
      <ul className="akg-characs-list list">
        {data.akg_charac_root.map((charac_root, index) => (
          <li key={charac_root.charac.id}>
            <div className="akg-charac-title title">
              {getInLang(charac_root.charac.charac_trs).name} (<SimpleDate d={charac_root.charac.updated_at}/>)
            </div>
            <button onClick={() => transfert(charac_root.charac.id, getInLang(charac_root.charac.charac_trs).name)}><FaLongArrowAltRight/></button>
            {/*<button onClick={() => downloadAkgCharac(apolloClient, charac_root.charac.id)}>Download</button>*/}
          </li>
        ))}
      </ul>
    )
  }

  return (
    <div className="AdminAkgCharacs AkgPanel">
      <h2>Arkeogis</h2>
      {content}
      {/*Import/Mise à jour d'une characlogie : <input type="file" onChange={e => myUploadAkgCharac(e) }/>*/}
      {transfering !== null ? (
        <ProgressDialog
          title="Transfering"
          body={`Transfering charac ${transfering.characTitle}`}
          percent={transfering.percent}
          onClose={() => setTransfering(null)}
          onConfirm={() => doTransfert()}
          error={transfering.error}
        />
      ) : ''}
    </div>
  )
}

AdminAkgCharacs.propTypes = {}

export default AdminAkgCharacs
