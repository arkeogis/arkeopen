import React from 'react'
import PropTypes from 'prop-types'

import AdminAkgCharacs from '../AdminAkgCharacs/AdminAkgCharacs'
import AdminAkoCharacs from '../AdminAkoCharacs/AdminAkoCharacs'

const AdminCharacs = props => {
  return (
    <div className="AdminCharacs RightComponent">
      <h1>Administration des Charactéristiques</h1>
      <div className="AdminCharacsPanel AdminPanel">
        <AdminAkgCharacs/>
        <AdminAkoCharacs/>
      </div>
    </div>
  )
}

AdminCharacs.propTypes = {}

export default AdminCharacs
