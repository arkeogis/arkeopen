// React
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

// GraphQL
import { useQuery, useApolloClient } from '@apollo/client'

// Components
import ProgressDialog from '../ProgressDialog/ProgressDialog'

// libs
import { transfertShapefile, downloadAkgShapefile, uploadAkgShapefile } from '../../lib/transfertShapefile'
import { GetAKGShapefiles } from '../../lib/queries/shapefile'
import { getInLang } from '../../lib/translate'

// store
import useStore from '../../lib/store'
import {shallow} from 'zustand/shallow'

import { FaLongArrowAltRight } from 'react-icons/fa';
import SimpleDate from '../SimpleDate/SimpleDate'

const AdminAkgShapefiles = props => {
  const apolloClient = useApolloClient();
  const { loading, error, data, refetch } = useQuery(GetAKGShapefiles, {
    //variables: { public: true }
  })
  const [ transfering, setTransfering ] = useState(null);
  const [ akgShapefilesValid, setAkgShapefilesValid, setAkoShapefilesValid ] = useStore(state => [
    state.akgShapefilesValid, state.setAkgShapefilesValid, state.setAkoShapefilesValid
  ], shallow);

  useEffect(() => {
    if (!akgShapefilesValid)
      refetch();
  }, [ akgShapefilesValid, refetch ])

  useEffect(() => {
    if (!loading && data)
      setAkgShapefilesValid(true);
  }, [ data, loading, setAkgShapefilesValid ]);

  const myUploadAkgShapefile = (e) => {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0], "UTF-8");
    fileReader.onload = e => {
      console.log("e.target.result", e.target.result);
      //uploadAkgShapefile(apolloClient, JSON.parse(e.target.result));
    };
  }

  const doTransfert = () => {
    transfertShapefile(apolloClient, transfering.id_shapefile).then(res => {
      setTransfering({
        ...transfering,
        percent: 100,
      })
      setAkoShapefilesValid(false);
    }).catch(err => {
      console.error(err);
      setTransfering({
        ...transfering,
        percent: 100,
        error: err
      })
    })
  }

  const transfert = (id_shapefile, shapefileTitle) => {
    setTransfering({
      percent: 0,
      shapefileTitle,
      id_shapefile,
    })
  }

  let content;

  if (loading) {
    content=<div>Loading...</div>
  } else if (error) {
    content=<div className="error">Error: {error.message}</div>
  } else {
    console.log("data", data);
    content=(
      <ul className="akg-shapefiles-list list">
        {data.akg_shapefile.map((shapefile, index) => (
          <li key={shapefile.id}>
            <div className="akg-shapefile-title title">
              {getInLang(shapefile.shapefile_trs).name} (<SimpleDate d={shapefile.updated_at}/>)
            </div>
            <button onClick={() => transfert(shapefile.id, getInLang(shapefile.shapefile_trs).name)}><FaLongArrowAltRight/></button>
            {/*<button onClick={() => downloadAkgShapefile(apolloClient, shapefile.id)}>Download</button>*/}
          </li>
        ))}
      </ul>
    )
  }

  return (
    <div className="AdminAkgShapefiles AkgPanel">
      <h2>Arkeogis</h2>
      {content}
      {/*Import/Mise à jour d'un shapefile : <input type="file" onChange={e => myUploadAkgShapefile(e) }/>*/}
      {transfering !== null ? (
        <ProgressDialog
          title="Transfering"
          body={`Transfering shapefile ${transfering.shapefileTitle}`}
          percent={transfering.percent}
          onClose={() => setTransfering(null)}
          onConfirm={() => doTransfert()}
          error={transfering.error}
        />
      ) : ''}
    </div>
  )
}

AdminAkgShapefiles.propTypes = {}

export default AdminAkgShapefiles
