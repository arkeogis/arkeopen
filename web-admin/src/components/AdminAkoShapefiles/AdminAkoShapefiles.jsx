// React
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

// GraphQL
import { useQuery, useApolloClient } from '@apollo/client'

// Components
import ProgressDialog from '../ProgressDialog/ProgressDialog'

// libs
import { deleteAkoShapefile } from '../../lib/transfertShapefile'
//import { transfertShapefile, downloadAkoShapefile, uploadAkoShapefile } from '../../lib/transfertShapefile'
import { getInLang } from '../../lib/translate'
import { GetAKOShapefiles } from '../../lib/queries/shapefile'

import { BsTrash } from 'react-icons/bs'

// store
import useStore from '../../lib/store'
import {shallow} from 'zustand/shallow'
import SimpleDate from '../SimpleDate/SimpleDate'

const AdminAkoShapefiles = props => {
  const apolloClient = useApolloClient();
  const { loading, error, data, refetch } = useQuery(GetAKOShapefiles, {
    //variables: { public: true }
  })
  const [ deleting, setDeleting ] = useState(null);
  const [ akoShapefilesValid, setAkoShapefilesValid ] = useStore(state => [
    state.akoShapefilesValid, state.setAkoShapefilesValid
  ], shallow);

  useEffect(() => {
    if (!akoShapefilesValid)
      refetch();
  }, [ akoShapefilesValid, refetch ])

  useEffect(() => {
    if (!loading && data)
      setAkoShapefilesValid(true);
  }, [ data, loading, setAkoShapefilesValid ]);

  const doDeleteShapefile = () => {
    deleteAkoShapefile(apolloClient, deleting.id_shapefile).then(res => {
      setDeleting({
        ...deleting,
        percent: 100,
      })
      setAkoShapefilesValid(false);
    }).catch(err => {
      setDeleting({
        ...deleting,
        percent: 100,
        error: err
      })
    })
  }

  const deleteShapefile = (id_shapefile, shapefileTitle) => {
    setDeleting({
      percent: 0,
      shapefileTitle,
      id_shapefile,
    })
  }

  const myUploadAkoShapefile = (e) => {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0], "UTF-8");
    fileReader.onload = e => {
      console.log("e.target.result", e.target.result);
      //uploadAkoShapefile(apolloClient, JSON.parse(e.target.result));
    };
  }

  let content;

  if (loading) {
    content=<div>Loading...</div>
  } else if (error) {
    content=<div className="error">Error: {error.message}</div>
  } else {
    content=(
      <ul className="ako-shapefiles-list list">
          {data.ako_shapefile.map((shapefile, index) => (
              <li key={shapefile.id}>
                <div className="ako-shapefile-title title">
                  {getInLang(shapefile.shapefile_trs).name} (<SimpleDate d={shapefile.updated_at}/>)
                </div>
                <button onClick={() => deleteShapefile(shapefile.id, getInLang(shapefile.shapefile_trs).name)}><BsTrash/></button>
              </li>
          ))}
      </ul>
    )
  }

  return (
    <div className="AdminAkoShapefiles AkoPanel">
      <h2>ArkeOpen</h2>
      {content}
      {/*Import/update a shapefile : <input type="file" onChange={e => myUploadAkoShapefile(e) }/>*/}
      {deleting !== null ? (
        <ProgressDialog
          title="Deleting"
          body={`Deleting shapefile ${deleting.shapefileTitle}`}
          percent={deleting.percent}
          onClose={() => setDeleting(null)}
          onConfirm={() => doDeleteShapefile()}
          error={deleting.error}
        />
      ) : ''}
    </div>
  )
}

AdminAkoShapefiles.propTypes = {}

export default AdminAkoShapefiles
