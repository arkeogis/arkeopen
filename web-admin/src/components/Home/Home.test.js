import React from 'react';
import { render, screen } from '@testing-library/react';
import Home from './Home';

describe('Home Component', () => {
  test('renders Home component without crashing', () => {
    render(<Home />);
    const homeElement = screen.getByTestId('home-component');
    expect(homeElement).toBeInTheDocument();
  });

  test('displays the correct title', () => {
    render(<Home />);
    const titleElement = screen.getByText(/Administration/i);
    expect(titleElement).toBeInTheDocument();
  });

  // Add more tests as needed
});
