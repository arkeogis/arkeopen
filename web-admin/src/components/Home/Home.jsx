import React from 'react'

const Home = () => {
  return (
    <div data-testid="home-component" className="HomeComponent RightComponent">
      <h1>Administration d'ArkeOpen</h1>

      <p>Permet le transfert de données depuis ArkeoGIS vers ArkeOPEN, ainsi que la suppression de données ArkeOPEN précédament transferé</p>

      <p>Veuillez choisir une rubrique a gauche</p>

    </div>
  )
}

export default Home;
