// React
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

// GraphQL
import { useQuery, useApolloClient } from '@apollo/client'

// Components
import ProgressDialog from '../ProgressDialog/ProgressDialog'

// libs
import { transfertDatabase, downloadAkgDatabase, uploadAkgDatabase } from '../../lib/transfertDatabase'
import { AKGGetDatabases } from '../../lib/queries/database'
import { getInLang } from '../../lib/translate'

// store
import useStore from '../../lib/store'
import {shallow} from 'zustand/shallow'

import { FaLongArrowAltRight } from 'react-icons/fa';
import SimpleDate from '../SimpleDate/SimpleDate'

const AdminAkgDatabases = props => {
  const apolloClient = useApolloClient();
  const { loading, error, data, refetch } = useQuery(AKGGetDatabases, {
    //variables: { public: true }
  })
  const [ transfering, setTransfering ] = useState(null);
  const [ akgDatabasesValid, setAkgDatabasesValid, setAkoDatabasesValid ] = useStore(state => [
    state.akgDatabasesValid, state.setAkgDatabasesValid, state.setAkoDatabasesValid
  ], shallow);

  useEffect(() => {
    if (!akgDatabasesValid)
      refetch();
  }, [ akgDatabasesValid, refetch ])

  useEffect(() => {
    if (!loading && data)
      setAkgDatabasesValid(true);
  }, [ data, loading, setAkgDatabasesValid ]);

  const myUploadAkgDatabase = (e) => {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0], "UTF-8");
    fileReader.onload = e => {
      console.log("e.target.result", e.target.result);
      //uploadAkgDatabase(apolloClient, JSON.parse(e.target.result));
    };
  }

  const doTransfert = () => {
    transfertDatabase(apolloClient, transfering.id_database).then(res => {
      setTransfering({
        ...transfering,
        percent: 100,
      })
      setAkoDatabasesValid(false);
    }).catch(err => {
      setTransfering({
        ...transfering,
        percent: 100,
        error: err
      })
    })
  }

  const transfert = (id_database, databaseTitle) => {
    setTransfering({
      percent: 0,
      databaseTitle,
      id_database,
    })
  }

  let content;

  if (loading) {
    content=<div>Loading...</div>
  } else if (error) {
    content=<div className="error">Error: {error.message}</div>
  } else {
    console.log("data", data);
    content=(
      <ul className="akg-databases-list list">
        {data.akg_database.map((database, index) => (
          <li key={database.id}>
            <div className="akg-database-title title">
              {database.name} (<SimpleDate d={database.updated_at}/>)
            </div>
            <button onClick={() => transfert(database.id, database.name)}><FaLongArrowAltRight/></button>
            {/*<button onClick={() => downloadAkgDatabase(apolloClient, database_root.database.id)}>Download</button>*/}
          </li>
        ))}
      </ul>
    )
  }

  return (
    <div className="AdminAkgDatabases AkgPanel">
      <h2>Arkeogis</h2>
      {content}
      {/*Import/Mise à jour d'une databaselogie : <input type="file" onChange={e => myUploadAkgDatabase(e) }/>*/}
      {transfering !== null ? (
        <ProgressDialog
          title="Transfering"
          body={`Transfering database ${transfering.databaseTitle}`}
          percent={transfering.percent}
          onClose={() => setTransfering(null)}
          onConfirm={() => doTransfert()}
          error={transfering.error}
        />
      ) : ''}
    </div>
  )
}

AdminAkgDatabases.propTypes = {}

export default AdminAkgDatabases
