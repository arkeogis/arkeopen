// React
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

// GraphQL
import { useQuery, useApolloClient } from '@apollo/client'

// Components
import ProgressDialog from '../ProgressDialog/ProgressDialog'

// libs
import { deleteAkoDatabase } from '../../lib/transfertDatabase'
//import { transfertDatabase, downloadAkoDatabase, uploadAkoDatabase } from '../../lib/transfertDatabase'
import { getInLang } from '../../lib/translate'
import { AKOGetDatabases } from '../../lib/queries/database'

import { BsTrash } from 'react-icons/bs'

// store
import useStore from '../../lib/store'
import {shallow} from 'zustand/shallow'
import SimpleDate from '../SimpleDate/SimpleDate'

const AdminAkoDatabases = props => {
  const apolloClient = useApolloClient();
  const { loading, error, data, refetch } = useQuery(AKOGetDatabases, {
    //variables: { public: true }
  })
  const [ deleting, setDeleting ] = useState(null);
  const [ akoDatabasesValid, setAkoDatabasesValid ] = useStore(state => [
    state.akoDatabasesValid, state.setAkoDatabasesValid
  ], shallow);

  useEffect(() => {
    if (!akoDatabasesValid)
      refetch();
  }, [ akoDatabasesValid, refetch ])

  useEffect(() => {
    if (!loading && data)
      setAkoDatabasesValid(true);
  }, [ data, loading, setAkoDatabasesValid ]);

  const doDeleteDatabase = () => {
    deleteAkoDatabase(apolloClient, deleting.id_database).then(res => {
      setDeleting({
        ...deleting,
        percent: 100,
      })
      setAkoDatabasesValid(false);
    }).catch(err => {
      setDeleting({
        ...deleting,
        percent: 100,
        error: err
      })
    })
  }

  const deleteDatabase = (id_database, databaseTitle) => {
    setDeleting({
      percent: 0,
      databaseTitle,
      id_database,
    })
  }

  const myUploadAkoDatabase = (e) => {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0], "UTF-8");
    fileReader.onload = e => {
      console.log("e.target.result", e.target.result);
      //uploadAkoDatabase(apolloClient, JSON.parse(e.target.result));
    };
  }

  let content;

  if (loading) {
    content=<div>Loading...</div>
  } else if (error) {
    content=<div className="error">Error: {error.message}</div>
  } else {
    content=(
      <ul className="ako-databases-list list">
          {data.ako_database.map((database, index) => (
              <li key={database.id}>
                <div className="ako-database-title title">
                  {database.name} (<SimpleDate d={database.updated_at}/>)
                </div>
                <button onClick={() => deleteDatabase(database.id, database.name)}><BsTrash/></button>
              </li>
          ))}
      </ul>
    )
  }

  return (
    <div className="AdminAkoDatabases AkoPanel">
      <h2>ArkeOpen</h2>
      {content}
      {/*Import/update a database : <input type="file" onChange={e => myUploadAkoDatabase(e) }/>*/}
      {deleting !== null ? (
        <ProgressDialog
          title="Deleting"
          body={`Deleting database ${deleting.databaseTitle}`}
          percent={deleting.percent}
          onClose={() => setDeleting(null)}
          onConfirm={() => doDeleteDatabase()}
          error={deleting.error}
        />
      ) : ''}
    </div>
  )
}

AdminAkoDatabases.propTypes = {}

export default AdminAkoDatabases
