import React from 'react'
import PropTypes from 'prop-types'

import AdminAkgChronos from '../AdminAkgChronos/AdminAkgChronos'
import AdminAkoChronos from '../AdminAkoChronos/AdminAkoChronos'

const AdminChronos = props => {
  return (
    <div className="AdminChronos RightComponent">
      <h1>Administration des chronologies</h1>
      <div className="AdminChronosPanel AdminPanel">
        <AdminAkgChronos/>
        <AdminAkoChronos/>
      </div>
    </div>
  )
}

AdminChronos.propTypes = {}

export default AdminChronos
