// React
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

// GraphQL
import { useQuery, useApolloClient } from '@apollo/client'

// Components
import ProgressDialog from '../ProgressDialog/ProgressDialog'

// libs
import { transfertChrono, downloadAkgChrono, uploadAkgChrono } from '../../lib/transfertChrono'
import { AKGGetChronosRoot } from '../../lib/queries/chronology'
import { getInLang } from '../../lib/translate'

// store
import useStore from '../../lib/store'
import {shallow} from 'zustand/shallow'

import { FaLongArrowAltRight } from 'react-icons/fa';
import SimpleDate from '../SimpleDate/SimpleDate'

const AdminAkgChronos = props => {
  const apolloClient = useApolloClient();
  const { loading, error, data, refetch } = useQuery(AKGGetChronosRoot, {
    //variables: { public: true }
  })
  const [ transfering, setTransfering ] = useState(null);
  const [ akgChronosValid, setAkgChronosValid, setAkoChronosValid ] = useStore(state => [
    state.akgChronosValid, state.setAkgChronosValid, state.setAkoChronosValid
  ], shallow);

  useEffect(() => {
    if (!akgChronosValid)
      refetch();
  }, [ akgChronosValid, refetch ])

  useEffect(() => {
    if (!loading && data)
      setAkgChronosValid(true);
  }, [ data, loading, setAkgChronosValid ]);

  const myUploadAkgChrono = (e) => {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0], "UTF-8");
    fileReader.onload = e => {
      console.log("e.target.result", e.target.result);
      //uploadAkgChrono(apolloClient, JSON.parse(e.target.result));
    };
  }

  const doTransfert = () => {
    transfertChrono(apolloClient, transfering.id_chrono).then(res => {
      setTransfering({
        ...transfering,
        percent: 100,
      })
      setAkoChronosValid(false);
    }).catch(err => {
      setTransfering({
        ...transfering,
        percent: 100,
        error: err
      })
    })
  }

  const transfert = (id_chrono, chronoTitle) => {
    setTransfering({
      percent: 0,
      chronoTitle,
      id_chrono,
    })
  }

  let content;

  if (loading) {
    content=<div>Loading...</div>
  } else if (error) {
    content=<div className="error">Error: {error.message}</div>
  } else {
    console.log("data", data);
    content=(
      <ul className="akg-chronos-list list">
        {data.akg_chronology_root.map((chronology_root, index) => (
          <li key={chronology_root.chronology.id}>
            <div className="akg-chrono-title title">
              {getInLang(chronology_root.chronology.chronology_trs).name} (<SimpleDate d={chronology_root.chronology.updated_at}/>)
            </div>
            <button onClick={() => transfert(chronology_root.chronology.id, getInLang(chronology_root.chronology.chronology_trs).name)}><FaLongArrowAltRight/></button>
            {/*<button onClick={() => downloadAkgChrono(apolloClient, chronology_root.chronology.id)}>Download</button>*/}
          </li>
        ))}
      </ul>
    )
  }

  return (
    <div className="AdminAkgChronos AkgPanel">
      <h2>Arkeogis</h2>
      {content}
      {/*Import/Mise à jour d'une chronologie : <input type="file" onChange={e => myUploadAkgChrono(e) }/>*/}
      {transfering !== null ? (
        <ProgressDialog
          title="Transfering"
          body={`Transfering chronology ${transfering.chronoTitle}`}
          percent={transfering.percent}
          onClose={() => setTransfering(null)}
          onConfirm={() => doTransfert()}
          error={transfering.error}
        />
      ) : ''}
    </div>
  )
}

AdminAkgChronos.propTypes = {}

export default AdminAkgChronos
