import React from 'react'
import PropTypes from 'prop-types'

const SimpleDate = props => {
  const { d } = props;
  const date = new Date(d).toLocaleDateString('fr-FR', {
    hour: 'numeric',
    minute: 'numeric'
  });
  return (
    <span className="SimpleDate">{date}</span>
  )
}

SimpleDate.propTypes = {
  /**
   * The date to display
   */
  d: PropTypes.string.isRequired,
}

export default SimpleDate
