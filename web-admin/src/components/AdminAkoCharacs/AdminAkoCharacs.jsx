// React
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

// GraphQL
import { useQuery, useApolloClient } from '@apollo/client'

// Components
import ProgressDialog from '../ProgressDialog/ProgressDialog'

// libs
import { deleteAkoCharac } from '../../lib/transfertCharac'
//import { transfertCharac, downloadAkoCharac, uploadAkoCharac } from '../../lib/transfertCharac'
import { getInLang } from '../../lib/translate'
import { AKOGetCharacsRoot } from '../../lib/queries/charac'

import { BsTrash } from 'react-icons/bs'

// store
import useStore from '../../lib/store'
import {shallow} from 'zustand/shallow'
import SimpleDate from '../SimpleDate/SimpleDate'

const AdminAkoCharacs = props => {
  const apolloClient = useApolloClient();
  const { loading, error, data, refetch } = useQuery(AKOGetCharacsRoot, {
    //variables: { public: true }
  })
  const [ deleting, setDeleting ] = useState(null);
  const [ akoCharacsValid, setAkoCharacsValid ] = useStore(state => [
    state.akoCharacsValid, state.setAkoCharacsValid
  ], shallow);

  useEffect(() => {
    if (!akoCharacsValid)
      refetch();
  }, [ akoCharacsValid, refetch ])

  useEffect(() => {
    if (!loading && data)
      setAkoCharacsValid(true);
  }, [ data, loading, setAkoCharacsValid ]);

  const doDeleteCharac = () => {
    deleteAkoCharac(apolloClient, deleting.id_charac).then(res => {
      setDeleting({
        ...deleting,
        percent: 100,
      })
      setAkoCharacsValid(false);
    }).catch(err => {
      setDeleting({
        ...deleting,
        percent: 100,
        error: err
      })
    })
  }

  const deleteCharac = (id_charac, characTitle) => {
    setDeleting({
      percent: 0,
      characTitle,
      id_charac,
    })
  }

  const myUploadAkoCharac = (e) => {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0], "UTF-8");
    fileReader.onload = e => {
      console.log("e.target.result", e.target.result);
      //uploadAkoCharac(apolloClient, JSON.parse(e.target.result));
    };
  }

  let content;

  if (loading) {
    content=<div>Loading...</div>
  } else if (error) {
    content=<div className="error">Error: {error.message}</div>
  } else {
    content=(
      <ul className="ako-characs-list list">
          {data.ako_charac_root.map((charac_root, index) => (
              <li key={charac_root.charac.id}>
                <div className="ako-charac-title title">
                  {getInLang(charac_root.charac.charac_trs).name} (<SimpleDate d={charac_root.charac.updated_at}/>)
                </div>
                <button onClick={() => deleteCharac(charac_root.charac.id, getInLang(charac_root.charac.charac_trs).name)}><BsTrash/></button>
              </li>
          ))}
      </ul>
    )
  }

  return (
    <div className="AdminAkoCharacs AkoPanel">
      <h2>ArkeOpen</h2>
      {content}
      {/*Import/update a charac : <input type="file" onChange={e => myUploadAkoCharac(e) }/>*/}
      {deleting !== null ? (
        <ProgressDialog
          title="Deleting"
          body={`Deleting charac ${deleting.characTitle}`}
          percent={deleting.percent}
          onClose={() => setDeleting(null)}
          onConfirm={() => doDeleteCharac()}
          error={deleting.error}
        />
      ) : ''}
    </div>
  )
}

AdminAkoCharacs.propTypes = {}

export default AdminAkoCharacs
