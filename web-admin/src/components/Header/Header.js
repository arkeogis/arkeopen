import React from 'react'
import './Header.css'

const Header = () => {
  return (
    <div className="ComponentHeader">
      <header>
        <div className="header-title">Arkeopen - Admin</div>
      </header>
    </div>
  )
}

export default Header
