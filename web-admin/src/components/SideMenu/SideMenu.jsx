import React from 'react'

import { Link } from "react-router-dom"


const SideMenu = () => {
  return (
    <div className="SideMenuComponent">
      <h2>Menu</h2>
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/AdminDatabases">Admin Databases</Link></li>
        <li><Link to="/AdminChronos">Admin Chronos</Link></li>
        <li><Link to="/AdminCharacs">Admin Characs</Link></li>
        <li><Link to="/AdminShapefiles">Admin Shapefiles</Link></li>
      </ul>
    </div>
  )
}

export default SideMenu
