import React from 'react'
import PropTypes from 'prop-types'

import AdminAkgDatabases from '../AdminAkgDatabases/AdminAkgDatabases'
import AdminAkoDatabases from '../AdminAkoDatabases/AdminAkoDatabases'

const AdminDatabases = props => {
  return (
    <div className="AdminDatabases RightComponent">
      <h1>Administration des Databases</h1>
      <div className="AdminDatabasesPanel AdminPanel">
        <AdminAkgDatabases/>
        <AdminAkoDatabases/>
      </div>
    </div>
  )
}

AdminDatabases.propTypes = {}

export default AdminDatabases
