// React
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

// GraphQL
import { useQuery, useApolloClient } from '@apollo/client'

// Components
import ProgressDialog from '../ProgressDialog/ProgressDialog'

// libs
import { deleteAkoChrono } from '../../lib/transfertChrono'
//import { transfertChrono, downloadAkoChrono, uploadAkoChrono } from '../../lib/transfertChrono'
import { getInLang } from '../../lib/translate'
import { AKOGetChronosRoot } from '../../lib/queries/chronology'

import { BsTrash } from 'react-icons/bs'

// store
import useStore from '../../lib/store'
import {shallow} from 'zustand/shallow'
import SimpleDate from '../SimpleDate/SimpleDate'

const AdminAkoChronos = props => {
  const apolloClient = useApolloClient();
  const { loading, error, data, refetch } = useQuery(AKOGetChronosRoot, {
    //variables: { public: true }
  })
  const [ deleting, setDeleting ] = useState(null);
  const [ akoChronosValid, setAkoChronosValid ] = useStore(state => [
    state.akoChronosValid, state.setAkoChronosValid
  ], shallow);

  useEffect(() => {
    if (!akoChronosValid)
      refetch();
  }, [ akoChronosValid, refetch ])

  useEffect(() => {
    if (!loading && data)
      setAkoChronosValid(true);
  }, [ data, loading, setAkoChronosValid ]);

  const doDeleteChrono = () => {
    deleteAkoChrono(apolloClient, deleting.id_chrono).then(res => {
      setDeleting({
        ...deleting,
        percent: 100,
      })
      setAkoChronosValid(false);
    }).catch(err => {
      setDeleting({
        ...deleting,
        percent: 100,
        error: err
      })
    })
  }

  const deleteChrono = (id_chrono, chronoTitle) => {
    setDeleting({
      percent: 0,
      chronoTitle,
      id_chrono,
    })
  }

  const myUploadAkoChrono = (e) => {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0], "UTF-8");
    fileReader.onload = e => {
      console.log("e.target.result", e.target.result);
      //uploadAkoChrono(apolloClient, JSON.parse(e.target.result));
    };
  }

  let content;

  if (loading) {
    content=<div>Loading...</div>
  } else if (error) {
    content=<div className="error">Error: {error.message}</div>
  } else {
    content=(
      <ul className="ako-chronos-list list">
          {data.ako_chronology_root.map((chronology_root, index) => (
              <li key={chronology_root.chronology.id}>
                <div className="ako-chrono-title title">
                  {getInLang(chronology_root.chronology.chronology_trs).name} (<SimpleDate d={chronology_root.chronology.updated_at}/>)
                </div>
                <button onClick={() => deleteChrono(chronology_root.chronology.id, getInLang(chronology_root.chronology.chronology_trs).name)}><BsTrash/></button>
              </li>
          ))}
      </ul>
    )
  }

  return (
    <div className="AdminAkoChronos AkoPanel">
      <h2>ArkeOpen</h2>
      {content}
      {/*Import/update a chronology : <input type="file" onChange={e => myUploadAkoChrono(e) }/>*/}
      {deleting !== null ? (
        <ProgressDialog
          title="Deleting"
          body={`Deleting chronology ${deleting.chronoTitle}`}
          percent={deleting.percent}
          onClose={() => setDeleting(null)}
          onConfirm={() => doDeleteChrono()}
          error={deleting.error}
        />
      ) : ''}
    </div>
  )
}

AdminAkoChronos.propTypes = {}

export default AdminAkoChronos
