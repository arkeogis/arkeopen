import raw from "raw.macro";
const pluralize = require('pluralize')
const { XMLParser } = require("fast-xml-parser");
const xml = raw('../../../arkeogis.xml'); // 42 <= when changing arkeogis.xml file, increment this comment number, to invalidate cache

const xmlparser = new XMLParser({
  ignoreAttributes: false,
  attributesGroupName : "_attributes",
  attributeNamePrefix : ""
});
const xmlinjs = xmlparser.parse(xml);

const tables = xmlinjs.sql.table.map(t => {
  const table = {};
  Object.assign(table, t._attributes);
  table.pluralName = pluralize.plural(table.name);
  table.cols = t.row.map(c => { // a row for sqldesigner is a sql column. To avoid confusion, we rename it immediatly
    const col = {};
    Object.assign(col, c._attributes);
    col.pluralName = pluralize.plural(col.name);
    if (c.datatype) col.datatype = c.datatype;
    if (c.comment) col.comment = c.comment;
    if (c.default) col.default = c.default;
    if (c.relation) col.relation = c.relation._attributes;
    return col;
  });
  return table;
})

// build tablesByName and colsByName
const tablesByName = {};
tables.forEach(t => {
  t.colsByName = {};
  t.cols.forEach(c => t.colsByName[c.name] = c);
  tablesByName[t.name] = t;
})

// build relations
tables.forEach(t => {
  t.cols.forEach(c => {
    if (c.relation) {

      if (!('relations' in tablesByName[c.relation.table]))
        tablesByName[c.relation.table].relations = {};

      //tablesByName[c.relation.table].relations.push({table: t, col: c, putByTable: t.name, putByRow: c.name})
      tablesByName[c.relation.table].relations[t.pluralName]={
        table: t, col: c, multiple: true,
      };

      if (!('relations' in t)) t.relations={};
      //t.relations.push({table: c.relation.table, byowncol: c, putByTable: t.name, putByRow: c.name })

      let relationName = c.relation.table;

      // special case : if a column already exist with the same name that the relation will be made by asura by default
      // we use the name hasura will use in taht cas, for example :
      // if a column "license" and "license_id" eixsts in the same table, le license object linked to "license_id" will be
      // named to "licenseByLicenseId" by default by hasura.
      if (t.colsByName[c.relation.table] !== undefined) {
        function snakeToCamel(str){
          str=str.replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase());
          return str.charAt(0).toUpperCase() + str.slice(1)
        }
        relationName = `${c.relation.table}By${snakeToCamel(c.name)}`;
      }

      t.relations[relationName]={
        table: tablesByName[c.relation.table], col: c, multiple: false
      };
    }
  });
})

exports.tables = tables;
exports.tablesByName = tablesByName;

console.log("tablesByName", tablesByName);

/*
* stupid hack
*/
//console.warn("stupid hack on user is there (src/lib/db/index.js)")
//tablesByName.users = tablesByName.user;
