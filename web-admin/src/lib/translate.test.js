import { getInLang } from '../lib/translate';

describe('getInLang', () => {
  const translations = [
    { lang_isocode: 'en', text: 'Hello' },
    { lang_isocode: 'fr', text: 'Bonjour' },
    { lang_isocode: 'es', text: 'Hola' },
    { lang_isocode: 'de', text: 'Hallo' }
  ];

  test('should return the French translation when wantedlang is "fr"', () => {
    const result = getInLang(translations, 'fr');
    expect(result).toEqual({ lang_isocode: 'fr', text: 'Bonjour' });
  });

  test('should return the English translation when wantedlang is "en"', () => {
    const result = getInLang(translations, 'en');
    expect(result).toEqual({ lang_isocode: 'en', text: 'Hello' });
  });

  test('should return the German translation when wantedlang is "de"', () => {
    const result = getInLang(translations, 'de');
    expect(result).toEqual({ lang_isocode: 'de', text: 'Hallo' });
  });

  test('should return the Spanish translation when wantedlang is "es"', () => {
    const result = getInLang(translations, 'es');
    expect(result).toEqual({ lang_isocode: 'es', text: 'Hola' });
  });

  test('should return the default French translation when wantedlang is not provided', () => {
    const result = getInLang(translations);
    expect(result).toEqual({ lang_isocode: 'fr', text: 'Bonjour' });
  });

  test('should return undefined if no translation is found for the wanted language', () => {
    const result = getInLang([{ lang_isocode: 'it', text: 'Ciao' }], 'fr');
    expect(result).toBeUndefined();
  });
});
