
const langprefsorder = {
  'fr': [ 'fr', 'en', 'es', 'de'],
  'en': [ 'en', 'fr', 'es', 'de'],
  'de': [ 'de', 'en', 'fr', 'es'],
  'es': [ 'es', 'en', 'fr', 'de'],
}

export function getInLang(trs, wantedlang='fr') {
  for (const lang of langprefsorder[wantedlang]) {
    for (const tr of trs) {
      if (tr.lang_isocode === lang)
        return tr;
    }
  }
}
