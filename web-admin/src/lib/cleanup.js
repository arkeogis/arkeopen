import _ from 'underscore';
import {
  AKOGetUsersId,
  AKODeleteUserByID,
} from './queries/user';


function logerr(s, err) {
  if (err.graphQLErrors && err.graphQLErrors[0] && err.graphQLErrors[0].extensions.internal && err.graphQLErrors[0].extensions.internal.error) {
    console.log(s+"err 4 : ", err.graphQLErrors[0].extensions.internal.error);
  } else if (err.graphQLErrors && err.graphQLErrors[0] && err.graphQLErrors[0].extensions.internal) {
    console.log(s+"err 3 : ", err.graphQLErrors[0].extensions.internal);
  } else if (err.graphQLErrors[0]) {
    console.log(s+"err 2 : ", err.graphQLErrors[0]);
  } else if (err.graphQLErrors) {
    console.log(s+"err 1 : ", err.graphQLErrors);
  } else {
    console.log(s+"err 0 : ", err);
  }
}

/**
 * Delete all unused users, ton cleanup database
 * @param {object} apolloClient
 * @param {integer} id_shapefile
 */
export async function deleteUnusedUsers(apolloClient) {
  const userids = await apolloClient.query({
    query: AKOGetUsersId
  });

  for (let user of userids.data.ako_user) {
    await apolloClient.mutate({
      mutation: AKODeleteUserByID,
      variables: {
        id: user.id,
      }
    }).catch(err => {
      logerr(`not deleting user id ${user.id} : `, err);
    });
  }
}

export async function cleanup(apolloClient) {
  console.log("cleanup...");
  deleteUnusedUsers(apolloClient);
}
