import { create } from "zustand"


const useStore = create((set) => ({
  akgChronosValid: false,
  setAkgChronosValid: akgChronosValid => set({akgChronosValid}),

  akoChronosValid: false,
  setAkoChronosValid: akoChronosValid => set({akoChronosValid}),

  akgCharacsValid: false,
  setAkgCharacsValid: akgCharacsValid => set({akgCharacsValid}),

  akoCharacsValid: false,
  setAkoCharacsValid: akoCharacsValid => set({akoCharacsValid}),

  akgDatabasesValid: false,
  setAkgDatabasesValid: akgDatabasesValid => set({akgDatabasesValid}),

  akoDatabasesValid: false,
  setAkoDatabasesValid: akoDatabasesValid => set({akoDatabasesValid}),

  akgShapefilesValid: false,
  setAkgShapefilesValid: akgShapefilesValid => set({akgShapefilesValid}),

  akoShapefilesValid: false,
  setAkoShapefilesValid: akoShapefilesValid => set({akoShapefilesValid}),
}));


export default useStore;
