import _ from 'underscore';
import { cleanup } from './cleanup';
import {
  DELETE_AKO_DATABASE_MUTATION_str,
  DELETE_AKO_DATABASE_MUTATION,
  GET_AKG_DATABASE_QUERY
} from './queries/database';

import { queryToUpsertMutation } from './queryToMutation';
import { downloadFile } from './uploadDownload';

/**
 * Transfert a database from arkeogis to arkeopen
 * @param {ApolloClient} apolloClient
 * @param {integer} id_database
 */
export async function transfertDatabase(apolloClient, id_database) {
  const database = await apolloClient.query({
    query: GET_AKG_DATABASE_QUERY,
    variables: {
      database_id: id_database,
    }
  });

  const deleteMutation = {
    params: [`$database_id: Int!`],
    data: {
      database_id: id_database,
    },
    query: DELETE_AKO_DATABASE_MUTATION_str
  }

  //console.log("database", database)

  const result = await queryToUpsertMutation(
    apolloClient,
    database.data.akg_database, "akg_", "ako_",
    deleteMutation);

  await cleanup(apolloClient);

  return result;
}

/**
 * Download an Arkeogis Database in a .json format
 * @param {object} apolloClient
 * @param {integer} id_database
 */
export async function downloadAkgDatabase(apolloClient, id_database) {
  const datadatabase = await apolloClient.query({
    query: GET_AKG_DATABASE_QUERY,
    variables: {
      database_id: id_database,
    }
  });

  const database = datadatabase.data.akg_database[0];
  downloadFile(database, "database");
}

/**
 * Delete a database from Arkeopen
 * @param {object} apolloClient
 * @param {integer} id_database
 */
export async function deleteAkoDatabase(apolloClient, id_database) {
  console.log("deleteAkoDatabase", id_database);
  const result = await apolloClient.mutate({
    mutation: DELETE_AKO_DATABASE_MUTATION,
    variables: {
      database_id: id_database,
    }
  });

  console.log("result : ", result);

  await cleanup(apolloClient);
}
