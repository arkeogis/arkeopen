import _ from 'underscore';
import { gql } from '@apollo/client';
import { tablesByName } from './db';

const nodupstables = [
  'site', 'site_tr', 'site_range', 'site_range__charac', 'site_range__charac_tr'
];

function addMutationWithoutDup(submutations, tablename, newmutation) {
  if (nodupstables.includes(tablename) // optimize, there will be no dups in theses tables
    || !submutations.some(sub => _.isEqual(sub, newmutation))) {
    submutations.push(newmutation);
  }
}

function objToMutation(mutations, obj, in_prefix, out_prefix, debugpath="* ") {
  //debugpath+=`=> ${obj.__typename}.${obj.id}`;
  //console.log("- ", debugpath, obj);
  const resultrow={};
  if (!('__typename' in obj))
    throw new Error(`there is no typename in this object`);

  let typename = obj.__typename;
  if (typename.startsWith(in_prefix)) {
    typename = typename.slice(in_prefix.length);
  } else {
    throw new Error(`typename(${typename}) does not start with in_prefix(${in_prefix})`);
  }

  if (!(typename in tablesByName))
    throw new Error(`typename(${typename}) is not in our schema`);
  const schema_table = tablesByName[typename];

  Object.assign(resultrow, _.pick(obj, schema_table.cols.map(c => c.name)));
  //console.log("tablesByName[typename]", tablesByName[typename])
  if (schema_table.relations) {
    //console.log("relations = ", schema_table.relations)
    _.each(schema_table.relations, (relation, relation_table_name) => {
      if (obj.hasOwnProperty(relation_table_name)) {
        if (relation.multiple && _.isArray(obj[relation_table_name])) {
          obj[relation_table_name].forEach(subobj => {
            objToMutation(mutations, subobj, in_prefix, out_prefix, debugpath);
          })
        } else if (!relation.multiple) {
          //console.log("relation_table_name", relation_table_name);
          objToMutation(mutations, obj[relation_table_name], in_prefix, out_prefix, debugpath);
        }
      }
    })
  }

  if (!(typename in mutations)) mutations[typename]=[];
  addMutationWithoutDup(mutations[typename], typename, resultrow)
  //mutations[typename].push(resultrow);
}

function buildUpsertMutationForTable(tablename, out_prefix) {
  const paramname=`all_${tablename}`;
  const param=`$${paramname}: [${out_prefix}${tablename}_insert_input!]!`;
  const query=`
    ${out_prefix}insert_${tablename}(
      objects: $${paramname}
      on_conflict: {
        constraint: ${tablename}_pkey
        update_columns: [
          ${
            tablesByName[tablename].cols
              .map(o => o.name)
              .filter(n => n !== 'id')
              .reduce((p,c) => `${p},${c}`)
          }
        ]
      }
    ) {
      affected_rows
    }
  `;
  return [ paramname, param, query ];
}

function buildUpsertMutations(mutations, out_prefix, deleteMutation = undefined) {

  //mutations = _.mapObject(mutations, (mut, tablename) => removeDups(mut, tablename));
  //console.log("newmutations: ", mutations);

  const params=deleteMutation ? deleteMutation.params : [];
  let bigquery=deleteMutation ? deleteMutation.query+"\n" : '';
  const data=deleteMutation ? { ...deleteMutation.data } : {};

  for (const tablename in mutations) {
    const [ paramname, param, query ] = buildUpsertMutationForTable(tablename, out_prefix);
    params.push(param);
    bigquery+=query;
    data[paramname]=mutations[tablename];
  }
  bigquery=`
  mutation Put(${params.reduce((p,c) => `${p},${c}`)}) {
    ${bigquery}
  }
  `;
  return [ bigquery, data ];
}

/**
 * QueryToMutation
 * @param {array[object]} data        An array of objects comming from a graphql query
 * @param {string}        in_prefix   the table prefix (akg_ or ako_) used from data
 * @param {string}        out_prefix  the table prefix (akg_ or ako_) to use for mutation
 */
export async function queryToUpsertMutation(apolloClient, data, in_prefix="akg_", out_prefix="ako_", deleteMutation = undefined) {
  const mutations={};
  data.map(obj => objToMutation(mutations, obj, in_prefix, out_prefix));

  console.log("mutations: ", mutations);

  let [ query, query_data ] = buildUpsertMutations(mutations, out_prefix, deleteMutation);

  console.log("query", query, query_data);

  query=gql(query);

  let mutationResult = null;
  try {
    mutationResult = await apolloClient.mutate({
      mutation: query,
      variables: query_data
    });
  } catch (err) {
    if (err.graphQLErrors && err.graphQLErrors[0] && err.graphQLErrors[0].extensions.internal && err.graphQLErrors[0].extensions.internal.error) {
      console.warn("mutate error 4 : ", err.graphQLErrors[0].extensions.internal.error);
      throw err.graphQLErrors[0].extensions.internal.error;
    } else if (err.graphQLErrors && err.graphQLErrors[0] && err.graphQLErrors[0].extensions.internal) {
      console.warn("mutate error 3 : ", err.graphQLErrors[0].extensions.internal);
      throw new Error(err.graphQLErrors[0].extensions.internal);
    } else if (err.graphQLErrors[0]) {
      console.warn("mutate error 2 : ", err.graphQLErrors[0]);
      throw new Error(err.graphQLErrors[0].message);
    } else if (err.graphQLErrors) {
      console.warn("mutate error 1 : ", err.graphQLErrors);
      throw new Error(err.graphQLErrors);
    } else {
      console.warn("mutate error 0 : ", err);
      throw new Error(err);
    }
  }

  console.log("mutationResult : ", mutationResult);

  return mutationResult;
}
