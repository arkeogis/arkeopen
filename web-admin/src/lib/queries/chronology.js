import { gql } from '@apollo/client';
import _ from 'underscore';
import { tablesByName } from '../db';
import { AKGUserFields, AKOUserFields, AKGGroupFields, AKGUserGroupFields } from './user';
import { AKGCityRecursiveFields } from './geoname';

export const GET_AKO_CHRONOLOGY_IDS = gql`
query GET_AKO_CHRONOLOGY_IDS($id_chronology: Int!) {
  ako_chronology(where: {id: {_eq: $id_chronology}}) {
    id
    chronologies {
      id
	    chronologies {
  	    id
        chronologies {
          id
          chronologies {
            id
          }
        }
      }
    }
  }
}
`


export const convertFlatChronoPeriodToUpsertMutation = (period) => {
  let gqlperiod = {
    ..._.pick(period, tablesByName.chronology.cols.map(c => c.name)),
    chronology_trs: {
      data: period.chronology_trs
        .map(tr => _.pick(tr, tablesByName.chronology_tr.cols.map(c => c.name))),
      on_conflict: {
        constraint: 'chronology_tr_pkey',
        update_columns: tablesByName.chronology_tr.cols.map(c => c.name)
      }
    }
  };
  return gqlperiod;
};

export const PUT_AKO_CHRONOLOGY_ROOT_MUTATION = gql`
mutation PutRootChrono($root_chronology_id: Int!, $ako_chrono_ids: [Int!]!, $rootchrono: [ako_chronology_root_insert_input!]!) {
  ako_delete_chronology_root(where: {root_chronology_id: {_eq: $root_chronology_id}}) { affected_rows }
  ako_delete_chronology(where: {id: {_in: $ako_chrono_ids}}) { affected_rows }
  ako_insert_chronology_root(
    objects: $rootchrono
    on_conflict: {
      constraint: chronology_root_pkey
      update_columns: [
        ${
          tablesByName.chronology_root.cols
            .map(o => o.name)
            .filter(n => n !== 'id')
            .reduce((p,c) => `${p},${c}`)
        }
      ]
    }
  ) {
    affected_rows
  }
}
`;

export const DELETE_AKO_CHRONOLOGY_MUTATION = gql`
mutation DeleteChrono($root_chronology_id: Int!, $ako_chrono_ids: [Int!]!) {
  ako_delete_chronology_root(where: {root_chronology_id: {_eq: $root_chronology_id}}) { affected_rows }
  ako_delete_chronology(where: {id: {_in: $ako_chrono_ids}}) { affected_rows }
}
`;

/**
 *
 */

export const INIT_CHRONOS_MUTATION = gql`
mutation InitChronos {
  ako_insert_chronology(
    objects: {
      id: 0,
      parent_id: 0,
      start_date: 0,
      end_date: 0,
      color: "#fff",
      created_at: "2001-02-01 14:22:22",
      updated_at: "2001-02-01 14:22:22",
      chronology_trs: {
        data: [
          {
            lang_isocode: "fr",
            name: "Le parent de tous",
            description: ""
          },
          {
            lang_isocode: "en",
            name: "Parent of all",
            description: ""
          }
        ]
      }
    },
  ) {
    returning {
      chronology {
        id
      }
    }
  }
}
`;

/*
 *  Chrono Fragments GET
 */
export const AKGChronologyRootFields = gql`
fragment AKGChronologyRootFields on akg_chronology_root {
  ${tablesByName.chronology_root.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  chronology_root_trs {
    ${
      tablesByName.chronology_root_tr.cols
        .reduce((p,c) => `${p} ${c.name}`, '')
    }
  }
}
`
export const AKOChronologyRootFields = gql`
fragment AKOChronologyRootFields on ako_chronology_root {
  ${tablesByName.chronology_root.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  chronology_root_trs {
    ${
      tablesByName.chronology_root_tr.cols
        .reduce((p,c) => `${p} ${c.name}`, '')
    }
  }
}
`


export const AKGChronologyFields = gql`
fragment AKGChronologyFields on akg_chronology {
  ${tablesByName.chronology.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  chronology_trs {
    ${
      tablesByName.chronology_tr.cols
        //.filter(c => c.name !== 'chronology_id')
        .reduce((p,c) => `${p} ${c.name}`, '')
    }
  }
}
`
export const AKOChronologyFields = gql`
fragment AKOChronologyFields on ako_chronology {
  ${tablesByName.chronology.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  chronology_trs {
    ${
      tablesByName.chronology_tr.cols
        .filter(c => c.name !== 'chronology_id')
        .reduce((p,c) => `${p} ${c.name}`, '')
    }
  }
}
`

export const AKGChronologyRecursiveFields = gql`
${AKGChronologyFields}
fragment AKGChronologyRecursiveFields on akg_chronology {
  ...AKGChronologyFields
  chronologies {
    ...AKGChronologyFields
    chronologies {
      ...AKGChronologyFields
      chronologies {
        ...AKGChronologyFields
        chronologies {
          ...AKGChronologyFields
        }
      }
    }
  }
}
`

export const AKGGetFullChrono = gql`
${AKGChronologyRecursiveFields}
query AKGGetFullChrono($id_chronology:Int!) {
  akg_chronology(where:{id: {_eq: $id_chronology}}) {
    ...AKGChronologyRecursiveFields
  }
}
`

export const AKGGetFullChronoRoot = gql`
${AKGChronologyRootFields}
${AKGChronologyRecursiveFields}
${AKGUserFields}
${AKGGroupFields}
${AKGUserGroupFields}
${AKGCityRecursiveFields}
query AKGGetFullChronoRoot($chronology_id:Int!) {
  akg_chronology_root(where:{root_chronology_id: {_eq: $chronology_id}}) {
    ...AKGChronologyRootFields
    chronology {
      ...AKGChronologyRecursiveFields
    }
    group {
      ...AKGGroupFields
    }
    user {
      ...AKGUserFields
      user__groups {
        ...AKGUserGroupFields
        group {
          ...AKGGroupFields
        }
      }
      city {
        ...AKGCityRecursiveFields
      }
    }
  }
}
`

export const AKGGetFullChronos = gql`
${AKGChronologyRecursiveFields}
query GetChronologiesRecursive {
  akg_chronology(where: {parent_id: {_eq: 0}, id: {_neq: 0}}) {
    ...AKGChronologyRecursiveFields
  }
}
`

export const AKGGetChronosRoot = gql`
${AKGChronologyRootFields}
${AKGChronologyFields}
query AKGGetChronoRoot {
  akg_chronology_root(where:{_and: {active: {_eq: true}, opened: {_eq: true}}}) {
    ...AKGChronologyRootFields
    chronology {
      ...AKGChronologyFields
    },
  }
}
`

export const AKOGetChronosRoot = gql`
${AKOChronologyRootFields}
${AKOChronologyFields}
query AKOGetFullChronoRoot {
  ako_chronology_root(where:{active: {_eq: true}}) {
    ...AKOChronologyRootFields
    chronology {
      ...AKOChronologyFields
    },
  }
}
`

export const AKOChronologyRecursiveFields = gql`
${AKOChronologyFields}
fragment AKOChronologyRecursiveFields on ako_chronology {
  ...AKOChronologyFields
  chronologies {
    ...AKOChronologyFields
    chronologies {
      ...AKOChronologyFields
      chronologies {
        ...AKOChronologyFields
        chronologies {
          ...AKOChronologyFields
        }
      }
    }
  }
}
`

export const AKOGetFullChrono = gql`
${AKOChronologyRecursiveFields}
query AKOGetFullChrono($id_chronology:Int!) {
  ako_chronology(where:{id: {_eq: $id_chronology}}) {
    ...AKOChronologyRecursiveFields
  }
}
`

export const AKOGetFullChronos = gql`
${AKOChronologyRecursiveFields}
query GetChronologiesRecursive {
  ako_chronology(where: {parent_id: {_eq: 0}, id: {_neq: 0}}) {
    ...AKOChronologyRecursiveFields
  }
}
`

