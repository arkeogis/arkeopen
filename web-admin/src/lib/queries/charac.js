import { gql } from '@apollo/client';
import _ from 'underscore';
import { tablesByName } from '../db';
import { AKGUserFields, AKOUserFields, AKGGroupFields, AKGUserGroupFields } from './user';
import { AKGCityRecursiveFields } from './geoname';

export const GET_AKO_CHARAC_IDS = gql`
query GET_AKO_CHARAC_IDS($id_charac: Int!) {
  ako_charac(where: {id: {_eq: $id_charac}}) {
    id
    characs {
      id
	    characs {
  	    id
        characs {
          id
          characs {
            id
          }
        }
      }
    }
  }
}
`


export const convertFlatCharacPeriodToUpsertMutation = (period) => {
  let gqlperiod = {
    ..._.pick(period, tablesByName.charac.cols.map(c => c.name)),
    charac_trs: {
      data: period.charac_trs
        .map(tr => _.pick(tr, tablesByName.charac_tr.cols.map(c => c.name))),
      on_conflict: {
        constraint: 'charac_tr_pkey',
        update_columns: tablesByName.charac_tr.cols.map(c => c.name)
      }
    }
  };
  return gqlperiod;
};

export const PUT_AKO_CHARAC_ROOT_MUTATION = gql`
mutation PutRootCharac($root_charac_id: Int!, $ako_charac_ids: [Int!]!, $rootcharac: [ako_charac_root_insert_input!]!) {
  ako_delete_charac_root(where: {root_charac_id: {_eq: $root_charac_id}}) { affected_rows }
  ako_delete_charac(where: {id: {_in: $ako_charac_ids}}) { affected_rows }
  ako_insert_charac_root(
    objects: $rootcharac
    on_conflict: {
      constraint: charac_root_pkey
      update_columns: [
        ${
          tablesByName.charac_root.cols
            .map(o => o.name)
            .filter(n => n !== 'id')
            .reduce((p,c) => `${p},${c}`)
        }
      ]
    }
  ) {
    affected_rows
  }
}
`;

export const DELETE_AKO_CHARAC_MUTATION = gql`
mutation DeleteCharac($root_charac_id: Int!, $ako_charac_ids: [Int!]!) {
  ako_delete_charac_root(where: {root_charac_id: {_eq: $root_charac_id}}) { affected_rows }
  ako_delete_charac(where: {id: {_in: $ako_charac_ids}}) { affected_rows }
}
`;

/**
 *
 */

export const INIT_CHARACS_MUTATION = gql`
mutation InitCharacs {
  ako_insert_charac(
    objects: {
      id: 0,
      parent_id: 0,
      start_date: 0,
      end_date: 0,
      color: "#fff",
      created_at: "2001-02-01 14:22:22",
      updated_at: "2001-02-01 14:22:22",
      charac_trs: {
        data: [
          {
            lang_isocode: "fr",
            name: "Le parent de tous",
            description: ""
          },
          {
            lang_isocode: "en",
            name: "Parent of all",
            description: ""
          }
        ]
      }
    },
  ) {
    returning {
      charac {
        id
      }
    }
  }
}
`;

/*
 *  Charac Fragments GET
 */
export const AKGCharacRootFields = gql`
fragment AKGCharacRootFields on akg_charac_root {
  ${tablesByName.charac_root.cols.reduce((p,c) => `${p} ${c.name}`, '')}
}
`
export const AKOCharacRootFields = gql`
fragment AKOCharacRootFields on ako_charac_root {
  ${tablesByName.charac_root.cols.reduce((p,c) => `${p} ${c.name}`, '')}
}
`


export const AKGCharacFields = gql`
${AKGUserFields}
${AKGUserGroupFields}
${AKGGroupFields}
${AKGCityRecursiveFields}
fragment AKGCharacFields on akg_charac {
  ${tablesByName.charac.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  charac_trs {
    ${
      tablesByName.charac_tr.cols
        //.filter(c => c.name !== 'charac_id')
        .reduce((p,c) => `${p} ${c.name}`, '')
    }
  }
  user {
    ...AKGUserFields
    user__groups {
      ...AKGUserGroupFields
      group {
        ...AKGGroupFields
      }
    }
    city {
      ...AKGCityRecursiveFields
    }
  }
}
`
export const AKOCharacFields = gql`
fragment AKOCharacFields on ako_charac {
  ${tablesByName.charac.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  charac_trs {
    ${
      tablesByName.charac_tr.cols
        .filter(c => c.name !== 'charac_id')
        .reduce((p,c) => `${p} ${c.name}`, '')
    }
  }
}
`

export const AKGCharacRecursiveFields = gql`
${AKGCharacFields}
fragment AKGCharacRecursiveFields on akg_charac {
  ...AKGCharacFields
  characs {
    ...AKGCharacFields
    characs {
      ...AKGCharacFields
      characs {
        ...AKGCharacFields
        characs {
          ...AKGCharacFields
        }
      }
    }
  }
}
`

export const AKGGetFullCharac = gql`
${AKGCharacRecursiveFields}
query AKGGetFullCharac($id_charac:Int!) {
  akg_charac(where:{id: {_eq: $id_charac}}) {
    ...AKGCharacRecursiveFields
  }
}
`

export const AKGGetFullCharacRoot = gql`
${AKGCharacRootFields}
${AKGCharacRecursiveFields}
${AKGUserFields}
${AKGGroupFields}
${AKGUserGroupFields}
${AKGCityRecursiveFields}
query AKGGetFullCharacRoot($charac_id:Int!) {
  akg_charac_root(where:{root_charac_id: {_eq: $charac_id}}) {
    ...AKGCharacRootFields
    charac {
      ...AKGCharacRecursiveFields
    }
    group {
      ...AKGGroupFields
    }
  }
}
`

export const AKGGetFullCharacs = gql`
${AKGCharacRecursiveFields}
query GetCharacsRecursive {
  akg_charac(where: {parent_id: {_eq: 0}, id: {_neq: 0}}) {
    ...AKGCharacRecursiveFields
  }
}
`

export const AKGGetCharacsRoot = gql`
${AKGCharacRootFields}
${AKGCharacFields}
${AKGUserFields}
${AKGGroupFields}
${AKGUserGroupFields}
${AKGCityRecursiveFields}
query AKGGetCharacRoot {
  akg_charac_root {
    ...AKGCharacRootFields
    charac {
      ...AKGCharacFields
    },
    group {
      ...AKGGroupFields
    }
  }
}
`

export const AKOGetCharacsRoot = gql`
${AKOCharacRootFields}
${AKOCharacFields}
${AKOUserFields}
query AKOGetFullCharacRoot {
  ako_charac_root {
    charac {
      ...AKOCharacFields
    },
  }
}
`

export const AKOCharacRecursiveFields = gql`
${AKOCharacFields}
fragment AKOCharacRecursiveFields on ako_charac {
  ...AKOCharacFields
  characs {
    ...AKOCharacFields
    characs {
      ...AKOCharacFields
      characs {
        ...AKOCharacFields
        characs {
          ...AKOCharacFields
        }
      }
    }
  }
}
`

export const AKOGetFullCharac = gql`
${AKOCharacRecursiveFields}
query AKOGetFullCharac($id_charac:Int!) {
  ako_charac(where:{id: {_eq: $id_charac}}) {
    ...AKOCharacRecursiveFields
  }
}
`

export const AKOGetFullCharacs = gql`
${AKOCharacRecursiveFields}
query GetCharacsRecursive {
  ako_charac(where: {parent_id: {_eq: 0}, id: {_neq: 0}}) {
    ...AKOCharacRecursiveFields
  }
}
`

