import { gql } from '@apollo/client';
import _ from 'underscore';
import { tablesByName } from '../db';

export const AKGUserGroupFields = gql`
fragment AKGUserGroupFields on akg_user__group {
  ${tablesByName.user__group.cols.reduce((p,r) => `${p} ${r.name}`, '')}
}
`

export const AKGGroupFields = gql`
fragment AKGGroupFields on akg_group {
  ${tablesByName.group.cols.reduce((p,r) => `${p} ${r.name}`, '')}
}
`
export const AKGPhotoFields = gql`
fragment AKGPhotoFields on akg_photo {
  ${tablesByName.photo.cols.reduce((p,r) => `${p} ${r.name}`, '')}
}
`

export const AKGUserFields = gql`
${AKGPhotoFields}
fragment AKGUserFields on akg_user {
  ${tablesByName.user.cols.reduce((p,r) => `${p} ${r.name}`, '')}
  photo {
    ...AKGPhotoFields
  }
}
`

export const AKOUserFields = gql`
fragment AKOUserFields on ako_user {
  ${tablesByName.user.cols.reduce((p,r) => `${p} ${r.name}`, '')}
  photo {
    photo
  }
}
`

export const AKGLimitedUserFields = gql`
fragment AKGLimitedUserFields on akg_user {
  id
  username
  firstname
  lastname
  photo {
    photo
  }
}
`

export const AKOLimitedUserFields = gql`
fragment AKOLimitedUserFields on ako_user {
  id
  username
  firstname
  lastname
  photo {
    photo
  }
}
`

export const AKOGetUsersId = gql`
query AKOGetUsersId {
  ako_user {
    id
  }
}
`

export const AKODeleteUserByID = gql`
mutation DeleteUserByID($id: Int!) {
  ako_delete_user(where: {id: {_eq: $id}}) {
    affected_rows
  }
  ako_delete_user__group(where: {user_id: {_eq: $id}}) {
    affected_rows
  }
}
`
