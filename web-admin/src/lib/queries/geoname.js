import { gql } from '@apollo/client';
import _ from 'underscore';
import { tablesByName } from '../db';

export const AKGContinentFields = gql`
fragment AKGContinentFields on akg_continent {
  ${tablesByName.continent.cols.reduce((p,r) => `${p} ${r.name}`, '')}
  continent_trs {
    ${tablesByName.continent_tr.cols.reduce((p,r) => `${p} ${r.name}`, '')}
  }
}
`

export const AKGCountryFields = gql`
fragment AKGCountryFields on akg_country {
  ${tablesByName.country.cols.reduce((p,r) => `${p} ${r.name}`, '')}
  country_trs {
    ${tablesByName.country_tr.cols.reduce((p,r) => `${p} ${r.name}`, '')}
  }
}
`

export const AKGCityFields = gql`
fragment AKGCityFields on akg_city {
  ${tablesByName.city.cols.reduce((p,r) => `${p} ${r.name}`, '')}
  city_trs {
    ${tablesByName.city_tr.cols.reduce((p,r) => `${p} ${r.name}`, '')}
  }
}
`

export const AKGCityRecursiveFields = gql`
${AKGCountryFields}
fragment AKGCityRecursiveFields on akg_city {
  ${tablesByName.city.cols.reduce((p,r) => `${p} ${r.name}`, '')}
  country {
    ...AKGCountryFields
  }
}
`
