import { gql } from '@apollo/client';
import _ from 'underscore';
import { tablesByName } from '../db';
import { AKGUserFields } from './user';
import { AKGContinentFields, AKGCountryFields } from './geoname';

export const AKGDatabaseFields = gql`
fragment AKGDatabaseFields on akg_database {
  ${tablesByName.database.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  database_trs {
    ${
      tablesByName.database_tr.cols
        .reduce((p,c) => `${p} ${c.name}`, '')
    }
  }
}
`

export const AKODatabaseFields = gql`
fragment AKODatabaseFields on ako_database {
  ${tablesByName.database.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  database_trs {
    ${
      tablesByName.database_tr.cols
        .reduce((p,c) => `${p} ${c.name}`, '')
    }
  }
}
`

export const AKGSiteFields = gql`
fragment AKGSiteFields on akg_site {
  ${tablesByName.site.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  site_trs {
    ${
      tablesByName.site_tr.cols
        .reduce((p,c) => `${p} ${c.name}`, '')
    }
  }
}
`

export const AKGSiteRangeFields = gql`
fragment AKGSiteRangeFields on akg_site_range {
  ${tablesByName.site_range.cols.reduce((p,c) => `${p} ${c.name}`, '')}
}
`

export const AKGSiteRangeCharacFields = gql`
fragment AKGSiteRangeCharacFields on akg_site_range__charac {
  ${tablesByName.site_range__charac.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  site_range__charac_trs {
    ${
      tablesByName.site_range__charac_tr.cols
        .reduce((p,c) => `${p} ${c.name}`, '')
    }
  }
}
`

export const AKGCharacFieldsDb = gql`
fragment AKGCharacFieldsDb on akg_charac {
  ${tablesByName.charac.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  charac_trs {
    ${
      tablesByName.charac_tr.cols
        .reduce((p,c) => `${p} ${c.name}`, '')
    }
  }
}
`

export const AKGDatabaseAuthorsFields = gql`
fragment AKGDatabaseAuthorsFields on akg_database__authors {
  ${tablesByName.database__authors.cols.reduce((p,c) => `${p} ${c.name}`, '')}
}
`

export const AKGDatabaseHandleFields = gql`
fragment AKGDatabaseHandleFields on akg_database_handle {
  ${tablesByName.database_handle.cols.reduce((p,c) => `${p} ${c.name}`, '')}
}
`

export const AKGDatabaseContextFields = gql`
fragment AKGDatabaseContextFields on akg_database_context {
  ${tablesByName.database_context.cols.reduce((p,c) => `${p} ${c.name}`, '')}
}
`

export const AKGDatabaseCountryFields = gql`
fragment AKGDatabaseCountryFields on akg_database__country {
  ${tablesByName.database__country.cols.reduce((p,c) => `${p} ${c.name}`, '')}
}
`

export const AKGDatabaseCountinentFields = gql`
fragment AKGDatabaseContinentFields on akg_database__continent {
  ${tablesByName.database__continent.cols.reduce((p,c) => `${p} ${c.name}`, '')}
}
`

export const AKGLicenseFields = gql`
fragment AKGLicenseFields on akg_license {
  ${tablesByName.license.cols.reduce((p,c) => `${p} ${c.name}`, '')}
}
`

export const AKGImportFields = gql`
fragment AKGImportFields on akg_import {
  ${tablesByName.import.cols.reduce((p,c) => `${p} ${c.name}`, '')}
}
`

/*
export const AKGGetDatabases = gql`
${AKGDatabaseFields}
  query GetDatabases($public: Boolean! = true) {
    akg_database(where: {public: {_eq: $public}, published: {_eq: true}}, order_by: {name: asc}) {
      ...AKGDatabaseFields
    }
  }
`;

export const AKOGetDatabases = gql`
${AKODatabaseFields}
  query GetDatabases($public: Boolean! = true) {
    ako_database(where: {public: {_eq: $public}, published: {_eq: true}}, order_by: {name: asc}) {
      ...AKODatabaseFields
    }
  }
`;
*/

export const AKGGetDatabases = gql`
${AKGDatabaseFields}
  query GetDatabases {
    akg_database(where: {_and: {published: {_eq: true}, public: {_eq: true}, soft_deleted: {_eq: false}}}, order_by: {name: asc}) {
      ...AKGDatabaseFields
    }
  }
`;

export const AKOGetDatabases = gql`
${AKODatabaseFields}
  query GetDatabases {
    ako_database(where: {published: {_eq: true}}, order_by: {name: asc}) {
      ...AKODatabaseFields
    }
  }
`;

export const GET_AKG_DATABASE_QUERY = gql`
  ${AKGDatabaseFields}
  ${AKGSiteFields}
  ${AKGSiteRangeFields}
  ${AKGSiteRangeCharacFields}
  ${AKGCharacFieldsDb}
  ${AKGUserFields}
  ${AKGDatabaseAuthorsFields}
  ${AKGDatabaseHandleFields}
  ${AKGDatabaseContextFields}
  ${AKGDatabaseCountryFields}
  ${AKGCountryFields}
  ${AKGDatabaseCountinentFields}
  ${AKGContinentFields}
  ${AKGLicenseFields}
  ${AKGImportFields}

  query GetDatabase($database_id: Int!) {
    akg_database(where: {id: {_eq: $database_id}, published: {_eq: true}}) {
      ...AKGDatabaseFields
      user {
        ...AKGUserFields
      }
      database__authors {
        ...AKGDatabaseAuthorsFields
        user {
          ...AKGUserFields
        }
      }
      database_handles {
        ...AKGDatabaseHandleFields
        import {
          ...AKGImportFields
          user {
            ...AKGUserFields
          }
        }
      }
      database_contexts {
        ...AKGDatabaseContextFields
      }
      database__countries {
        ...AKGDatabaseCountryFields
        country {
          ...AKGCountryFields
        }
      }
      database__continents {
        ...AKGDatabaseContinentFields
        continent {
          ...AKGContinentFields
        }
      }
      sites {
        ...AKGSiteFields
        site_ranges {
          ...AKGSiteRangeFields
          site_range__characs {
            ...AKGSiteRangeCharacFields
          }
        }
      }
      imports {
        ...AKGImportFields
        user {
          ...AKGUserFields
        }
      }
      license {
        ...AKGLicenseFields
      }
    }
  }
`;

export const DELETE_AKO_DATABASE_MUTATION_str = `
  ako_delete_database(where: {id: {_eq: $database_id}}) {
    affected_rows
  }
  ako_delete_import(where: {database_id: {_eq: $database_id}}) {
    affected_rows
  }
  ako_delete_site(where: {database_id: {_eq: $database_id}}) {
    affected_rows
  }
  ako_delete_database__authors(where: {database_id: {_eq: $database_id}}) {
    affected_rows
  }
  ako_delete_database__continent(where: {database_id: {_eq: $database_id}}) {
    affected_rows
  }
  ako_delete_database__country(where: {database_id: {_eq: $database_id}}) {
    affected_rows
  }
  ako_delete_database_context(where: {database_id: {_eq: $database_id}}) {
    affected_rows
  }
  ako_delete_database_handle(where: {database_id: {_eq: $database_id}}) {
    affected_rows
  }
`

export const DELETE_AKO_DATABASE_MUTATION = gql`
mutation DeleteDatabase($database_id: Int!) {
  ${DELETE_AKO_DATABASE_MUTATION_str}
}
`
