import { gql } from '@apollo/client';
import _ from 'underscore';
import { tablesByName } from '../db';

import { AKGUserFields, AKGGroupFields, AKGUserGroupFields } from './user';
import { AKGCityFields, AKGCountryFields, AKGContinentFields } from './geoname';
import { AKGChronologyFields } from './chronology';

export const AKGGetInit = gql`
${AKGChronologyFields}
${AKGUserFields}
${AKGGroupFields}
${AKGUserGroupFields}
${AKGCityFields}
${AKGCountryFields}
${AKGContinentFields}
query AKGGetInit {
  akg_chronology(where:{id: {_eq: 0}}) {
    ...AKGChronologyFields
  }
  akg_user(where:{id: {_eq: 0}}) {
    ...AKGUserFields
  }
  akg_group(where:{id: {_eq: 0}}) {
    ...AKGGroupFields
  }
  akg_city {
    ...AKGCityFields
  }
  akg_country {
    ...AKGCountryFields
  }
  akg_continent {
    ...AKGCountryFields
  }
}
`
