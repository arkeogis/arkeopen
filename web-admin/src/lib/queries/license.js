import { gql } from '@apollo/client';
import _ from 'underscore';
import { tablesByName } from '../db';

/*
 *  Charac Fragments GET
 */
export const AKGLicenseFields = gql`
fragment AKGLicenseFields on akg_license {
  ${tablesByName.license.cols.reduce((p,c) => `${p} ${c.name}`, '')}
}
`
export const AKOLicenseFields = gql`
fragment AKOLicenseFields on ako_license {
  ${tablesByName.license.cols.reduce((p,c) => `${p} ${c.name}`, '')}
}
`
