import { gql } from '@apollo/client';
import _ from 'underscore';
import { tablesByName } from '../db';
import { AKGUserFields, AKOUserFields, AKGGroupFields, AKGUserGroupFields } from './user';
import { AKGCityRecursiveFields } from './geoname';
import { AKGLicenseFields, AKOLicenseFields } from './license';

export const GetAKGShapefiles = gql`
query GetAKGShapefiles {
  akg_shapefile(where: {opened: {_eq: true}}) {
    id
    updated_at
    shapefile_trs {
      name
      lang_isocode
    }
  }
}
`

export const GetAKOShapefiles = gql`
query GetAKOShapefiles {
  ako_shapefile {
    id
    updated_at
    shapefile_trs {
      name
      lang_isocode
    }
  }
}
`

export const DeleteAkoShapefile = gql`
mutation DeleteAkoShapefile($id: Int!) {
  ako_delete_shapefile(where: {id: {_eq: $id}}) { affected_rows }
  ako_delete_shapefile_tr(where: {shapefile_id: {_eq: $id}}) { affected_rows }
  ako_delete_shapefile__authors(where: {shapefile_id: {_eq: $id}}) { affected_rows }
}
`;

/*
 *  Shapefile Fragments GET
 */
export const AKGShapefileFields = gql`
fragment AKGShapefileFields on akg_shapefile {
  ${tablesByName.shapefile.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  shapefile_trs {
    ${
      tablesByName.shapefile_tr.cols
        //.filter(c => c.name !== 'shapefile_id')
        .reduce((p,c) => `${p} ${c.name}`, '')
    }
  }
}
`
export const AKOShapefileFields = gql`
fragment AKOShapefileFields on ako_shapefile_root {
  ${tablesByName.shapefile.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  shapefile_trs {
    ${
      tablesByName.shapefile_tr.cols
        //.filter(c => c.name !== 'shapefile_id')
        .reduce((p,c) => `${p} ${c.name}`, '')
    }
  }
}
`
export const AKGShapefileAuthorsFields = gql`
fragment AKGShapefileAuthorsFields on akg_shapefile__authors {
  ${tablesByName.shapefile__authors.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  user {
      ...AKGUserFields
      user__groups {
        ...AKGUserGroupFields
        group {
          ...AKGGroupFields
        }
      }
      city {
        ...AKGCityRecursiveFields
      }
    }
}
`
export const AKOShapefileAuthorsFields = gql`
fragment AKOShapefileAuthorsFields on ako_shapefile__authors {
  ${tablesByName.shapefile__authors.cols.reduce((p,c) => `${p} ${c.name}`, '')}
  user {
      ...AKOUserFields
      user__groups {
        ...AKOUserGroupFields
        group {
          ...AKOGroupFields
        }
      }
      city {
        ...AKOCityRecursiveFields
      }
    }
}
`

export const AKGGetFullShapefile = gql`
${AKGShapefileFields}
${AKGUserFields}
${AKGGroupFields}
${AKGUserGroupFields}
${AKGCityRecursiveFields}
${AKGLicenseFields}
${AKGShapefileAuthorsFields}
query AKGGetFullShapefile($shapefile_id:Int!) {
  akg_shapefile(where:{id: {_eq: $shapefile_id}}) {
    ...AKGShapefileFields
    user {
      ...AKGUserFields
      user__groups {
        ...AKGUserGroupFields
        group {
          ...AKGGroupFields
        }
      }
      city {
        ...AKGCityRecursiveFields
      }
    }
    licenseByLicenseId {
      ...AKGLicenseFields
    }
    shapefile__authors {
      ...AKGShapefileAuthorsFields
    }
  }
}
`
