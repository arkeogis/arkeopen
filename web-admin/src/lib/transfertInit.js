import _ from 'underscore';
import {
  AKGGetInit
} from './queries/init';

import { queryToUpsertMutation } from './queryToMutation';


/**
 * Transfert a chronology from arkeogis to arkeopen
 * @param {ApolloClient} apolloClient
 * @param {integer} id_chrono
 */
export async function transfertInit(apolloClient) {
  const init = await apolloClient.query({
    query: AKGGetInit,
  });

  const result = await queryToUpsertMutation(
    apolloClient,
    init, "akg_", "ako_");

  return result;
}
