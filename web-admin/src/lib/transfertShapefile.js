import _ from 'underscore';
import { cleanup } from './cleanup';
import {
  DeleteAkoShapefile,
  AKGGetFullShapefile,
} from './queries/shapefile';

import { queryToUpsertMutation } from './queryToMutation';
import { downloadFile } from './uploadDownload';

/**
 * Transfert a shapefile from arkeogis to arkeopen
 * @param {ApolloClient} apolloClient
 * @param {integer} id_shapefile
 */
export async function transfertShapefile(apolloClient, shapefile_id) {
  const shapefile = await apolloClient.query({
    query: AKGGetFullShapefile,
    variables: {
      shapefile_id: shapefile_id,
    }
  });

  const deleteMutation = {
    params: [`$shapefile_id: Int!`],
    data: {
      shapefile_id: shapefile_id,
    },
    query: `
    ako_delete_shapefile(where: {id: {_eq: $shapefile_id}}) { affected_rows }
    ako_delete_shapefile_tr(where: {shapefile_id: {_eq: $shapefile_id}}) { affected_rows }
    ako_delete_shapefile__authors(where: {shapefile_id: {_eq: $shapefile_id}}) { affected_rows }
    `
  }

  //console.log("shapefile", shapefile)

  const result = await queryToUpsertMutation(
    apolloClient,
    shapefile.data.akg_shapefile, "akg_", "ako_",
    deleteMutation);

  await cleanup(apolloClient);

  return result;
}

/**
 * Download an Arkeogis Shapefile in a .json format
 * @param {object} apolloClient
 * @param {integer} id_shapefile
 */
export async function downloadAkgShapefile(apolloClient, shapefile_id) {
  const datashapefile = await apolloClient.query({
    query: AKGGetFullShapefile,
    variables: {
      shapefile_id: shapefile_id,
    }
  });

  const shapefile = datashapefile.data.akg_shapefile[0];
  downloadFile(shapefile, "shapefile");
}

/**
 * Delete a shapefile from Arkeopen
 * @param {object} apolloClient
 * @param {integer} id_shapefile
 */
export async function deleteAkoShapefile(apolloClient, shapefile_id) {
  const result = await apolloClient.mutate({
    mutation: DeleteAkoShapefile,
    variables: {
      id: shapefile_id,
    }
  });

  console.log("result : ", result);

  await cleanup(apolloClient);

}
