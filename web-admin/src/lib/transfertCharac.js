import _ from 'underscore';
import {
  GET_AKO_CHARAC_IDS,
  DELETE_AKO_CHARAC_MUTATION,
  AKGGetFullCharac,
  AKGGetFullCharacRoot
} from './queries/charac';

import { cleanup } from './cleanup';

import { queryToUpsertMutation } from './queryToMutation';
import { downloadFile } from './uploadDownload';

/**
 * Get an array of all id of a charac from arkeopen
 * @param {object} apolloClient
 * @param {integer} id_charac
 * @returns
 */
async function getAkoCharacIds(apolloClient, id_charac) {
  const datacharac = await apolloClient.query({
    query: GET_AKO_CHARAC_IDS,
    variables: {
      id_charac: id_charac,
    }
  });
  const ids = [];
  function fillIds(ids, characlogies) {
    characlogies.forEach(c => {
      ids.push(c.id)
      if (c.characlogies)
        fillIds(ids, c.characlogies)
    });
  }
  fillIds(ids, datacharac.data.ako_charac);
  return ids;
}

/**
 * Transfert a charac from arkeogis to arkeopen
 * @param {ApolloClient} apolloClient
 * @param {integer} id_charac
 */
export async function transfertCharac(apolloClient, id_charac) {
  const rootcharac = await apolloClient.query({
    query: AKGGetFullCharacRoot,
    variables: {
      charac_id: id_charac,
    }
  });

  const deleteMutation = {
    params: [`$root_charac_id: Int!`, `$ako_charac_ids: [Int!]!`],
    data: {
      ako_charac_ids: await getAkoCharacIds(apolloClient, id_charac),
      root_charac_id: id_charac,
    },
    query: `
      ako_delete_charac_root(where: {root_charac_id: {_eq: $root_charac_id}}) { affected_rows }
      ako_delete_charac(where: {id: {_in: $ako_charac_ids}}) { affected_rows }
    `
  }

  console.log("rootcharac", rootcharac)

  const result = await queryToUpsertMutation(
    apolloClient,
    rootcharac.data.akg_charac_root, "akg_", "ako_",
    deleteMutation
  );

  await cleanup(apolloClient);

  return result;
}

/**
 * Download an Arkeogis Charac in a .json format
 * @param {object} apolloClient
 * @param {integer} id_charac
 */
export async function downloadAkgCharac(apolloClient, id_charac) {
  const datacharac = await apolloClient.query({
    query: AKGGetFullCharacRoot,
    variables: {
      charac_id: id_charac,
    }
  });

  const charac = datacharac.data.akg_charac_root[0];
  downloadFile(charac, "charac");
}

/**
 * Delete a charac from Arkeopen
 * @param {object} apolloClient
 * @param {integer} id_charac
 */
export async function deleteAkoCharac(apolloClient, id_charac) {
  const result = await apolloClient.mutate({
    mutation: DELETE_AKO_CHARAC_MUTATION,
    variables: {
      root_charac_id: id_charac,
      ako_charac_ids: await getAkoCharacIds(apolloClient, id_charac),
    }
  });

  console.log("result : ", result);

  await cleanup(apolloClient);
}
