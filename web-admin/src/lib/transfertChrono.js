import _ from 'underscore';
import {
  GET_AKO_CHRONOLOGY_IDS,
  DELETE_AKO_CHRONOLOGY_MUTATION,
  AKGGetFullChrono,
  AKGGetFullChronoRoot
} from './queries/chronology';

import { queryToUpsertMutation } from './queryToMutation';
import { downloadFile } from './uploadDownload';
import { cleanup } from './cleanup';

/**
 * Get an array of all id of a chronology from arkeopen
 * @param {object} apolloClient
 * @param {integer} id_chrono
 * @returns
 */
async function getAkoChronoIds(apolloClient, id_chrono) {
  const datachrono = await apolloClient.query({
    query: GET_AKO_CHRONOLOGY_IDS,
    variables: {
      id_chronology: id_chrono,
    }
  });
  const ids = [];
  function fillIds(ids, chronologies) {
    chronologies.forEach(c => {
      ids.push(c.id)
      if (c.chronologies)
        fillIds(ids, c.chronologies)
    });
  }
  fillIds(ids, datachrono.data.ako_chronology);
  return ids;
}

/**
 * Transfert a chronology from arkeogis to arkeopen
 * @param {ApolloClient} apolloClient
 * @param {integer} id_chrono
 */
export async function transfertChrono(apolloClient, id_chrono) {
  const rootchrono = await apolloClient.query({
    query: AKGGetFullChronoRoot,
    variables: {
      chronology_id: id_chrono,
    }
  });

  const deleteMutation = {
    params: [`$root_chronology_id: Int!`, `$ako_chrono_ids: [Int!]!`],
    data: {
      ako_chrono_ids: await getAkoChronoIds(apolloClient, id_chrono),
      root_chronology_id: id_chrono,
    },
    query: `
      ako_delete_chronology_root(where: {root_chronology_id: {_eq: $root_chronology_id}}) { affected_rows }
      ako_delete_chronology(where: {id: {_in: $ako_chrono_ids}}) { affected_rows }
    `
  }

  console.log("rootchrono", rootchrono)

  const result = await queryToUpsertMutation(
    apolloClient,
    rootchrono.data.akg_chronology_root, "akg_", "ako_",
    deleteMutation);

  await cleanup(apolloClient);

  return result;
}

/**
 * Download an Arkeogis Chronology in a .json format
 * @param {object} apolloClient
 * @param {integer} id_chrono
 */
export async function downloadAkgChrono(apolloClient, id_chrono) {
  const datachrono = await apolloClient.query({
    query: AKGGetFullChronoRoot,
    variables: {
      chronology_id: id_chrono,
    }
  });

  const chrono = datachrono.data.akg_chronology_root[0];
  downloadFile(chrono, "chrono");
}

/**
 * Delete a chronology from Arkeopen
 * @param {object} apolloClient
 * @param {integer} id_chrono
 */
export async function deleteAkoChrono(apolloClient, id_chrono) {
  const result = await apolloClient.mutate({
    mutation: DELETE_AKO_CHRONOLOGY_MUTATION,
    variables: {
      root_chronology_id: id_chrono,
      ako_chrono_ids: await getAkoChronoIds(apolloClient, id_chrono),
    }
  });

  console.log("result : ", result);

  await cleanup(apolloClient);

}
