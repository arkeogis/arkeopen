import {
  Routes,
  Route,
  BrowserRouter,
} from "react-router-dom"
//

// css
import './App.css'
//

// components
import Header from './components/Header/Header'
import SideMenu from './components/SideMenu/SideMenu'
import Home from './components/Home/Home'
import AdminChronos from "./components/AdminChronos/AdminChronos"
import AdminCharacs from "./components/AdminCharacs/AdminCharacs"
import AdminDatabases from "./components/AdminDatabases/AdminDatabases"
import AdminShapefiles from "./components/AdminShapefiles/AdminShapefiles"
//

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Header></Header>
        <div className="Main">
          <SideMenu></SideMenu>
          <Routes>
          <Route exact path="/" element={<Home/>}/>
          <Route exact path="/AdminChronos" element={<AdminChronos/>}/>
          <Route exact path="/AdminCharacs" element={<AdminCharacs/>}/>
          <Route exact path="/AdminDatabases" element={<AdminDatabases/>}/>
          <Route exact path="/AdminShapefiles" element={<AdminShapefiles/>}/>
        </Routes>
        </div>
      </div>
    </BrowserRouter>
  )
}

export default App;
