import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import reportWebVitals from './reportWebVitals';

import { ApolloClient, ApolloProvider, InMemoryCache, HttpLink } from "@apollo/client";

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

// Apollo Client (GRAPHQL) SETUP
// This setup is only needed once per application;
const apolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  link: new HttpLink({
    //uri: "http://localhost:8080/v1/graphql",
    //uri: "http://home.keblo.net:5001/v1/graphql",
    uri: process.env.REACT_APP_GRAPHQL_URI,
    fetchOptions: {
      //mode: 'no-cors',
      credentials: 'include',
    },
    headers: {
      'x-hasura-admin-secret': process.env.REACT_APP_GRAPHQL_PASS,
    },
  }),
  connectToDevTools: true
});

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ApolloProvider client={apolloClient}>
      <App />
    </ApolloProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
