import { render, screen } from '@testing-library/react';
import App from './App';

test('renders Administration', () => {
  render(<App />);
  const linkElement = screen.getByText(/Administration/i);
  expect(linkElement).toBeInTheDocument();
});

test('renders SideMenu Component', () => {
  render(<App />);
  const sideMenuElement = screen.getByText(/Home/i);
  expect(sideMenuElement).toBeInTheDocument();
});
