module.exports = {
  client: {
    service: {
      name: "my-graphql-app",
      url: "http://localhost:8080/v1/graphql",
      includes: ["./src/**/*.js"],
      excludes: ["**/__tests__/**"]
    }
  }
};
