#!/bin/bash


[ -f .env.production ] && . .env.development
[ -f .env.production.local ] && . .env.development.local

export REACT_APP_GRAPHQL_URI
export REACT_APP_GRAPHQL_PASS

npm run build

echo "For production, it should be served with apache/nginx."
echo "however, to serve it for resting, run : ./node_modules/.bin/serve -p $PORT -s build"
