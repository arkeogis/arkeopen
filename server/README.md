# Arkeo Server

This Folder contain all the server side of Arkeo Project. It use :
- *Docker* (containers)
- *docker-compose* for managing dockers images
- *Hasura* as a docker image
- *PostgreSQL* as a docker image

## Installation

### Docker, and Docker-compose

- Installation of docker on ubuntu is available here : https://docs.docker.com/engine/install/ubuntu/
- Afin de pouvoir utiliser docker sans être root :
```shell
sudo usermod -aG docker $USER
newgrp docker
```
- Installation of docker-composer on ubuntu then can be done by typing :
```shell
sudo apt install docker-compose
```

### Download & run everything

```shell
cd server/install/server && docker-compose up -d
cd -
```

### Create database and user for arkeo

```shell
cd server
docker exec -it server-postgres-1 createuser -U postgres --pwprompt user_arkeogis
# when password prompted, choose a password for user_arkeogis

docker exec -it server-postgres-1 createdb -U postgres arkeogis
docker exec -it server-postgres-1 psql -U postgres -c "grant all privileges on database arkeogis to user_arkeogis"
cd -
```

### import arkeogis

```shell
gzip -cd arkeogis-prod-20220315.sql.gz | docker exec -i server-postgres-1 psql -U postgres arkeogis
```

### hasura metadata configuration
```shell
cd server/hasura
[ ! -f metadata/databases/databases.yaml ] && cp metadata/databases/databases.dist.yaml metadata/databases/databases.yaml

# edit databases.yaml to set the PostgreSQL Password for user_arkeogis
vi metadata/databases/databases.yaml
cd -
```

### import hasura metadata
```shell
cd server/hasura
./hasura metadata reload
cd -
```

- Go to : http://localhost:8080/


### Editing hasura / postgresql schema
After editing hasura / postgresql schema, using hasura console for example, we should save new modifications in git, by using hasura metadata export first :

```bash
cd server/hasura
./hasura metadata export

git status
# now look at what has been updated in metadata and commit thems
```

### Destroy docker data (volumes, containers, all will be lost !!)

```bash
docker compose down -v
```
