import express, { Request, Response, NextFunction } from "express";
import morgan from "morgan";
import winston, { Logform } from "winston";
import DailyRotateFile from "winston-daily-rotate-file";
import cors from "cors";
import logger from "./logger";
import auth from "./auth";
//import audit from "express-requests-logger";

// Initialize Express app
const app = express();
const port = process.env.ARKEOSERVER_PORT || 3000;
app.set('trust proxy', 1); // Don't set to "true", it's not secure. Make sure it matches your environment

// Audit middleware (express-requests-logger) for debugging
/*
app.use(
  audit({
    log: (auditData: any) => {
      logger.info(`Audit log: ${JSON.stringify(auditData)}`);
    },
    excludeURLs: ["/health"], // Example: exclude specific routes from logging
  })
);
*/

// Morgan middleware for HTTP request logging
app.use(
  morgan(
    ':remote-addr - :remote-user ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" - :response-time ms',
    {
      stream: {
        write: (message: string) => logger.http(message.trim()), // Redirect Morgan logs to Winston
      },
    })
);

// Middleware to parse JSON request bodies
app.use(express.json());

// Custom middleware to log each request using Winston
/*
app.use((req: Request, res: Response, next: NextFunction) => {
  logger.info(`Incoming request: ${req.method} ${req.url}`);
  next();
});
*/

// Add authentication routes
app.use(auth);

// Start the server
app.listen(port, () => {
  logger.info(`Auth server running on port ${port}.`);
});
