import { GraphQLClient } from "graphql-request";

const graphqlEndpoint = process.env.HASURA_GRAPHQL_ENDPOINT + "/v1/graphql";
const graphqlAdminPass = process.env.HASURA_GRAPHQL_ADMIN_SECRET
console.log("graphqlEndpoint", graphqlEndpoint);

export const client = new GraphQLClient(graphqlEndpoint, {
  headers: { "x-hasura-admin-secret": graphqlAdminPass },
});
