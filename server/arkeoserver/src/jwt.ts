import jwt from "jsonwebtoken";

interface HasuraJWT {
  type: string;
  key: string;
}

function getHasuraJWT(): HasuraJWT {
  const jwtjson = process.env.HASURA_GRAPHQL_JWT_SECRET;
  if (!jwtjson) {
    throw new Error("HASURA_GRAPHQL_JWT_SECRET not set");
  }
  const jwt = JSON.parse(jwtjson);
  if (!jwt.type || !jwt.key) {
    throw new Error("HASURA_GRAPHQL_JWT_SECRET must have type and key");
  }
  return jwt;
}

const hasurakey = getHasuraJWT();

const JWT_CONFIG: jwt.SignOptions = {
  algorithm: hasurakey.type as "HS256" | "RS512",
  expiresIn: "10h",
};

interface GenerateJWTParams {
  defaultRole: string;
  allowedRoles: string[];
  otherClaims?: Record<string, string | string[]>;
  anonymous?: boolean;
}

export function generateJWT(params: GenerateJWTParams): string {
  const payload = {
    "https://hasura.io/jwt/claims": {
      "x-hasura-allowed-roles": params.allowedRoles,
      "x-hasura-default-role": params.defaultRole,
      ...params.otherClaims,
    },
  };
  let jwt_config;
  if (params.anonymous) jwt_config = { ...JWT_CONFIG, expiresIn: "99years" };
  else jwt_config = JWT_CONFIG;
  return jwt.sign(payload, hasurakey.key, JWT_CONFIG);
}
