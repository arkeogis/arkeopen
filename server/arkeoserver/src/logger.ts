// logger.ts
import winston, { Logform } from "winston";
import DailyRotateFile from "winston-daily-rotate-file";

// Configure Winston with monthly file rotation
const logger = winston.createLogger({
  level: "http",
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.printf((info: Logform.TransformableInfo) => {
      const { timestamp, level, message } = info as {
        timestamp: string;
        level: string;
        message: string;
      };
      return `[${timestamp}] ${level.toUpperCase()}: ${message}`;
    })
  ),
  transports: [
    new winston.transports.Console(), // Log to the console
    new DailyRotateFile({
      dirname: process.env.ARKEOSERVER_LOGS_DIRECTORY, // Directory where log files are saved
      filename: "arkeoserver-%DATE%.log", // File naming pattern
      datePattern: "YYYY-MM", // Rotate monthly
      maxSize: "50m", // Maximum size per file before rotation
      maxFiles: "36", // Keep logs for the last 36 months
    }),
  ],
});

//console.log("winston.config.npm.levels: ", winston.config.npm.levels)

// Export the logger instance
export default logger;
