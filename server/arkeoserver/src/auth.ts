import bcrypt from "bcrypt";
import { gql } from "graphql-request";
import { client } from "./client";
import { generateJWT } from "./jwt";

import { Router, Request, Response } from "express";

import { rateLimit } from 'express-rate-limit';
import { slowDown } from 'express-slow-down';
import logger from "./logger";

const router = Router();

const rlimiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 5, // max 30 requests
  skipSuccessfulRequests: true,
});

const slimiter = slowDown({
  windowMs: 15 * 60 * 1000, // 15 minutes
  delayAfter: 2, // Allow 5 requests per 15 minutes.
  delayMs: (hits: number) => hits * 500, // Add 100 ms of delay to every request after the 5th one.
  skipSuccessfulRequests: true,

  /**
   * So:
   *
   * - requests 1-2 are not delayed.
   * - request 3 is delayed by 1.5s
   * - request 4 is delayed by 2s
   * - request 5 is delayed by 2.5s
   *
   * and so on. After 15 minutes, the delay is reset to 0.
   */
})

router.use(rlimiter);
router.use(slimiter);

/**
 * Represents a user object from the AkUser table.
 */
interface AkUser {
  id: number;
  username: string;
  password: string;
  email: string;
  firstname: string;
  lastname: string;
  first_lang_isocode: string;
  second_lang_isocode: string;
  user__groups: {
    group: {
      id: number;
      type: string;
      group__permissions: {
        permission: {
          id: number;
          name: string;
        };
      }[];
    };
  }[];
};

/**
 * Represents a query object for retrieving AkUser data.
 */
interface AkUserQuery {
  akg_user: AkUser[];
}

/**
 * Health check endpoint.
 */
router.get('/rest/auth/health', (req: Request, res: Response) => {
  res.send('OK');
});

/**
 * Login endpoint.
 */
router.post("/rest/auth/login",
  async (req: Request, res: Response) => {
    const { username, password } = req.body as Record<string, string>;

    if ((typeof (password) !== "string") || password === "") {
      logger.warn(`No Password provided for user ${username}`);
      res.sendStatus(401);
      return;
    }

    logger.info(`/auth/login: username=${username}`);

    let anonymous = username === "anonymous";

    const query = gql`
    query GetUserForAuth($username: String!, $active: Boolean!) {
      akg_user(where: {_and: {username: {_eq: $username}}, active: {_eq: $active }}) {
        id
        username
        password
        email
        firstname
        lastname
        first_lang_isocode
        second_lang_isocode
        user__groups {
          group {
            id
            type
            group__permissions {
              permission {
                id
                name
              }
            }
          }
        }
      }
    }
  `;

    let user: AkUser;
    try {
      const users: AkUserQuery = await client.request(query, { username, active: !anonymous });
      //console.log("users: ", users);
      user = users.akg_user[0];
    } catch (e) {
      logger.error(`Error while querying user from database: ${e}`);
      res.sendStatus(401);
      return;
    }

    if (!user) {
      logger.warn(`User not found: ${username}`);
      res.sendStatus(401);
      return;
    }

    if (!anonymous) {
      // Check if password matches the hashed version
      logger.warn(`Checking password ${password}, ${user.password}`);
      const passwordMatch = await bcrypt.compare(password, user.password);
      if (!passwordMatch) {
        logger.warn(`Password mismatch for user ${username}`);
        res.sendStatus(401);
        return;
      }
    }

    const allowedRoles = anonymous ? ["anonymous"] : ["user", "owner"];

    // check if we are an admin user
    if (user.user__groups.some((group) => group.group.id === 1))
      allowedRoles.push("admin");

    const jwtsigned = generateJWT({
      defaultRole: anonymous ? "anonymous" : "user",
      allowedRoles,
      otherClaims: {
        "X-Hasura-User-Id": `${user.id}`,
      },
      anonymous,
    });

    //console.log("JWT signed", Buffer.from(jwtsigned, 'base64').toString('utf-8'));

    res.send({
      token: jwtsigned,
      user: { ...user, password: undefined },
    });

    logger.info(`User logged in: ${username}`);
  });

export default router;
