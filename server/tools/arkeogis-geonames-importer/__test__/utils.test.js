import { asyncPipe, pipe } from "../lib/utils.js";
import jest from "jest-mock";

// Testing suite for the pipe() function
describe("Testing asyncPipe", () => {
  // applies multiple functions in sequence to a value
  it("should apply multiple functions in sequence to a value", async () => {
    const addOne = (x) => x + 1;
    const double = (x) => x * 2;
    const result = await asyncPipe(addOne, double)(2);
    expect(result).toBe(6);
  });

  // returns null if any function in the sequence returns null
  it("should return null if any function in the sequence returns null", async () => {
    const addOne = (x) => x + 1;
    const returnNull = () => null;
    const double = (x) => x * 2;
    const result = await asyncPipe(addOne, returnNull, double)(2);
    expect(result).toBeNull();
  });

  // handles a single function correctly
  it("should return the correct result when only one function is passed", async () => {
    const addOne = (x) => x + 1;
    const result = await asyncPipe(addOne)(5);
    expect(result).toBe(6);
  });

  // processes an array of functions without modifying the original array
  it("should process an array of functions without modifying the original array", async () => {
    const addOne = (num) => num + 1;
    const multiplyByTwo = (num) => num * 2;
    const subtractThree = (num) => num - 3;

    const functions = [addOne, multiplyByTwo, subtractThree];
    const asyncPipedFunction = await asyncPipe(...functions);

    const originalArray = [...functions];
    asyncPipedFunction(5);

    expect(functions).toEqual(originalArray);
  });

  // handles an empty array of functions by returning the initial value
  it("should return initial value when array of functions is empty", async () => {
    const result = await asyncPipe()("initial value");
    expect(result).toBe("initial value");
  });

  // processes functions that return undefined without breaking
  it("should process functions that return undefined without breaking", async () => {
    const addOne = (num) => num + 1;
    const multiplyByTwo = (num) => num * 2;
    const returnUndefined = () => undefined;

    const result = await asyncPipe(addOne, multiplyByTwo, returnUndefined)(3);

    expect(result).toBeUndefined();
  });

  // handles non-function elements in the array gracefully
  it("should throw an error if non-function elements are present in the array", async () => {
    const result = await asyncPipe(1, "test", () => console.log("function"));
    expect(result).rejects.toThrow(/asyncPipe/);
  });

  // manages asynchronous functions correctly
  it("should handle asynchronous functions correctly", async () => {
    const asyncFn1 = async (value) => {
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve(value * 2);
        }, 1000);
      });
    };

    const asyncFn2 = async (value) => {
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve(value + 5);
        }, 500);
      });
    };

    const result = await asyncPipe(asyncFn1, asyncFn2)(3);
    expect(result).toBe(11);
  });

  // supports functions with side effects
  it("should support functions with side effects", async () => {
    const mockFn1 = jest.fn((value) => value + 1);
    const mockFn2 = jest.fn((value) => value * 2);
    const mockFn3 = jest.fn((value) => value - 3);

    const result = await asyncPipe(mockFn1, mockFn2, mockFn3)(5);

    expect(result).toBe(9);
    expect(mockFn1).toHaveBeenCalledWith(5);
    expect(mockFn2).toHaveBeenCalledWith(6);
    expect(mockFn3).toHaveBeenCalledWith(12);
  });
});

// Testing suite for the pipe() function
describe("Testing pipe", () => {
  // applies multiple functions in sequence to a value
  it("should apply multiple functions in sequence to a value", () => {
    const addOne = (x) => x + 1;
    const double = (x) => x * 2;
    const result = pipe(addOne, double)(2);
    expect(result).toBe(6);
  });

  // returns null if any function in the sequence returns null
  it("should return null if any function in the sequence returns null", () => {
    const addOne = (x) => x + 1;
    const returnNull = () => null;
    const double = (x) => x * 2;
    const result = pipe(addOne, returnNull, double)(2);
    expect(result).toBeNull();
  });

  // handles a single function correctly
  it("should return the correct result when only one function is passed", () => {
    const addOne = (x) => x + 1;
    const result = pipe(addOne)(5);
    expect(result).toBe(6);
  });

  // processes an array of functions without modifying the original array
  it("should process an array of functions without modifying the original array", () => {
    const addOne = (num) => num + 1;
    const multiplyByTwo = (num) => num * 2;
    const subtractThree = (num) => num - 3;

    const functions = [addOne, multiplyByTwo, subtractThree];
    const pipedFunction = pipe(...functions);

    const originalArray = [...functions];
    pipedFunction(5);

    expect(functions).toEqual(originalArray);
  });

  // handles an empty array of functions by returning the initial value
  it("should return initial value when array of functions is empty", () => {
    const result = pipe()("initial value");
    expect(result).toBe("initial value");
  });

  // processes functions that return undefined without breaking
  it("should process functions that return undefined without breaking", () => {
    const addOne = (num) => num + 1;
    const multiplyByTwo = (num) => num * 2;
    const returnUndefined = () => undefined;

    const result = pipe(addOne, multiplyByTwo, returnUndefined)(3);

    expect(result).toBeUndefined();
  });

  // handles non-function elements in the array gracefully
  it("should throw an error if non-function elements are present in the array", () => {
    const result = pipe(1, "test", () => console.log("function"));
    expect(result).toThrow(/pipe/);
  });

  // supports functions with side effects
  it("should support functions with side effects", () => {
    const mockFn1 = jest.fn((value) => value + 1);
    const mockFn2 = jest.fn((value) => value * 2);
    const mockFn3 = jest.fn((value) => value - 3);

    const result = pipe(mockFn1, mockFn2, mockFn3)(5);

    expect(result).toBe(9);
    expect(mockFn1).toHaveBeenCalledWith(5);
    expect(mockFn2).toHaveBeenCalledWith(6);
    expect(mockFn3).toHaveBeenCalledWith(12);
  });
});
