import { toLowerCaseWithoutAccents } from "./utils.js";
import {
  ARKEO_SUPPORTED_LANGUAGES,
  ARKEO_DEFAULT_LANGS_TR,
  GEOM_NOWHERE,
} from "../constants.js";
import pg from "pg";
import {
  ACTIVATE_LANG_QUERY,
  INSERT_CITY_QUERY,
  INSERT_CITY_TR_QUERY,
  INSERT_CONTINENT_QUERY,
  INSERT_CONTINENT_TR_QUERY,
  INSERT_COUNTRY_QUERY,
  INSERT_COUNTRY_TR_QUERY,
  INSERT_LANG_QUERY,
  INSERT_LANG_TR_QUERY,
  INSERT_REGION_QUERY,
  INSERT_REGION_TR_QUERY,
} from "./queries.js";
const { Pool } = pg;

const pool = new Pool({
  user: process.env.ARKEOPEN_POSTGRESQL_USER,
  password: process.env.ARKEOPEN_POSTGRESQL_PASSWORD,
  host: process.env.ARKEOPEN_POSTGRESQL_HOST,
  port: process.env.ARKEOPEN_POSTGRESQL_PORT,
  database: process.env.ARKEOPEN_POSTGRESQL_DATABASE,
});

const cachedInsertedIds = {
  languages: new Set(),
  cities: new Set(),
  regions: new Set(),
  countries: new Set(),
  continents: new Set(),
};

const validateData = (data) => {
  return data.geonameid &&
    data.name &&
    data.latitude &&
    data.longitude &&
    data.country_code
    ? true
    : false;
};

const doQuery = async (queryString, queryValues = undefined) => {
  const client = await pool.connect();
  const result = await client.query(queryString, queryValues);
  await client.end();
  return result;
};

const doDatabaseTransation = async (subFunc, deferred = false) => {
  const client = await pool.connect();
  try {
    await client.query("BEGIN");
    if (deferred) {
      await client.query("SET CONSTRAINTS ALL DEFERRED");
    }
    await subFunc(client);
    await client.query("COMMIT");
  } catch (e) {
    await client.query("ROLLBACK");
    throw e;
  } finally {
    await client.end();
  }
};

const findCountryGeonameidFunc = (countries) => {
  const cache = {};
  return (country_code) => {
    if (cache[country_code]) {
      return cache[country_code];
    }
    const country = countries.find((country) => {
      if (country.country_code === country_code) {
        cache[country_code] = country.geonameid;
        return true;
      }
    });
    if (country) {
      return country.geonameid;
    }
  };
};

export const isoLanguageCodesToDb = async (context) => {
  console.log("-> Importing iso language codes");

  const result = await doQuery("SELECT count(*) from lang");
  const numberOfLangsInDb = result.rows[0].count;

  if (numberOfLangsInDb > 0) {
    if (!context.commandLineOptions.forceImportLangs) {
      const numberOfLangsToInsert = context.store.isoLanguageCodes.length + 1; // +1 for default lang "D"
      console.warn(
        `Trying to insert ${numberOfLangsToInsert} langs, but ${numberOfLangsInDb} langs are already present in DB. Use --force-import-lang option if you know what your are doing`
      );
      return context;
    }
  }

  const insertLangsFunc = async (pgClient) => {
    // Cleanup lang tables
    await pgClient.query("DELETE from lang");
    await pgClient.query("DELETE FROM lang_tr");

    // Insert default lang
    await pgClient.query(INSERT_LANG_QUERY, ["D", false]);

    // Insert langs
    for (const isolanguage of context.store.isoLanguageCodes) {
      await pgClient.query(INSERT_LANG_QUERY, [
        isolanguage["iso-639-1"],
        false,
      ]);
    }

    // Activate defined langs
    for (const isocode of ARKEO_SUPPORTED_LANGUAGES) {
      await pgClient.query(ACTIVATE_LANG_QUERY, [true, isocode]);
    }

    // Insert lang name translations
    for (const isolanguage_tr of ARKEO_DEFAULT_LANGS_TR) {
      await pgClient.query(INSERT_LANG_TR_QUERY, [
        isolanguage_tr["lang_isocode"],
        isolanguage_tr["lang_isocode_tr"],
        isolanguage_tr["name"],
      ]);
    }
  };

  // To the transaction
  await doDatabaseTransation(insertLangsFunc);
  return context;
};

/**
 * Imports continents data into the database.
 *
 * @param {Array} continentList - List of continents to be imported.
 * @returns {Promise<boolean>} - A promise that resolves to true if any continents were imported successfully, otherwise false.
 */
export const continentsToDb = async (continentList) => {
  const insertContinentsFunc = async (pgClient) => {
    console.log("-> Importing continents");
    // Insert default continent
    await pgClient.query(INSERT_CONTINENT_QUERY, [0, "U"]);

    for (const continent of continentList) {
      // Import continents
      await pgClient.query(INSERT_CONTINENT_QUERY, [
        continent.geonameid,
        continent.iso_code,
      ]);
      // Import continents default name
      await pgClient.query(INSERT_CONTINENT_TR_QUERY, [
        continent.geonameid,
        "D",
        continent.name,
        toLowerCaseWithoutAccents(continent.name),
      ]);
      // Store inserted id for translations
      cachedInsertedIds.continents.add(continent.geonameid);
    }
  };

  // To the transaction
  await doDatabaseTransation(insertContinentsFunc);
  console.log(
    `✓ Number of inserted continents: ${cachedInsertedIds.continents.size}`
  );
  return cachedInsertedIds.continents.size > 0;
};

/**
 * Imports countries data into the database.
 *
 * This function inserts countries from the context object into the database. It validates the data for each country, inserts the country details, imports the default country name, and stores the inserted IDs for translations.
 *
 * @param {Object} context - The context object containing the countries data to be imported.
 * @returns {Object} The updated context object after importing the countries.
 */
export const countriesToDb = async (context) => {
  const insertCountriesFunc = async (pgClient) => {
    // Import countries
    console.log("---> Importing countries");

    // Insert default country
    await pgClient.query(INSERT_COUNTRY_QUERY, [0, "U"]);
    for (const country of context.store.countries) {
      if (!validateData(country)) {
        console.warn(`Data not valid for country ${country.geonameid}`);
        console.warn(country);
        continue;
      }
      await pgClient.query(INSERT_COUNTRY_QUERY, [
        country.geonameid,
        country.country_code,
      ]);
      // Import countries default name
      await pgClient.query(INSERT_COUNTRY_TR_QUERY, [
        country.geonameid,
        "D",
        country.name,
        country.ascii_name || toLowerCaseWithoutAccents(country.name),
      ]);
      // Store inserted id for translations
      cachedInsertedIds.countries.add(country.geonameid);
    }
  };

  // Do the transation
  await doDatabaseTransation(insertCountriesFunc);
  console.log(
    `✓ Number of inserted countries: ${cachedInsertedIds.countries.size}`
  );
  return context;
};

/**
 * Imports cities data into the database.
 *
 * This function inserts cities from the context object into the database. It validates the data for each city, inserts the city details, imports the default city name, and stores the inserted IDs for translations.
 *
 * @param {Object} context - The context object containing the cities data to be imported.
 * @returns {Object} The updated context object after importing the cities.
 */
export const citiesToDb = async (context) => {
  console.log("---> Importing cities");
  const insertCitiesFunc = async (pgClient) => {
    // Insert default city
    await pgClient.query(INSERT_CITY_QUERY, [
      0,
      0,
      `POINT(${GEOM_NOWHERE.lon} ${GEOM_NOWHERE.lat})`,
    ]);
    const findCountryGeonameid = findCountryGeonameidFunc(
      context.store.countries
    );
    for (const city of context.store.cities) {
      if (!validateData(city)) {
        console.warn(`Data not valid for city ${city.geonameid}`);
        console.warn(city);
        continue;
      }
      const country_geonameid = findCountryGeonameid(city.country_code);
      if (!country_geonameid) {
        continue;
      }
      // Import cities
      await pgClient.query(INSERT_CITY_QUERY, [
        city.geonameid,
        country_geonameid,
        `POINT(${city.longitude} ${city.latitude})`,
      ]);
      // Import countries default name
      await pgClient.query(INSERT_CITY_TR_QUERY, [
        city.geonameid,
        "D",
        city.name,
        city.ascii_name || toLowerCaseWithoutAccents(city.name),
      ]);
      // Store inserted id for translations
      cachedInsertedIds.cities.add(city.geonameid);
    }
  };

  // Do the transation
  await doDatabaseTransation(insertCitiesFunc);
  console.log(`✓ Number of inserted cities: ${cachedInsertedIds.cities.size}`);

  return context;
};

/**
 * Imports regions data into the database.
 *
 * This function inserts regions from the context object into the database. It validates the data for each region, inserts the region details, imports the default region name, and stores the inserted IDs for translations.
 *
 * @param {Object} context - The context object containing the regions data to be imported.
 * @returns {Object} The updated context object after importing the regions.
 */
export const regionsToDb = async (context) => {
  console.log("-> Importing regions");

  const findCountryGeonameid = findCountryGeonameidFunc(
    context.store.countries
  );

  const insertRegionsFunc = async (pgClient) => {
    // Insert default region
    await pgClient.query(INSERT_REGION_QUERY, [
      0,
      0,
      `POINT(${GEOM_NOWHERE.lon} ${GEOM_NOWHERE.lat})`,
    ]);
    for (const region of context.store.regions) {
      if (!validateData(region)) {
        console.warn(`Data not valid for region ${region.geonameid}`);
        console.log(region);
        continue;
      }
      const country_geonameid = findCountryGeonameid(region.country_code);
      if (!country_geonameid) {
        console.warn(
          `Insert region: No geonameid found for country with iso_code ${region.country_code}`
        );
        continue;
      }
      // Insert regions
      await pgClient.query(INSERT_REGION_QUERY, [
        region.geonameid,
        country_geonameid,
        `POINT(${region.longitude} ${region.latitude})`,
      ]);
      // Import countries default name
      await pgClient.query(INSERT_REGION_TR_QUERY, [
        region.geonameid,
        "D",
        region.name,
        region.ascii_name || toLowerCaseWithoutAccents(region.name),
      ]);
      // Store inserted id for translations
      cachedInsertedIds.regions.add(region.geonameid);
    }
  };

  // Do the transation
  await doDatabaseTransation(insertRegionsFunc);

  console.log(
    `✓ Number of inserted regions: ${cachedInsertedIds.regions.size}`
  );
  return context;
};

/**
 * Imports alternate names into the database based on the provided contexts.
 *
 * @param {Object} cityContext - The context for cities.
 * @param {Object} countryContext - The context for countries.
 * @param {Object} regionContext - The context for regions.
 * @param {Object} continentResult - The result for continents.
 * @returns {Object} The updated alternate name context after importing.
 */
export const alternateNamesToDb =
  (cityContext, countryContext, regionContext, continentResult) =>
  async (alternateNameContext) => {
    console.log("-> Importing alternate names");

    const uniqCodesAlreadyProcessed = new Set();
    const processCountries = cachedInsertedIds.countries.size > 0;
    const processCities = cachedInsertedIds.cities.size > 0;
    const processRegions = cachedInsertedIds.regions.size > 0;
    const processContinents = cachedInsertedIds.continents.size > 0;
    let lineAlreadyProcessed = false;
    let uniqCode = "";

    const alternateNameDoQuery = async (
      pgClient,
      queryString,
      alternateNameObj
    ) => {
      await pgClient.query(queryString, [
        alternateNameObj.geonameid,
        alternateNameObj.isolanguage,
        alternateNameObj.alternate_name,
        toLowerCaseWithoutAccents(alternateNameObj.alternate_name),
      ]);
    };

    const insertAlternateNamesFunc = async (pgClient) => {
      for (const alternateName of alternateNameContext.store.alternateNames) {
        uniqCode = `${alternateName.geonameid}${alternateName.isolanguage}`;
        lineAlreadyProcessed = false;

        // Already processed and NOT a preferred name, we skip it
        if (
          uniqCodesAlreadyProcessed.has(uniqCode) &&
          !alternateName.isPreferredName
        ) {
          continue;
        }

        if (
          cityContext &&
          processCities &&
          cachedInsertedIds.cities.has(alternateName.geonameid)
        ) {
          // Insert city translations
          await alternateNameDoQuery(
            pgClient,
            INSERT_CITY_TR_QUERY,
            alternateName
          );
          lineAlreadyProcessed = true;
        }

        if (
          !lineAlreadyProcessed &&
          countryContext &&
          processCountries &&
          cachedInsertedIds.countries.has(alternateName.geonameid)
        ) {
          // Insert country translations
          await alternateNameDoQuery(
            pgClient,
            INSERT_COUNTRY_TR_QUERY,
            alternateName
          );
          lineAlreadyProcessed = true;
        }

        if (
          !lineAlreadyProcessed &&
          regionContext &&
          processRegions &&
          cachedInsertedIds.regions.has(alternateName.geonameid)
        ) {
          // Insert region translations
          await alternateNameDoQuery(
            pgClient,
            INSERT_REGION_TR_QUERY,
            alternateName
          );
          lineAlreadyProcessed = true;
        }
        if (
          !lineAlreadyProcessed &&
          continentResult &&
          processContinents &&
          cachedInsertedIds.continents.has(alternateName.geonameid)
        ) {
          // Insert continent translations
          await alternateNameDoQuery(
            pgClient,
            INSERT_CONTINENT_TR_QUERY,
            alternateName
          );
          lineAlreadyProcessed = true;
        }

        uniqCodesAlreadyProcessed.add(uniqCode);
      }
    };
    // Do the transation
    await doDatabaseTransation(insertAlternateNamesFunc);
    `✓ Alternate names import done.`;
    return alternateNameContext;
  };
