import path from "node:path";
import AdmZip from "adm-zip";
import { newResultObject } from "./utils.js";
import { fileExists, unlinkFile } from "./file.js";

/**
 * Unzips a specific file from a given zip file to a destination folder.
 *
 * @param {string} zipFile - The path to the zip file.
 * @param {string} fileToExtract - The name of the file to extract from the zip.
 * @param {string} destinationFolder - The path to the destination folder where the file will be extracted.
 * @param {boolean} [overwrite=false] - Flag indicating whether to overwrite the file if it already exists in the destination folder.
 * @returns {object} - An object containing the success status and a message indicating the result of the operation.
 */
export const unzipSpecificFile = (
  zipFile,
  fileToExtract,
  destinationFolder,
  overwrite = false
) => {
  const resultObject = newResultObject();
  try {
    const file = path.join(destinationFolder, fileToExtract);
    if (fileExists(file).success) {
      if (overwrite) {
        unlinkFile(file);
      } else {
        throw Error(`File  ${zipFile} already exists`);
      }
    }
    new AdmZip(zipFile).extractEntryTo(fileToExtract, destinationFolder);
    return resultObject.set({ success: true, message: "File unzipped." });
  } catch (err) {
    return resultObject.set({ success: false, message: err.message });
  }
};
