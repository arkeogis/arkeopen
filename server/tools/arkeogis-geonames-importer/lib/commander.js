import { Command, Argument } from "commander";

/**
 * Initializes a Commander program for the ArkeoGIS Geonames Importer CLI.
 *
 * @param {Function} action - The action to be executed when the command is triggered.
 * @returns {Command} The configured Commander program.
 */
const initCommander = (action) => {
  const program = new Command();

  program
    .name("arkeogis-geonames-importer")
    .description("CLI to import geonames into ArkeoGIS database")
    .version("1.0.0");

  program
    .command("import")
    .description("Import a geoname dataset")
    .addArgument(
      new Argument("<subset>", "subset to import").choices([
        "all",
        "cities",
        "regions",
        "countries",
        "continents",
        "languages",
      ])
    )
    .option("--skip-download", "Do not download files from geonames.org")
    .option("--skip-unzip", "Do not unzip the files")
    .option("--overwrite", "Overwrite the already download files")
    .option(
      "--force-import-langs",
      "Force importing the langs even some already exists in database"
    )
    .option("-m, --monitor", "Monitor ram consumption")
    .option("-e, --execution-time", "Monitor execution time")
    .action(action);

  return program;
};
export default initCommander;
