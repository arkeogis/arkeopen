import { parse } from "csv-parse";
import fs from "node:fs";
import { checkResultObjectOrNull, pipe } from "./utils.js";
import { fileExists } from "./file.js";

export const initContext = (config) => {
  const { csv: csvOptions } = config;
  const dataKeys = Object.keys(csvOptions.data);
  const store = dataKeys.reduce((store, property) => {
    store[property] = [];
    return store;
  }, {});
  let cachedIds = null;
  if (csvOptions.cachedIds) {
    cachedIds = dataKeys.reduce((cachedId, property) => {
      cachedId[property] = [];
      return cachedId;
    }, {});
  }
  return {
    processRecord(record) {
      if (!record || typeof record !== "object") {
        throw new Error("Invalid record. Please provide a valid record.");
      }
      if (
        !config ||
        typeof config !== "object" ||
        !config.csv ||
        typeof config.csv !== "object" ||
        !config.csv.data ||
        typeof config.csv.data !== "object"
      ) {
        throw new Error(
          "Invalid configuration object. Please provide a valid configuration."
        );
      }
      for (const property of Object.keys(store)) {
        const data = csvOptions.data[property];
        if (
          typeof data.regexp === "undefined" ||
          data.regexp.test(record[data.field])
        ) {
          if (csvOptions.cachedIds) {
            cachedIds[property]?.push(record[csvOptions.cachedIds]);
          }
          if (Array.isArray(data.fieldsToStore)) {
            store[property].push(
              data.fieldsToStore.reduce((returnedObject, prop) => {
                returnedObject[prop] = record[prop];
                return returnedObject;
              }, {})
            );
          }
        }
      }
    },
    get fieldnames() {
      return csvOptions.fieldnames;
    },
    get filename() {
      return csvOptions.filename;
    },
    get store() {
      return store;
    },
    get cachedIds() {
      return cachedIds || null;
    },
    get commandLineOptions() {
      return config.commandLineOptions;
    },
  };
};

/**
 * Reads a CSV file, processes each record using a provided context, and ensures the file exists before processing.
 * @param {Object} context - An object containing filename, fieldnames, and processRecord function.
 * @returns {Object|null} - Returns the context object if the file exists and processing is completed, otherwise returns null.
 */
export const processCSV = async (context) => {
  // Check if the file exists
  const isFileExist = pipe(
    fileExists,
    checkResultObjectOrNull
  )(context.filename);

  // If the file does not exist, log a warning and return null
  if (!isFileExist) {
    console.error(`File ${context.filename} does not exist`);
    return null;
  }

  console.log(`-> Processing file ${context.filename}`);

  // Create a CSV parser with specified options and read the file
  const parser = fs.createReadStream(context.filename).pipe(
    parse({
      delimiter: "\t",
      trim: true,
      quote: null,
      skip_empty_lines: true,
      encoding: "utf-8",
      columns: () => context.fieldnames,
    })
  );

  // Process each record in the CSV file using context.processRecord
  for await (const record of parser) {
    context.processRecord(record);
  }

  // Return the context object after processing
  return context;
};
