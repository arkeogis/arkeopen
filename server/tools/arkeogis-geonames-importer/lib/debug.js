/**
 * Converts a given number of bytes into gigabytes and returns the result as a string with "GB" appended.
 * @param {number} bytes - The number of bytes to convert.
 * @returns {string} - The converted entry in gigabytes, formatted to two decimal places.
 */
const bytesToGigabytes = (bytes) => {
  const GIGABYTE = 1024 * 1024 * 1024;

  if (bytes === 0) {
    return "0 GB";
  }

  return (bytes / GIGABYTE).toFixed(2) + " GB";
};

/**
 * Monitors memory usage at a specified frequency interval.
 *
 * @param {number} [frequency=5000] - The frequency interval in milliseconds to monitor memory usage.
 * @returns {Function} A function to stop the memory monitoring.
 */

export const memoryMonitor = (frequency = 5000) => {
  const intervalID = setInterval(() => {
    const memoryUsage = process.memoryUsage();
    console.log(
      `Memory Usage: RSS: ${bytesToGigabytes(
        memoryUsage.rss
      )} - Heap Total: ${bytesToGigabytes(
        memoryUsage.heapTotal
      )} - Heap Used: ${bytesToGigabytes(memoryUsage.heapUsed)}`
    );
  }, frequency);

  return () => {
    clearInterval(intervalID);
  };
};

/**
 * Measures the execution time of a code block.
 * It starts a timer when called and returns a function that stops the timer and logs the elapsed time.
 * @returns {Function} A function that stops the timer and logs the execution time when invoked.
 */
export const executionTimeMonitor = () => {
  console.time("Execution time");
  return () => {
    console.timeEnd("Execution time");
  };
};

/**
 * Prints the number of entries for each subset in the provided context's store object.
 *
 * @param {Object} context - The context object containing the store to be analyzed.
 * @returns {Object} The same context object that was provided as input.
 */
export const printStoreStats = (context) => {
  for (const [subsetName, entries] of Object.entries(context.store)) {
    console.log(`Number of entries for ${subsetName}: ${entries.length}`);
  }
  return context;
};

/**
 * Replaces placeholders in a query string with corresponding values from an array.
 *
 * @param {string} queryString - The query string with placeholders in the format of $1, $2, etc.
 * @param {Array} arrayOfValues - An array containing values to replace the placeholders in the query string.
 * @returns {string} - The query string with placeholders replaced by actual values.
 */
export const getRawQuery = (queryString, arrayOfValues) => {
  return queryString.replace(/\$\d+/g, (match) => {
    const index = parseInt(match.slice(1)) - 1;
    return typeof arrayOfValues[index] === "string"
      ? `'${arrayOfValues[index]}'`
      : arrayOfValues[index];
  });
};
