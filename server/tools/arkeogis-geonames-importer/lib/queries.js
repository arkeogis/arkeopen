export const INSERT_LANG_QUERY = `INSERT INTO lang (isocode, active) VALUES ($1, $2) ON CONFLICT (isocode) DO NOTHING`;

export const INSERT_LANG_TR_QUERY = `INSERT INTO lang_tr (lang_isocode, lang_isocode_tr, name) VALUES ($1, $2, $3)`;

export const ACTIVATE_LANG_QUERY =
  "UPDATE lang SET active=$1 where isocode = $2";

export const INSERT_COUNTRY_QUERY = `INSERT INTO country (geonameid, iso_code, created_at, updated_at)
    VALUES ($1::int4, $2, NOW(), NOW())
    ON CONFLICT (geonameid) DO UPDATE
    SET geonameid = $1::int4, iso_code = $2, updated_at = NOW()`;

export const INSERT_COUNTRY_TR_QUERY = `INSERT INTO country_tr (country_geonameid, lang_isocode, name, name_ascii)
    VALUES ($1::int4, $2, $3, $4)
    ON CONFLICT (country_geonameid, lang_isocode) DO UPDATE
    SET country_geonameid = $1, lang_isocode = $2, name = $3, name_ascii = $4`;

export const INSERT_CITY_QUERY = `INSERT INTO city (geonameid, country_geonameid, geom_centroid, created_at, updated_at)
    VALUES ($1::int4, $2, ST_GeometryFromText($3, 4326::integer), NOW(), NOW())
    ON CONFLICT (geonameid) DO UPDATE
    SET geonameid = $1::int4, country_geonameid = $2, geom_centroid = ST_GeometryFromText($3, 4326::integer), updated_at = NOW()`;

export const INSERT_CITY_TR_QUERY = `INSERT INTO city_tr (city_geonameid, lang_isocode, name, name_ascii)
    VALUES ($1::int4, $2, $3, $4)
    ON CONFLICT (city_geonameid, lang_isocode) DO UPDATE
    SET city_geonameid = $1::int4, lang_isocode = $2, name = $3, name_ascii = $4`;

export const INSERT_REGION_QUERY = `INSERT INTO region (geonameid, country_geonameid, geom_centroid, created_at, updated_at)
    VALUES ($1::int4, $2, ST_GeometryFromText($3, 4326::integer), NOW(), NOW())
    ON CONFLICT (geonameid) DO UPDATE
    SET geonameid = $1::int4, country_geonameid = $2, geom_centroid = ST_GeometryFromText($3, 4326::integer), updated_at = NOW()`;

export const INSERT_REGION_TR_QUERY = `INSERT INTO region_tr (region_geonameid, lang_isocode, name, name_ascii)
    VALUES ($1::int4, $2, $3, $4)
    ON CONFLICT (region_geonameid, lang_isocode) DO UPDATE
    SET region_geonameid = $1::int4, lang_isocode = $2, name = $3, name_ascii = $4`;

export const INSERT_CONTINENT_QUERY = `INSERT INTO continent (geonameid, iso_code, created_at, updated_at)
      VALUES ($1::int4, $2, NOW(), NOW())
      ON CONFLICT (geonameid) DO UPDATE
      SET geonameid = $1::int4, iso_code = $2, updated_at = NOW()`;

export const INSERT_CONTINENT_TR_QUERY = `INSERT INTO continent_tr (continent_geonameid, lang_isocode, name, name_ascii)
    VALUES ($1::int4, $2, $3, $4)
    ON CONFLICT (continent_geonameid, lang_isocode) DO UPDATE
    SET continent_geonameid = $1::int4, lang_isocode = $2, name = $3, name_ascii = $4`;
