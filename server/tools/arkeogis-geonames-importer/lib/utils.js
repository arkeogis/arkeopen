/**
 * Applies multiple functions in sequence to a value.
 *
 * @param {...Function} fns - Functions to be applied in sequence.
 * @param {*} value - The initial value to be processed by the functions.
 * @returns {*} - The final result after applying all functions in sequence.
 * @throws {Error} - If any of the passed arguments is not a function.
 */
export const pipe =
  (...fns) =>
  (value) => {
    for (const fn of fns) {
      if (typeof fn !== "function") {
        throw Error("pipe(): passed argument is not a function");
      }
      const result = fn(value);
      if (result === null) return null;
      value = result;
    }
    return value;
  };

/**
 * Applies multiple asynchronous functions in sequence to a value.
 *
 * @param {...Function} fns - Functions to be applied in sequence.
 * @param {*} value - The initial value to be processed by the functions.
 * @returns {Promise<*>} - A promise that resolves to the final result after applying all functions in sequence.
 * @throws {Error} - If any of the passed arguments is not a function.
 */
export const asyncPipe =
  (...fns) =>
  async (value) => {
    for (const fn of fns) {
      if (typeof fn !== "function") {
        throw Error("asyncPipe(): passed argument is not a function");
      }
      const result = await fn(value);
      if (result === null) return null;
      value = result;
    }
    return value;
  };

/**
 * Converts a given string to a number if possible.
 *
 * @param {string} str - The string to be converted to a number.
 * @returns {number|string} - The converted number if conversion is possible, otherwise returns the original string.
 */
export const convertToNumberIfPossible = (str) =>
  /^\d+$/.test(str)
    ? parseInt(str)
    : /^-?\d+(\.\d+)?$/.test(str)
    ? parseFloat(str)
    : str;

/**
 * Creates a new result object with properties for success status, message, and content.
 *
 * @returns {Object} - An object with getter and setter methods for accessing and updating result properties.
 * @property {boolean} success - The success status of the result.
 * @property {string} message - The message associated with the result.
 * @property {*} content - The content of the result.
 * @method get - A method to get the current result object.
 * @method set - A method to set new values for the result object properties.
 */
export const newResultObject = () => {
  let resultObject = {
    success: false,
    message: "",
    content: null,
  };
  return {
    get: () => resultObject,
    get success() {
      return resultObject.success;
    },
    get message() {
      return resultObject.message;
    },
    get content() {
      return resultObject.message;
    },
    set: (values) => {
      resultObject = Object.keys(values)
        .filter((key) => Object.keys(resultObject).includes(key))
        .reduce((obj, key) => {
          obj[key] = values[key];
          return obj;
        }, {});
      return resultObject;
    },
  };
};

/**
 * Checks the success status of a result object and throws an error if it is not successful.
 *
 * @param {Object} resultObject - The result object to be checked.
 * @returns {boolean} - Returns true if the result object is successful.
 * @throws {Error} - Throws an error with the message from the result object if it is not successful.
 */
export const checkResultObjectOrThrowError = (resultObject) => {
  if (resultObject.success) {
    return true;
  } else {
    throw Error(resultObject.message);
  }
};

/**
 * Checks the success property of a given result object and returns true if successful, otherwise returns null.
 * @param {Object} resultObject - An object that contains a success property.
 * @returns {boolean | null} - true if success is true, null if success is false.
 */
export const checkResultObjectOrNull = (resultObject) =>
  resultObject.success ? true : null;

/**
 * Creates a hierarchical tree structure based on the input data.
 *
 * @param {Array} data - The input data containing objects with parent and child relationships.
 * @returns {Object} - The hierarchical tree structure built from the input data.
 */
export const createTree = (data) => {
  const tree = {};

  data.forEach((obj) => {
    if (!tree[obj.parentId]) {
      tree[obj.parentId] = { children: [] };
    }
    tree[obj.parentId].children.push(obj.childId);
  });

  return buildTree(tree);
};

/**
 * Recursively builds a hierarchical tree structure based on the input node.
 *
 * @param {Object} node - The node containing children relationships.
 * @returns {Object} - The node with its children as a hierarchical tree structure.
 */

function buildTree(node) {
  const children = [];
  if (node.children) {
    for (const childId of node.children) {
      const childNode = buildTree(tree[childId]);
      children.push(childNode);
    }
  } else {
    return node;
  }
  return { ...node, children }; // spread operator to add children property to node
}

/**
 * Dummy function that simply returns the input parameter.
 *
 * @param {*} params - The input parameter to be returned.
 * @returns {*} - The same input parameter passed to the function.
 */
export const dummyFunc = (params) => params;

export const findInfosByGeonameId = (store, property, geonameid) => {
  console.log(store[property]);
};

/**
 * Converts a given string to ASCII by normalizing it to decompose accented characters,
 * removing diacritical marks, and single and double quotes.
 *
 * @param {string} str - The input string to be converted to ASCII.
 * @returns {string} The converted ASCII string.
 */

export function toLowerCaseWithoutAccents(str) {
  // Lowercase string, remove accents and diacritical marks
  return str
    .toLowerCase()
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .replace(/['"]/g, "")
    .replace(/[^\x00-\x7F]/g, "");
}
