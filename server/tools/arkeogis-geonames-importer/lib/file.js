import path from "node:path";
import fs from "node:fs";
import { unlink, writeFile } from "node:fs/promises";
import { Readable } from "node:stream";
import { checkResultObjectOrNull, newResultObject, pipe } from "./utils.js";

/**
 * Checks if a specified file or folder exists in the filesystem.
 * @param {string} fileOrFolder - The path to the file or folder to check.
 * @returns {Object} An object indicating the result with success and message properties.
 */
export const fileExists = (fileOrFolder) => {
  const resultObject = newResultObject();
  return fs.existsSync(fileOrFolder)
    ? { success: true, message: "File exists." }
    : { ...resultObject, message: "File does not exist." };
};

/**
 * Asynchronously deletes a file.
 *
 * @param {string} filename - The path to the file to be deleted.
 * @returns {Object} An object containing the success status and a message.
 *                   If successful, returns { success: true, message: "File deleted." }.
 *                   If an error occurs, returns an object with the error message.
 */
export const unlinkFile = async (filename) => {
  const resultObject = newResultObject();
  try {
    await unlink(filename);
    return { success: true, message: "File deleted." };
  } catch (err) {
    return {
      ...resultObject,
      message: err.message,
    };
  }
};

/**
 * Downloads a file from the specified URL to the given download folder.
 *
 * @param {string} url - The URL of the file to download.
 * @param {string} downloadFolder - The folder where the file will be downloaded.
 * @param {boolean} [overwrite=false] - Flag indicating whether to overwrite the file if it already exists.
 * @returns {Promise<Object>} A promise that resolves to a result object with success status and message.
 */
export const downloadFile = async (url, downloadFolder, overwrite = false) => {
  const resultObject = newResultObject();
  const filename = path.basename(url);
  try {
    const isFileExist = pipe(
      fileExists,
      checkResultObjectOrNull
    )(`${downloadFolder}/${filename}`);
    if (isFileExist && !overwrite) {
      throw Error(`File ${filename} already exists.`);
    }
    const response = await fetch(url);
    if (!response.ok) {
      throw Error(response.statusText);
    }
    const body = Readable.fromWeb(response.body);
    await writeFile(path.join(downloadFolder, filename), body);
    return resultObject.set({ success: true, message: "File downloaded." });
  } catch (err) {
    return resultObject.set({
      message: err.message,
    });
  }
};

const getRawQuery = (queryString, arrayOfValues) => {
  return queryString.replace(/\$\d+/g, (match) => {
    const index = parseInt(match.slice(1)) - 1;
    return typeof arrayOfValues[index] === "string"
      ? `"${arrayOfValues[index]}"`
      : arrayOfValues[index];
  });
};
