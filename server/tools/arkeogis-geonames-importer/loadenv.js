// loadEnv.js
import dotenvx from "@dotenvx/dotenvx";
import fs from "node:fs";
import path from "node:path";

const envFile = path.join(process.cwd(), "../../.env");
let errorFound = false;
if (!fs.existsSync(envFile)) {
  console.error(`Env file ${envFile} not found.`);
  errorFound = true;
}

dotenvx.config({ path: envFile });

for (const envVar of [
  "ARKEOPEN_POSTGRESQL_USER",
  "ARKEOPEN_POSTGRESQL_PASSWORD",
  "ARKEOPEN_POSTGRESQL_HOST",
  "ARKEOPEN_POSTGRESQL_PORT",
  "ARKEOPEN_POSTGRESQL_DATABASE",
]) {
  if (!process.env[envVar]) {
    console.error(
      `Environment variable ${envVar} not found. Please fill it in ${envFile}`
    );
    errorFound = true;
  }
}

if (errorFound) {
  process.exit(1);
}
