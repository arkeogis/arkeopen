import path from "node:path";
import { ARKEO_SUPPORTED_LANGUAGES } from "./constants.js";

import {
  GEONAME_FIELDS,
  GEONAME_ALTERNATE_NAME_FIELDS,
  GEONAME_ISO_LANGUAGE_CODE_FIELDS,
  GEONAME_HIERARCHY_FIELDS,
} from "./constants.js";

const DOWNLOAD_FOLDER = path.join(
  new URL(".", import.meta.url).pathname,
  "tmp"
);

const defaultConfig = {
  commandLineOptions: {
    skipDownload: false,
    skipUnzip: false,
    overwrite: false,
    forceImportLangs: false,
  },
};

export const isoLanguageCodesConfig = {
  ...defaultConfig,
  download: {
    geonamesURL:
      "https://download.geonames.org/export/dump/iso-languagecodes.txt",
    filename: "iso-languagecodes.txt",
    downloadFolder: DOWNLOAD_FOLDER,
  },
  csv: {
    fieldnames: GEONAME_ISO_LANGUAGE_CODE_FIELDS,
    filename: path.join(DOWNLOAD_FOLDER, "iso-languagecodes.txt"),
    cachedIds: null,
    data: {
      isoLanguageCodes: {
        field: "iso-639-1",
        regexp: /^[a-z]{2}$/,
        fieldsToStore: ["iso-639-1", "language-name"],
      },
    },
  },
};

export const allCountryConfig = {
  ...defaultConfig,
  download: {
    geonamesURL: "http://download.geonames.org/export/dump/allCountries.zip",
    filename: "allCountries.txt",
    downloadFolder: DOWNLOAD_FOLDER,
  },
  csv: {
    filename: path.join(DOWNLOAD_FOLDER, "allCountries.txt"),
    fieldnames: GEONAME_FIELDS,
    cachedIds: "geonameid",
    data: {
      cities: {
        field: "feature_code",
        regexp: /^(PPL|PPLA|PPLA2|PPLA3|PPLA4|PPLC)$/,
        fieldsToStore: [
          "geonameid",
          "name",
          "ascii_name",
          "latitude",
          "longitude",
          "country_code",
        ],
      },
      countries: {
        field: "feature_code",
        regexp: /^PCLI|PCLS$/,
        fieldsToStore: [
          "geonameid",
          "name",
          "ascii_name",
          "latitude",
          "longitude",
          "country_code",
        ],
      },
      subregions: {
        field: "feature_code",
        regexp: /^(ADM1|PCLD|PCLIX|TERR)$/,
        fieldsToStore: [
          "geonameid",
          "name",
          "ascii_name",
          "latitude",
          "longitude",
          "country_code",
        ],
      },
    },
  },
};

export const alternateNamesConfig = {
  ...defaultConfig,
  download: {
    geonamesURL:
      "https://download.geonames.org/export/dump/alternateNamesV2.zip",
    filename: "alternateNamesV2.txt",
    downloadFolder: DOWNLOAD_FOLDER,
  },
  csv: {
    filename: path.join(DOWNLOAD_FOLDER, "alternateNamesV2.txt"),
    fieldnames: GEONAME_ALTERNATE_NAME_FIELDS,
    cachedIds: null,
    data: {
      alternateNames: {
        field: "isolanguage",
        regexp: new RegExp(
          String.raw`^(${ARKEO_SUPPORTED_LANGUAGES.join("|")})$`
        ),
        fieldsToStore: [
          "geonameid",
          "isolanguage",
          "alternate_name",
          "isPreferredName",
        ],
      },
    },
  },
};

export const hierarchyConfig = {
  ...defaultConfig,
  download: {
    geonamesURL: "https://download.geonames.org/export/dump/hierarchy.zip",
    filename: "hierarchy.txt",
    downloadFolder: DOWNLOAD_FOLDER,
  },
  csv: {
    filename: path.join(DOWNLOAD_FOLDER, "hierarchy.txt"),
    fieldnames: GEONAME_HIERARCHY_FIELDS,
    cachedIds: "parentId",
    data: {
      hierarchy: {
        field: "type",
        regexp: /^(ADM|dependency)$/,
        fieldsToStore: ["parentId", "childId"],
      },
    },
  },
};
