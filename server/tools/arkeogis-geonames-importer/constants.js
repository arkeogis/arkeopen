export const ARKEO_SUPPORTED_LANGUAGES = ["fr", "en", "es", "de"];

export const GEONAME_FIELDS = [
  "geonameid",
  "name",
  "ascii_name",
  "alternatenames",
  "latitude",
  "longitude",
  "feature_class",
  "feature_code",
  "country_code",
  "cc2",
  "admin1_code",
  "admin2_code",
  "admin3_code",
  "admin4_code",
  "population",
  "elevation",
  "dem",
  "timezone",
  "modification_date",
];

export const GEONAME_ALTERNATE_NAME_FIELDS = [
  "alternateNameId",
  "geonameid",
  "isolanguage",
  "alternate_name",
  "isPreferredName",
  "isShortName",
  "isColloquial",
  "isHistoric",
  "from",
  "to",
];

export const GEONAME_ISO_LANGUAGE_CODE_FIELDS = [
  "iso-639-3",
  "iso-639-2",
  "iso-639-1",
  "language-name",
];

export const GEONAME_HIERARCHY_FIELDS = ["parentId", "childId", "type"];

export const GEOM_NOWHERE = { lon: 2.5559, lat: 49.0083 }; // Paris (CDG)

export const ARKEO_DEFAULT_LANGS_TR = [
  {
    lang_isocode: "fr",
    lang_isocode_tr: "fr",
    name: "Français",
  },
  {
    lang_isocode: "fr",
    lang_isocode_tr: "en",
    name: "French",
  },
  {
    lang_isocode: "fr",
    lang_isocode_tr: "de",
    name: "Französisch",
  },
  {
    lang_isocode: "fr",
    lang_isocode_tr: "es",
    name: "Francés",
  },
  {
    lang_isocode: "en",
    lang_isocode_tr: "fr",
    name: "Anglais",
  },
  {
    lang_isocode: "en",
    lang_isocode_tr: "en",
    name: "English",
  },
  {
    lang_isocode: "en",
    lang_isocode_tr: "de",
    name: "Englisch",
  },
  {
    lang_isocode: "en",
    lang_isocode_tr: "es",
    name: "Inglés",
  },
  {
    lang_isocode: "de",
    lang_isocode_tr: "fr",
    name: "Allemand",
  },
  {
    lang_isocode: "de",
    lang_isocode_tr: "en",
    name: "German",
  },
  {
    lang_isocode: "de",
    lang_isocode_tr: "de",
    name: "Deutsch",
  },
  {
    lang_isocode: "de",
    lang_isocode_tr: "es",
    name: "Alemán",
  },
  {
    lang_isocode: "es",
    lang_isocode_tr: "fr",
    name: "Espagnol",
  },
  {
    lang_isocode: "es",
    lang_isocode_tr: "en",
    name: "Spanish",
  },
  {
    lang_isocode: "es",
    lang_isocode_tr: "de",
    name: "Spanisch",
  },
  {
    lang_isocode: "es",
    lang_isocode_tr: "es",
    name: "Español",
  },
];

export const GEONAME_CONTINENTS = [
  {
    geonameid: "0",
    iso_code: "U",
    name: "Default undefined",
  },
  {
    geonameid: "6255146",
    iso_code: "AF",
    name: "Africa",
  },
  {
    geonameid: "6255147",
    iso_code: "AS",
    name: "Asia",
  },
  {
    geonameid: "6255148",
    iso_code: "EU",
    name: "Europe",
  },
  {
    geonameid: "6255149",
    iso_code: "NA",
    name: "North America",
  },
  {
    geonameid: "6255150",
    iso_code: "SA",
    name: "South America",
  },
  {
    geonameid: "6255151",
    iso_code: "OC",
    name: "Oceania",
  },
  {
    geonameid: "6255152",
    iso_code: "AN",
    name: "Antartica",
  },
];
