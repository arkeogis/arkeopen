#!/usr/bin/env node
import "./loadenv.js";
import path from "node:path";
import {
  allCountryConfig,
  alternateNamesConfig,
  isoLanguageCodesConfig,
  hierarchyConfig,
} from "./config.js";
import { downloadFile, fileExists } from "./lib/file.js";
import { unzipSpecificFile } from "./lib/zip.js";
import { asyncPipe, createTree, dummyFunc } from "./lib/utils.js";
import { initContext, processCSV } from "./lib/csv.js";
import {
  executionTimeMonitor,
  memoryMonitor,
  printStoreStats,
} from "./lib/debug.js";
import {
  alternateNamesToDb,
  citiesToDb,
  continentsToDb,
  countriesToDb,
  isoLanguageCodesToDb,
  regionsToDb,
} from "./lib/db.js";
import { GEONAME_CONTINENTS } from "./constants.js";
import initCommander from "./lib/commander.js";

/**
 * Check if the provided commander command is included in the specified list of commands.
 *
 * @param {Array} check - The list of commands to check against.
 * @param {string} commanderCommand - The commander command to check.
 * @returns {Function} A function that takes a config object and returns the config object if the commander command is included in the check list, otherwise returns null.
 */
const isRequestedByUser = (check, commanderCommand) => (config) => {
  return check.includes(commanderCommand) ? config : null;
};

/**
 * Applies the provided commander options to the given configuration object.
 *
 * @param {Object} options - The commander options to apply.
 * @param {boolean} options.skipDownload - Whether to skip the download.
 * @param {boolean} options.skipUnzip - Whether to skip the unzip.
 * @param {boolean} options.overwrite - Whether to overwrite existing files.
 * @returns {Function} A function that takes a configuration object and returns a new configuration object with the applied options.
 */
const applyCommanderOptions =
  ({ skipDownload, skipUnzip, overwrite, forceImportLangs } = {}) =>
  (config) => {
    return {
      ...config,
      commandLineOptions: {
        skipDownload: skipDownload
          ? true
          : config.commandLineOptions.skipDownload,
        skipUnzip: skipUnzip ? true : config.commandLineOptions.skipUnzip,
        overwrite: overwrite ? true : config.commandLineOptions.overwrite,
        forceImportLangs: forceImportLangs
          ? true
          : config.commandLineOptions.forceImportLangs,
      },
    };
  };

/**
 * Check if any of the previous results is null and return null if true, otherwise return the provided config object.
 *
 * @param {Array} previousResults - The array of previous results to check for null values.
 * @returns {Function} A function that takes a config object and returns null if any of the previous results is null, otherwise returns the config object.
 */
const skipIfPreviousOperationFailed = (previousResults) => (config) => {
  return previousResults.includes(null) ? null : config;
};

/**
 * Downloads a file if requested based on the provided configuration object.
 *
 * @param {object} config - The configuration object containing download details.
 * @returns {object|null} The updated configuration object if the file was downloaded successfully, otherwise null.
 * @throws {Error} If the config object is invalid or missing required properties.
 */
const downloadFileIfRequested = async (config) => {
  if (config.commandLineOptions.skipDownload) {
    const file = path.join(
      config.download.downloadFolder,
      path.basename(config.download.geonamesURL)
    );
    if (!fileExists(file).success) {
      console.error(
        `File ${file} does not exist and --skip-download flag is set.`
      );
      return null;
    }
    return config;
  }
  const fileDownloadResult = await downloadFile(
    config.download.geonamesURL,
    config.download.downloadFolder,
    config.commandLineOptions.overwrite
  );
  if (!fileDownloadResult.success) {
    console.error(
      `Unable to download file: ${fileDownloadResult.message}. Use --overwrite flag to force to download again.`
    );
    return null;
  }
  return config;
};

/**
 * Unzips a specific file if requested in the provided configuration object.
 *
 * @param {Object} config - The configuration object containing information about the file to unzip.
 * @returns {Object|null} The updated configuration object if successful, otherwise null.
 */
const unzipFileIfRequested = async (config) => {
  if (config.commandLineOptions.skipUnzip) return config;
  const zipFilename = path.basename(config.download.geonamesURL);
  const unzipResult = unzipSpecificFile(
    path.join(config.download.downloadFolder, zipFilename),
    config.download.filename,
    config.download.downloadFolder,
    config.commandLineOptions.overwrite
  );
  if (!unzipResult.success) {
    console.warn(`Unable to unzip file: ${unzipResult.message}`);
    return null;
  }
  return config;
};

/**
 * Find a country object in the provided array based on the given geonameid.
 *
 * @param {Array} countries - The array of country objects to search through.
 * @param {number} id - The geonameid to match against the country objects.
 * @returns {Object|undefined} The country object if found, otherwise undefined.
 */
const findCountry = (countries, id) => {
  return countries.find((country) => country.geonameid === id);
};

/**
 * Find a region object in the provided array based on the given geonameid.
 *
 * @param {Array} regions - The array of region objects to search through.
 * @param {number} id - The geonameid to match against the region objects.
 * @returns {Object|undefined} The region object if found, otherwise undefined.
 */
const findRegion = (regions, id) => {
  return regions.find((region) => region.geonameid === id);
};

const addTopLevelRegionsToContext = (originalContext) => (hierarchyResult) => {
  // Create a tree of regions from the hierarchy.txt parsing
  const tree = createTree(hierarchyResult.store.hierarchy);
  // Get the cached countries ids
  const countriesIds = originalContext.cachedIds.countries;
  const regionsByCountry = [];

  // Process the hierarchy
  Object.entries(tree).forEach(([topLevelId, children]) => {
    // Get only regions which the parent is a country
    if (countriesIds.includes(topLevelId)) {
      // Get the corresponding country infos
      const country = findCountry(originalContext.store.countries, topLevelId);
      children.children.forEach((regionId) => {
        // Get the corresponding region infos
        const region = findRegion(originalContext.store.subregions, regionId);
        // If we got all the infos needed, compute the region infos
        if (country && region) {
          regionsByCountry.push({
            ...region,
            country_code: country.country_code,
          });
        }
      });
    }
  });

  originalContext.store.regions = regionsByCountry;

  return originalContext;
};

/**
 * Imports geonames data based on the provided commander command and options.
 *
 * @param {string} commanderCmd - The commander command to determine the import operation.
 * @param {object} commanderOpts - The commander options object containing flags for monitoring and execution time.
 * @returns {Promise<void>} - A promise that resolves once the geonames data import is completed.
 */
const importGeonames = async (commanderCmd, commanderOpts) => {
  let stopMonitor;
  let displayExecutiontime;
  if (commanderOpts.monitor) {
    stopMonitor = memoryMonitor();
    console.warn("Memory monitor started");
  }
  if (commanderOpts.executionTime) {
    displayExecutiontime = executionTimeMonitor();
    console.warn("Execution time monitor started");
  }

  // Process langs
  const isoLanguageCodeContext = await asyncPipe(
    applyCommanderOptions(commanderOpts),
    downloadFileIfRequested,
    initContext,
    processCSV,
    ["all", "languages"].includes(commanderCmd)
      ? isoLanguageCodesToDb
      : dummyFunc
  )(isoLanguageCodesConfig);

  // Prepare datas from allCountries.txt to be used for city/countries/regions
  const allCountryContext = await asyncPipe(
    isRequestedByUser(["all", "cities", "countries", "regions"], commanderCmd),
    skipIfPreviousOperationFailed([isoLanguageCodeContext]),
    applyCommanderOptions(commanderOpts),
    downloadFileIfRequested,
    unzipFileIfRequested,
    initContext,
    processCSV,
    printStoreStats
  )(allCountryConfig);

  // Process countries
  const countryResult = await asyncPipe(
    isRequestedByUser(["all", "countries"], commanderCmd),
    skipIfPreviousOperationFailed([allCountryContext]),
    countriesToDb
  )(allCountryContext);

  // Process cities
  const cityResult = await asyncPipe(
    isRequestedByUser(["all", "cities"], commanderCmd),
    skipIfPreviousOperationFailed([allCountryContext]),
    citiesToDb
  )(allCountryContext);

  // Process regions
  const regionResult = await asyncPipe(
    isRequestedByUser(["all", "regions"], commanderCmd),
    skipIfPreviousOperationFailed([allCountryContext]),
    applyCommanderOptions(commanderOpts),
    downloadFileIfRequested,
    unzipFileIfRequested,
    initContext,
    processCSV,
    addTopLevelRegionsToContext(allCountryContext),
    printStoreStats,
    regionsToDb
  )(hierarchyConfig);

  // Process continents
  const continentResult = await asyncPipe(
    isRequestedByUser(["all", "continents"], commanderCmd),
    continentsToDb
  )(GEONAME_CONTINENTS);

  // Process alternate names
  await asyncPipe(
    skipIfPreviousOperationFailed([
      isoLanguageCodeContext,
      allCountryContext,
      cityResult || countryResult || regionResult || continentResult,
    ]),
    isRequestedByUser(
      ["all", "cities", "countries", "regions", "continents"],
      commanderCmd
    ),
    applyCommanderOptions(commanderOpts),
    downloadFileIfRequested,
    unzipFileIfRequested,
    initContext,
    processCSV,
    printStoreStats,
    alternateNamesToDb(cityResult, countryResult, regionResult, continentResult)
  )(alternateNamesConfig);

  if (commanderOpts.monitor) {
    stopMonitor();
  }
  if (commanderOpts.executionTime) {
    displayExecutiontime();
  }
};

(async () => {
  const program = initCommander(async (cmd, opts) => {
    importGeonames(cmd, opts);
  });
  await program.parseAsync(process.argv);
})();
