ALTER TABLE "database" ADD COLUMN "root_chronology_id" INTEGER NOT NULL DEFAULT 0;

ALTER TABLE "map_layer_tr" RENAME COLUMN "copyright" TO "citation";

ALTER TABLE "shapefile" ADD COLUMN "opened" BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE "shapefile" ADD COLUMN "editor" VARCHAR NOT NULL DEFAULT '';
ALTER TABLE "shapefile" ADD COLUMN "editor_uri" VARCHAR NOT NULL DEFAULT '';
ALTER TABLE "shapefile" ADD COLUMN "deposit_uri" VARCHAR NOT NULL DEFAULT '';
ALTER TABLE "shapefile_tr" RENAME COLUMN "copyright" TO "citation";
ALTER TABLE "shapefile_tr" ADD COLUMN "geographical_covering" TEXT NOT NULL DEFAULT '';

ALTER TABLE "chronology_root" ADD COLUMN "opened" BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE "chronology_root" ADD COLUMN "editor" VARCHAR NOT NULL DEFAULT '';
ALTER TABLE "chronology_root" ADD COLUMN "editor_uri" VARCHAR NOT NULL DEFAULT '';
ALTER TABLE "chronology_root" ADD COLUMN "deposit_uri" VARCHAR NOT NULL DEFAULT '';

CREATE TABLE "chronology_root_tr" (
  "lang_isocode" CHAR(2) NOT NULL,
  "root_chronology_id" INTEGER NOT NULL,
  "geographical_covering" TEXT NOT NULL DEFAULT '',
  PRIMARY KEY ("lang_isocode", "root_chronology_id")
);

ALTER TABLE "database" ADD CONSTRAINT "c_database.root_chronology_id" FOREIGN KEY ("root_chronology_id") REFERENCES "chronology_root" ("root_chronology_id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE "chronology_root_tr" ADD CONSTRAINT "c_chronology_root_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "chronology_root_tr" ADD CONSTRAINT "c_chronology_root_tr.root_chronology_id" FOREIGN KEY ("root_chronology_id") REFERENCES "chronology_root" ("root_chronology_id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "i_chronology_root_tr.lang_isocode" ON "chronology_root_tr" ("lang_isocode");
CREATE INDEX "i_chronology_root_tr.root_chronology_id" ON "chronology_root_tr" ("root_chronology_id");
