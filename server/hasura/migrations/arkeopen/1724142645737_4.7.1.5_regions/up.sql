CREATE TABLE "region" (
    "geonameid" integer NOT NULL,
    "country_geonameid" integer NOT NULL,
    "geom_centroid" geography(Point,4326) NOT NULL,
    "created_at" timestamp with time zone NOT NULL,
    "updated_at" timestamp with time zone NOT NULL
);
CREATE TABLE "region_tr" (
    "region_geonameid" integer NOT NULL,
    "lang_isocode" character(2) NOT NULL,
    "name" character varying(255) NOT NULL,
    "name_ascii" character varying(255) NOT NULL
);
CREATE TABLE "database__region" (
    "database_id" integer NOT NULL,
    "region_geonameid" integer NOT NULL
);
ALTER TABLE ONLY "region"
    ADD CONSTRAINT "region_pkey" PRIMARY KEY ("geonameid");
ALTER TABLE ONLY "region_tr"
    ADD CONSTRAINT "region_tr_pkey" PRIMARY KEY ("region_geonameid", "lang_isocode");
ALTER TABLE ONLY "database__region"
    ADD CONSTRAINT "database__region_pkey" PRIMARY KEY ("database_id", "region_geonameid");
ALTER TABLE ONLY "region"
    ADD CONSTRAINT "c_region_country_geonameid" FOREIGN KEY ("country_geonameid") REFERENCES "country" ("geonameid") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY "region_tr"
    ADD CONSTRAINT "c_region_tr_region_geonameid" FOREIGN KEY ("region_geonameid") REFERENCES "region" ("geonameid") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY "region_tr"
    ADD CONSTRAINT "c_region_tr_lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY "database__region"
    ADD CONSTRAINT "c_database__region_region_geonameid" FOREIGN KEY ("region_geonameid") REFERENCES "region" ("geonameid") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY "database__region"
    ADD CONSTRAINT "c_database__region_database_id" FOREIGN KEY ("database_id") REFERENCES "database" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "i_region_country_geonameid" ON "region" USING btree ("country_geonameid");
CREATE INDEX "i_region_tr_region_geonameid" ON "region_tr" USING btree ("region_geonameid");
CREATE INDEX "i_region_tr_lang_isocode" ON "region_tr" USING btree ("lang_isocode");
CREATE INDEX "i_region_tr_name" ON "region_tr" USING btree ("name");
CREATE INDEX "i_region_tr_name_ascii" ON "region_tr" USING btree ("name_ascii");
CREATE INDEX "i_database__region_region_geonameid" ON "database__region" USING btree ("region_geonameid");
CREATE INDEX "i_database__region_database_id" ON "database__region" USING btree ("database_id");

ALTER TABLE ONLY "continent" ADD CONSTRAINT "c_continent_uniq_isocode" UNIQUE ("iso_code");
ALTER TABLE ONLY "country" ADD CONSTRAINT "c_country_uniq_isocode" UNIQUE ("iso_code");
