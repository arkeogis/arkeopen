ALTER TABLE ONLY public.continent_tr
    DROP CONSTRAINT IF EXISTS "c_continent_tr.continent_geonameid";
ALTER TABLE ONLY public.continent_tr
    ADD CONSTRAINT "c_continent_tr.continent_geonameid" FOREIGN KEY (continent_geonameid) REFERENCES public.continent(geonameid) DEFERRABLE INITIALLY DEFERRED;
