SET check_function_bodies = false;
CREATE TYPE public.database_context_context AS ENUM (
    'undefined',
    'academic-work',
    'contract',
    'research_team',
    'other'
);
CREATE TYPE public.database_geographical_extent AS ENUM (
    'undefined',
    'country',
    'continent',
    'international_waters',
    'world'
);
CREATE TYPE public.database_scale_resolution AS ENUM (
    'undefined',
    'object',
    'site',
    'watershed',
    'micro-region',
    'region',
    'country',
    'continent',
    'world'
);
CREATE TYPE public.database_state AS ENUM (
    'undefined',
    'in-progress',
    'finished'
);
CREATE TYPE public.database_type AS ENUM (
    'undefined',
    'inventory',
    'research',
    'literary-work'
);
CREATE TYPE public.group_type AS ENUM (
    'user',
    'chronology',
    'charac'
);
CREATE TYPE public.map_layer_type AS ENUM (
    'wms',
    'wmts'
);
CREATE TYPE public.site_occupation AS ENUM (
    'not_documented',
    'single',
    'continuous',
    'multiple'
);
CREATE TYPE public.site_range__charac_knowledge_type AS ENUM (
    'not_documented',
    'literature',
    'prospected_aerial',
    'prospected_pedestrian',
    'surveyed',
    'dig'
);
CREATE TABLE public.charac (
    id integer NOT NULL,
    parent_id integer NOT NULL,
    "order" integer NOT NULL,
    author_user_id integer NOT NULL,
    ark_id character varying(255) NOT NULL,
    pactols_id character varying(255) NOT NULL,
    aat_id character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
CREATE SEQUENCE public.charac_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.charac_id_seq OWNED BY public.charac.id;
CREATE TABLE public.charac_root (
    root_charac_id integer NOT NULL,
    admin_group_id integer NOT NULL,
    cached_langs text NOT NULL
);
CREATE TABLE public.charac_tr (
    lang_isocode character(2) NOT NULL,
    charac_id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text NOT NULL
);
CREATE TABLE public.chronology (
    id integer NOT NULL,
    parent_id integer NOT NULL,
    start_date integer NOT NULL,
    end_date integer NOT NULL,
    color character varying(6) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
CREATE SEQUENCE public.chronology_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.chronology_id_seq OWNED BY public.chronology.id;
CREATE TABLE public.chronology_root (
    root_chronology_id integer NOT NULL,
    admin_group_id integer NOT NULL,
    author_user_id integer NOT NULL,
    credits character varying NOT NULL,
    active boolean NOT NULL,
    geom public.geography(Polygon,4326) NOT NULL,
    cached_langs text NOT NULL
);
CREATE TABLE public.chronology_tr (
    lang_isocode character(2) NOT NULL,
    chronology_id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text NOT NULL
);
CREATE TABLE public.city (
    geonameid integer NOT NULL,
    country_geonameid integer NOT NULL,
    geom public.geography(Polygon,4326),
    geom_centroid public.geography(Point,4326) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
CREATE TABLE public.city_tr (
    city_geonameid integer NOT NULL,
    lang_isocode character(2) NOT NULL,
    name character varying(255) NOT NULL,
    name_ascii character varying(255) NOT NULL
);
CREATE TABLE public.company (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    city_geonameid integer NOT NULL
);
CREATE SEQUENCE public.company_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.company_id_seq OWNED BY public.company.id;
CREATE TABLE public.continent (
    geonameid integer NOT NULL,
    iso_code character varying(2) NOT NULL,
    geom public.geography(Polygon,4326),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
CREATE SEQUENCE public.continent_geonameid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.continent_geonameid_seq OWNED BY public.continent.geonameid;
CREATE TABLE public.continent_tr (
    continent_geonameid integer NOT NULL,
    lang_isocode character(2) NOT NULL,
    name character varying(255) NOT NULL,
    name_ascii character varying(255) NOT NULL
);
CREATE SEQUENCE public.continent_tr_continent_geonameid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.continent_tr_continent_geonameid_seq OWNED BY public.continent_tr.continent_geonameid;
CREATE TABLE public.country (
    geonameid integer NOT NULL,
    iso_code character varying(2),
    geom public.geography(Polygon,4326),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
CREATE TABLE public.country_tr (
    country_geonameid integer NOT NULL,
    lang_isocode character(2) NOT NULL,
    name character varying(128) NOT NULL,
    name_ascii character varying(255) NOT NULL
);
CREATE TABLE public.database (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    scale_resolution public.database_scale_resolution NOT NULL,
    geographical_extent public.database_geographical_extent NOT NULL,
    type public.database_type NOT NULL,
    owner integer NOT NULL,
    editor text NOT NULL,
    editor_url character varying(255) NOT NULL,
    contributor text NOT NULL,
    default_language character(2) NOT NULL,
    state public.database_state NOT NULL,
    license_id integer NOT NULL,
    published boolean NOT NULL,
    soft_deleted boolean NOT NULL,
    geographical_extent_geom public.geography(Polygon,4326) NOT NULL,
    start_date integer NOT NULL,
    end_date integer NOT NULL,
    declared_creation_date timestamp with time zone NOT NULL,
    public boolean NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
CREATE TABLE public.database__authors (
    database_id integer NOT NULL,
    user_id integer NOT NULL
);
CREATE TABLE public.database__continent (
    database_id integer NOT NULL,
    continent_geonameid integer NOT NULL
);
CREATE TABLE public.database__country (
    database_id integer NOT NULL,
    country_geonameid integer NOT NULL
);
CREATE TABLE public.database_context (
    id integer NOT NULL,
    database_id integer NOT NULL,
    context public.database_context_context NOT NULL
);
CREATE SEQUENCE public.database_context_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.database_context_id_seq OWNED BY public.database_context.id;
CREATE TABLE public.database_handle (
    id integer NOT NULL,
    database_id integer NOT NULL,
    import_id integer NOT NULL,
    identifier character varying(255) NOT NULL,
    url character varying(255) NOT NULL,
    declared_creation_date timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL
);
CREATE SEQUENCE public.database_handle_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.database_handle_id_seq OWNED BY public.database_handle.id;
CREATE SEQUENCE public.database_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.database_id_seq OWNED BY public.database.id;
CREATE TABLE public.database_tr (
    database_id integer NOT NULL,
    lang_isocode character(2) NOT NULL,
    description text NOT NULL,
    geographical_limit text NOT NULL,
    bibliography text NOT NULL,
    context_description text NOT NULL,
    source_description character varying NOT NULL,
    source_relation text NOT NULL,
    copyright text NOT NULL,
    subject text NOT NULL,
    re_use text NOT NULL
);
CREATE TABLE public."group" (
    id integer NOT NULL,
    type public.group_type NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
CREATE TABLE public.group__permission (
    group_id integer NOT NULL,
    permission_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
CREATE SEQUENCE public.group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.group_id_seq OWNED BY public."group".id;
CREATE TABLE public.group_tr (
    group_id integer NOT NULL,
    lang_isocode character(2) NOT NULL,
    name character varying(255) NOT NULL,
    description text NOT NULL
);
CREATE TABLE public.import (
    id integer NOT NULL,
    database_id integer NOT NULL,
    user_id integer NOT NULL,
    md5sum character varying(32) NOT NULL,
    filename character varying(255) NOT NULL,
    number_of_lines integer NOT NULL,
    number_of_sites integer NOT NULL,
    created_at timestamp with time zone NOT NULL
);
CREATE SEQUENCE public.import_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.import_id_seq OWNED BY public.import.id;
CREATE TABLE public.lang (
    isocode character(2) NOT NULL,
    active boolean NOT NULL
);
CREATE TABLE public.lang_tr (
    lang_isocode character(2) NOT NULL,
    lang_isocode_tr character(2) NOT NULL,
    name character varying(255) NOT NULL
);
CREATE TABLE public.license (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    url character varying NOT NULL
);
CREATE SEQUENCE public.license_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.license_id_seq OWNED BY public.license.id;
CREATE TABLE public.map_layer (
    id integer NOT NULL,
    creator_user_id integer NOT NULL,
    type public.map_layer_type NOT NULL,
    url character varying(255) NOT NULL,
    identifier character varying(255) NOT NULL,
    min_scale integer NOT NULL,
    max_scale integer NOT NULL,
    start_date integer NOT NULL,
    end_date integer NOT NULL,
    image_format character varying(25) NOT NULL,
    geographical_extent_geom public.geography(Polygon,4326) NOT NULL,
    published boolean NOT NULL,
    license character varying(255) NOT NULL,
    license_id integer NOT NULL,
    max_usage_date timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    tile_matrix_set character varying(255) NOT NULL,
    tile_matrix_string text NOT NULL,
    use_proxy boolean NOT NULL
);
CREATE TABLE public.map_layer__authors (
    user_id integer NOT NULL,
    map_layer_id integer NOT NULL
);
CREATE SEQUENCE public.map_layer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.map_layer_id_seq OWNED BY public.map_layer.id;
CREATE TABLE public.map_layer_tr (
    map_layer_id integer NOT NULL,
    lang_isocode character(2) NOT NULL,
    name character varying(255) NOT NULL,
    attribution text NOT NULL,
    copyright text NOT NULL,
    description text NOT NULL
);
CREATE TABLE public.permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
CREATE SEQUENCE public.permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.permission_id_seq OWNED BY public.permission.id;
CREATE TABLE public.permission_tr (
    permission_id integer NOT NULL,
    lang_isocode character(2) NOT NULL,
    name character varying(255) NOT NULL,
    description text NOT NULL
);
CREATE TABLE public.photo (
    id integer NOT NULL,
    photo bytea NOT NULL
);
CREATE SEQUENCE public.photo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.photo_id_seq OWNED BY public.photo.id;
CREATE TABLE public.project (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    start_date integer NOT NULL,
    end_date integer NOT NULL,
    geom public.geography(Polygon,4326) NOT NULL
);
CREATE TABLE public.project__charac (
    project_id integer NOT NULL,
    root_charac_id integer NOT NULL
);
CREATE SEQUENCE public.project__charac_project_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.project__charac_project_id_seq OWNED BY public.project__charac.project_id;
CREATE TABLE public.project__chronology (
    project_id integer NOT NULL,
    root_chronology_id integer NOT NULL
);
CREATE TABLE public.project__database (
    project_id integer NOT NULL,
    database_id integer NOT NULL
);
CREATE TABLE public.project__map_layer (
    project_id integer NOT NULL,
    map_layer_id integer NOT NULL
);
CREATE TABLE public.project__shapefile (
    project_id integer NOT NULL,
    shapefile_id integer NOT NULL
);
CREATE TABLE public.project_hidden_characs (
    project_id integer NOT NULL,
    charac_id integer NOT NULL
);
CREATE SEQUENCE public.project_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.project_id_seq OWNED BY public.project.id;
CREATE TABLE public.saved_query (
    project_id integer NOT NULL,
    name character varying(255) NOT NULL,
    params text NOT NULL
);
CREATE TABLE public.session (
    token character(42) NOT NULL,
    value bytea NOT NULL
);
CREATE TABLE public.shapefile (
    id integer NOT NULL,
    creator_user_id integer NOT NULL,
    filename character varying(255) NOT NULL,
    md5sum character varying(32) NOT NULL,
    geojson text NOT NULL,
    geojson_with_data text NOT NULL,
    start_date integer NOT NULL,
    end_date integer NOT NULL,
    geographical_extent_geom public.geography(Polygon,4326) NOT NULL,
    published boolean NOT NULL,
    license character varying(255) NOT NULL,
    license_id integer NOT NULL,
    declared_creation_date timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
CREATE TABLE public.shapefile__authors (
    user_id integer NOT NULL,
    shapefile_id integer NOT NULL
);
CREATE SEQUENCE public.shapefile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.shapefile_id_seq OWNED BY public.shapefile.id;
CREATE TABLE public.shapefile_tr (
    shapefile_id integer NOT NULL,
    lang_isocode character(2) NOT NULL,
    name character varying(255) NOT NULL,
    attribution character varying(255) NOT NULL,
    copyright text NOT NULL,
    description text NOT NULL
);
CREATE TABLE public.site (
    id integer NOT NULL,
    code character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    city_name character varying(255) NOT NULL,
    city_geonameid integer NOT NULL,
    geom public.geography(Point,4326) NOT NULL,
    geom_3d public.geography(PointZ,4326) NOT NULL,
    centroid boolean NOT NULL,
    occupation public.site_occupation NOT NULL,
    database_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    altitude double precision NOT NULL,
    start_date1 integer NOT NULL,
    start_date2 integer NOT NULL,
    end_date1 integer NOT NULL,
    end_date2 integer NOT NULL
);
CREATE SEQUENCE public.site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.site_id_seq OWNED BY public.site.id;
CREATE TABLE public.site_range (
    id integer NOT NULL,
    site_id integer NOT NULL,
    start_date1 integer NOT NULL,
    start_date2 integer NOT NULL,
    end_date1 integer NOT NULL,
    end_date2 integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
CREATE TABLE public.site_range__charac (
    id integer NOT NULL,
    site_range_id integer NOT NULL,
    charac_id integer NOT NULL,
    exceptional boolean NOT NULL,
    knowledge_type public.site_range__charac_knowledge_type NOT NULL
);
CREATE SEQUENCE public.site_range__charac_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.site_range__charac_id_seq OWNED BY public.site_range__charac.id;
CREATE TABLE public.site_range__charac_tr (
    site_range__charac_id integer NOT NULL,
    lang_isocode character(2) NOT NULL,
    comment text NOT NULL,
    bibliography text NOT NULL
);
CREATE SEQUENCE public.site_range_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.site_range_id_seq OWNED BY public.site_range.id;
CREATE TABLE public.site_tr (
    site_id integer NOT NULL,
    lang_isocode character(2) NOT NULL,
    description text NOT NULL
);
CREATE TABLE public."user" (
    id integer NOT NULL,
    username character varying(32) NOT NULL,
    firstname character varying(32) NOT NULL,
    lastname character varying(32) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(256) NOT NULL,
    description character varying NOT NULL,
    active boolean NOT NULL,
    first_lang_isocode character(2) NOT NULL,
    second_lang_isocode character(2) NOT NULL,
    city_geonameid integer NOT NULL,
    photo_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
CREATE TABLE public.user__company (
    user_id integer NOT NULL,
    company_id integer NOT NULL
);
CREATE TABLE public.user__group (
    group_id integer NOT NULL,
    user_id integer NOT NULL
);
CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;
CREATE TABLE public.user_preferences (
    user_id integer NOT NULL,
    key character varying(32) NOT NULL,
    value bytea NOT NULL
);
ALTER TABLE ONLY public.charac ALTER COLUMN id SET DEFAULT nextval('public.charac_id_seq'::regclass);
ALTER TABLE ONLY public.chronology ALTER COLUMN id SET DEFAULT nextval('public.chronology_id_seq'::regclass);
ALTER TABLE ONLY public.company ALTER COLUMN id SET DEFAULT nextval('public.company_id_seq'::regclass);
ALTER TABLE ONLY public.continent ALTER COLUMN geonameid SET DEFAULT nextval('public.continent_geonameid_seq'::regclass);
ALTER TABLE ONLY public.continent_tr ALTER COLUMN continent_geonameid SET DEFAULT nextval('public.continent_tr_continent_geonameid_seq'::regclass);
ALTER TABLE ONLY public.database ALTER COLUMN id SET DEFAULT nextval('public.database_id_seq'::regclass);
ALTER TABLE ONLY public.database_context ALTER COLUMN id SET DEFAULT nextval('public.database_context_id_seq'::regclass);
ALTER TABLE ONLY public.database_handle ALTER COLUMN id SET DEFAULT nextval('public.database_handle_id_seq'::regclass);
ALTER TABLE ONLY public."group" ALTER COLUMN id SET DEFAULT nextval('public.group_id_seq'::regclass);
ALTER TABLE ONLY public.import ALTER COLUMN id SET DEFAULT nextval('public.import_id_seq'::regclass);
ALTER TABLE ONLY public.license ALTER COLUMN id SET DEFAULT nextval('public.license_id_seq'::regclass);
ALTER TABLE ONLY public.map_layer ALTER COLUMN id SET DEFAULT nextval('public.map_layer_id_seq'::regclass);
ALTER TABLE ONLY public.permission ALTER COLUMN id SET DEFAULT nextval('public.permission_id_seq'::regclass);
ALTER TABLE ONLY public.photo ALTER COLUMN id SET DEFAULT nextval('public.photo_id_seq'::regclass);
ALTER TABLE ONLY public.project ALTER COLUMN id SET DEFAULT nextval('public.project_id_seq'::regclass);
ALTER TABLE ONLY public.project__charac ALTER COLUMN project_id SET DEFAULT nextval('public.project__charac_project_id_seq'::regclass);
ALTER TABLE ONLY public.shapefile ALTER COLUMN id SET DEFAULT nextval('public.shapefile_id_seq'::regclass);
ALTER TABLE ONLY public.site ALTER COLUMN id SET DEFAULT nextval('public.site_id_seq'::regclass);
ALTER TABLE ONLY public.site_range ALTER COLUMN id SET DEFAULT nextval('public.site_range_id_seq'::regclass);
ALTER TABLE ONLY public.site_range__charac ALTER COLUMN id SET DEFAULT nextval('public.site_range__charac_id_seq'::regclass);
ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);
ALTER TABLE ONLY public.charac
    ADD CONSTRAINT charac_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.charac_root
    ADD CONSTRAINT charac_root_pkey PRIMARY KEY (root_charac_id);
ALTER TABLE ONLY public.charac_tr
    ADD CONSTRAINT charac_tr_pkey PRIMARY KEY (charac_id, lang_isocode);
ALTER TABLE ONLY public.chronology
    ADD CONSTRAINT chronology_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.chronology_root
    ADD CONSTRAINT chronology_root_pkey PRIMARY KEY (root_chronology_id);
ALTER TABLE ONLY public.chronology_tr
    ADD CONSTRAINT chronology_tr_pkey PRIMARY KEY (chronology_id, lang_isocode);
ALTER TABLE ONLY public.city
    ADD CONSTRAINT city_pkey PRIMARY KEY (geonameid);
ALTER TABLE ONLY public.city_tr
    ADD CONSTRAINT city_tr_pkey PRIMARY KEY (city_geonameid, lang_isocode);
ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.continent
    ADD CONSTRAINT continent_pkey PRIMARY KEY (geonameid);
ALTER TABLE ONLY public.continent_tr
    ADD CONSTRAINT continent_tr_pkey PRIMARY KEY (continent_geonameid, lang_isocode);
ALTER TABLE ONLY public.country
    ADD CONSTRAINT country_pkey PRIMARY KEY (geonameid);
ALTER TABLE ONLY public.country_tr
    ADD CONSTRAINT country_tr_pkey PRIMARY KEY (country_geonameid, lang_isocode);
ALTER TABLE ONLY public.database__authors
    ADD CONSTRAINT database__authors_pkey PRIMARY KEY (database_id, user_id);
ALTER TABLE ONLY public.database__continent
    ADD CONSTRAINT database__continent_pkey PRIMARY KEY (database_id, continent_geonameid);
ALTER TABLE ONLY public.database__country
    ADD CONSTRAINT database__country_pkey PRIMARY KEY (database_id, country_geonameid);
ALTER TABLE ONLY public.database_context
    ADD CONSTRAINT database_context_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.database_handle
    ADD CONSTRAINT database_handle_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.database
    ADD CONSTRAINT database_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.database_tr
    ADD CONSTRAINT database_tr_pkey PRIMARY KEY (database_id, lang_isocode);
ALTER TABLE ONLY public.group__permission
    ADD CONSTRAINT group__permission_pkey PRIMARY KEY (group_id, permission_id);
ALTER TABLE ONLY public."group"
    ADD CONSTRAINT group_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.group_tr
    ADD CONSTRAINT group_tr_pkey PRIMARY KEY (group_id, lang_isocode);
ALTER TABLE ONLY public.import
    ADD CONSTRAINT import_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.lang
    ADD CONSTRAINT lang_pkey PRIMARY KEY (isocode);
ALTER TABLE ONLY public.lang_tr
    ADD CONSTRAINT lang_tr_pkey PRIMARY KEY (lang_isocode, lang_isocode_tr);
ALTER TABLE ONLY public.license
    ADD CONSTRAINT license_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.map_layer__authors
    ADD CONSTRAINT map_layer__authors_pkey PRIMARY KEY (user_id, map_layer_id);
ALTER TABLE ONLY public.map_layer
    ADD CONSTRAINT map_layer_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.map_layer_tr
    ADD CONSTRAINT map_layer_tr_pkey PRIMARY KEY (map_layer_id, lang_isocode);
ALTER TABLE ONLY public.permission
    ADD CONSTRAINT permission_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.permission_tr
    ADD CONSTRAINT permission_tr_pkey PRIMARY KEY (permission_id, lang_isocode);
ALTER TABLE ONLY public.photo
    ADD CONSTRAINT photo_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.project__charac
    ADD CONSTRAINT project__charac_pkey PRIMARY KEY (project_id, root_charac_id);
ALTER TABLE ONLY public.project__chronology
    ADD CONSTRAINT project__chronology_pkey PRIMARY KEY (project_id, root_chronology_id);
ALTER TABLE ONLY public.project__database
    ADD CONSTRAINT project__database_pkey PRIMARY KEY (project_id, database_id);
ALTER TABLE ONLY public.project__map_layer
    ADD CONSTRAINT project__map_layer_pkey PRIMARY KEY (project_id, map_layer_id);
ALTER TABLE ONLY public.project__shapefile
    ADD CONSTRAINT project__shapefile_pkey PRIMARY KEY (project_id, shapefile_id);
ALTER TABLE ONLY public.project_hidden_characs
    ADD CONSTRAINT project_hidden_characs_pkey PRIMARY KEY (charac_id, project_id);
ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.saved_query
    ADD CONSTRAINT saved_query_pkey PRIMARY KEY (project_id, name);
ALTER TABLE ONLY public.session
    ADD CONSTRAINT session_pkey PRIMARY KEY (token);
ALTER TABLE ONLY public.shapefile__authors
    ADD CONSTRAINT shapefile__authors_pkey PRIMARY KEY (shapefile_id, user_id);
ALTER TABLE ONLY public.shapefile
    ADD CONSTRAINT shapefile_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.shapefile_tr
    ADD CONSTRAINT shapefile_tr_pkey PRIMARY KEY (shapefile_id, lang_isocode);
ALTER TABLE ONLY public.site
    ADD CONSTRAINT site_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.site_range__charac
    ADD CONSTRAINT site_range__charac_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.site_range__charac_tr
    ADD CONSTRAINT site_range__charac_tr_pkey PRIMARY KEY (site_range__charac_id, lang_isocode);
ALTER TABLE ONLY public.site_range
    ADD CONSTRAINT site_range_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.site_tr
    ADD CONSTRAINT site_tr_pkey PRIMARY KEY (site_id, lang_isocode);
ALTER TABLE ONLY public.user__company
    ADD CONSTRAINT user__company_pkey PRIMARY KEY (user_id, company_id);
ALTER TABLE ONLY public.user__group
    ADD CONSTRAINT user__group_pkey PRIMARY KEY (group_id, user_id);
ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.user_preferences
    ADD CONSTRAINT user_preferences_pkey PRIMARY KEY (user_id, key);
CREATE INDEX "i_charac.author_user_id" ON public.charac USING btree (author_user_id);
CREATE INDEX "i_charac.parent_id" ON public.charac USING btree (parent_id);
CREATE INDEX "i_charac_root.admin_group_id" ON public.charac_root USING btree (admin_group_id);
CREATE INDEX "i_charac_root.root_charac_id" ON public.charac_root USING btree (root_charac_id);
CREATE INDEX "i_charac_tr.charac_id" ON public.charac_tr USING btree (charac_id);
CREATE INDEX "i_charac_tr.lang_isocode" ON public.charac_tr USING btree (lang_isocode);
CREATE INDEX "i_chronology.parent_id" ON public.chronology USING btree (parent_id);
CREATE INDEX "i_chronology_root.admin_group_id" ON public.chronology_root USING btree (admin_group_id);
CREATE INDEX "i_chronology_root.author_user_id" ON public.chronology_root USING btree (author_user_id);
CREATE INDEX "i_chronology_root.root_chronology_id" ON public.chronology_root USING btree (root_chronology_id);
CREATE INDEX "i_chronology_tr.chronology_id" ON public.chronology_tr USING btree (chronology_id);
CREATE INDEX "i_chronology_tr.lang_isocode" ON public.chronology_tr USING btree (lang_isocode);
CREATE INDEX "i_city.country_geonameid" ON public.city USING btree (country_geonameid);
CREATE INDEX "i_city_tr.city_geonameid" ON public.city_tr USING btree (city_geonameid);
CREATE INDEX "i_city_tr.lang_isocode" ON public.city_tr USING btree (lang_isocode);
CREATE INDEX "i_city_tr.name" ON public.city_tr USING btree (name);
CREATE INDEX "i_city_tr.name_ascii" ON public.city_tr USING btree (name_ascii);
CREATE INDEX "i_company.city_geonameid" ON public.company USING btree (city_geonameid);
CREATE INDEX "i_continent.iso_code" ON public.continent USING btree (iso_code);
CREATE INDEX "i_continent_tr.lang_isocode" ON public.continent_tr USING btree (lang_isocode);
CREATE INDEX "i_continent_tr.name,name_ascii" ON public.continent_tr USING btree (name, name_ascii);
CREATE INDEX "i_country.iso_code" ON public.country USING btree (iso_code);
CREATE INDEX "i_country_tr.country_geonameid" ON public.country_tr USING btree (country_geonameid);
CREATE INDEX "i_country_tr.lang_isocode" ON public.country_tr USING btree (lang_isocode);
CREATE INDEX "i_country_tr.name" ON public.country_tr USING btree (name);
CREATE INDEX "i_country_tr.name_ascii" ON public.country_tr USING btree (name_ascii);
CREATE INDEX "i_database.default_language" ON public.database USING btree (default_language);
CREATE INDEX "i_database.license_id" ON public.database USING btree (license_id);
CREATE INDEX "i_database.owner" ON public.database USING btree (owner);
CREATE INDEX "i_database__authors.database_id" ON public.database__authors USING btree (database_id);
CREATE INDEX "i_database__authors.user_id" ON public.database__authors USING btree (user_id);
CREATE INDEX "i_database__continent.continent_geonameid" ON public.database__continent USING btree (continent_geonameid);
CREATE INDEX "i_database__continent.database_id" ON public.database__continent USING btree (database_id);
CREATE INDEX "i_database__country.country_geonameid" ON public.database__country USING btree (country_geonameid);
CREATE INDEX "i_database__country.database_id" ON public.database__country USING btree (database_id);
CREATE INDEX "i_database_context.database_id" ON public.database_context USING btree (database_id);
CREATE INDEX "i_database_handle.database_id" ON public.database_handle USING btree (database_id);
CREATE INDEX "i_database_handle.import_id" ON public.database_handle USING btree (import_id);
CREATE INDEX "i_database_tr.database_id" ON public.database_tr USING btree (database_id);
CREATE INDEX "i_database_tr.lang_isocode" ON public.database_tr USING btree (lang_isocode);
CREATE INDEX "i_group__permission.group_id" ON public.group__permission USING btree (group_id);
CREATE INDEX "i_group__permission.permission_id" ON public.group__permission USING btree (permission_id);
CREATE INDEX "i_group_tr.group_id" ON public.group_tr USING btree (group_id);
CREATE INDEX "i_group_tr.lang_isocode" ON public.group_tr USING btree (lang_isocode);
CREATE INDEX "i_import.database_id" ON public.import USING btree (database_id);
CREATE INDEX "i_import.user_id" ON public.import USING btree (user_id);
CREATE INDEX "i_lang_tr.lang_isocode" ON public.lang_tr USING btree (lang_isocode);
CREATE INDEX "i_lang_tr.lang_isocode_tr" ON public.lang_tr USING btree (lang_isocode_tr);
CREATE INDEX "i_map_layer.creator_user_id" ON public.map_layer USING btree (creator_user_id);
CREATE INDEX "i_map_layer.license_id" ON public.map_layer USING btree (license_id);
CREATE INDEX "i_map_layer__authors.map_layer_id" ON public.map_layer__authors USING btree (map_layer_id);
CREATE INDEX "i_map_layer__authors.user_id" ON public.map_layer__authors USING btree (user_id);
CREATE INDEX "i_map_layer_tr.lang_isocode" ON public.map_layer_tr USING btree (lang_isocode);
CREATE INDEX "i_map_layer_tr.map_layer_id" ON public.map_layer_tr USING btree (map_layer_id);
CREATE INDEX "i_permission_tr.lang_isocode" ON public.permission_tr USING btree (lang_isocode);
CREATE INDEX "i_permission_tr.permission_id" ON public.permission_tr USING btree (permission_id);
CREATE INDEX "i_project.user_id" ON public.project USING btree (user_id);
CREATE INDEX "i_project__charac.project_id" ON public.project__charac USING btree (project_id);
CREATE INDEX "i_project__charac.root_charac_id" ON public.project__charac USING btree (root_charac_id);
CREATE INDEX "i_project__chronology.project_id" ON public.project__chronology USING btree (project_id);
CREATE INDEX "i_project__chronology.root_chronology_id" ON public.project__chronology USING btree (root_chronology_id);
CREATE INDEX "i_project__database.database_id" ON public.project__database USING btree (database_id);
CREATE INDEX "i_project__database.project_id" ON public.project__database USING btree (project_id);
CREATE INDEX "i_project__map_layer.map_layer_id" ON public.project__map_layer USING btree (map_layer_id);
CREATE INDEX "i_project__map_layer.project_id" ON public.project__map_layer USING btree (project_id);
CREATE INDEX "i_project__shapefile.project_id" ON public.project__shapefile USING btree (project_id);
CREATE INDEX "i_project__shapefile.shapefile_id" ON public.project__shapefile USING btree (shapefile_id);
CREATE INDEX "i_project_hidden_characs.charac_id" ON public.project_hidden_characs USING btree (charac_id);
CREATE INDEX "i_project_hidden_characs.project_id" ON public.project_hidden_characs USING btree (project_id);
CREATE INDEX "i_saved_query.project_id" ON public.saved_query USING btree (project_id);
CREATE INDEX "i_shapefile.creator_user_id" ON public.shapefile USING btree (creator_user_id);
CREATE INDEX "i_shapefile.license_id" ON public.shapefile USING btree (license_id);
CREATE INDEX "i_shapefile__authors.shapefile_id" ON public.shapefile__authors USING btree (shapefile_id);
CREATE INDEX "i_shapefile__authors.user_id" ON public.shapefile__authors USING btree (user_id);
CREATE INDEX "i_shapefile_tr.lang_isocode" ON public.shapefile_tr USING btree (lang_isocode);
CREATE INDEX "i_shapefile_tr.shapefile_id" ON public.shapefile_tr USING btree (shapefile_id);
CREATE INDEX "i_site.database_id" ON public.site USING btree (database_id);
CREATE INDEX "i_site.end_date1" ON public.site USING btree (end_date1);
CREATE INDEX "i_site.end_date2" ON public.site USING btree (end_date2);
CREATE INDEX "i_site.geom,geom_3d" ON public.site USING gist (geom, geom_3d);
CREATE INDEX "i_site.start_date1" ON public.site USING btree (start_date1);
CREATE INDEX "i_site.start_date2" ON public.site USING btree (start_date2);
CREATE INDEX "i_site_range.site_id" ON public.site_range USING btree (site_id);
CREATE INDEX "i_site_range__charac.charac_id" ON public.site_range__charac USING btree (charac_id);
CREATE INDEX "i_site_range__charac.site_range_id" ON public.site_range__charac USING btree (site_range_id);
CREATE INDEX "i_site_range__charac_tr.lang_isocode" ON public.site_range__charac_tr USING btree (lang_isocode);
CREATE INDEX "i_site_range__charac_tr.site_range__charac_id" ON public.site_range__charac_tr USING btree (site_range__charac_id);
CREATE INDEX "i_site_tr.lang_isocode" ON public.site_tr USING btree (lang_isocode);
CREATE INDEX "i_site_tr.site_id" ON public.site_tr USING btree (site_id);
CREATE INDEX "i_user.city_geonameid" ON public."user" USING btree (city_geonameid);
CREATE INDEX "i_user.first_lang_isocode" ON public."user" USING btree (first_lang_isocode);
CREATE INDEX "i_user.photo_id" ON public."user" USING btree (photo_id);
CREATE INDEX "i_user.second_lang_isocode" ON public."user" USING btree (second_lang_isocode);
CREATE INDEX "i_user__company.company_id" ON public.user__company USING btree (company_id);
CREATE INDEX "i_user__company.user_id" ON public.user__company USING btree (user_id);
CREATE INDEX "i_user__group.group_id" ON public.user__group USING btree (group_id);
CREATE INDEX "i_user__group.user_id" ON public.user__group USING btree (user_id);
CREATE INDEX "i_user_preferences.user_id" ON public.user_preferences USING btree (user_id);
ALTER TABLE ONLY public.charac
    ADD CONSTRAINT "c_charac.author_user_id" FOREIGN KEY (author_user_id) REFERENCES public."user"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.charac
    ADD CONSTRAINT "c_charac.parent_id" FOREIGN KEY (parent_id) REFERENCES public.charac(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.charac_root
    ADD CONSTRAINT "c_charac_root.admin_group_id" FOREIGN KEY (admin_group_id) REFERENCES public."group"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.charac_root
    ADD CONSTRAINT "c_charac_root.root_charac_id" FOREIGN KEY (root_charac_id) REFERENCES public.charac(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.charac_tr
    ADD CONSTRAINT "c_charac_tr.charac_id" FOREIGN KEY (charac_id) REFERENCES public.charac(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.charac_tr
    ADD CONSTRAINT "c_charac_tr.lang_isocode" FOREIGN KEY (lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.chronology
    ADD CONSTRAINT "c_chronology.parent_id" FOREIGN KEY (parent_id) REFERENCES public.chronology(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.chronology_root
    ADD CONSTRAINT "c_chronology_root.admin_group_id" FOREIGN KEY (admin_group_id) REFERENCES public."group"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.chronology_root
    ADD CONSTRAINT "c_chronology_root.author_user_id" FOREIGN KEY (author_user_id) REFERENCES public."user"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.chronology_root
    ADD CONSTRAINT "c_chronology_root.root_chronology_id" FOREIGN KEY (root_chronology_id) REFERENCES public.chronology(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.chronology_tr
    ADD CONSTRAINT "c_chronology_tr.chronology_id" FOREIGN KEY (chronology_id) REFERENCES public.chronology(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.chronology_tr
    ADD CONSTRAINT "c_chronology_tr.lang_isocode" FOREIGN KEY (lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.city
    ADD CONSTRAINT "c_city.country_geonameid" FOREIGN KEY (country_geonameid) REFERENCES public.country(geonameid) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.city_tr
    ADD CONSTRAINT "c_city_tr.city_geonameid" FOREIGN KEY (city_geonameid) REFERENCES public.city(geonameid) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.city_tr
    ADD CONSTRAINT "c_city_tr.lang_isocode" FOREIGN KEY (lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.company
    ADD CONSTRAINT "c_company.city_geonameid" FOREIGN KEY (city_geonameid) REFERENCES public.city(geonameid) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.continent_tr
    ADD CONSTRAINT "c_continent_tr.lang_isocode" FOREIGN KEY (lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.country_tr
    ADD CONSTRAINT "c_country_tr.country_geonameid" FOREIGN KEY (country_geonameid) REFERENCES public.country(geonameid) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.country_tr
    ADD CONSTRAINT "c_country_tr.lang_isocode" FOREIGN KEY (lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database
    ADD CONSTRAINT "c_database.default_language" FOREIGN KEY (default_language) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database
    ADD CONSTRAINT "c_database.license_id" FOREIGN KEY (license_id) REFERENCES public.license(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database
    ADD CONSTRAINT "c_database.owner" FOREIGN KEY (owner) REFERENCES public."user"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database__authors
    ADD CONSTRAINT "c_database__authors.database_id" FOREIGN KEY (database_id) REFERENCES public.database(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database__authors
    ADD CONSTRAINT "c_database__authors.user_id" FOREIGN KEY (user_id) REFERENCES public."user"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database__continent
    ADD CONSTRAINT "c_database__continent.continent_geonameid" FOREIGN KEY (continent_geonameid) REFERENCES public.continent(geonameid) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database__continent
    ADD CONSTRAINT "c_database__continent.database_id" FOREIGN KEY (database_id) REFERENCES public.database(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database__country
    ADD CONSTRAINT "c_database__country.country_geonameid" FOREIGN KEY (country_geonameid) REFERENCES public.country(geonameid) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database__country
    ADD CONSTRAINT "c_database__country.database_id" FOREIGN KEY (database_id) REFERENCES public.database(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database_context
    ADD CONSTRAINT "c_database_context.database_id" FOREIGN KEY (database_id) REFERENCES public.database(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database_handle
    ADD CONSTRAINT "c_database_handle.database_id" FOREIGN KEY (database_id) REFERENCES public.database(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database_handle
    ADD CONSTRAINT "c_database_handle.import_id" FOREIGN KEY (import_id) REFERENCES public.import(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database_tr
    ADD CONSTRAINT "c_database_tr.database_id" FOREIGN KEY (database_id) REFERENCES public.database(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.database_tr
    ADD CONSTRAINT "c_database_tr.lang_isocode" FOREIGN KEY (lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.group__permission
    ADD CONSTRAINT "c_group__permission.group_id" FOREIGN KEY (group_id) REFERENCES public."group"(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.group__permission
    ADD CONSTRAINT "c_group__permission.permission_id" FOREIGN KEY (permission_id) REFERENCES public.permission(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.group_tr
    ADD CONSTRAINT "c_group_tr.group_id" FOREIGN KEY (group_id) REFERENCES public."group"(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.group_tr
    ADD CONSTRAINT "c_group_tr.lang_isocode" FOREIGN KEY (lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.import
    ADD CONSTRAINT "c_import.database_id" FOREIGN KEY (database_id) REFERENCES public.database(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.import
    ADD CONSTRAINT "c_import.user_id" FOREIGN KEY (user_id) REFERENCES public."user"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.lang_tr
    ADD CONSTRAINT "c_lang_tr.lang_isocode" FOREIGN KEY (lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.lang_tr
    ADD CONSTRAINT "c_lang_tr.lang_isocode_tr" FOREIGN KEY (lang_isocode_tr) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.map_layer
    ADD CONSTRAINT "c_map_layer.creator_user_id" FOREIGN KEY (creator_user_id) REFERENCES public."user"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.map_layer
    ADD CONSTRAINT "c_map_layer.license_id" FOREIGN KEY (license_id) REFERENCES public.license(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.map_layer__authors
    ADD CONSTRAINT "c_map_layer__authors.map_layer_id" FOREIGN KEY (map_layer_id) REFERENCES public.map_layer(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.map_layer__authors
    ADD CONSTRAINT "c_map_layer__authors.user_id" FOREIGN KEY (user_id) REFERENCES public."user"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.map_layer_tr
    ADD CONSTRAINT "c_map_layer_tr.lang_isocode" FOREIGN KEY (lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.map_layer_tr
    ADD CONSTRAINT "c_map_layer_tr.map_layer_id" FOREIGN KEY (map_layer_id) REFERENCES public.map_layer(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.permission_tr
    ADD CONSTRAINT "c_permission_tr.lang_isocode" FOREIGN KEY (lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.permission_tr
    ADD CONSTRAINT "c_permission_tr.permission_id" FOREIGN KEY (permission_id) REFERENCES public.permission(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.project
    ADD CONSTRAINT "c_project.user_id" FOREIGN KEY (user_id) REFERENCES public."user"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.project__charac
    ADD CONSTRAINT "c_project__charac.project_id" FOREIGN KEY (project_id) REFERENCES public.project(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.project__charac
    ADD CONSTRAINT "c_project__charac.root_charac_id" FOREIGN KEY (root_charac_id) REFERENCES public.charac(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.project__chronology
    ADD CONSTRAINT "c_project__chronology.project_id" FOREIGN KEY (project_id) REFERENCES public.project(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.project__chronology
    ADD CONSTRAINT "c_project__chronology.root_chronology_id" FOREIGN KEY (root_chronology_id) REFERENCES public.chronology(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.project__database
    ADD CONSTRAINT "c_project__database.database_id" FOREIGN KEY (database_id) REFERENCES public.database(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.project__database
    ADD CONSTRAINT "c_project__database.project_id" FOREIGN KEY (project_id) REFERENCES public.project(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.project__map_layer
    ADD CONSTRAINT "c_project__map_layer.map_layer_id" FOREIGN KEY (map_layer_id) REFERENCES public.map_layer(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.project__map_layer
    ADD CONSTRAINT "c_project__map_layer.project_id" FOREIGN KEY (project_id) REFERENCES public.project(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.project__shapefile
    ADD CONSTRAINT "c_project__shapefile.project_id" FOREIGN KEY (project_id) REFERENCES public.project(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.project__shapefile
    ADD CONSTRAINT "c_project__shapefile.shapefile_id" FOREIGN KEY (shapefile_id) REFERENCES public.shapefile(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.project_hidden_characs
    ADD CONSTRAINT "c_project_hidden_characs.charac_id" FOREIGN KEY (charac_id) REFERENCES public.charac(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.project_hidden_characs
    ADD CONSTRAINT "c_project_hidden_characs.project_id" FOREIGN KEY (project_id) REFERENCES public.project(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.saved_query
    ADD CONSTRAINT "c_saved_query.project_id" FOREIGN KEY (project_id) REFERENCES public.project(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.shapefile
    ADD CONSTRAINT "c_shapefile.creator_user_id" FOREIGN KEY (creator_user_id) REFERENCES public."user"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.shapefile
    ADD CONSTRAINT "c_shapefile.license_id" FOREIGN KEY (license_id) REFERENCES public.license(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.shapefile__authors
    ADD CONSTRAINT "c_shapefile__authors.shapefile_id" FOREIGN KEY (shapefile_id) REFERENCES public.shapefile(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.shapefile__authors
    ADD CONSTRAINT "c_shapefile__authors.user_id" FOREIGN KEY (user_id) REFERENCES public."user"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.shapefile_tr
    ADD CONSTRAINT "c_shapefile_tr.lang_isocode" FOREIGN KEY (lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.shapefile_tr
    ADD CONSTRAINT "c_shapefile_tr.shapefile_id" FOREIGN KEY (shapefile_id) REFERENCES public.shapefile(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.site
    ADD CONSTRAINT "c_site.database_id" FOREIGN KEY (database_id) REFERENCES public.database(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.site_range
    ADD CONSTRAINT "c_site_range.site_id" FOREIGN KEY (site_id) REFERENCES public.site(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.site_range__charac
    ADD CONSTRAINT "c_site_range__charac.charac_id" FOREIGN KEY (charac_id) REFERENCES public.charac(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.site_range__charac
    ADD CONSTRAINT "c_site_range__charac.site_range_id" FOREIGN KEY (site_range_id) REFERENCES public.site_range(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.site_range__charac_tr
    ADD CONSTRAINT "c_site_range__charac_tr.lang_isocode" FOREIGN KEY (lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.site_range__charac_tr
    ADD CONSTRAINT "c_site_range__charac_tr.site_range__charac_id" FOREIGN KEY (site_range__charac_id) REFERENCES public.site_range__charac(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.site_tr
    ADD CONSTRAINT "c_site_tr.lang_isocode" FOREIGN KEY (lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.site_tr
    ADD CONSTRAINT "c_site_tr.site_id" FOREIGN KEY (site_id) REFERENCES public.site(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "c_user.city_geonameid" FOREIGN KEY (city_geonameid) REFERENCES public.city(geonameid) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "c_user.first_lang_isocode" FOREIGN KEY (first_lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "c_user.photo_id" FOREIGN KEY (photo_id) REFERENCES public.photo(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "c_user.second_lang_isocode" FOREIGN KEY (second_lang_isocode) REFERENCES public.lang(isocode) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.user__company
    ADD CONSTRAINT "c_user__company.company_id" FOREIGN KEY (company_id) REFERENCES public.company(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.user__company
    ADD CONSTRAINT "c_user__company.user_id" FOREIGN KEY (user_id) REFERENCES public."user"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.user__group
    ADD CONSTRAINT "c_user__group.group_id" FOREIGN KEY (group_id) REFERENCES public."group"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.user__group
    ADD CONSTRAINT "c_user__group.user_id" FOREIGN KEY (user_id) REFERENCES public."user"(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE ONLY public.user_preferences
    ADD CONSTRAINT "c_user_preferences.user_id" FOREIGN KEY (user_id) REFERENCES public."user"(id) DEFERRABLE INITIALLY DEFERRED;
