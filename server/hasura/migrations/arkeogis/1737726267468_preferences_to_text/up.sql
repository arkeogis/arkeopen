ALTER TABLE user_preferences ADD COLUMN value_text TEXT DEFAULT '';

UPDATE user_preferences SET value_text = convert_from(value, 'UTF8');

ALTER TABLE user_preferences DROP COLUMN value;

ALTER TABLE user_preferences RENAME COLUMN value_text TO value;

ALTER TABLE user_preferences ALTER COLUMN value SET NOT NULL;
ALTER TABLE user_preferences ALTER COLUMN value SET DEFAULT '';
