ALTER TABLE user_preferences ADD COLUMN value_bytea BYTEA;

UPDATE user_preferences SET value_bytea = encode(value::bytea, 'escape');

ALTER TABLE user_preferences DROP COLUMN value;

ALTER TABLE user_preferences RENAME COLUMN value_bytea TO value;

ALTER TABLE user_preferences ALTER COLUMN value DROP DEFAULT;
ALTER TABLE user_preferences ALTER COLUMN value DROP NOT NULL;
