ALTER TABLE "chronology_root_tr" DROP CONSTRAINT "c_chronology_root_tr.lang_isocode";

ALTER TABLE "database" DROP CONSTRAINT "c_database.root_chronology_id";

DELETE FROM "chronology_root" WHERE root_chronology_id=0;

DROP TABLE "chronology_root_tr";

ALTER TABLE "chronology_root" DROP COLUMN "deposit_uri";
ALTER TABLE "chronology_root" DROP COLUMN "editor_uri";
ALTER TABLE "chronology_root" DROP COLUMN "editor";
ALTER TABLE "chronology_root" DROP COLUMN "opened";

ALTER TABLE "shapefile_tr" DROP COLUMN "geographical_covering";
ALTER TABLE "shapefile_tr" RENAME COLUMN "citation" TO "copyright";

ALTER TABLE "shapefile" DROP COLUMN "deposit_uri";
ALTER TABLE "shapefile" DROP COLUMN "editor_uri";
ALTER TABLE "shapefile" DROP COLUMN "editor";
ALTER TABLE "shapefile" DROP COLUMN "opened";

ALTER TABLE "map_layer_tr" RENAME COLUMN "citation" TO "copyright";

ALTER TABLE "database" DROP COLUMN "root_chronology_id";
