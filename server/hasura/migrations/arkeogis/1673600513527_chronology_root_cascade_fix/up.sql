ALTER TABLE "database" DROP CONSTRAINT "c_database.root_chronology_id";
ALTER TABLE "database" ADD CONSTRAINT "c_database.root_chronology_id" FOREIGN KEY ("root_chronology_id") REFERENCES "chronology_root" ("root_chronology_id") DEFERRABLE INITIALLY DEFERRED;
