ALTER TABLE "user" ADD COLUMN orcid VARCHAR(255);

CREATE TABLE database_collection (
    id INTEGER PRIMARY KEY,
    name VARCHAR(75) NOT NULL
);
INSERT INTO database_collection (id, name) VALUES (0, 'ArkeoGIS');

CREATE TABLE chronology_collection (
    id INTEGER PRIMARY KEY,
    name VARCHAR(75) NOT NULL
);
INSERT INTO chronology_collection (id, name) VALUES (0, 'ArkeoGIS');

CREATE TABLE map_layer_collection (
    id INTEGER PRIMARY KEY,
    name VARCHAR(75) NOT NULL
);
INSERT INTO map_layer_collection (id, name) VALUES (0, 'ArkeoGIS');

ALTER TABLE database ADD COLUMN database_collection_id INTEGER NOT NULL DEFAULT 0;

ALTER TABLE database
ADD CONSTRAINT fk_database_collection
FOREIGN KEY (database_collection_id)
REFERENCES database_collection(id);

ALTER TABLE map_layer ADD COLUMN map_layer_collection_id INTEGER NOT NULL DEFAULT 0;

ALTER TABLE map_layer
ADD CONSTRAINT fk_map_layer_collection
FOREIGN KEY (map_layer_collection_id)
REFERENCES map_layer_collection(id);

ALTER TABLE chronology_root ADD COLUMN chronology_collection_id INTEGER NOT NULL DEFAULT 0;

ALTER TABLE chronology_root
ADD CONSTRAINT fk_chronology_collection
FOREIGN KEY (chronology_collection_id)
REFERENCES chronology_collection(id);
