ALTER TABLE "user" DROP COLUMN IF EXISTS orcid;

ALTER TABLE "database" DROP CONSTRAINT IF EXISTS fk_database_collection;

ALTER TABLE "database" DROP COLUMN IF EXISTS database_collection_id;

ALTER TABLE "map_layer" DROP CONSTRAINT IF EXISTS fk_map_layer_collection;

ALTER TABLE "map_layer" DROP COLUMN IF EXISTS map_layer_collection_id;

ALTER TABLE "chronology_root" DROP CONSTRAINT IF EXISTS fk_chronology_collection;

ALTER TABLE "chronology_root" DROP COLUMN IF EXISTS chronology_collection_id;

DROP TABLE IF EXISTS database_collection;

DROP TABLE IF EXISTS chronology_collection;

DROP TABLE IF EXISTS map_layer_collection;
