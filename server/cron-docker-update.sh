#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "$SCRIPT_DIR"

. .env

#DATE_FILTER=$(date --iso-8601=ns | sed 's/:.*$//g')

echo -n "" > docker-pull.log
echo -n "" > docker-up.log
echo -n "" > arkeopen-docker-upgrade-mail.txt

function myerr() {
  echo "ArkeOpen auto upgrade error while executing $1 command" > arkeopen-docker-upgrade-mail.txt
  echo "" >> arkeopen-docker-upgrade-mail.txt
  echo "docker-pull log :" >> arkeopen-docker-upgrade-mail.txt
  cat docker-pull.log >> arkeopen-docker-upgrade-mail.txt
  echo "" >> arkeopen-docker-upgrade-mail.txt
  echo "docker-up log :" >> arkeopen-docker-upgrade-mail.txt
  cat docker-up.log >> arkeopen-docker-upgrade-mail.txt
  mail ndimitrijevic -s "ArkeOpen auto upgrade error" < arkeopen-docker-upgrade-mail.txt
  exit
}

docker-compose pull &> docker-pull.log || myerr "docker-compose pull"

docker-compose up -d &> docker-up.log || myerr "docker-compose up"

grep "created" docker-up.log || exit

sleep 120
#hasura_started=$(docker logs arkeopendev-nico-hasura | grep '"starting API server"' | grep "$DATE_FILTER" | wc -l)
#echo "hasura_started ? $hasura_started"

version=$(wget -O - https://graphql.arkeopen.org/v1/version 2> /dev/null)

echo "ArkeOpen Hasura or PostgreSQL was upgraded" > arkeopen-docker-upgrade-mail.txt
echo "Hasura is now running on version $version" >> arkeopen-docker-upgrade-mail.txt
echo "PostgreSQL : " >> arkeopen-docker-upgrade-mail.txt
docker logs arkeopen-postgres 2>&1 | grep 'starting PostgreSQL' | tail -n 1 >> arkeopen-docker-upgrade-mail.txt
mail $DOCKERUPGRADEMAILTO -s "ArkeOpen upgraded" < arkeopen-docker-upgrade-mail.txt
