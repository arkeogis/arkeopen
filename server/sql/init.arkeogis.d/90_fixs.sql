drop index "i_user.username";

drop index "i_project__databases.database_id";
drop index "i_project__databases.project_id";

alter table "project__database" drop constraint "c_project__databases.database_id";
alter table "project__database" drop constraint "c_project__databases.project_id";

alter table "saved_query" drop constraint "saved_query_project_id_fkey";
