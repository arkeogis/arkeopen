CREATE USER user_arkeogis WITH PASSWORD 'arkeoP4SS';
CREATE DATABASE arkeogis;
GRANT ALL PRIVILEGES ON DATABASE arkeogis TO user_arkeogis;

\c arkeogis
