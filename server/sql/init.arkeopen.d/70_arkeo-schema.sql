create extension postgis;
create extension ltree;

CREATE TYPE database_scale_resolution AS ENUM('undefined','object','site','watershed','micro-region','region','country','continent','world');
CREATE TYPE database_geographical_extent AS ENUM('undefined','country','continent','international_waters','world');
CREATE TYPE database_type AS ENUM('undefined', 'inventory', 'research', 'literary-work' );
CREATE TYPE database_state AS ENUM('undefined','in-progress', 'finished');
CREATE TYPE site_occupation AS ENUM('not_documented', 'single', 'continuous', 'multiple');
CREATE TYPE map_layer_type AS ENUM('wms', 'wmts');
CREATE TYPE site_range__charac_knowledge_type AS ENUM('not_documented', 'literature', 'prospected_aerial', 'prospected_pedestrian', 'surveyed', 'dig');
CREATE TYPE group_type AS ENUM('user','chronology','charac');
CREATE TYPE database_context_context AS ENUM('undefined','academic-work','contract','research_team','other');

CREATE TABLE "user" (
  "id" serial NOT NULL,
  "username" VARCHAR(32) NOT NULL,
  "firstname" VARCHAR(32) NOT NULL,
  "lastname" VARCHAR(32) NOT NULL,
  "email" VARCHAR(255) NOT NULL,
  "password" VARCHAR(256) NOT NULL,
  "description" VARCHAR NOT NULL,
  "active" BOOLEAN NOT NULL,
  "first_lang_isocode" CHAR(2) NOT NULL,
  "second_lang_isocode" CHAR(2) NOT NULL,
  "city_geonameid" INTEGER NOT NULL,
  "photo_id" INTEGER NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "lang" (
  "isocode" CHAR(2) NOT NULL,
  "active" BOOLEAN NOT NULL,
  PRIMARY KEY ("isocode")
);

CREATE TABLE "user__company" (
  "user_id" INTEGER NOT NULL,
  "company_id" INTEGER NOT NULL,
  PRIMARY KEY ("user_id", "company_id")
);

CREATE TABLE "company" (
  "id" serial NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "city_geonameid" INTEGER NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "project" (
  "id" serial NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "user_id" INTEGER NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  "start_date" INTEGER NOT NULL,
  "end_date" INTEGER NOT NULL,
  "geom" geography(POLYGON,4326) NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "chronology" (
  "id" serial NOT NULL,
  "parent_id" INTEGER NOT NULL,
  "start_date" INTEGER NOT NULL,
  "end_date" INTEGER NOT NULL,
  "color" VARCHAR(6) NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  "id_ark_periodo" VARCHAR(255) NOT NULL,
  "id_ark_pactols" VARCHAR(255) NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "chronology_tr" (
  "lang_isocode" CHAR(2) NOT NULL,
  "chronology_id" INTEGER NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "description" TEXT NOT NULL,
  PRIMARY KEY ("chronology_id", "lang_isocode")
);

CREATE TABLE "project__chronology" (
  "project_id" INTEGER NOT NULL,
  "root_chronology_id" INTEGER NOT NULL,
  PRIMARY KEY ("project_id", "root_chronology_id")
);

CREATE TABLE "database" (
  "id" serial NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "scale_resolution" database_scale_resolution NOT NULL,
  "geographical_extent" database_geographical_extent NOT NULL,
  "type" database_type NOT NULL,
  "owner" INTEGER NOT NULL,
  "editor" TEXT NOT NULL,
  "editor_url" VARCHAR(255) NOT NULL,
  "contributor" TEXT NOT NULL,
  "default_language" CHAR(2) NOT NULL,
  "state" database_state NOT NULL,
  "license_id" INTEGER NOT NULL,
  "published" BOOLEAN NOT NULL,
  "soft_deleted" BOOLEAN NOT NULL,
  "geographical_extent_geom" geography(POLYGON,4326) NOT NULL,
  "start_date" INTEGER NOT NULL,
  "end_date" INTEGER NOT NULL,
  "declared_creation_date" timestamp with time zone NOT NULL,
  "public" BOOLEAN NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "site" (
  "id" serial NOT NULL,
  "code" VARCHAR(255) NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "city_name" VARCHAR(255) NOT NULL,
  "city_geonameid" INTEGER NOT NULL,
  "geom" geography(POINT,4326) NOT NULL,
  "geom_3d" geography(POINTZ,4326) NOT NULL,
  "centroid" BOOLEAN NOT NULL,
  "occupation" site_occupation NOT NULL,
  "database_id" INTEGER NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  "altitude" double precision NOT NULL,
  "start_date1" INTEGER NOT NULL,
  "start_date2" INTEGER NOT NULL,
  "end_date1" INTEGER NOT NULL,
  "end_date2" INTEGER NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "database_tr" (
  "database_id" INTEGER NOT NULL,
  "lang_isocode" CHAR(2) NOT NULL,
  "description" TEXT NOT NULL,
  "geographical_limit" TEXT NOT NULL,
  "bibliography" TEXT NOT NULL,
  "context_description" TEXT NOT NULL,
  "source_description" VARCHAR NOT NULL,
  "source_relation" TEXT NOT NULL,
  "copyright" TEXT NOT NULL,
  "subject" TEXT NOT NULL,
  "re_use" TEXT NOT NULL,
  PRIMARY KEY ("database_id", "lang_isocode")
);

CREATE TABLE "city" (
  "geonameid" INTEGER NOT NULL,
  "country_geonameid" INTEGER NOT NULL,
  "geom" geography(POLYGON,4326),
  "geom_centroid" geography(POINT,4326) NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  PRIMARY KEY ("geonameid")
);

CREATE TABLE "site_tr" (
  "site_id" INTEGER NOT NULL,
  "lang_isocode" CHAR(2) NOT NULL,
  "description" TEXT NOT NULL,
  PRIMARY KEY ("site_id", "lang_isocode")
);

CREATE TABLE "site_range" (
  "id" serial NOT NULL,
  "site_id" INTEGER NOT NULL,
  "start_date1" INTEGER NOT NULL,
  "start_date2" INTEGER NOT NULL,
  "end_date1" INTEGER NOT NULL,
  "end_date2" INTEGER NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "site_range__charac_tr" (
  "site_range__charac_id" INTEGER NOT NULL,
  "lang_isocode" CHAR(2) NOT NULL,
  "comment" TEXT NOT NULL,
  "bibliography" TEXT NOT NULL,
  PRIMARY KEY ("site_range__charac_id", "lang_isocode")
);

CREATE TABLE "city_tr" (
  "city_geonameid" INTEGER NOT NULL,
  "lang_isocode" CHAR(2) NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "name_ascii" VARCHAR(255) NOT NULL,
  PRIMARY KEY ("city_geonameid", "lang_isocode")
);

CREATE TABLE "charac_tr" (
  "lang_isocode" CHAR(2) NOT NULL,
  "charac_id" INTEGER NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "description" TEXT NOT NULL,
  PRIMARY KEY ("charac_id", "lang_isocode")
);

CREATE TABLE "charac" (
  "id" serial NOT NULL,
  "parent_id" INTEGER NOT NULL,
  "order" INTEGER NOT NULL,
  "author_user_id" INTEGER NOT NULL,
  "ark_id" VARCHAR(255) NOT NULL,
  "pactols_id" VARCHAR(255) NOT NULL,
  "aat_id" VARCHAR(255) NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "map_layer" (
  "id" serial NOT NULL,
  "creator_user_id" INTEGER NOT NULL,
  "type" map_layer_type NOT NULL,
  "url" VARCHAR(255) NOT NULL,
  "identifier" VARCHAR(255) NOT NULL,
  "min_scale" INTEGER NOT NULL,
  "max_scale" INTEGER NOT NULL,
  "start_date" INTEGER NOT NULL,
  "end_date" INTEGER NOT NULL,
  "image_format" VARCHAR(25) NOT NULL,
  "geographical_extent_geom" geography(POLYGON,4326) NOT NULL,
  "published" BOOLEAN NOT NULL,
  "license" VARCHAR(255) NOT NULL,
  "license_id" INTEGER NOT NULL,
  "max_usage_date" timestamp with time zone NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  "tile_matrix_set" VARCHAR(255) NOT NULL,
  "tile_matrix_string" TEXT NOT NULL,
  "use_proxy" BOOLEAN NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "map_layer_tr" (
  "map_layer_id" INTEGER NOT NULL,
  "lang_isocode" CHAR(2) NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "attribution" TEXT NOT NULL,
  "copyright" TEXT NOT NULL,
  "description" TEXT NOT NULL,
  PRIMARY KEY ("map_layer_id", "lang_isocode")
);

CREATE TABLE "shapefile" (
  "id" serial NOT NULL,
  "creator_user_id" INTEGER NOT NULL,
  "filename" VARCHAR(255) NOT NULL,
  "md5sum" VARCHAR(32) NOT NULL,
  "geojson" TEXT NOT NULL,
  "geojson_with_data" TEXT NOT NULL,
  "start_date" INTEGER NOT NULL,
  "end_date" INTEGER NOT NULL,
  "geographical_extent_geom" geography(POLYGON,4326) NOT NULL,
  "published" BOOLEAN NOT NULL,
  "license" VARCHAR(255) NOT NULL,
  "license_id" INTEGER NOT NULL,
  "declared_creation_date" timestamp with time zone NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "shapefile_tr" (
  "shapefile_id" INTEGER NOT NULL,
  "lang_isocode" CHAR(2) NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "attribution" VARCHAR(255) NOT NULL,
  "copyright" TEXT NOT NULL,
  "description" TEXT NOT NULL,
  PRIMARY KEY ("shapefile_id", "lang_isocode")
);

CREATE TABLE "project__database" (
  "project_id" INTEGER NOT NULL,
  "database_id" INTEGER NOT NULL,
  PRIMARY KEY ("project_id", "database_id")
);

CREATE TABLE "project__map_layer" (
  "project_id" INTEGER NOT NULL,
  "map_layer_id" INTEGER NOT NULL,
  PRIMARY KEY ("project_id", "map_layer_id")
);

CREATE TABLE "project__shapefile" (
  "project_id" INTEGER NOT NULL,
  "shapefile_id" INTEGER NOT NULL,
  PRIMARY KEY ("project_id", "shapefile_id")
);

CREATE TABLE "site_range__charac" (
  "id" serial NOT NULL,
  "site_range_id" INTEGER NOT NULL,
  "charac_id" INTEGER NOT NULL,
  "exceptional" BOOLEAN NOT NULL,
  "knowledge_type" site_range__charac_knowledge_type NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "project_hidden_characs" (
  "project_id" INTEGER NOT NULL,
  "charac_id" INTEGER NOT NULL,
  PRIMARY KEY ("charac_id", "project_id")
);

CREATE TABLE "group" (
  "id" serial NOT NULL,
  "type" group_type NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "permission" (
  "id" serial NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "group_tr" (
  "group_id" INTEGER NOT NULL,
  "lang_isocode" CHAR(2) NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "description" TEXT NOT NULL,
  PRIMARY KEY ("group_id", "lang_isocode")
);

CREATE TABLE "permission_tr" (
  "permission_id" INTEGER NOT NULL,
  "lang_isocode" CHAR(2) NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "description" TEXT NOT NULL,
  PRIMARY KEY ("permission_id", "lang_isocode")
);

CREATE TABLE "user__group" (
  "group_id" INTEGER NOT NULL,
  "user_id" INTEGER NOT NULL,
  PRIMARY KEY ("group_id", "user_id")
);

CREATE TABLE "group__permission" (
  "group_id" INTEGER NOT NULL,
  "permission_id" INTEGER NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  PRIMARY KEY ("group_id", "permission_id")
);

CREATE TABLE "country" (
  "geonameid" INTEGER NOT NULL,
  "iso_code" VARCHAR(2),
  "geom" geography(POLYGON,4326),
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  PRIMARY KEY ("geonameid")
);

CREATE TABLE "user_preferences" (
  "user_id" INTEGER NOT NULL,
  "key" VARCHAR(32) NOT NULL,
  "value" BYTEA NOT NULL,
  PRIMARY KEY ("user_id", "key")
);

CREATE TABLE "country_tr" (
  "country_geonameid" INTEGER NOT NULL,
  "lang_isocode" CHAR(2) NOT NULL,
  "name" VARCHAR(128) NOT NULL,
  "name_ascii" VARCHAR(255) NOT NULL,
  PRIMARY KEY ("country_geonameid", "lang_isocode")
);

CREATE TABLE "database__authors" (
  "database_id" INTEGER NOT NULL,
  "user_id" INTEGER NOT NULL,
  PRIMARY KEY ("database_id", "user_id")
);

CREATE TABLE "import" (
  "id" serial NOT NULL,
  "database_id" INTEGER NOT NULL,
  "user_id" INTEGER NOT NULL,
  "md5sum" VARCHAR(32) NOT NULL,
  "filename" VARCHAR(255) NOT NULL,
  "number_of_lines" INTEGER NOT NULL,
  "number_of_sites" INTEGER NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "license" (
  "id" serial NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "url" VARCHAR NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "database__country" (
  "database_id" INTEGER NOT NULL,
  "country_geonameid" INTEGER NOT NULL,
  PRIMARY KEY ("database_id", "country_geonameid")
);

CREATE TABLE "database_handle" (
  "id" serial,
  "database_id" INTEGER NOT NULL,
  "import_id" INTEGER NOT NULL,
  "identifier" VARCHAR(255) NOT NULL,
  "url" VARCHAR(255) NOT NULL,
  "declared_creation_date" timestamp with time zone NOT NULL,
  "created_at" timestamp with time zone NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "shapefile__authors" (
  "user_id" INTEGER NOT NULL,
  "shapefile_id" INTEGER NOT NULL,
  PRIMARY KEY ("shapefile_id", "user_id")
);

CREATE TABLE "continent_tr" (
  "continent_geonameid" serial,
  "lang_isocode" CHAR(2),
  "name" VARCHAR(255) NOT NULL,
  "name_ascii" VARCHAR(255) NOT NULL,
  PRIMARY KEY ("continent_geonameid", "lang_isocode")
);

CREATE TABLE "continent" (
  "geonameid" serial,
  "iso_code" VARCHAR(2) NOT NULL,
  "geom" geography(POLYGON,4326),
  "created_at" timestamp with time zone NOT NULL,
  "updated_at" timestamp with time zone NOT NULL,
  PRIMARY KEY ("geonameid")
);

CREATE TABLE "database__continent" (
  "database_id" INTEGER NOT NULL,
  "continent_geonameid" INTEGER,
  PRIMARY KEY ("database_id", "continent_geonameid")
);

CREATE TABLE "lang_tr" (
  "lang_isocode" CHAR(2) NOT NULL,
  "lang_isocode_tr" CHAR(2) NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  PRIMARY KEY ("lang_isocode", "lang_isocode_tr")
);

CREATE TABLE "session" (
  "token" CHAR(42) NOT NULL,
  "value" BYTEA NOT NULL,
  PRIMARY KEY ("token")
);

CREATE TABLE "photo" (
  "id" serial,
  "photo" BYTEA NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "database_context" (
  "id" serial,
  "database_id" INTEGER NOT NULL,
  "context" database_context_context NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "charac_root" (
  "root_charac_id" INTEGER NOT NULL,
  "admin_group_id" INTEGER NOT NULL,
  "cached_langs" TEXT NOT NULL,
  PRIMARY KEY ("root_charac_id")
);

CREATE TABLE "chronology_root" (
  "root_chronology_id" INTEGER NOT NULL,
  "admin_group_id" INTEGER NOT NULL,
  "author_user_id" INTEGER NOT NULL,
  "credits" VARCHAR NOT NULL,
  "active" BOOLEAN NOT NULL,
  "geom" geography(POLYGON,4326) NOT NULL,
  "cached_langs" TEXT NOT NULL,
  PRIMARY KEY ("root_chronology_id")
);

CREATE TABLE "map_layer__authors" (
  "user_id" INTEGER NOT NULL,
  "map_layer_id" INTEGER NOT NULL,
  PRIMARY KEY ("user_id", "map_layer_id")
);

CREATE TABLE "project__charac" (
  "project_id" serial,
  "root_charac_id" INTEGER NOT NULL,
  PRIMARY KEY ("project_id", "root_charac_id")
);

CREATE TABLE "saved_query" (
  "project_id" INTEGER NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "params" TEXT NOT NULL,
  PRIMARY KEY ("project_id", "name")
);



ALTER TABLE "user" ADD CONSTRAINT "c_user.first_lang_isocode" FOREIGN KEY ("first_lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "user" ADD CONSTRAINT "c_user.second_lang_isocode" FOREIGN KEY ("second_lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "user" ADD CONSTRAINT "c_user.city_geonameid" FOREIGN KEY ("city_geonameid") REFERENCES "city" ("geonameid")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "user" ADD CONSTRAINT "c_user.photo_id" FOREIGN KEY ("photo_id") REFERENCES "photo" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "user__company" ADD CONSTRAINT "c_user__company.user_id" FOREIGN KEY ("user_id") REFERENCES "user" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "user__company" ADD CONSTRAINT "c_user__company.company_id" FOREIGN KEY ("company_id") REFERENCES "company" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "company" ADD CONSTRAINT "c_company.city_geonameid" FOREIGN KEY ("city_geonameid") REFERENCES "city" ("geonameid")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "project" ADD CONSTRAINT "c_project.user_id" FOREIGN KEY ("user_id") REFERENCES "user" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "chronology" ADD CONSTRAINT "c_chronology.parent_id" FOREIGN KEY ("parent_id") REFERENCES "chronology" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "chronology_tr" ADD CONSTRAINT "c_chronology_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "chronology_tr" ADD CONSTRAINT "c_chronology_tr.chronology_id" FOREIGN KEY ("chronology_id") REFERENCES "chronology" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "project__chronology" ADD CONSTRAINT "c_project__chronology.project_id" FOREIGN KEY ("project_id") REFERENCES "project" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "project__chronology" ADD CONSTRAINT "c_project__chronology.root_chronology_id" FOREIGN KEY ("root_chronology_id") REFERENCES "chronology" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database" ADD CONSTRAINT "c_database.owner" FOREIGN KEY ("owner") REFERENCES "user" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database" ADD CONSTRAINT "c_database.default_language" FOREIGN KEY ("default_language") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database" ADD CONSTRAINT "c_database.license_id" FOREIGN KEY ("license_id") REFERENCES "license" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "site" ADD CONSTRAINT "c_site.database_id" FOREIGN KEY ("database_id") REFERENCES "database" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database_tr" ADD CONSTRAINT "c_database_tr.database_id" FOREIGN KEY ("database_id") REFERENCES "database" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database_tr" ADD CONSTRAINT "c_database_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "city" ADD CONSTRAINT "c_city.country_geonameid" FOREIGN KEY ("country_geonameid") REFERENCES "country" ("geonameid")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "site_tr" ADD CONSTRAINT "c_site_tr.site_id" FOREIGN KEY ("site_id") REFERENCES "site" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "site_tr" ADD CONSTRAINT "c_site_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "site_range" ADD CONSTRAINT "c_site_range.site_id" FOREIGN KEY ("site_id") REFERENCES "site" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "site_range__charac_tr" ADD CONSTRAINT "c_site_range__charac_tr.site_range__charac_id" FOREIGN KEY ("site_range__charac_id") REFERENCES "site_range__charac" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "site_range__charac_tr" ADD CONSTRAINT "c_site_range__charac_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "city_tr" ADD CONSTRAINT "c_city_tr.city_geonameid" FOREIGN KEY ("city_geonameid") REFERENCES "city" ("geonameid")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "city_tr" ADD CONSTRAINT "c_city_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "charac_tr" ADD CONSTRAINT "c_charac_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "charac_tr" ADD CONSTRAINT "c_charac_tr.charac_id" FOREIGN KEY ("charac_id") REFERENCES "charac" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "charac" ADD CONSTRAINT "c_charac.parent_id" FOREIGN KEY ("parent_id") REFERENCES "charac" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "charac" ADD CONSTRAINT "c_charac.author_user_id" FOREIGN KEY ("author_user_id") REFERENCES "user" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "map_layer" ADD CONSTRAINT "c_map_layer.creator_user_id" FOREIGN KEY ("creator_user_id") REFERENCES "user" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "map_layer" ADD CONSTRAINT "c_map_layer.license_id" FOREIGN KEY ("license_id") REFERENCES "license" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "map_layer_tr" ADD CONSTRAINT "c_map_layer_tr.map_layer_id" FOREIGN KEY ("map_layer_id") REFERENCES "map_layer" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "map_layer_tr" ADD CONSTRAINT "c_map_layer_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "shapefile" ADD CONSTRAINT "c_shapefile.creator_user_id" FOREIGN KEY ("creator_user_id") REFERENCES "user" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "shapefile" ADD CONSTRAINT "c_shapefile.license_id" FOREIGN KEY ("license_id") REFERENCES "license" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "shapefile_tr" ADD CONSTRAINT "c_shapefile_tr.shapefile_id" FOREIGN KEY ("shapefile_id") REFERENCES "shapefile" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "shapefile_tr" ADD CONSTRAINT "c_shapefile_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "project__database" ADD CONSTRAINT "c_project__database.project_id" FOREIGN KEY ("project_id") REFERENCES "project" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "project__database" ADD CONSTRAINT "c_project__database.database_id" FOREIGN KEY ("database_id") REFERENCES "database" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "project__map_layer" ADD CONSTRAINT "c_project__map_layer.project_id" FOREIGN KEY ("project_id") REFERENCES "project" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "project__map_layer" ADD CONSTRAINT "c_project__map_layer.map_layer_id" FOREIGN KEY ("map_layer_id") REFERENCES "map_layer" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "project__shapefile" ADD CONSTRAINT "c_project__shapefile.project_id" FOREIGN KEY ("project_id") REFERENCES "project" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "project__shapefile" ADD CONSTRAINT "c_project__shapefile.shapefile_id" FOREIGN KEY ("shapefile_id") REFERENCES "shapefile" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "site_range__charac" ADD CONSTRAINT "c_site_range__charac.site_range_id" FOREIGN KEY ("site_range_id") REFERENCES "site_range" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "site_range__charac" ADD CONSTRAINT "c_site_range__charac.charac_id" FOREIGN KEY ("charac_id") REFERENCES "charac" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "project_hidden_characs" ADD CONSTRAINT "c_project_hidden_characs.project_id" FOREIGN KEY ("project_id") REFERENCES "project" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "project_hidden_characs" ADD CONSTRAINT "c_project_hidden_characs.charac_id" FOREIGN KEY ("charac_id") REFERENCES "charac" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "group_tr" ADD CONSTRAINT "c_group_tr.group_id" FOREIGN KEY ("group_id") REFERENCES "group" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "group_tr" ADD CONSTRAINT "c_group_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "permission_tr" ADD CONSTRAINT "c_permission_tr.permission_id" FOREIGN KEY ("permission_id") REFERENCES "permission" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "permission_tr" ADD CONSTRAINT "c_permission_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "user__group" ADD CONSTRAINT "c_user__group.group_id" FOREIGN KEY ("group_id") REFERENCES "group" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "user__group" ADD CONSTRAINT "c_user__group.user_id" FOREIGN KEY ("user_id") REFERENCES "user" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "group__permission" ADD CONSTRAINT "c_group__permission.group_id" FOREIGN KEY ("group_id") REFERENCES "group" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "group__permission" ADD CONSTRAINT "c_group__permission.permission_id" FOREIGN KEY ("permission_id") REFERENCES "permission" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "user_preferences" ADD CONSTRAINT "c_user_preferences.user_id" FOREIGN KEY ("user_id") REFERENCES "user" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "country_tr" ADD CONSTRAINT "c_country_tr.country_geonameid" FOREIGN KEY ("country_geonameid") REFERENCES "country" ("geonameid")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "country_tr" ADD CONSTRAINT "c_country_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database__authors" ADD CONSTRAINT "c_database__authors.database_id" FOREIGN KEY ("database_id") REFERENCES "database" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database__authors" ADD CONSTRAINT "c_database__authors.user_id" FOREIGN KEY ("user_id") REFERENCES "user" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "import" ADD CONSTRAINT "c_import.database_id" FOREIGN KEY ("database_id") REFERENCES "database" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "import" ADD CONSTRAINT "c_import.user_id" FOREIGN KEY ("user_id") REFERENCES "user" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database__country" ADD CONSTRAINT "c_database__country.database_id" FOREIGN KEY ("database_id") REFERENCES "database" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database__country" ADD CONSTRAINT "c_database__country.country_geonameid" FOREIGN KEY ("country_geonameid") REFERENCES "country" ("geonameid")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database_handle" ADD CONSTRAINT "c_database_handle.database_id" FOREIGN KEY ("database_id") REFERENCES "database" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database_handle" ADD CONSTRAINT "c_database_handle.import_id" FOREIGN KEY ("import_id") REFERENCES "import" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "shapefile__authors" ADD CONSTRAINT "c_shapefile__authors.user_id" FOREIGN KEY ("user_id") REFERENCES "user" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "shapefile__authors" ADD CONSTRAINT "c_shapefile__authors.shapefile_id" FOREIGN KEY ("shapefile_id") REFERENCES "shapefile" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "continent_tr" ADD CONSTRAINT "c_continent_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database__continent" ADD CONSTRAINT "c_database__continent.database_id" FOREIGN KEY ("database_id") REFERENCES "database" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database__continent" ADD CONSTRAINT "c_database__continent.continent_geonameid" FOREIGN KEY ("continent_geonameid") REFERENCES "continent" ("geonameid")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "lang_tr" ADD CONSTRAINT "c_lang_tr.lang_isocode" FOREIGN KEY ("lang_isocode") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "lang_tr" ADD CONSTRAINT "c_lang_tr.lang_isocode_tr" FOREIGN KEY ("lang_isocode_tr") REFERENCES "lang" ("isocode")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "database_context" ADD CONSTRAINT "c_database_context.database_id" FOREIGN KEY ("database_id") REFERENCES "database" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "charac_root" ADD CONSTRAINT "c_charac_root.root_charac_id" FOREIGN KEY ("root_charac_id") REFERENCES "charac" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "charac_root" ADD CONSTRAINT "c_charac_root.admin_group_id" FOREIGN KEY ("admin_group_id") REFERENCES "group" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "chronology_root" ADD CONSTRAINT "c_chronology_root.root_chronology_id" FOREIGN KEY ("root_chronology_id") REFERENCES "chronology" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "chronology_root" ADD CONSTRAINT "c_chronology_root.admin_group_id" FOREIGN KEY ("admin_group_id") REFERENCES "group" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "chronology_root" ADD CONSTRAINT "c_chronology_root.author_user_id" FOREIGN KEY ("author_user_id") REFERENCES "user" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "map_layer__authors" ADD CONSTRAINT "c_map_layer__authors.user_id" FOREIGN KEY ("user_id") REFERENCES "user" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "map_layer__authors" ADD CONSTRAINT "c_map_layer__authors.map_layer_id" FOREIGN KEY ("map_layer_id") REFERENCES "map_layer" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "project__charac" ADD CONSTRAINT "c_project__charac.project_id" FOREIGN KEY ("project_id") REFERENCES "project" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "project__charac" ADD CONSTRAINT "c_project__charac.root_charac_id" FOREIGN KEY ("root_charac_id") REFERENCES "charac" ("id")  DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "saved_query" ADD CONSTRAINT "c_saved_query.project_id" FOREIGN KEY ("project_id") REFERENCES "project" ("id") ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;

CREATE INDEX "i_user.first_lang_isocode" ON "user" ("first_lang_isocode");
CREATE INDEX "i_user.second_lang_isocode" ON "user" ("second_lang_isocode");
CREATE INDEX "i_user.city_geonameid" ON "user" ("city_geonameid");
CREATE INDEX "i_user.photo_id" ON "user" ("photo_id");
CREATE UNIQUE INDEX "i_user.username" ON "user" ( "username" );
CREATE INDEX "i_user__company.user_id" ON "user__company" ("user_id");
CREATE INDEX "i_user__company.company_id" ON "user__company" ("company_id");
CREATE INDEX "i_company.city_geonameid" ON "company" ("city_geonameid");
CREATE INDEX "i_project.user_id" ON "project" ("user_id");
CREATE INDEX "i_chronology.parent_id" ON "chronology" ("parent_id");
CREATE INDEX "i_chronology_tr.lang_isocode" ON "chronology_tr" ("lang_isocode");
CREATE INDEX "i_chronology_tr.chronology_id" ON "chronology_tr" ("chronology_id");
CREATE INDEX "i_project__chronology.project_id" ON "project__chronology" ("project_id");
CREATE INDEX "i_project__chronology.root_chronology_id" ON "project__chronology" ("root_chronology_id");
CREATE INDEX "i_database.owner" ON "database" ("owner");
CREATE INDEX "i_database.default_language" ON "database" ("default_language");
CREATE INDEX "i_database.license_id" ON "database" ("license_id");
CREATE INDEX "i_site.database_id" ON "site" ("database_id");
CREATE INDEX "i_site.geom,geom_3d" ON "site" USING GIST ( "geom", "geom_3d" );
CREATE INDEX "i_site.start_date1" ON "site" ( "start_date1" );
CREATE INDEX "i_site.start_date2" ON "site" ( "start_date2" );
CREATE INDEX "i_site.end_date1" ON "site" ( "end_date1" );
CREATE INDEX "i_site.end_date2" ON "site" ( "end_date2" );
CREATE INDEX "i_database_tr.database_id" ON "database_tr" ("database_id");
CREATE INDEX "i_database_tr.lang_isocode" ON "database_tr" ("lang_isocode");
CREATE INDEX "i_city.country_geonameid" ON "city" ("country_geonameid");
CREATE INDEX "i_site_tr.site_id" ON "site_tr" ("site_id");
CREATE INDEX "i_site_tr.lang_isocode" ON "site_tr" ("lang_isocode");
CREATE INDEX "i_site_range.site_id" ON "site_range" ("site_id");
CREATE INDEX "i_site_range__charac_tr.site_range__charac_id" ON "site_range__charac_tr" ("site_range__charac_id");
CREATE INDEX "i_site_range__charac_tr.lang_isocode" ON "site_range__charac_tr" ("lang_isocode");
CREATE INDEX "i_city_tr.city_geonameid" ON "city_tr" ("city_geonameid");
CREATE INDEX "i_city_tr.lang_isocode" ON "city_tr" ("lang_isocode");
CREATE INDEX "i_city_tr.name_ascii" ON "city_tr" ( "name_ascii" );
CREATE INDEX "i_city_tr.name" ON "city_tr" ( "name" );
CREATE INDEX "i_charac_tr.lang_isocode" ON "charac_tr" ("lang_isocode");
CREATE INDEX "i_charac_tr.charac_id" ON "charac_tr" ("charac_id");
CREATE INDEX "i_charac.parent_id" ON "charac" ("parent_id");
CREATE INDEX "i_charac.author_user_id" ON "charac" ("author_user_id");
CREATE INDEX "i_map_layer.creator_user_id" ON "map_layer" ("creator_user_id");
CREATE INDEX "i_map_layer.license_id" ON "map_layer" ("license_id");
CREATE INDEX "i_map_layer_tr.map_layer_id" ON "map_layer_tr" ("map_layer_id");
CREATE INDEX "i_map_layer_tr.lang_isocode" ON "map_layer_tr" ("lang_isocode");
CREATE INDEX "i_shapefile.creator_user_id" ON "shapefile" ("creator_user_id");
CREATE INDEX "i_shapefile.license_id" ON "shapefile" ("license_id");
CREATE INDEX "i_shapefile_tr.shapefile_id" ON "shapefile_tr" ("shapefile_id");
CREATE INDEX "i_shapefile_tr.lang_isocode" ON "shapefile_tr" ("lang_isocode");
CREATE INDEX "i_project__database.project_id" ON "project__database" ("project_id");
CREATE INDEX "i_project__database.database_id" ON "project__database" ("database_id");
CREATE INDEX "i_project__map_layer.project_id" ON "project__map_layer" ("project_id");
CREATE INDEX "i_project__map_layer.map_layer_id" ON "project__map_layer" ("map_layer_id");
CREATE INDEX "i_project__shapefile.project_id" ON "project__shapefile" ("project_id");
CREATE INDEX "i_project__shapefile.shapefile_id" ON "project__shapefile" ("shapefile_id");
CREATE INDEX "i_site_range__charac.site_range_id" ON "site_range__charac" ("site_range_id");
CREATE INDEX "i_site_range__charac.charac_id" ON "site_range__charac" ("charac_id");
CREATE INDEX "i_project_hidden_characs.project_id" ON "project_hidden_characs" ("project_id");
CREATE INDEX "i_project_hidden_characs.charac_id" ON "project_hidden_characs" ("charac_id");
CREATE INDEX "i_group_tr.group_id" ON "group_tr" ("group_id");
CREATE INDEX "i_group_tr.lang_isocode" ON "group_tr" ("lang_isocode");
CREATE INDEX "i_permission_tr.permission_id" ON "permission_tr" ("permission_id");
CREATE INDEX "i_permission_tr.lang_isocode" ON "permission_tr" ("lang_isocode");
CREATE INDEX "i_user__group.group_id" ON "user__group" ("group_id");
CREATE INDEX "i_user__group.user_id" ON "user__group" ("user_id");
CREATE INDEX "i_group__permission.group_id" ON "group__permission" ("group_id");
CREATE INDEX "i_group__permission.permission_id" ON "group__permission" ("permission_id");
CREATE INDEX "i_country.iso_code" ON "country" ( "iso_code" );
CREATE INDEX "i_user_preferences.user_id" ON "user_preferences" ("user_id");
CREATE INDEX "i_country_tr.country_geonameid" ON "country_tr" ("country_geonameid");
CREATE INDEX "i_country_tr.lang_isocode" ON "country_tr" ("lang_isocode");
CREATE INDEX "i_country_tr.name_ascii" ON "country_tr" ( "name_ascii" );
CREATE INDEX "i_country_tr.name" ON "country_tr" ( "name" );
CREATE INDEX "i_database__authors.database_id" ON "database__authors" ("database_id");
CREATE INDEX "i_database__authors.user_id" ON "database__authors" ("user_id");
CREATE INDEX "i_import.database_id" ON "import" ("database_id");
CREATE INDEX "i_import.user_id" ON "import" ("user_id");
CREATE INDEX "i_database__country.database_id" ON "database__country" ("database_id");
CREATE INDEX "i_database__country.country_geonameid" ON "database__country" ("country_geonameid");
CREATE INDEX "i_database_handle.database_id" ON "database_handle" ("database_id");
CREATE INDEX "i_database_handle.import_id" ON "database_handle" ("import_id");
CREATE INDEX "i_shapefile__authors.user_id" ON "shapefile__authors" ("user_id");
CREATE INDEX "i_shapefile__authors.shapefile_id" ON "shapefile__authors" ("shapefile_id");
CREATE INDEX "i_continent_tr.lang_isocode" ON "continent_tr" ("lang_isocode");
CREATE INDEX "i_continent_tr.name,name_ascii" ON "continent_tr" ( "name", "name_ascii" );
CREATE INDEX "i_continent.iso_code" ON "continent" ( "iso_code" );
CREATE INDEX "i_database__continent.database_id" ON "database__continent" ("database_id");
CREATE INDEX "i_database__continent.continent_geonameid" ON "database__continent" ("continent_geonameid");
CREATE INDEX "i_lang_tr.lang_isocode" ON "lang_tr" ("lang_isocode");
CREATE INDEX "i_lang_tr.lang_isocode_tr" ON "lang_tr" ("lang_isocode_tr");
CREATE INDEX "i_database_context.database_id" ON "database_context" ("database_id");
CREATE INDEX "i_charac_root.root_charac_id" ON "charac_root" ("root_charac_id");
CREATE INDEX "i_charac_root.admin_group_id" ON "charac_root" ("admin_group_id");
CREATE INDEX "i_chronology_root.root_chronology_id" ON "chronology_root" ("root_chronology_id");
CREATE INDEX "i_chronology_root.admin_group_id" ON "chronology_root" ("admin_group_id");
CREATE INDEX "i_chronology_root.author_user_id" ON "chronology_root" ("author_user_id");
CREATE INDEX "i_map_layer__authors.user_id" ON "map_layer__authors" ("user_id");
CREATE INDEX "i_map_layer__authors.map_layer_id" ON "map_layer__authors" ("map_layer_id");
CREATE INDEX "i_project__charac.project_id" ON "project__charac" ("project_id");
CREATE INDEX "i_project__charac.root_charac_id" ON "project__charac" ("root_charac_id");
CREATE INDEX "i_saved_query.project_id" ON "saved_query" ("project_id");

set timezone TO 'GMT';
alter table "user" alter column "created_at" type timestamp with time zone;
alter table "user" alter column "updated_at" type timestamp with time zone;
alter table "project" alter column "created_at" type timestamp with time zone;
alter table "project" alter column "updated_at" type timestamp with time zone;
alter table "chronology" alter column "created_at" type timestamp with time zone;
alter table "chronology" alter column "updated_at" type timestamp with time zone;
alter table "database" alter column "declared_creation_date" type timestamp with time zone;
alter table "database" alter column "created_at" type timestamp with time zone;
alter table "database" alter column "updated_at" type timestamp with time zone;
alter table "site" alter column "created_at" type timestamp with time zone;
alter table "site" alter column "updated_at" type timestamp with time zone;
alter table "city" alter column "created_at" type timestamp with time zone;
alter table "city" alter column "updated_at" type timestamp with time zone;
alter table "site_range" alter column "created_at" type timestamp with time zone;
alter table "site_range" alter column "updated_at" type timestamp with time zone;
alter table "charac" alter column "created_at" type timestamp with time zone;
alter table "charac" alter column "updated_at" type timestamp with time zone;
alter table "map_layer" alter column "max_usage_date" type timestamp with time zone;
alter table "map_layer" alter column "created_at" type timestamp with time zone;
alter table "map_layer" alter column "updated_at" type timestamp with time zone;
alter table "shapefile" alter column "declared_creation_date" type timestamp with time zone;
alter table "shapefile" alter column "created_at" type timestamp with time zone;
alter table "shapefile" alter column "updated_at" type timestamp with time zone;
alter table "group" alter column "created_at" type timestamp with time zone;
alter table "group" alter column "updated_at" type timestamp with time zone;
alter table "permission" alter column "created_at" type timestamp with time zone;
alter table "permission" alter column "updated_at" type timestamp with time zone;
alter table "group__permission" alter column "created_at" type timestamp with time zone;
alter table "group__permission" alter column "updated_at" type timestamp with time zone;
alter table "country" alter column "created_at" type timestamp with time zone;
alter table "country" alter column "updated_at" type timestamp with time zone;
alter table "import" alter column "created_at" type timestamp with time zone;
alter table "database_handle" alter column "declared_creation_date" type timestamp with time zone;
alter table "database_handle" alter column "created_at" type timestamp with time zone;
alter table "continent" alter column "created_at" type timestamp with time zone;
alter table "continent" alter column "updated_at" type timestamp with time zone;

