# Arkeopen Project

## Arborescence

```
|-- doc                                         # The developer documentation
|-- server                                      # The server code
|-- web-app                                     # The web Application code
|-- web-admin                                   # The admin web-application code
`-- README.md                                   # This Readme
```

## Web-App Arborescence

```
|-- public
|   |-- index.html                              # the main web application page
|   `-- ...
|-- server                                      # the server code
|   |-- README.md                               # the Readme for the server code
|   `-- ...
|-- src                                         # the web application code source
|   |-- components                              # all components
|   |   |-- Main                                # One component (here, "Main")
|   |   |   |-- index.jsx                       # the code of this component
|   |   |   |-- Main.css                        # the style...
|   |   |   `-- Readme.md                       # And the Readme for this component usage
|   |   `-- ...
|   |-- enums                                   # Enumms (const) that can be used widely in client side
|   |   `-- Language.ts                         # here are enums for languages (fr, en, ...)
|   |-- lib                                     # arkeo lib code
|   |   |-- queries                             # code for graphql queries
|   |   |   |-- database.jsx                    # code for database graphql queries
|   |   |   `-- ...
|   |   `-- ...
|   |-- locales                                 # all translations in all languages
|   |-- styles                                  # main app styles
[...]
`-- README.md
```

## Server Arborescence

```
|-- hasura                                      # Hasura
|   |-- metadata                                # metadata(generated from hasura)
|   |-- config.yaml                             # hasura configuration
|-- install                                     # Install scripts
|-- docker-compose.yaml                         # docker compose for running all the server
`-- README.md                                   # A Readme for server side
```

## Architecture

![Architecture](doc/arkeohasura.png)

## Planed installation (Arkeopen)

![Planed Installation](doc/planed-install.png)

## React-JS

### The web side use ReactJS framework. https://reactjs.org/

- [React-JS - Technical choices](doc/react.md)
- It was created using Create React App, here is the original readme.md from it : [doc/create-react-app.md](doc/react-create-app.md)

### Components documentation

Go to : [Components documentation](doc/components/) for documentation each components

## This repository contain Arkeopen and future ArkeoGIS Applications.

![How to code / build ArkeOpen or ArkeoGIS](doc/multi-apps.md)

## Translate the app

Go to : [Translate the app](doc/i18n.md) for documentation about this

## UI

### Bootstrap

(Bootstrap)[https://getbootstrap.com/] is a powerful, extensible, and feature-packed frontend toolkit. It allow to build and customize with Sass, utilize prebuilt grid system and components, and bring projects to life with powerful JavaScript plugins.

### Icons

(React-Icons)[https://react-icons.github.io/react-icons/] is used for general icons needs.

## State management

We use zustand for State Management. Here is the [local documentation of zustand in arkeo](doc/zustand.md)

## Routing (web)

react router [local documentation of routing in arkeo](doc/react-router.md)

## Map

We use maplibre-gl for map rendering. There are many way to implement it in react, with benefits and drawbacks for each. We choose the Here is the [maplibre implementation documentation](doc/maplibre.md)

## Documenting

We use docgen react styleguidist for documenting components. Here is the [local documentation of the stylguidist implementation in arkeo](doc/styleguidist.md)

## GraphQL

[GraphQL](https://graphql.org/) is a query language for APIs and a runtime for fulfilling those queries with your existing data. GraphQL provides a complete and understandable description of the data in your API, gives clients the power to ask for exactly what they need and nothing more, makes it easier to evolve APIs over time, and enables powerful developer tools.
Implementation of GraphQL in Arkeo is using Apollo (see next item).

## Apollo

The [Apollo](https://www.apollographql.com/) Supergraph Platform unifies GraphQL across your apps and services, unlocking faster delivery for your engineering teams.

Here is the [Apollo Implementation documentation in Arkeo](doc/graphql-apollo.md)

## Server

We use hasura + PostgreSQL + PostGIS + node.js

### go to [server/README.md](server/README.md)

## Install

### [Install from scratch](doc/install.md)

## Editor settings

### vscode

We use theses plugins for vscode :

- EditorConfig for VS Code
- ES7+ React/Redux/React-Native snippets / Simple React Snippets
- Apollo GraphQL / GraphQL for vscode
- ESLint
