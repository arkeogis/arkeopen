// react
import { Routes, Route, BrowserRouter, Outlet, Navigate } from "react-router-dom";
import React, { Suspense } from "react";

// translations
import "./i18n";

import StoreRoute from "./components/StoreRoute/StoreRoute";

// css
import styles from "./App.module.scss";
//

// components
import Main from "./components/Main/Main";
import InitMainData from "./components/InitMainData/InitMainData";
import Loader from "./components/Loader/Loader";
import { Toaster } from "sonner"; // Assuming Sonner is used for notifications
// import About from './components/About/About'
// import Boot from './components/Boot/Boot'
import LoginForm from "./components/LoginForm/LoginForm";
import ProtectedRoute from "./components/ProtectedRoute/ProtectedRoute";
import ApolloSetup from "./components/ApolloSetup/ApolloSetup";
import InactivityMonitor from "./components/InactivityMonitor/InactivityMonitor";
//

import LayoutArkeogis from "./components/Arkeogis/Layout/Layout";
import LayoutArkeopen from "./components/Arkeopen/Layout/Layout";

// /!\ Import DatabaseImport AFTER i18n
import DatabaseImport from "./components/DatabaseImport/DatabaseImport";
import IndexChronologies from "./components/IndexChronologies/IndexChronologies";
import IndexThesaurus from "./components/IndexThesaurus/IndexThesaurus";

const Layout = __ARKEOGIS__ ? LayoutArkeogis : LayoutArkeopen;

function App() {
  return (
    <BrowserRouter>
      <ApolloSetup>
        <div className={styles.App}>
          <Routes>
            {/* Routes with no auth*/}
            {__ARKEOGIS__ && <Route path="/login" element={<LoginForm />} />}
            {/* Protected routes */}
            <Route element={__ARKEOGIS__ ? <ProtectedRoute /> : <Outlet />}>
              {/* Protected routes that require main data init*/}
              <Route
                element={
                  <Suspense fallback={<Loader fullHeight={true} />}>
                    <InitMainData />
                  </Suspense>
                }
              >
                <Route
                  exact
                  path="/"
                  element={
                    <Layout>
                      <Main />
                      <Outlet />
                      <StoreRoute />
                    </Layout>
                  }
                >
                  <Route path="/chronologies" element={<IndexChronologies />} />
                  <Route path="/thesaurus" element={<IndexThesaurus />} />
                </Route>
              </Route>
              {/* Protected routes without data */}
              {/* <Route path="/about" element={<About/>}/> */}
              {/* <Route path="/boot" element={<Boot/>}/> */}
              <Route path="/import" element={<DatabaseImport />} />
            </Route>
            {/* Catch-all route to redirect to home */}
            <Route path="*" element={<Navigate to="/" replace />} />
          </Routes>
        </div>
        <Toaster position="bottom-right" />
        {__ARKEOGIS__ && <InactivityMonitor />}
      </ApolloSetup>
    </BrowserRouter>
  );
}

export default App;
