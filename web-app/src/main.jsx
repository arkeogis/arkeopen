import React from "react";
import ReactDOM from "react-dom/client";
import "./styles/index.scss";
import "./styles/overrides.scss";
import { IS_ARKEOGIS, IS_ARKEOPEN } from "./lib/utils";

import RealApp from "./App";
import ChooseApp from "./ChooseApp";

const App = __ARKEOGIS__ || __ARKEOPEN__ ? RealApp : ChooseApp;

//import reportWebVitals from './reportWebVitals';

console.log("running application : ", __ARKEOGIS__ ? "arkeogis" : __ARKEOPEN__ ? "arkeopen" : "unknown");

// remove the primary loading element from index.html
document.getElementById("primary-loading").remove();
document.getElementById("primary-loading-text").remove();

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<App />);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals(console.log);
