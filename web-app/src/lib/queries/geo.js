import { gql } from "@apollo/client";

import ak from "../akprefix";

export const getCityAndCountryByGeonameId = gql`
 query getCityAndCountryByGeonameId($geonameid: Int!, $lang: bpchar!) {
  ${ak}city(where: {geonameid: {_eq: $geonameid}}) {
    geonameid
    city_trs(where: {lang_isocode: {_eq: $lang}}) {
      name
    }
    country {
      country_trs(where: {lang_isocode: {_eq: $lang}}) {
        name
      }
    }
  }
}
`
