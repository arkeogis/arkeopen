import { gql } from "@apollo/client";

import ak from "../akprefix";

export const GetProject = gql`
  query GetProject($user_id: Int!) {
    ${ak}project(where: { user_id: { _eq: $user_id } }) {
      user_id
      end_date
      geom
      name
      id
      start_date
      project__chronologies {
        root_chronology_id
      }
      project__databases {
        database_id
      }
      project__map_layers {
        map_layer_id
      }
      project__shapefiles {
        shapefile_id
      }
      project_hidden_characs {
        charac_id
      }
      saved_queries {
        name
        params
      }
      project__charac {
        root_charac_id
      }
    }
  }
`;
