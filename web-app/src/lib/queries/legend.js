import { gql } from "@apollo/client";

import ak from "../akprefix";

export const GetWhat = gql`
query GetWhat($site_ids: [Int!]) {
  ${ak}site(where: {id: {_in: $site_ids}}) {
    site_ranges {
      site_range__characs {
        charac_id
      }
    }
  }
}
`;

export const GetOther = gql`
query GetOther($site_ids: [Int!]) {
  knowledge_not_documented: ${ak}site_aggregate(where: {id: {_in: $site_ids}, site_ranges: {site_range__characs: {knowledge_type: {_eq: "not_documented"}}}}) {
    aggregate {
      count
    }
  }
  knowledge_literature: ${ak}site_aggregate(where: {id: {_in: $site_ids}, site_ranges: {site_range__characs: {knowledge_type: {_eq: "literature"}}}}) {
    aggregate {
      count
    }
  }
  knowledge_prospected_aerial: ${ak}site_aggregate(where: {id: {_in: $site_ids}, site_ranges: {site_range__characs: {knowledge_type: {_eq: "prospected_aerial"}}}}) {
    aggregate {
      count
    }
  }
  knowledge_prospected_pedestrian: ${ak}site_aggregate(where: {id: {_in: $site_ids}, site_ranges: {site_range__characs: {knowledge_type: {_eq: "prospected_pedestrian"}}}}) {
    aggregate {
      count
    }
  }
  knowledge_surveyed: ${ak}site_aggregate(where: {id: {_in: $site_ids}, site_ranges: {site_range__characs: {knowledge_type: {_eq: "surveyed"}}}}) {
    aggregate {
      count
    }
  }
  knowledge_dig: ${ak}site_aggregate(where: {id: {_in: $site_ids}, site_ranges: {site_range__characs: {knowledge_type: {_eq: "dig"}}}}) {
    aggregate {
      count
    }
  }
  centroid_yes: ${ak}site_aggregate(where: {id: {_in: $site_ids}, centroid: {_eq: true}}) {
    aggregate {
      count
    }
  }
  centroid_no: ${ak}site_aggregate(where: {id: {_in: $site_ids}, centroid: {_eq: false}}) {
    aggregate {
      count
    }
  }
  exceptional_no: ${ak}site_aggregate(where: {id: {_in: $site_ids}, site_ranges: {site_range__characs: {exceptional: {_eq: false}}}}) {
    aggregate {
      count
    }
  }
  exceptional_yes: ${ak}site_aggregate(where: {id: {_in: $site_ids}, site_ranges: {site_range__characs: {exceptional: {_eq: true}}}}) {
    aggregate {
      count
    }
  }
  datasets: ${ak}database(where: {sites: {id: {_in: $site_ids}}}) {
    id
    sites_aggregate(where: {id: {_in: $site_ids}}) {
      aggregate {
        count
      }
    }
    name
    editor
    editor_url
    user {
      firstname
      lastname
      id
    }
  }
  authors: ${ak}database__authors(where: {database: {sites: {id: {_in: $site_ids}}}}) {
    user {
      firstname
      lastname
      id
    }
    database {
      sites_aggregate(where: {id: {_in: $site_ids}}) {
        aggregate {
          count
        }
      }
    }
  }
}
`;

export const GetCitations = gql`
query GetCitations($site_ids: [Int!]) {
  datasets: ${ak}database(where: {sites: {id: {_in: $site_ids}}}) {
    id
    name
    editor
    editor_url
    database_trs {
      lang_isocode
      re_use
      source_description
      source_relation
      subject
      __typename
      bibliography
      copyright
      database_id
      context_description
      description
    }
  }
}
`;
