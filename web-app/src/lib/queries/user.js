import { gql } from "@apollo/client";

import ak from "../akprefix";

export const GetUserByNameQuery = gql`
  query GetUsersByName($search: String!) {
    ${ak}user(where: { _or: [{ firstname: { _ilike: $search } }, { lastname: { _ilike: $search } }] }) {
      id
      firstname
      lastname
    }
  }
`;

export const GetUserByIdQuery = gql`
  query GetUsersById($search: [Int!]!) {
    ${ak}user(where: { id: { _in: $search } }) {
      id
      firstname
      lastname
    }
  }
`;
