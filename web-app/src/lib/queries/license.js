import { gql } from "@apollo/client";

import ak from "../akprefix";

export const GetLicenseQuery = gql`
  query GetLicense($search: String!) {
    ${ak}license(where: { name: { _ilike: $search }}) {
      id
      name
    }
  }
`;
