import { gql } from "@apollo/client";
import ak from "../akprefix";

export const GetUserSettings = gql`
  query GetUserSettings {
    ${ak}user_preferences {
      key
      value
    }
  }
`;

export const SetUserSetting = gql`
  mutation SetUserSetting($object: ${ak}user_preferences_insert_input!) {
    ${ak}insert_user_preferences_one(on_conflict: {constraint: user_preferences_pkey, update_columns: value}, object: $object) {
      key
      user_id
      value
    }
  }
`;

export const DeleteUserSetting = gql`
  mutation DeleteUserSetting($user_id: Int!) {
    akg_delete_user_preferences(where: { user_id: { _eq: $user_id } }) {
      affected_rows
    }
  }
`;
