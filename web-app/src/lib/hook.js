import { useEffect, useRef, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import Papa from "papaparse";
import jschardet from "jschardet";
import { z } from "zod";
import { createValidatedStoreAsync } from "./utils";

/**
 * Custom hook to manage fullscreen mode for a specified element or the whole document.
 * @param {string | HTMLElement} elementOrElementId - The element or element ID to display in fullscreen mode.
 * @returns {[boolean, function, boolean]} - An array containing the fullscreen mode status, function to toggle fullscreen, and whether fullscreen mode is enabled.
 */
export const useFullScreen = (elementOrElementId = null) => {
  // On umount, exit fullscreen
  const [fullscreenIsActive, setFullscreenIsActive] = useState(false);
  const element = useRef();

  element.current = !elementOrElementId
    ? document.documentElement
    : elementOrElementId === "string"
    ? document.getElementById(elementOrElementId)
    : element;

  useEffect(() => {
    const handleChange = () => {
      setFullscreenIsActive(document.fullscreenElement === element.current);
    };
    element.current.addEventListener("fullscreenchange", handleChange);
    return () => {
      if (element.current) {
        element.current.removeEventListener("fullscreenchange", handleChange);
      }
      if (document.fullscreenElement === element.current) {
        return document.exitFullscreen();
      }
    };
  }, []);

  const toggleFullscreen = () => {
    // if there's another element currently full screen, exit first
    if (document.fullscreenElement && document.fullscreenElement !== element.current) {
      document.exitFullscreen();
    }
    if (!document.fullscreenElement) {
      element.current.requestFullscreen();
      setFullscreenIsActive(true);
    } else {
      document.exitFullscreen();
      setFullscreenIsActive(false);
    }
  };

  return [fullscreenIsActive, toggleFullscreen, document.fullscreenEnabled];
};

/**
 * Custom hook that manages transition states based on mount and unmount events.
 *
 * @param {boolean} isMounted - Flag indicating if the component is mounted.
 * @param {number} unmountDelay - Delay in milliseconds before transitioning out on unmount.
 * @returns {boolean} - State indicating if the transition has occurred.
 */
export const useMountTransition = (isMounted, unmountDelay) => {
  const [hasTransitionedIn, setHasTransitionedIn] = useState(false);

  useEffect(() => {
    let timeoutId;

    if (isMounted && !hasTransitionedIn) {
      setHasTransitionedIn(true);
    } else if (!isMounted && hasTransitionedIn) {
      timeoutId = setTimeout(() => setHasTransitionedIn(false), unmountDelay);
    }

    return () => {
      clearTimeout(timeoutId);
    };
  }, [unmountDelay, isMounted, hasTransitionedIn]);

  return hasTransitionedIn;
};

export const useLocalStorage = (key, initialValue) => {
  // State to store the current value
  const [storedValue, setStoredValue] = useState(() => {
    try {
      // Get from localStorage by key
      const item = window.localStorage.getItem(key);
      // Parse stored json or, if none, return initialValue
      return item ? JSON.parse(item) : initialValue;
    } catch (error) {
      console.error(`Error retrieving localStorage key "${key}":`, error);
      return initialValue;
    }
  });

  // Function to set value in state and localStorage
  const setValue = (value) => {
    try {
      // Allow value to be a function so we have the same API as useState
      const valueToStore = value instanceof Function ? value(storedValue) : value;
      // Save to local storage
      window.localStorage.setItem(key, JSON.stringify(valueToStore));
      // Save state
      setStoredValue(valueToStore);
    } catch (error) {
      console.error(`Error setting localStorage key "${key}":`, error);
    }
  };

  return [storedValue, setValue];
};

/**
 * Custom hook for parsing CSV files using the PapaParse library.
 *
 * @returns {Object} An object containing:
 * - `parsedData`: The parsed data from the CSV file.
 * - `error`: Any error encountered during parsing.
 * - `processing`: A boolean indicating if the parsing is in progress.
 * - `asyncParseCSV`: A function to initiate the parsing of a CSV file.
 *
 * The `asyncParseCSV` function accepts a File object and an optional custom configuration
 * object to override default parsing settings. It updates the state with parsed data,
 * errors, and loading status.
 */

export const useCSVParser = () => {
  const { t } = useTranslation();
  const [parsedData, setParsedData] = useState(null);
  const [error, setError] = useState(null);
  const [processing, setProcessing] = useState(false);

  const checkUTF8 = async (file) => {
    try {
      // Convert File to ArrayBuffer using a Promise
      const arrayBuffer = await new Promise((resolve, reject) => {
        const reader = new FileReader();

        reader.onload = () => resolve(reader.result);
        reader.onerror = () => reject(t("hooks.useCSVParser.error.unreadableFile"));

        reader.readAsArrayBuffer(file);
      });

      // Convert ArrayBuffer to string (fallback to UTF-8 for detection)
      const uint8Array = new Uint8Array(arrayBuffer);
      const textContent = new TextDecoder("utf-8", { fatal: false }).decode(uint8Array);

      // Use jschardet to detect encoding
      const detection = jschardet.detect(textContent);

      if (!["utf-8", "windows-1255"].includes(detection.encoding)) {
        return t("hooks.useCSVParser.error.notUTF-8");
      }
      return true;
    } catch (error) {
      // Handle errors during file reading
      return t("hooks.useCSVParser.error.unreadableFile");
    }
  };

  const asyncParseCSV = async (file, headers = null, customConfig = {}) => {
    if (!file) {
      setError(t("hooks.useCSVParser.error.noFileProvided"));
    }

    if (file.type !== "text/csv") {
      // Invalid file type. Please upload a CSV file.
      setError(t("hooks.useCSVParser.error.invalidFileType"));
    }

    const utf8Check = await checkUTF8(file);
    if (utf8Check !== true) {
      setError(utf8Check);
      return;
    }

    setProcessing(true);
    setError(null);

    Papa.parse(file, {
      skipEmptyLines: true,
      dynamicTyping: true,
      encoding: "UTF-8",
      ...customConfig,
      complete: (results) => {
        let data = results.data;

        if (headers && Array.isArray(headers)) {
          // Check if the number of columns matches the number of headers
          const firstRow = data[0];
          const numberOfFields = Object.keys(firstRow).length;
          if (numberOfFields !== headers.length) {
            // CSV column count does not match the expected header count
            setError(`${t("hooks.useCSVParser.error.headerCount")} (${numberOfFields} / ${headers.length}).`);
            setParsedData(null);
            setProcessing(false);
            return;
          }

          if (!headers.every((header) => Object.keys(firstRow).includes(header))) {
            setError(`${t("hooks.useCSVParser.error.headerMismatch")}.`);
            setParsedData(null);
            setProcessing(false);
            return;
          }

          // Check if the first row matches the headers
          const firstRowMatchesHeaders = headers.every((header, index) => header === firstRow[index]);

          // If the first row matches the headers, remove it
          if (firstRowMatchesHeaders) {
            data = data.slice(1);
          }

          // Map rows to objects with the provided headers
          data = data.map((row) =>
            headers.reduce((acc, header) => {
              acc[header] = row[header];
              return acc;
            }, {})
          );
        }

        setParsedData(data);
        setError(results.errors.length ? results.errors : null);
        setProcessing(false);
      },
      error: (error) => {
        setError(error.message);
        setParsedData(null);
        setProcessing(false);
      },
    });
  };

  return {
    parsedData,
    error,
    loading: processing,
    asyncParseCSV,
  };
};

export const useValidatedFormStore = (schema) => {
  if (schema && typeof schema === "object" && typeof schema.innerType === "function") {
    schema = schema.innerType();
  }

  schema = schema.extend({
    formErrors: z.any().default({}),
    submitButtonEnabled: z.boolean().default(false),
    isValid: z.boolean().default(false),
  });

  const useFormStore = useMemo(
    () =>
      createValidatedStoreAsync(schema, (set) => ({
        setSubmitButtonEnabled: (state) => {
          set({ submitButtonEnabled: state });
        },
        setErrors: (formErrors) => {
          set({ formErrors });
        },
      })),
    []
  );

  const validatePartialSchema = async (schema) => {
    const state = useFormStore.getState();
    const result = await state.validatePartialSchema(schema);
    if (!result.success) {
      const newErrors = result.error.issues.reduce((acc, error) => {
        const field = error.path[0]
        if (field) {
          acc[field] = error.message;
        } else {
          console.error("Unable to find the error name");
        }
        return acc
      }, {});
        state.setErrors({
          ...state.formErrors,
          ...newErrors
        });
    }
    return result.success;
  };

  const register = (field) => {
    const fieldSchema = schema.shape[field];
    const maxLength = fieldSchema
      .safeParse("a".repeat(5000))
      .error?.issues.find((issue) => issue.code === "too_big")?.maximum;
    return {
      name: field,
      value: useFormStore.getState()[field] || "",
      maxLength,
      onChange: async (e) => {
        const state = useFormStore.getState();
        const value = e.target ? e.target.value : e;
        const result = await state.setValidated({ [field]: value });
        if (!result.success) {
          // Merge the new error for the field into the existing errors
          state.setErrors({
            ...state.formErrors,
            [field]: result.errors[field] || result.errors[`${field}-0`], // Dirty hack for multi select
          });
        } else {
          // Remove the error for this field if validation passes
          const { [field]: removed, ...rest } = state.formErrors;
          state.setErrors(rest);
        }
      },
    };
  };

  return {
    register,
    validatePartialSchema,
    useFormStore,
  };
};
