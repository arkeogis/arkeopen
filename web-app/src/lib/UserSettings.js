import { useMutation, useQuery } from "@apollo/client";
import { GetUserSettings, SetUserSetting, DeleteUserSetting } from "./queries/userSettings";
import { useAuthStore } from "./auth";
import { ak } from "./akprefix";
import { useEffect, useState } from "react";

/**
 * The `useUserSettings` hook is used to manage user settings in the application.
 * It provides functionalities to fetch, set, and delete user settings.
 *
 * @returns {Object} An object containing the following properties:
 * - `userSettings`: An object containing the current user settings.
 * - `setUserSetting`: A function to set a user setting.
 * - `deleteUserSetting`: A function to delete a user setting.
 *
 * @example
 * // Fetching User Settings
 * const { userSettings } = useUserSettings();
 * const { theme } = userSettings;

 * useEffect(() => {
 *   console.log(userSettings);
 *
 * // Log the "theme" setting
 *   if (theme) {
 *     console.log('Theme setting:', theme);
 *   }
 * }, [userSettings]);
 *
 * @example
 * // Setting a User Setting
 * const { setUserSetting } = useUserSettings();
 *
 * const handleSetSetting = () => {
 *   setUserSetting('theme', 'dark');
 * };
 *
 * @example
 * // Deleting a User Setting
 * const { deleteUserSetting } = useUserSettings();
 *
 * const handleDeleteSetting = () => {
 *   deleteUserSetting('theme');
 * };
 *
 * @example
 * // Full Example
 * import React, { useEffect } from 'react';
 * import { useUserSettings } from './path/to/UserSettings';
 *
 * const UserSettingsComponent = () => {
 *   const { userSettings, setUserSetting, deleteUserSetting } = useUserSettings();
 *
 *   useEffect(() => {
 *     console.log('User Settings:', userSettings);
 *   }, [userSettings]);
 *
 *   const handleSetSetting = () => {
 *     setUserSetting('theme', 'dark');
 *   };
 *
 *   const handleDeleteSetting = () => {
 *     deleteUserSetting('theme');
 *   };
 *
 *   return (
 *     <div>
 *       <h1>User Settings</h1>
 *       <button onClick={handleSetSetting}>Set Theme to Dark</button>
 *       <button onClick={handleDeleteSetting}>Delete Theme Setting</button>
 *     </div>
 *   );
 * };
 *
 * export default UserSettingsComponent;
 */

const useUserSettings = __ARKEOGIS__
  ? () => {
      const { data, loading, error } = useQuery(GetUserSettings);
      const [mutation_setUserSetting] = useMutation(SetUserSetting);
      const [mutation_deleteUserSetting] = useMutation(DeleteUserSetting);
      const [userSettings, setUserSettings] = useState([]);
      const user = useAuthStore((state) => state.user);

      useEffect(() => {
        if (data)
          setUserSettings(
            data[`${ak}user_preferences`].reduce((obj, item) => {
              obj[item.key] = item.value;
              return obj;
            }, {})
          );
        else setUserSettings([]);
      }, [data]);

      const setUserSetting = (key, value) => {
        //console.log("setting user setting", key, value);
        mutation_setUserSetting({
          variables: {
            object: {
              user_id: user.id,
              key,
              value,
            },
          },
        });
      };

      const deleteUserSetting = (key) => {
        //console.log("deleting user setting", key);
        mutation_deleteUserSetting({
          variables: {
            user_id: user.id,
          },
        });
      };

      return { userSettings, setUserSetting, deleteUserSetting };
    }
  : undefined;

export default __ARKEOGIS__ ? useUserSettings : undefined;
