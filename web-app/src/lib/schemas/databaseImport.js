import { z } from "zod";
import config from "../../config.json";
import { t, getFixedT } from "i18next";

import { getApolloClient } from "../../components/ApolloSetup/ApolloSetup.jsx";
import { GetAllCharacsT } from "../queries/charac.js";
import { getCityAndCountryByGeonameId } from "../queries/geo.js";
import { asyncPipe, isValidNumber } from "../utils.js";

// CREATE EXTENSION IF NOT EXISTS unaccent;

/*
  t("import.boolean.yes");
  t("import.boolean.no");
  t("import.period.undetermined")
  t("import.occupation.continuous");
  t("import.occupation.multiple");
  t("import.occupation.not_documented");
  t("import.occupation.multiple");
  t("import.occupation.unique");
  t("import.knowledge_type.dig");
  t("import.knowledge_type.literature");
  t("import.knowledge_type.not_documented");
  t("import.knowledge_type.prospected_pedestrian");
  t("import.knowledge_type.prospected_aerial");
  t("import.knowledge_type.surveyed");
*/

const geonameCache = {}; // format {geonameid: int, latitude: float, longitude: float, cityName: string, countryName: string}

const characSeparator = " -> ";
async function buildCharacPaths(lang = "fr") {
  const result = {};

  const apolloClient = getApolloClient();
  const data = await apolloClient.query({
    query: GetAllCharacsT,
    variables: { lang },
  });

  function traverse(node, currentPath) {
    // Extract the name if it exists
    let newPath = "";

    if (node.charac_trs && node.charac_trs.length > 0 && node.charac_trs[0].name) {
      const name = node.charac_trs[0].name;
      newPath = currentPath ? `${currentPath}${characSeparator}${name}` : name;
      // result.push(newPath);
      result[newPath] = node.id;
    }

    // Traverse children if they exist
    if (node.characs && node.characs.length > 0) {
      node.characs.forEach((child) => traverse(child, newPath));
    }
  }

  traverse({ characs: data.data.akg_charac }, "");

  return result;
}

const isUnset = (val) => val === null;

const validateGeoname = async (context) => {
  const { line, lineNum, lang, errors } = context;
  const { SITE_SOURCE_ID, GEONAME_ID } = line;
  if (isValidNumber(GEONAME_ID)) {
    const apolloClient = getApolloClient();
    const data = await apolloClient.query({
      query: getCityAndCountryByGeonameId,
      variables: { lang, geonameid: GEONAME_ID },
    });
    if (!data.data.akg_city) {
      errors.push({
        path: ["GEONAME_ID"],
        message: t("import.error.GEONAME_ID.doesNotExist"),
        code: z.ZodIssueCode.custom,
        meta: {
          lineNum,
          siteSourceId: SITE_SOURCE_ID,
          value: "",
        },
      });
    }
  }
  return { ...context, errors };
};

const validateLocalisationInformations = (context) => {
  const { line, lineNum, errors } = context;
  const { SITE_SOURCE_ID, GEONAME_ID, LATITUDE, LONGITUDE, ALTITUDE } = line;
  if (isUnset(LATITUDE) && LONGITUDE) {
    errors.push({
      path: ["LATITUDE"],
      message: t("import.error.LONLAT.latMandatoryIfLon"),
      code: z.ZodIssueCode.custom,
      meta: {
        lineNum,
        siteSourceId: SITE_SOURCE_ID,
        value: "",
      },
    });
  } else if (LATITUDE && isUnset(LONGITUDE)) {
    errors.push({
      path: ["LONGITUDE"],
      message: t("import.error.LONLAT.lonMandatoryIfLat"),
      code: z.ZodIssueCode.custom,
      meta: {
        lineNum,
        siteSourceId: SITE_SOURCE_ID,
        value: "",
      },
    });
  } else if (ALTITUDE && (isUnset(LATITUDE) || isUnset(LONGITUDE))) {
    errors.push({
      path: ["ALTITUDE"],
      message: "ALTITUDE cannot be set without both LONGITUDE and LATITUDE.",
      message: t("import.error.ALTITUDE.needsLonLat"),
      code: z.ZodIssueCode.custom,
      meta: {
        lineNum,
        siteSourceId: SITE_SOURCE_ID,
        value: "",
      },
    });
  }
  if (!GEONAME_ID && isUnset(LATITUDE) && isUnset(LONGITUDE)) {
    errors.push({
      path: ["GEONAME_ID", "LATITUDE", "LONGITUDE"],
      message: "Provide GEONAME_ID or a couple of LONGITUDE and LATITUDE",
      message: t("import.error.GEONAME_ID.orLonLat"),
      code: z.ZodIssueCode.custom,
      meta: {
        lineNum,
        siteSourceId: SITE_SOURCE_ID,
        value: "",
      },
    });
  }
  return { ...context, errors };
};

const validateProjectionSystem = (context) => {
  const { line, lineNum, errors } = context;
  const { PROJECTION_SYSTEM, SITE_SOURCE_ID } = line;

  const findSupportedEpsg = (epsgCode) => {
    const result = config.import.epsg.find((epsgList) => epsgList[epsgCode] !== undefined);
    return result ? result[epsgCode] : null;
  };

  if (findSupportedEpsg(PROJECTION_SYSTEM) === null) {
    errors.push({
      path: ["PROJECTION_SYSTEM"],
      message: t("import.error.PROJECTION_SYSTEM.notSupported"),
      code: z.ZodIssueCode.custom,
      meta: {
        lineNum,
        siteSourceId: SITE_SOURCE_ID,
        value: PROJECTION_SYSTEM,
      },
    });
  }
  return { ...context, errors };
};

const validateCityCentroid = (context) => {
  const { line, lineNum, errors, translateValue } = context;
  const { CITY_CENTROID, SITE_SOURCE_ID } = line;
  if (![translateValue("import.boolean.yes"), translateValue("import.boolean.no")].includes(CITY_CENTROID)) {
    errors.push({
      path: ["CITY_CENTROID"],
      message: t("import.error.generic.invalidEnum"),
      code: z.ZodIssueCode.invalid_enum_value,
      meta: {
        lineNum,
        siteSourceId: SITE_SOURCE_ID,
        value: CITY_CENTROID,
      },
    });
  }
  return { ...context, errors };
};

const validateKnowledgeType = (context) => {
  const { line, lineNum, errors, translateValue } = context;
  const { STATE_OF_KNOWLEDGE, SITE_SOURCE_ID } = line;
  if (
    ![
      translateValue("import.knowledge_type.dig"),
      translateValue("import.knowledge_type.literature"),
      translateValue("import.knowledge_type.not_documented"),
      translateValue("import.knowledge_type.prospected_pedestrian"),
      translateValue("import.knowledge_type.prospected_aerial"),
      translateValue("import.knowledge_type.surveyed"),
    ].includes(STATE_OF_KNOWLEDGE)
  ) {
    errors.push({
      path: ["STATE_OF_KNOWLEDGE"],
      message: t("import.error.generic.invalidEnum"),
      code: z.ZodIssueCode.invalid_enum_value,
      meta: {
        lineNum,
        siteSourceId: SITE_SOURCE_ID,
        value: STATE_OF_KNOWLEDGE,
      },
    });
  }
  return { ...context, errors };
};

const validateOccupation = (context) => {
  const { line, lineNum, errors, translateValue } = context;
  const { OCCUPATION, SITE_SOURCE_ID, STATE_OF_KNOWLEDGE } = line;
  if (
    ![
      translateValue("import.occupation.continuous"),
      translateValue("import.occupation.multiple"),
      translateValue("import.occupation.not_documented"),
      translateValue("import.occupation.unique"),
    ].includes(OCCUPATION)
  ) {
    errors.push({
      path: ["OCCUPATION"],
      message: t("import.error.generic.invalidEnum"),
      code: z.ZodIssueCode.invalid_enum_value,
      meta: {
        lineNum,
        siteSourceId: SITE_SOURCE_ID,
        value: STATE_OF_KNOWLEDGE,
      },
    });
  }
  return { ...context, errors };
};

const validatePeriods = (context) => {
  const { line, lineNum, errors, translateValue } = context;
  const { STARTING_PERIOD, ENDING_PERIOD, SITE_SOURCE_ID } = line;
  const undetermined = translateValue("import.period.undetermined");
  const regex = new RegExp(`^[+-]?\\d+(:[+-]?\\d+)?$|^${undetermined}$`, "i");
  if (!regex.test(STARTING_PERIOD)) {
    errors.push({
      path: ["STARTING_PERIOD"],
      message: t("import.error.STARTING_PERIOD.invalidValue"),
      code: z.ZodIssueCode.custom,
      meta: {
        lineNum,
        siteSourceId: SITE_SOURCE_ID,
        value: STARTING_PERIOD,
      },
    });
  }
  if (!regex.test(ENDING_PERIOD)) {
    errors.push({
      path: ["ENDING_PERIOD"],
      message: t("import.error.ENDING_PERIOD.invalidValue"),
      code: z.ZodIssueCode.custom,
      meta: {
        lineNum,
        value: ENDING_PERIOD,
      },
    });
  }
  return { ...context, errors };
};
const validateCharacs = (context) => {
  const { line, lineNum, errors, arkeoCharacs } = context;
  const { CARAC_NAME, CARAC_LVL1, CARAC_LVL2, CARAC_LVL3, CARAC_LVL4, SITE_SOURCE_ID } = line;
  const filteredCharacs = [CARAC_NAME, CARAC_LVL1, CARAC_LVL2, CARAC_LVL3, CARAC_LVL4].filter((value) => value);
  const characLevels = filteredCharacs.map((_, index) => filteredCharacs.slice(0, index + 1).join(characSeparator));
  const fieldNames = ["CARAC_NAME", "CARAC_LVL1", "CARAC_LVL2", "CARAC_LVL3", "CARAC_LVL4"];

  // Validate characs name and levels
  characLevels.some((characLvl, index) => {
    if (!Object.hasOwn(arkeoCharacs, characLvl)) {
      errors.push({
        path: [fieldNames[index]],
        message: t(`import.error.CHARAC.doesNotExist`),
        code: z.ZodIssueCode.custom,
        meta: {
          lineNum,
          siteSourceId: SITE_SOURCE_ID,
          value: characLvl,
        },
      });
      return true;
    }
  });

  return { ...context, errors };
};

const validateCharacExp = (context) => {
  const { line, lineNum, errors, translateValue } = context;
  const { CARAC_EXP, STATE_OF_KNOWLEDGE, SITE_SOURCE_ID } = line;

  // Validate charac exp
  if (![translateValue("import.boolean.yes"), translateValue("import.boolean.no")].includes(CARAC_EXP)) {
    errors.push({
      path: ["CARAC_EXP"],
      message: t("import.error.generic.invalidEnum"),
      code: z.ZodIssueCode.invalid_enum_value,
      meta: {
        lineNum,
        siteSourceId: SITE_SOURCE_ID,
        value: CARAC_EXP,
      },
    });
  }

  // Charac exp can not be exceptional if the knowledge type is not_documented
  if (
    CARAC_EXP === translateValue("import.boolean.yes") &&
    STATE_OF_KNOWLEDGE === translateValue("import.knowledge_type.not_documented")
  ) {
    errors.push({
      path: ["CARAC_EXP"],
      message: t("import.error.CARAC_EXP.cantBeExceptionalIfNotDocumented"),
      code: z.ZodIssueCode.custom,
      meta: {
        lineNum,
        siteSourceId: SITE_SOURCE_ID,
        value: CARAC_EXP,
      },
    });
  }

  return { ...context, errors };
};

const validateWebImages = (context) => {
  const { line, lineNum, errors } = context;
  const { WEB_IMAGES, SITE_SOURCE_ID } = line;

  if (!WEB_IMAGES) {
    return context;
  }

  // Split the input into an array based on commas
  const urls = WEB_IMAGES.split(",").map((url) => url.trim());

  if (Array.isArray(urls)) {
    const urlPattern = /^(https?:\/\/[^\s]+)$/i;

    const isValid = urls.every((url) => {
      if (urlPattern.test(url)) {
        return true;
      }
    });

    if (!isValid) {
      errors.push({
        path: ["WEB_IMAGES"],
        message: t("import.error.WEB_IMAGES.invalidValue"),
        code: z.ZodIssueCode.custom,
        meta: {
          lineNum,
          siteSourceId: SITE_SOURCE_ID,
          value: WEB_IMAGES,
        },
      });
    }
  }

  // Validate each URL using a regex pattern for HTTP/HTTPS URLs

  return { ...context, errors };
};

const validateSitesWithSameId = (lines) => {
  const firstEntry = lines[0];
  const otherEntries = lines.slice(1);
  const errors = [];
  const fields = [
    "SITE_NAME",
    "LOCALISATION",
    "CITY_CENTROID",
    "LONGITUDE",
    "LATITUDE",
    "ALTITUDE",
    "GEONAME_ID",
    "OCCUPATION",
  ];

  otherEntries.forEach((line) => {
    // console.log("L", line.lineNum);
    fields.forEach((field) => {
      // console.log(field, line.lineNum, line[field], firstEntry[field]);
      if (line[field] !== "" && line[field] !== firstEntry[field]) {
        errors.push({
          path: [field],
          message: t("import.error.generic.valueDiffers"),
          code: z.ZodIssueCode.custom,
          meta: {
            lineNum: line.lineNum,
            siteSourceId: line.SITE_SOURCE_ID,
            value: line[field],
          },
        });
      }
    });
  });

  return errors;
};

// Zod definition

const customErrorMap = (issue, ctx) => {
  if (issue.code === z.ZodIssueCode.invalid_type) {
    return { message: t("import.error.generic.invalidValue") };
  }
  return { message: ctx.defaultError };
};

z.setErrorMap(customErrorMap);

export const importCsvLineSchema = z.object({
  SITE_SOURCE_ID: z
    .string()
    .trim()
    .regex(/^[a-zA-Z0-9](?:[a-zA-Z0-9-_]*[a-zA-Z0-9])?$/, { message: t("import.error.SITE_SOURCE_ID.invalidValue") })
    .min(1, {
      message: t("import.error.SITE_SOURCE_ID.notEmpty"),
    }),
  SITE_NAME: z.string().trim().nullable(),
  LOCALISATION: z.string().trim().nullable(),
  GEONAME_ID: z.coerce.number().int().nullable(),
  PROJECTION_SYSTEM: z.number().positive().finite(),
  LONGITUDE: z.coerce.number().nullable(),
  LATITUDE: z.coerce.number().nullable(),
  ALTITUDE: z.coerce.number().nullable(),
  CITY_CENTROID: z.string().trim().optional(),
  STATE_OF_KNOWLEDGE: z.string().trim(),
  OCCUPATION: z.string().trim(),
  STARTING_PERIOD: z.coerce.string().trim(),
  ENDING_PERIOD: z.coerce.string().trim(),
  CARAC_NAME: z.string().trim(),
  CARAC_LVL1: z.string().trim(),
  CARAC_LVL2: z.string().trim().nullable(),
  CARAC_LVL3: z.string().trim().nullable(),
  CARAC_LVL4: z.string().trim().nullable(),
  CARAC_EXP: z.string().trim(),
  BIBLIOGRAPHY: z
    .string()
    .trim()
    .max(2048, { message: t("import.error.generic.tooLong") })
    .nullable(),
  COMMENTS: z
    .string()
    .trim()
    .max(2048, { message: t("import.error.generic.tooLong") })
    .nullable(),
  WEB_IMAGES: z
    .string()
    .max(500)
    .trim()
    .max(2048, { message: t("import.error.generic.tooLong") })
    .nullable(),
});

export const buildImportCsvFileSchemaFromLang = (lang) =>
  z
    .array(z.any() /* We use z.any() here to prevents that the validation stops, and check them with superRefine */)
    .superRefine(async (lines, ctx) => {
      // Get translations in the selected lang
      const translateValue = getFixedT(lang);

      const arkeoCharacs = await buildCharacPaths(lang);

      const groupedBySourceId = {};

      // validateDatabaseName(databaseName) TODO with graphql query to check if exists.

      for (const [index, line] of lines.entries()) {
        // Validate each line using importCsvLineSchema
        const result = importCsvLineSchema.safeParse(line);
        const lineNum = index + 2;
        if (!result.success) {
          result.error.errors.forEach((error) => {
            ctx.addIssue({
              ...error,
              meta: {
                lineNum,
                siteSourceId: line.SITE_SOURCE_ID || "",
                value: line[error.path[0]],
              },
            });
          });
        }

        // Validate each line using custom validators
        const { errors } = await asyncPipe(
          validateLocalisationInformations,
          validateGeoname,
          validateProjectionSystem,
          validateCityCentroid,
          validateKnowledgeType,
          validateOccupation,
          validatePeriods,
          validateCharacs,
          validateCharacExp,
          validateWebImages
        )({ line, lineNum, lang, translateValue, arkeoCharacs, errors: [] });
        // Set the zod errors from the custom validators
        errors.forEach((error) => {
          ctx.addIssue(error);
        });

        // Group lines by SITE_SOURCE_ID
        const id = line.SITE_SOURCE_ID;
        if (id) {
          if (!groupedBySourceId[id]) {
            groupedBySourceId[id] = [];
          }
          // await validateCharacs(line, characNameTree);
          groupedBySourceId[id].push({ ...line, lineNum });
        }
        // Compare and validate lines grouped by SITE_SOURCE_ID
      }
      const groupError = [];
      for (const [id, group] of Object.entries(groupedBySourceId)) {
        groupError.push(...validateSitesWithSameId(group));
      }
      groupError.forEach((error) => {
        ctx.addIssue(error);
      });
    });

export const databaseImportStep1And2Schema = z.object({
    name: z.string().min(1).max(75),
    default_language: z.enum(config.supportedLanguages.arkeogis).default("fr"),
    csvLines: z.array(importCsvLineSchema).optional(),
    csvValidatedLines: z.array(importCsvLineSchema).optional(),
    csvErrors: z.array(z.any()).optional(),
})

export const databaseImportStep3Schema = z.object({
  authors: z.array(z.number().int()).default(),
  license: z.number().int().default(),
  editor: z.string().min(1).max(75),
  context: z.array(z.enum(["undefined", "academic-work", "contract", "research_team", "other"])),
  context_description: z.string().max(500).optional(),
  declared_creation_date: z.date().optional(),
  contributor: z.string().max(500).optional(),
  editor_url: z.string().url().min(1).max(255),
});

export const databaseImportStep4Schema = z.object({
    type: z.enum(["undefined", "inventory", "research", "literary-work"]),
    scale_resolution: z.enum([
      "undefined",
      "object",
      "site",
      "watershed",
      "micro-region",
      "region",
      "country",
      "continent",
      "world",
    ]), // ????
    state: z.enum(["undefined", "in-progress", "finished"]),
    database_collection_id: z.number().int(),
    root_chronology_id: z.number().int(),
    geographical_extent: z.enum(["undefined", "country", "continent", "international_waters", "world"]),
    translation_lang: z.enum(config.supportedLanguages.arkeogis),
    geographical_limit: z.string().max(500),
    geographical_limit_translation: z.string().max(500),
    subject: z.string().max(500), // mots cles
    subject_translated: z.string().max(500), // mots cles
    description: z.string().max(2048), // mots cles
    description_translated: z.string().max(2048), // mots cles
    illustrations: z.string().max(500), // mots cles
    published: z.boolean(),
});

export const databaseImportStep5Schema = z.object({
})

export const databaseImportSchema = z
  .object({})
  .merge(databaseImportStep1And2Schema)
  .merge(databaseImportStep3Schema)
  .merge(databaseImportStep4Schema)
  .merge(databaseImportStep5Schema)
  .strict();
