import { z } from "zod";
import config from "../../config.json";

export const storeSchema = z.object({
  basemap: z.string().default(config.map.basemap),
  sidePanelOpened: z.string().default("none"),
  leftPanel: z.string().default("search"), // "search", "requests", "bookmarks"
  rightPanel: z.object({ type: z.string(), id: z.coerce.string() }).default({ type: "help", id: "interface" }),
  hideRightSidePanelHandle: z.boolean().default(false),
  showWelcomeModal: z.boolean().default(true),
  showBurgerModal: z.boolean().default(false),
  lng: z.number().optional().default(config.map.longitude),
  lat: z.number().optional().default(config.map.latitude),
  zoom: z.number().optional().default(config.map.zoom),
  flyTo: z.object({ lng: z.number(), lat: z.number() }).nullable().default(null),
  lastSiteIdSelected: z.number().int().nullable().default(null),
  ungrouped: z.boolean().default(false),
  geoLocateTrigger: z.number().int().default(0),
  mapObject: z.any().nullable().default(null),
  mapUpdated: z.number().int().default(0),
  allMapDrawed: z.boolean().default(false),
  firstMapDrawed: z.boolean().default(false),
  // cached graphql datas
  characs: z.any().array().default([]),
  chronologies: z.any().array().default([]),
  shapefiles: z.any().array().default([]),
  zoneDrawing: z.string().nullable().default(null),
  arkeoLayers: z.any().array().default([]),
});
