import * as z from "zod";

export const userSchema = z.object({
  id: z.number().nonnegative(),
  username: z.string().max(32).min(1),
  firstname: z.string().max(32),
  lastname: z.string().max(32),
  email: z.string().email().max(255),
  password: z.string().min(6),
  description: z.string(),
  active: z.boolean(),
  first_lang_isocode: z.string().max(2).min(2),
  second_lang_isocode: z.string().max(2).min(2),
  city_geonameid: z.number().nonnegative(),
  photo_id: z.number().nonnegative(),
  created_at: z.string().datetime(),
  updated_at: z.string().datetime(),
  company: z.object({
    id: z.number(),
    name: z.string().max(255),
    city_geonameid: z.number(),
  }),
});

export const authSchema = z.object({
  isAuthenticated: z.boolean().default(false),
  token: z.string().nullable().default(null),
  username: userSchema.shape.username.nullable().default(null),
  password: userSchema.shape.password.nullable().default(null),
  user: z.any().nullable().default(null),
  project: z.any().default(null),
  reloadProject: z.number().default(0),
});
