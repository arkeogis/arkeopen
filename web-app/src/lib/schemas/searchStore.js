import { z } from "zod";
import config from "../../config.json";

export const searchStoreSchema = z.object({
  characSelection: z.number().int().optional().array().default([]),
  characSelectionCompacted: z.number().int().optional().array().default([]),
  selectedChronologyId: z.number().int().default(config.chronology.default),
  chronologyStartDate: z.coerce.number().int().default(-2147483648),
  chronologyEndDate: z.coerce.number().int().default(2147483647),
  chronologyFindIncludeUndetermined: z.boolean().default(true),
  chronologyFindOnlyInside: z.boolean().default(false),
  selectedShapefiles: z.number().int().array().default([]),
  knowledgeTypes: z.array(z.enum(config.knowledgeTypes)).default([]),
  occupations: z.array(z.enum(config.occupations)).default([]),
  datasetTypes: z.array(z.enum(config.datasetTypes)).default([]),
  scaleResolutions: z.array(z.string()).default([]),
  centroid: z.boolean().nullable().default(false),
  exceptional: z.boolean().nullable().default(false),
  illustrated: z.boolean().nullable().default(false),
  editors: z.string().array().default([]),
  authors: z.number().int().array().default([]),
  databases: z.number().int().array().default([]),
  textual: z.string().default(""),
  textualOn: z.enum(config.textualOn).default("all"),
  zone: z.any().default(null),
  searchCount: z.number().int().default(0),
});
