import { authSchema } from "./schemas/auth";
import { persist } from "zustand/middleware";
import { createValidatedStore } from "./utils";

export const useAuthStore = createValidatedStore(
  authSchema,
  persist(
    (set) => ({
      setLoggedIn: (token, user) => set({ token, user, isAuthenticated: true }),
      setLoggedOut: () => set({ token: null, user: null, project: null, isAuthenticated: false }),
      /**
       * the user project
       */
      setProject: (project) => {
        set({ project });
      },
      askProjectReload: () => {
        set((state) => {
          return { reloadProject: state.reloadProject + 1 };
        });
      },
    }),
    {
      name: "user-storage", // key for localStorage
      partialize: (state) => ({ token: state.token, isAuthenticated: state.isAuthenticated, user: state.user }), // store variables stored in localstorage
    }
  )
);

let authAbortController = null;

export const checkAuth = async (username, password) => {
  // Cancel the previous request if it exists
  if (authAbortController) {
    authAbortController.abort();
  }

  // Create a new abort controller for the current request
  authAbortController = new AbortController();

  try {
    const response = await fetch(`${import.meta.env.VITE_SERVER_URI}/rest/auth/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ username, password }),
      signal: authAbortController.signal, // attach the signal for cancellation
    });

    if (response.status === 401) {
      // Handle unauthorized
      console.log("Login failed: Unauthorized");
      return { token: null, user: null };
    } else if (response.ok) {
      const data = await response.json();
      if (!data.token) {
        console.error("No token has been returned by the server.");
        return { token: null, user: null };
      }
      return { token: data.token, user: data.user };
    } else {
      // Handle other errors
      console.log(`Login failed: ${response.statusText}`);
      return { token: null, user: null };
    }
  } catch (error) {
    if (error.name === "AbortError") {
      console.log("Previous request aborted");
    } else {
      console.error("An unexpected error occurred during login phase:", error);
    }
    return { token: null, user: null };
  }
};
