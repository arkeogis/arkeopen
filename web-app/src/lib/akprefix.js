import { IS_ARKEOGIS, IS_ARKEOPEN } from "./utils";

export const ak = __ARKEOPEN__ ? "ako_" : __ARKEOGIS__ ? "akg_" : "";

export default ak;
