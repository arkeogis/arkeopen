import { cloneDeep } from "lodash";
import { compactCharacsSelection, decompactCharacsSelection } from "./characsSelection";
import { searchStoreSchema } from "./schemas/searchStore";
import { storeSchema } from "./schemas/store";
import { createValidatedStore } from "./utils";

export const UNDETERMINED_LEFT = -2147483648;
export const UNDETERMINED_RIGHT = 2147483647;

/* *********************** */
/*     The Main store      */
/* *********************** */

const useStore = createValidatedStore(storeSchema, (set, get) => ({
  setBasemap: (basemap) => set({ basemap }),
  setSidePanelOpened: (sidePanelOpened) => set({ sidePanelOpened }),
  setLeftPanel: (leftPanel) => set({ leftPanel }),
  setRightPanel: (rightPanel, openit = true) =>
    set((state) => {
      // make sure that if rightPanel is of type 'site', it's id is of type number
      const ret = {};
      const oldRightPanel = state.rightPanel;
      if (rightPanel && rightPanel.type === "site") {
        rightPanel = { ...rightPanel, id: parseInt(rightPanel.id) };
      }
      if (oldRightPanel && oldRightPanel.type === "site" && oldRightPanel.id) {
        if (
          !rightPanel ||
          rightPanel.type !== "site" ||
          (rightPanel.type === "site" && rightPanel.id !== oldRightPanel.id)
        ) {
          ret.lastSiteIdSelected = oldRightPanel.id;
        }
      }
      ret.rightPanel = { ...rightPanel };
      if (openit) {
        ret.showBurgerModal = false;
        ret.sidePanelOpened = "right";
      }
      return ret;
    }),
  setProjectPage: (id) => {
    get().setRightPanel({ type: "project", id }, true);
  },
  setHideRightSidePanelHandle: (hideRightSidePanelHandle) => set({ hideRightSidePanelHandle }),
  setHelpPage: (id) => {
    get().setRightPanel({ type: "help", id }, true);
  },
  setShowWelcomeModal: (showWelcomeModal) => {
    if (showWelcomeModal === true) set({ showWelcomeModal, sidePanelOpened: "none" });
    else set({ showWelcomeModal });
  },
  setShowBurgerModal: (showBurgerModal) => set({ showBurgerModal }),
  setLng: (lng) => set({ lng }),
  setLat: (lat) => set({ lat }),
  setZoom: (zoom) => set({ zoom }),
  setFlyTo: (flyTo) => set({ flyTo }),
  setLastSiteIdSelected: (lastSiteIdSelected) => set({ lastSiteIdSelected }),
  setUngrouped: (ungrouped) => set({ ungrouped }),
  triggerGeoLocate: () =>
    set((state) => {
      return { geoLocateTrigger: state.geoLocateTrigger + 1 };
    }),
  setMapObject: (mapObject) =>
    set((state) => {
      return { mapObject, mapUpdated: state.mapUpdated + 1 };
    }),
  setMapUpdated: () =>
    set((state) => {
      return { mapUpdated: state.mapUpdated + 1 };
    }),
  setAllMapDrawed: (allMapDrawed) => set({ allMapDrawed }),
  setFirstMapDrawed: (firstMapDrawed) => set({ firstMapDrawed }),
  /**
   * liste of all characs
   */
  setCharacs: (characs) => {
    set({ characs });
  },
  /**
   * liste of all chronologies
   */
  setChronologies: (chronologies) => {
    set({ chronologies });
  },
  /**
   * liste of all shapefiles
   */
  setShapefiles: (shapefiles) => {
    set({ shapefiles });
  },
  setZoneDrawing: (zoneDrawing) => {
    set({ zoneDrawing });
  },
  setArkeoLayers: (arkeoLayers) => {
    set({ arkeoLayers });
  },
  removeArkeoLayerByInstance: (arkeoLayer) => {
    set((state) => ({
      arkeoLayers: state.arkeoLayers.filter((e) => e !== arkeoLayer),
    }));
  },
}));

/* ***********************************/
/*         The Search store          */
/*  And the Validated Search store   */
/* ***********************************/

const commonStoreActions = (set) => ({
  /**
   * Charac selection in the menus
   */
  setCharacSelection: (characSelection) =>
    set({
      characSelection,
      characSelectionCompacted: compactCharacsSelection(characSelection),
    }),
  /**
   * Charac selection in the menus, compacted version
   */
  setCharacSelectionCompacted: (characSelectionCompacted) => {
    set({
      characSelectionCompacted,
      characSelection: decompactCharacsSelection(characSelectionCompacted),
    });
  },
  //selectedChronologyId: defaults.selectedChronologyId,
  setSelectedChronologyId: (selectedChronologyId) => set({ selectedChronologyId }),
  /**
   * chronologyStartDate: a year number
   */
  setChronologyStartDate: (chronologyStartDate) => set({ chronologyStartDate }),
  /**
   * chronologyEndDate: a year number
   */
  setChronologyEndDate: (chronologyEndDate) => set({ chronologyEndDate }),
  /**
   * setChronologyStartEndDate: set start and end date (the two previous functions in one)
   * @param {number} chronologyStartDate
   * @param {number} chronologyEndDate
   */
  setChronologyStartEndDate: (chronologyStartDate, chronologyEndDate) =>
    set({ chronologyStartDate, chronologyEndDate }),
  /**
   * chronologyFindIncludeUndetermined: boolean
   */
  setChronologyFindIncludeUndetermined: (chronologyFindIncludeUndetermined) =>
    set({ chronologyFindIncludeUndetermined }),
  /**
   * chronologyFindOnlyInside: boolean
   */
  setChronologyFindOnlyInside: (chronologyFindOnlyInside) => set({ chronologyFindOnlyInside }),
  /**
   * The selected shapefiles (array of id)
   */
  setSelectedShapefiles: (selectedShapefiles) => set({ selectedShapefiles }),
  /**
   * knowledgeTypes: array of words ('not-documented', 'literature', 'dig', ...)
   */
  setKnowledgeTypes: (knowledgeTypes) => set({ knowledgeTypes }),
  updateKnowledgeTypes: (sel, knowledgeType) =>
    set((state) => {
      return {
        knowledgeTypes: sel
          ? [...state.knowledgeTypes, knowledgeType]
          : state.knowledgeTypes.filter((v) => v !== knowledgeType),
      };
    }),
  /**
   * occupations: array of words ('not_documented', 'single', 'continuous', 'multiple')
   */
  setOccupations: (occupations) => set({ occupations }),
  updateOccupations: (sel, knowledgeType) =>
    set((state) => {
      return {
        occupations: sel ? [...state.occupations, knowledgeType] : state.occupations.filter((v) => v !== knowledgeType),
      };
    }),
  /**
   * datasetTypes: array of words ('research', 'literary-work', 'invotory', 'undefined')
   */
  setDatasetTypes: (datasetTypes) => set({ datasetTypes }),
  updateDatasetTypes: (sel, datasetType) =>
    set((state) => {
      return {
        datasetTypes: sel ? [...state.datasetTypes, datasetType] : state.datasetTypes.filter((v) => v !== datasetType),
      };
    }),
  /**
   * scaleResolutions: array of words ('region', 'world', ...)
   */
  setScaleResolutions: (scaleResolutions) => set({ scaleResolutions }),
  updateScaleResolutions: (sel, scaleResolutions) =>
    set((state) => {
      return {
        scaleResolutions: sel
          ? [...state.scaleResolutions, scaleResolutions]
          : state.scaleResolutions.filter((v) => v !== scaleResolutions),
      };
    }),
  /**
   * centroid: boolean or null
   */
  setCentroid: (centroid) => set({ centroid }),
  /**
   * exceptional: boolean or null
   */
  setExceptional: (exceptional) => set({ exceptional }),
  /**
   * illustrated: boolean or null
   */
  setIllustrated: (illustrated) => set({ illustrated }),
  /**
   * editors: array of strings ('University of ...')
   */
  setEditors: (editors) => set({ editors }),
  updateEditors: (sel, editor) =>
    set((state) => {
      return {
        editors: sel ? [...state.editors, editor] : state.editors.filter((v) => v !== editor),
      };
    }),
  /**
   * authors: array of ids
   */
  setAuthors: (authors) => set({ authors }),
  updateAuthors: (sel, author) =>
    set((state) => {
      return {
        authors: sel ? [...state.authors, author] : state.authors.filter((v) => v !== author),
      };
    }),
  /**
   * databases: array of ids
   */
  setDatabases: (databases) => set({ databases }),
  updateDatabases: (sel, database) =>
    set((state) => {
      return {
        databases: sel ? [...state.databases, database] : state.databases.filter((v) => v !== database),
      };
    }),
  /**
   * textual: search string
   */
  setTextual: (textual) => set({ textual }),
  /**
   * textual: enum of ('all', 'dataset-name', 'resource-name', 'comments', 'bibliography', 'city-name')
   */
  setTextualOn: (textualOn) => set({ textualOn }),
  /**
   * zone: a GeoJSON with a geometrical zone used as filter. null if no zone
   */
  setZone: (zone) => set({ zone }),
  /**
   * Copy a store state (only properties) from another store
   */
  copyStateFromStore: (storeToCopy) => {
    const copy = Object.fromEntries(
      Object.entries(storeToCopy.getState()).filter(([_, value]) => typeof value !== "function")
    );
    set({ ...copy });
  },
});

export const useSearchStore = createValidatedStore(searchStoreSchema, (set) => ({
  ...cloneDeep(commonStoreActions(set)),
  // Additional actions specific to useSearchStore
  clearSearch: () => {
    set({ ...searchStoreSchema.parse({}) });
  },
}));

export const useValidatedSearchStore = createValidatedStore(searchStoreSchema, (set, get) => ({
  ...cloneDeep(commonStoreActions(set)),
  // Additional actions specific to useValidatedSearchStore
  setGeojsonResult: (geojsonResult) => set({ geojsonResult }),
  validateSearch: () => {
    get().copyStateFromStore(useSearchStore);
    set((state) => ({
      searchCount: state.searchCount + 1,
    }));
  },
}));

export default useStore;
