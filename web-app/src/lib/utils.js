import { create } from "zustand";
import { z } from "zod";

export const IS_ARKEOGIS = () => import.meta.env.VITE_MODE === "arkeogis";
export const IS_ARKEOPEN = () => import.meta.env.VITE_MODE === "arkeopen";
export const isValidFloat = (input) => {
  // Check if the input is a number or a string that can be converted to a number
  if (typeof input === "number") {
    return !isNaN(input) && input % 1 !== 0; // Check if it is not an integer
  }

  if (typeof input === "string") {
    const parsed = parseFloat(input);
    return !isNaN(parsed) && parsed % 1 !== 0;
  }

  return false; // If not a number or string, return false
};

export const isValidNumber = (input) => {
  return typeof input === "number" && !isNaN(input) && isFinite(input);
};

/**
 * Applies multiple functions in sequence to a value.
 *
 * @param {...Function} fns - Functions to be applied in sequence.
 * @param {*} value - The initial value to be processed by the functions.
 * @returns {*} - The final result after applying all functions in sequence.
 * @throws {Error} - If any of the passed arguments is not a function.
 */
export const pipe =
  (...fns) =>
  (value) => {
    for (const fn of fns) {
      if (typeof fn !== "function") {
        throw Error("pipe(): passed argument is not a function");
      }
      const result = fn(value);
      if (result === null) return null;
      value = result;
    }
    return value;
  };

/**
 * Applies multiple asynchronous functions in sequence to a value.
 *
 * @param {...Function} fns - Functions to be applied in sequence.
 * @param {*} value - The initial value to be processed by the functions.
 * @returns {Promise<*>} - A promise that resolves to the final result after applying all functions in sequence.
 * @throws {Error} - If any of the passed arguments is not a function.
 */
export const asyncPipe =
  (...fns) =>
  async (value) => {
    for (const fn of fns) {
      if (typeof fn !== "function") {
        throw Error("asyncPipe(): passed argument is not a function");
      }
      const result = await fn(value);
      if (result === null) return null;
      value = result;
    }
    return value;
  };

function createValidatedStore(schema, createActions, asyncValidation = false) {
  // Extract default values from the Zod schema.
  let effectiveSchema = schema;
  if (effectiveSchema && typeof effectiveSchema === "object" && typeof effectiveSchema.innerType === "function") {
    effectiveSchema = effectiveSchema.innerType();
  }
  if (!effectiveSchema) return {};

  const getInitialState = () =>
    Object.fromEntries(
      Object.entries(effectiveSchema.shape).map(([key, field]) => {
        const defaultValue = field._def.defaultValue;
        const value = typeof defaultValue === "function" ? defaultValue() : defaultValue;
        return [key, value];
      })
    );

  // Helper: compute the new partial state and the merged state.
  const computeStates = (currentState, update) => {
    const newPartial = typeof update === "function" ? update(currentState) : update;
    return { newPartial, mergedState: { ...currentState, ...newPartial } };
  };

  // Helper: validate using either async or sync Zod methods.
  const validateSchema = async (state, schemaToUse) => {
    if (asyncValidation) {
      return await schemaToUse.safeParseAsync(state);
    }
    return schemaToUse.safeParse(state);
  };

  const initialState = getInitialState();

  // Create the store with a validated setter.
  const useStore = create((set, get, api) => ({
    ...initialState,
    /**
     * Validated setter.
     *
     * Options:
     *   validatePartial -> validate only the keys being updated.
     *
     * Usage:
     *   store.setValidated({ someProp: "newValue" }, false, true);
     */
    setValidated: async (update, validatePartial = true) => {
      const currentState = get();
      const { newPartial, mergedState } = computeStates(currentState, update);

      let schemaToUse;
      let stateToValidate;
      if (validatePartial) {
        // Build a schema that only includes the keys being updated.
        const keysToPick = Object.keys(newPartial).reduce((acc, key) => {
          acc[key] = true;
          return acc;
        }, {});
        schemaToUse = effectiveSchema.pick(keysToPick);
        // Validate only the new partial update.
        stateToValidate = newPartial;
      } else {
        // Validate the entire merged state.
        schemaToUse = effectiveSchema;
        stateToValidate = mergedState;
      }
      const result = await validateSchema(stateToValidate, schemaToUse);
      if (!result.success) {
        const formattedErrors = result.error.issues.reduce(
          (acc, error) => ({ ...acc, [error.path.join("-")]: error.message }),
          {}
        );
        set(newPartial); // Update even if validation fails.
        return { success: false, errors: formattedErrors };
      }
      set(newPartial);
      return { success: true, errors: [] };
    },
    /**
     * Validates the current state of the store.
     * If validation fails, returns an object with success set to false and error messages.
     */
    validateFields: async (fields) => {
      // Filter state to only include keys defined in the schema.
      const keysToPick = fields.reduce((acc, key) => {
        acc[key] = true;
        return acc;
      }, {});
      const pickedSchema = effectiveSchema.pick(keysToPick);
      const fullState = get();
      const stateToValidate = Object.keys(pickedSchema.shape).reduce((acc, key) => {
        acc[key] = fullState[key];
        return acc;
      }, {});
      return await validateSchema(stateToValidate, pickedSchema);
    },
    validatePartialSchema: async (schema) => {
      const fullState = get();
      const stateToValidate = Object.keys(schema.shape).reduce((acc, key) => {
        acc[key] = fullState[key];
        return acc;
      }, {});
      return await validateSchema(stateToValidate, schema);
    },
    ...(typeof createActions === "function" ? createActions(set, get, api) : {}),
  }));

  return useStore;
}

//
/* Crée un store Zustand avec validation asynchrone.
 *
 * @param {z.ZodObject} schema - Le schéma Zod final avec tous les champs obligatoires.
 * @param {Function} [createActions] - Optionnel. Fonction recevant (set, get) et retournant un objet d'actions.
 * @returns {Function} - Le hook useStore du store Zustand.
 */
function createValidatedStoreAsync(schema, createActions) {
  return createValidatedStore(schema, createActions, true);
}

export { createValidatedStore, createValidatedStoreAsync };
