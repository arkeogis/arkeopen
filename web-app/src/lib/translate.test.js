import { getInLang, getInLangWKey } from "./translate";

describe("getInLang", () => {
  it("should return translation object in the preferred language if available", () => {
    const translations = [
      { lang_isocode: "en", text: "Hello" },
      { lang_isocode: "fr", text: "Bonjour" },
      { lang_isocode: "es", text: "Hola" },
    ];
    const result = getInLang(translations, "fr");
    expect(result).toEqual({ lang_isocode: "fr", text: "Bonjour" });
  });

  // Returns null if the list of translations is empty
  it("should return null when the list of translations is empty", () => {
    const translations = [];
    const result = getInLang(translations, "fr");
    expect(result).toBeNull();
  });

  it('should default to "fr" when no preferred language is specified', () => {
    const translations = [
      { lang_isocode: "en", text: "Hello" },
      { lang_isocode: "fr", text: "Bonjour" },
      { lang_isocode: "es", text: "Hola" },
    ];
    const result = getInLang(translations);
    expect(result).toEqual({ lang_isocode: "fr", text: "Bonjour" });
  });

  it("should return the first translation object when preferred language is not found", () => {
    const translations = [
      { lang_isocode: "en", text: "Hello" },
      { lang_isocode: "fr", text: "Bonjour" },
      { lang_isocode: "es", text: "Hola" },
    ];
    const result = getInLang(translations, "de");
    expect(result).toEqual({ lang_isocode: "en", text: "Hello" });
  });
});

describe("getInLangWKey", () => {
  it("should return the string in the primary wanted language if available", () => {
    const trs = [
      { lang_isocode: "fr", key1: "Bonjour" },
      { lang_isocode: "en", key1: "Hello" },
    ];
    const result = getInLangWKey(trs, "key1", "fr");
    expect(result).toBe("Bonjour");
  });

  // emptyIsUndefined mean that the empty answer is considered as undefined,
  // so it will try to find the answer in another language instead of returning an empty string
  it("should return the next language if the key exists but the value is an empty string and emptyIsUndefined is true", () => {
    const trs = [
      { lang_isocode: "fr", key1: "" },
      { lang_isocode: "en", key1: "Hello" },
    ];
    const result = getInLangWKey(trs, "key1", "fr", { emptyIsUndefined: true });
    expect(result).toBe("Hello");
  });

  it("should return undefined when key is not found", () => {
    const translations = [
      { lang_isocode: "en", greeting: "Hello" },
      { lang_isocode: "fr", greeting: "Bonjour" },
      { lang_isocode: "es", greeting: "Hola" },
    ];
    const result = getInLangWKey(translations, "missing_key", "fr");
    expect(result).toBeUndefined();
  });

  // test the getInLangWKey function when the specified language is not found in the translation object (it should return an another object using langprefsorder)
  it("should return another object using langprefsorder when specified language is not found", () => {
    const translations = [
      { lang_isocode: "en", text: "Hello" },
      { lang_isocode: "es", text: "Hola" },
    ];
    const result = getInLangWKey(translations, "text", "fr");
    expect(result).toEqual("Hello");
  });
});
