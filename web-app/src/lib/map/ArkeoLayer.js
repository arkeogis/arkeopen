"use strict";

import { create } from "zustand";
import { ArkeoClustering } from "./ArkeoClustering";
import { ArkeoSpiderfy } from "./ArkeoSpiderfy";
import config from "../../config";
import _ from "underscore";

export class ArkeoLayer {
  constructor(map, id, selectedChronologyId, geojson, ungrouped = false) {
    console.log("creating new ArkeoLayer instance with id : " + id, "ungrouped: ", ungrouped);
    this.map = map;
    this.id = id;
    this.selectedChronologyId = selectedChronologyId;
    this.orig_geojson = geojson;

    this.useStore = create((set) => ({
      ungrouped,
      setUngrouped: (ungrouped) => set(() => ({ ungrouped })),
      hide_database_ids: [],
      setHideDatabaseIds: (hide_database_ids) => set(() => ({ hide_database_ids })),
      name: "My Request " + (this._getNumber() + 1),
    }));

    this.unsub = this.useStore.subscribe((state, old_state) => {
      // hide_database_ids filter update
      if (!_.isEqual(state.hide_database_ids, old_state.hide_database_ids)) {
        this.removeFromMap();
        this.arkeocluster = new ArkeoClustering(this.map, this.id, this.selectedChronologyId);
        this.arkeoClusterInited = false;
        this.addToMap();
      }

      // ungrouped state update
      if (!_.isEqual(state.ungrouped, old_state.ungrouped)) {
        const layers = this.map.getStyle().layers;
        layers.forEach((layer) => {
          if (layer.id.startsWith(`cluster_${this.geojsonResultId}-spiderfy-`)) {
            console.log("spiderfy removing layer : ", layer.id);
            this.map.removeLayer(layer.id);
          }
        });

        //this.arkeospiderfy.removeSpiderfy();
        this.map.getSource(this.id).setClusterOptions({
          cluster: !state.ungrouped,
        });
        //this.arkeospiderfy = new ArkeoSpiderfy(this.map, this.id);
        //this.map.redraw();
      }
    });

    // map source
    this.mapSource = null;

    // arkeo cluster
    this.arkeocluster = new ArkeoClustering(this.map, this.id, this.selectedChronologyId);
    this.arkeoClusterInited = false;

    // arkeo spiderfy
    this.arkeospiderfy = new ArkeoSpiderfy(this.map, this.id);
  }

  dispose() {
    this.unsub();
  }

  setUngrouped(ungrouped) {
    this.useStore.setState({ ungrouped });
  }

  setDatabasesFilter(hide_database_ids) {
    this.useStore.setState({ hide_database_ids });
  }

  hideAllDatabases() {
    const alldbsids = [];
    for (const feature of this.orig_geojson.features) {
      if (!alldbsids.includes(feature.properties.database_id)) alldbsids.push(feature.properties.database_id);
    }
    this.setDatabasesFilter(alldbsids);
  }
  showAllDatabases() {
    this.setDatabasesFilter([]);
  }

  addToMap = () => {
    console.assert(this.mapSource === null, "mapSource not set");
    this.mapSource = this.map.addSource(this.id, this._makeSource());
    this.map.addLayer(this._makeClustersLayer());
    this.map.addLayer(this._makeDiscsLayer());

    // add events listener
    this.mapSource.on("data", this._onSourceAdded);
    //this.map.on('styledata', (e) => console.log("styledata: ", e));
  };

  removeFromMap = () => {
    console.log("removing ArkeoLayer instance with id : " + this.id);
    if (this.mapSource === null) {
      return;
    }
    // remove clusters & spiderfy
    this.arkeospiderfy.removeSpiderfy();
    this.arkeocluster.removeClustersMarkers();

    // remove events listener
    this.mapSource.off("data", this._onSourceAdded);

    // remove layers & source
    this.map.removeLayer("discs_" + this.id);
    this.map.removeLayer("cluster_" + this.id);
    this.map.removeSource(this.id);

    this.mapSource = null;
  };

  _filteredGeoJSON = () => {
    const hide_database_ids = this.useStore.getState().hide_database_ids;
    if (hide_database_ids.length == 0) return this.orig_geojson;
    const geojson = this.orig_geojson;
    const features = geojson.features;
    return {
      ...geojson,
      features: features.filter((feature) => !hide_database_ids.includes(feature.properties.database_id)),
    };
  };

  _makeSource = () => {
    return {
      type: "geojson",
      data: this._filteredGeoJSON(),
      cluster: !this.ungrouped,
      clusterRadius: 60,
      clusterMaxZoom: 22,
      clusterProperties: this.arkeocluster.clusterProperties,
      attribution: config.map.attribution,
    };
  };

  _makeDiscsLayer = () => {
    const iconImageLayout = [
      "concat",
      "arkeo_",
      ["get", "exceptional"],
      "_",
      ["get", "color"],
      "_",
      ["get", "knowledge"],
      "_",
      ["get", "centroid"],
    ];

    if (__ARKEOGIS__) {
      iconImageLayout.push("_" + this._getLetter());
    }

    return {
      id: "discs_" + this.id,
      type: "symbol",
      source: this.id,
      filter: ["!=", "cluster", true],
      layout: {
        "icon-image": iconImageLayout,
        "icon-anchor": ["match", ["get", "centroid"], 1, "center", "bottom"],
        "icon-offset": ["match", ["get", "centroid"], 1, ["literal", [0, 0]], ["literal", [0, 8]]],
        //'icon-size': 0.375,
        "icon-allow-overlap": true, // recommended
        //"text-field": letter,
        //"text-anchor": "top",
        //"text-font": ["Inter Bold"],
      },
      //paint: {
      //  "text-color": "white",
      //},
    };
  };

  _makeClustersLayer = () => {
    return {
      id: "cluster_" + this.id,
      type: "symbol", // must be symbol
      source: this.id,
      filter: ["==", "cluster", true],
      layout: {
        "icon-image": "arkeo-disc", //'arkeo_no_#f39306_5', //'arkeo-disc',
        "icon-allow-overlap": true, // recommended
      },
      paint: {
        "icon-color": "transparent",
      },
    };
  };

  _onSourceAdded = (e) => {
    if (this.id !== e.sourceId) return;
    //console.log("e: "+this.id, e.sourceId);
    if (!this.arkeoClusterInited && e.isSourceLoaded) {
      this.arkeocluster.loadClustersMarkers();
      this.arkeoClusterInited = true;

      this.arkeospiderfy.loadSpiderfy();
    }
  };

  _getLetter = () => {
    const queryidx = parseInt(this.id.split("-")[1]) - 1;
    //const letter =
    //  (queryidx > 25 ? String.fromCharCode(64 + queryidx / 26) : "") + String.fromCharCode(65 + (queryidx % 26));
    return String.fromCharCode(65 + (queryidx % 26));
  };

  _getNumber = () => {
    const queryidx = parseInt(this.id.split("-")[1]) - 1;
    return queryidx;
  };
}
