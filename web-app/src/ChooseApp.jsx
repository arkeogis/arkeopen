import React from "react";

const ChooseApp = () => {
  return (
    <div>
      <h1>Error</h1>
      <p>
        Please run the following command to choose which application to build:
      </p>
      <code>npm run arkeopen-build</code>
      <p>or</p>
      <code>npm run arkeogis-build</code>
      <br />
      <p>
        The same thing can be done for each command, for example, to run the dev
        server :
      </p>
      <code>npm run arkeopen-start</code>
      <p>or</p>
      <code>npm run arkeogis-start</code>
    </div>
  );
};

export default ChooseApp;
