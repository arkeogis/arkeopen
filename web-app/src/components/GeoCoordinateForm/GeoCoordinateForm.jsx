import React, { useCallback, useEffect, useState } from "react";
import { Form, Row, Col, Button } from "react-bootstrap";
import "./GeoCoordinateForm.scss";
import { MdFormatAlignJustify } from "react-icons/md";

// i18n
import { useTranslation } from "react-i18next";

const DECIMALSINSECONDS = 0; // 2 for more precision

// Fonction pour convertir DMS en décimal
function dmsToDecimal(degrees, minutes, seconds, direction) {
  let decimal = degrees + minutes / 60 + seconds / 3600;
  if (direction === "S" || direction === "W") {
    decimal = -decimal;
  }
  return decimal;
}

// Fonction pour convertir décimal en DMS
function decimalToDms(value) {
  const decimal = parseFloat(value);
  if (value === "" || isNaN(decimal)) return { degrees: "", minutes: "", seconds: "", direction: "" };
  const degrees = Math.floor(Math.abs(decimal));
  const minutes = Math.floor((Math.abs(decimal) - degrees) * 60);
  const seconds = ((Math.abs(decimal) - degrees - minutes / 60) * 3600).toFixed(DECIMALSINSECONDS);
  const direction = decimal >= 0 ? "N/S" : "S/W";
  return { degrees, minutes, seconds, direction };
}

const GeoCoordinateForm = (props) => {
  const { initialValue, valueUpdated } = props;
  const { t } = useTranslation();

  const [decimalLat, setDecimalLat] = useState(initialValue ? initialValue.lat : "");
  const [decimalLon, setDecimalLon] = useState(initialValue ? initialValue.lon : "");
  const [radius, setRadius] = useState(initialValue ? initialValue.radius : "");
  const [dmsLatDegrees, setDmsLatDegrees] = useState("");
  const [dmsLatMinutes, setDmsLatMinutes] = useState("");
  const [dmsLatSeconds, setDmsLatSeconds] = useState("");
  const [dmsLatDirection, setDmsLatDirection] = useState("N");
  const [dmsLonDegrees, setDmsLonDegrees] = useState("");
  const [dmsLonMinutes, setDmsLonMinutes] = useState("");
  const [dmsLonSeconds, setDmsLonSeconds] = useState("");
  const [dmsLonDirection, setDmsLonDirection] = useState("E");

  const [fire, setFire] = useState(0);

  useEffect(() => {
    if (!valueUpdated) return;
    const res = {
      lat: parseFloat(decimalLat),
      lon: parseFloat(decimalLon),
      radius: parseFloat(radius),
    };
    if (!isNaN(res.lat) && !isNaN(res.lon) && !isNaN(res.radius)) {
      valueUpdated(res);
    } else {
      valueUpdated(null);
    }
  }, [fire]);

  const fireValueUpdated = () => {
    setFire(fire + 1);
  };

  const handleDecimalChange = (event) => {
    const { name, value } = event.target;
    if (name === "decimalLat") {
      setDecimalLat(value);
      const { degrees, minutes, seconds, direction } = decimalToDms(value);
      setDmsLatDegrees(degrees);
      setDmsLatMinutes(minutes);
      setDmsLatSeconds(seconds);
      setDmsLatDirection(direction[0]);
    } else if (name === "decimalLon") {
      setDecimalLon(value);
      const { degrees, minutes, seconds, direction } = decimalToDms(value);
      setDmsLonDegrees(degrees);
      setDmsLonMinutes(minutes);
      setDmsLonSeconds(seconds);
      setDmsLonDirection(direction[2]);
    }
    fireValueUpdated();
  };

  const handleDmsChange = (event) => {
    const { name, value } = event.target;
    console.log("DmsChange", name, value);
    if (name.startsWith("dmsLat")) {
      let latDegrees = parseFloat(dmsLatDegrees) || 0;
      let latMinutes = parseFloat(dmsLatMinutes) || 0;
      let latSeconds = parseFloat(dmsLatSeconds) || 0;
      let latDirection = dmsLatDirection;
      if (name === "dmsLatDegrees") {
        setDmsLatDegrees(value);
        latDegrees = value;
      } else if (name === "dmsLatMinutes") {
        setDmsLatMinutes(value);
        latMinutes = value;
      } else if (name === "dmsLatSeconds") {
        setDmsLatSeconds(value);
        latSeconds = value;
      } else if (name === "dmsLatDirection") {
        setDmsLatDirection(value);
        latDirection = value;
      }
      setDecimalLat(dmsToDecimal(latDegrees, latMinutes, latSeconds, latDirection));
    } else if (name.startsWith("dmsLon")) {
      let lonDegrees = parseFloat(dmsLonDegrees) || 0;
      let lonMinutes = parseFloat(dmsLonMinutes) || 0;
      let lonSeconds = parseFloat(dmsLonSeconds) || 0;
      let lonDirection = dmsLonDirection;
      if (name === "dmsLonDegrees") {
        setDmsLonDegrees(value);
        lonDegrees = value;
      } else if (name === "dmsLonMinutes") {
        setDmsLonMinutes(value);
        lonMinutes = value;
      } else if (name === "dmsLonSeconds") {
        setDmsLonSeconds(value);
        lonSeconds = value;
      } else if (name === "dmsLonDirection") {
        setDmsLonDirection(value);
        lonDirection = value;
      }
      setDecimalLon(dmsToDecimal(lonDegrees, lonMinutes, lonSeconds, lonDirection));
    }

    fireValueUpdated();
  };

  const handleRadiusChange = (event) => {
    setRadius(parseFloat(event.target.value));
    fireValueUpdated();
  };

  return (
    <div className="GeoCoordinateForm">
      <Form>
        {/* Latitude */}
        <Form.Label>{t("components.GeoCoordinateForm.latitude.label")} *</Form.Label>
        <div className="align-items-center row-latitude">
          <Form.Group as={Col} controlId="formDecimalLat" className="formDecimalLat">
            <Form.Control
              type="number"
              step="0.0001"
              placeholder={t("components.GeoCoordinateForm.latitude.decimal.placeholder")}
              name="decimalLat"
              value={decimalLat}
              onChange={handleDecimalChange}
            />
          </Form.Group>

          <Col className="ou">{t("components.GeoCoordinateForm.or")}</Col>

          <Form.Group as={Col} className="formDMSLat">
            <div className="dms-row">
              <Form.Group as={Col} md="4">
                <Form.Control
                  type="number"
                  step="1"
                  placeholder={t("components.GeoCoordinateForm.latitude.dms.deg.placeholder")}
                  name="dmsLatDegrees"
                  value={dmsLatDegrees}
                  onChange={handleDmsChange}
                />
              </Form.Group>
              <Form.Group as={Col} md="4">
                <Form.Control
                  type="number"
                  step="1"
                  placeholder={t("components.GeoCoordinateForm.latitude.dms.min.placeholder")}
                  name="dmsLatMinutes"
                  value={dmsLatMinutes}
                  onChange={handleDmsChange}
                />
              </Form.Group>
              <Form.Group as={Col} md="4">
                <Form.Control
                  type="number"
                  step="any"
                  placeholder={t("components.GeoCoordinateForm.latitude.dms.sec.placeholder")}
                  name="dmsLatSeconds"
                  value={dmsLatSeconds}
                  onChange={handleDmsChange}
                />
              </Form.Group>
            </div>
          </Form.Group>
        </div>

        <div className="align-items-center latitude-polarity">
          <div></div>
          <div></div>
          <div>
            <Form.Group as={Col}>
              <Form.Check
                id="check_dmsLatDirection"
                type="radio"
                label={t("components.GeoCoordinateForm.latitude.dms.north.placeholder")}
                name="dmsLatDirection"
                value="N"
                checked={dmsLatDirection === "N"}
                onChange={handleDmsChange}
                inline
              />
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Check
                id="check_dmsLatDirection"
                type="radio"
                label={t("components.GeoCoordinateForm.latitude.dms.south.placeholder")}
                name="dmsLatDirection"
                value="S"
                checked={dmsLatDirection === "S"}
                onChange={handleDmsChange}
                inline
              />
            </Form.Group>
          </div>
        </div>

        <div className="exemple">
          <span className="bold">{t("components.GeoCoordinateForm.exemple-latitude.header")}</span>
          {t("components.GeoCoordinateForm.exemple-latitude.label")}
        </div>

        {/* Longitude */}
        <Form.Label>{t("components.GeoCoordinateForm.longitude.label")} *</Form.Label>
        <div className="align-items-center row-longitude">
          <Form.Group as={Col} controlId="formDecimalLon" className="formDecimalLon">
            <Form.Control
              type="number"
              step="0.0001"
              placeholder={t("components.GeoCoordinateForm.longitude.decimal.placeholder")}
              name="decimalLon"
              value={decimalLon}
              onChange={handleDecimalChange}
            />
          </Form.Group>

          <Col className="ou">{t("components.GeoCoordinateForm.or")}</Col>

          <Form.Group as={Col} className="formDMSLon">
            <div className="dms-row">
              <Form.Group as={Col} md="4">
                <Form.Control
                  type="number"
                  step="1"
                  placeholder={t("components.GeoCoordinateForm.longitude.dms.deg.placeholder")}
                  name="dmsLonDegrees"
                  value={dmsLonDegrees}
                  onChange={handleDmsChange}
                />
              </Form.Group>
              <Form.Group as={Col} md="4">
                <Form.Control
                  type="number"
                  step="1"
                  placeholder={t("components.GeoCoordinateForm.longitude.dms.min.placeholder")}
                  name="dmsLonMinutes"
                  value={dmsLonMinutes}
                  onChange={handleDmsChange}
                />
              </Form.Group>
              <Form.Group as={Col} md="4">
                <Form.Control
                  type="number"
                  step="any"
                  placeholder={t("components.GeoCoordinateForm.longitude.dms.sec.placeholder")}
                  name="dmsLonSeconds"
                  value={dmsLonSeconds}
                  onChange={handleDmsChange}
                />
              </Form.Group>
            </div>
          </Form.Group>
        </div>

        <div className="align-items-center longitude-polarity">
          <div></div>
          <div></div>
          <div>
            <Form.Group as={Col}>
              <Form.Check
                id="check_dmsLonDirection"
                type="radio"
                label={t("components.GeoCoordinateForm.latitude.dms.east.placeholder")}
                name="dmsLonDirection"
                value="E"
                checked={dmsLonDirection === "E"}
                onChange={handleDmsChange}
                inline
              />
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Check
                id="check_dmsLonDirection"
                type="radio"
                label={t("components.GeoCoordinateForm.latitude.dms.west.placeholder")}
                name="dmsLonDirection"
                value="W"
                checked={dmsLonDirection === "W"}
                onChange={handleDmsChange}
                inline
              />
            </Form.Group>
          </div>
        </div>

        <div className="exemple">
          <span className="bold">{t("components.GeoCoordinateForm.exemple-longitude.header")}</span>
          {t("components.GeoCoordinateForm.exemple-longitude.label")}
        </div>

        <Form.Label>{t("components.GeoCoordinateForm.radius.label")} *</Form.Label>
        <div className="radius">
          <Form.Group as={Col} md="4">
            <Form.Control
              type="number"
              step={100}
              placeholder={t("components.GeoCoordinateForm.radius.placeholder")}
              name="radius"
              value={radius}
              onChange={handleRadiusChange}
            />
          </Form.Group>
        </div>

        <div className="info">{t("components.GeoCoordinateForm.footer.info")}</div>
      </Form>
    </div>
  );
};

export default GeoCoordinateForm;
