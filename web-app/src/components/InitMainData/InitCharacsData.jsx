import React from "react";

// zustand
import useStore from "../../lib/store";
//import {shallow} from 'zustand/shallow'
//
import ak from "../../lib/akprefix";

// graphql
import { useQuery } from "@apollo/client";
import { GetAllCharacs } from "../../lib/queries/charac.js";
import { useEffect } from "react";
//

import _ from "underscore";

const convertCharacs = (characs, parent = null) => {
  return characs.map((charac) => {
    const _charac = _.pick(charac, "id", "ark_id", "aat_id", "pactols_id", "charac_trs");
    _charac.characs = charac.characs ? convertCharacs(charac.characs, _charac) : [];
    _charac.parent = parent;
    return _charac;
  });
};

export default function InitCharacsData(props) {
  const { /*loading, error, */ data } = useQuery(GetAllCharacs, {
    fetchPolicy: "no-cache",
    context: {
      //role: "BABAYAGA"
    },
  });
  const setCharacs = useStore((state) => state.setCharacs);

  useEffect(() => {
    if (data) {
      const convertedCharacs = data ? convertCharacs(data[`${ak}charac`]) : [];
      if (convertedCharacs.length > 0) {
        setCharacs(convertedCharacs);
      }
    }
  }, [data, setCharacs]);

  return <React.Fragment />;
}
