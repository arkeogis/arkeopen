import React from "react";

// zustand
import { useSearchStore, useValidatedSearchStore } from "../../lib/store";
import { useAuthStore } from "../../lib/auth.js";
//import {shallow} from 'zustand/shallow'
//

// graphql
import { useQuery } from "@apollo/client";
import { GetProject } from "../../lib/queries/project.js";
import { useEffect } from "react";
//

import ak from "../../lib/akprefix";

function ProjectUpdator(props) {
  const project = useAuthStore((state) => state.project);
  const setChronologyStartDate = useSearchStore((state) => state.setChronologyStartDate);
  const setChronologyEndDate = useSearchStore((state) => state.setChronologyEndDate);
  const setSelectedChronologyId = useSearchStore((state) => state.setSelectedChronologyId);
  const validateSearch = useValidatedSearchStore((state) => state.validateSearch);

  useEffect(() => {
    if (!project) return;
    setChronologyStartDate(project.start_date);
    setChronologyEndDate(project.end_date);
    if (project.project__chronologies.length > 0)
      setSelectedChronologyId(project.project__chronologies[0].root_chronology_id);
    //validateSearch();
  }, [project]);

  return <React.Fragment />;
}

export default function InitProjectData(props) {
  const user = props.user;
  const setProject = useAuthStore((state) => state.setProject);
  const reloadProject = useAuthStore((state) => state.reloadProject);
  const { /*loading, error, */ data, refetch } = useQuery(GetProject, {
    fetchPolicy: "no-cache",
    variables: { user_id: user.id },
  });

  useEffect(() => {
    if (data) {
      const project = data[`${ak}project`]?.[0];
      setProject(project);
    }
  }, [data, setProject]);

  useEffect(() => {
    refetch();
  }, [reloadProject, refetch]);

  return (
    <React.Fragment>
      <ProjectUpdator />
    </React.Fragment>
  );
}
