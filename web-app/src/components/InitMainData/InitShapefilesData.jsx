import React from "react";

// zustand
import useStore from "../../lib/store";
//import {shallow} from 'zustand/shallow'
//

// graphql
import { useQuery } from "@apollo/client";
import { GetShapefiles } from "../../lib/queries/shapefile.js";
import { useEffect } from "react";
//

import ak from "../../lib/akprefix";

export default function InitShapefilesData(props) {
  const { /*loading, error, */ data } = useQuery(GetShapefiles, { fetchPolicy: "no-cache" });
  const setShapefiles = useStore((state) => state.setShapefiles);

  useEffect(() => {
    if (data) {
      const shapefiles = data && data[`${ak}shapefile`] ? data[`${ak}shapefile`] : [];
      if (shapefiles.length > 0) {
        setShapefiles(shapefiles);
      }
    }
    //console.log("shapefiles", data ? data[`${ak}shapefile`] : []);
  }, [data, setShapefiles]);

  return <React.Fragment />;
}
