import React, { useState, useEffect } from "react";

// Router
import { Outlet } from "react-router-dom";

// zustand
import useStore from "../../lib/store";
import { shallow } from "zustand/shallow";
import { useAuthStore } from "../../lib/auth";
//

import InitCharacsData from "./InitCharacsData";
import InitChronologiesData from "./InitChronologiesData";
import InitShapefilesData from "./InitShapefilesData";
import InitProjectData from "./InitProjectData";

const InitMainData = (props) => {
  const user = __ARKEOGIS__ ? useAuthStore((state) => state.user) : null;
  const [ready, setReady] = useState(false);
  const [characs, chronologies] = useStore((state) => [state.characs, state.chronologies], shallow);

  useEffect(() => {
    if (characs && characs.length > 0 && chronologies && chronologies.length > 0) setReady(true);
  }, [characs, chronologies]);

  if (ready) {
    return props.children ? props.children : <Outlet />;
  } else {
    return (
      <React.Fragment>
        <InitCharacsData />
        <InitChronologiesData />
        <InitShapefilesData />
        {__ARKEOGIS__ && user ? <InitProjectData user={user} /> : null}
      </React.Fragment>
    );
  }
};

export default InitMainData;
