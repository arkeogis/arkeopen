import React, { useEffect } from "react";
//import PropTypes from 'prop-types'

import FlatSelect from "../FlatSelect/FlatSelect";

// zustand
import useStore, { useSearchStore } from "../../lib/store";
import { useShallow } from "zustand/react/shallow";
//

// bootstrap
import Form from "react-bootstrap/Form";
//import FormControl from 'react-bootstrap/FormControl';
//import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from "react-bootstrap/Popover";
//

import Markdown from "../Markdown/Markdown";

import { yearToHuman } from "../../lib/year";

// i18n
import { useTranslation } from "react-i18next";
import { getInLang } from "../../lib/translate";
//

import "./FormSearchWhere.scss";
import FormZone from "../FormZone/FormZone";

/**
 * The FormSearchWhere for Arkeopen
 * It contain SHP chooser
 *
 * @param {} props
 * @returns
 */
const FormSearchWhereArkeopen = (props) => {
  const { t, i18n } = useTranslation();
  const [selectedShapefiles, setSelectedShapefiles] = useSearchStore(
    useShallow((state) => [state.selectedShapefiles, state.setSelectedShapefiles])
  );

  const shapefiles = [...useStore((state) => state.shapefiles)].sort((a, b) =>
    getInLang(a.shapefile_trs, i18n.language).name.localeCompare(
      getInLang(b.shapefile_trs, i18n.language).name,
      i18n.language
    )
  );

  const toggleSelect = (shapefile) => {
    if (selectedShapefiles.includes(shapefile.id)) {
      setSelectedShapefiles(selectedShapefiles.filter((id) => id !== shapefile.id));
    } else {
      setSelectedShapefiles([...selectedShapefiles, shapefile.id]);
    }
  };

  return (
    <div className="FormSearchWhere">
      <div>
        <Form.Label>{t("components.FormSearchWhere.label")}</Form.Label>

        {shapefiles.map((shapefile) => (
          <FlatSelect
            key={shapefile.id}
            selected={selectedShapefiles.includes(shapefile.id)}
            label={getInLang(shapefile.shapefile_trs, i18n.language).name}
            onSelect={() => toggleSelect(shapefile)}
            leaf={true}
            pop={
              <Popover id={`popover-chronology`}>
                <Popover.Header as="h3">
                  <div>
                    <strong>
                      {yearToHuman(shapefile.start_date, i18n.language)} =&gt;{" "}
                      {yearToHuman(shapefile.end_date, i18n.language)}
                    </strong>
                  </div>
                </Popover.Header>
                <Popover.Body>
                  <div className="poptextwrap">
                    <Markdown>{getInLang(shapefile.shapefile_trs).description}</Markdown>
                  </div>
                </Popover.Body>
              </Popover>
            }
          ></FlatSelect>
        ))}
      </div>
    </div>
  );
};

/**
 * The FormSearchWhere for ArkeoGIS
 * It contain a geographical chooser
 *
 * @param {*} props
 * @returns
 */
const FormSearchWhereArkeogis = (props) => {
  const { t, i18n } = useTranslation();
  const mapObject = useStore((state) => state.mapObject);
  const setZoneDrawing = useStore((state) => state.setZoneDrawing);
  const sidePanelOpened = useStore((state) => state.sidePanelOpened);
  const [zone, setZone] = useSearchStore(useShallow((state) => [state.zone, state.setZone]));

  useEffect(() => {
    console.log("zone: ", zone);
  }, [zone]);

  return (
    <div className="FormSearchWhere">
      <div>
        {__ARKEOGIS__ && mapObject && (
          <FormZone
            map={mapObject}
            setZoneDrawing={setZoneDrawing}
            initialZone={zone}
            onZoneChange={setZone}
            hideZone={sidePanelOpened !== "left"}
          />
        )}
      </div>
    </div>
  );
};

const FormSearchWhere = __ARKEOGIS__ ? FormSearchWhereArkeogis : FormSearchWhereArkeopen;
FormSearchWhere.propTypes = {};

export default FormSearchWhere;
