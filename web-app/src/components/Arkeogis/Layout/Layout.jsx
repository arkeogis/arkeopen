import BurgerModal from "../../BurgerModal/BurgerModal";
import Header from "../../Header/Header";

const Layout = ({children}) => {
    return (<>
      {children}
      <Header />
    </>)
};

export default Layout;
