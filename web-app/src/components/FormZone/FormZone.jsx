import React, { useEffect, useRef, useState } from "react";
import Form, { Button } from "react-bootstrap/Form";
//

// i18n
import { useTranslation } from "react-i18next";
//import { getInLang } from '../../lib/translate';
//

import GeoCoordinateForm from "../GeoCoordinateForm/GeoCoordinateForm";

import "./FormZone.scss";
import { ZoneDisc, ZoneGeo, ZoneNone, ZonePolygon, ZoneRect, ZoneSelected } from "../Icons/Icons";

// terra
import { TerraDraw, TerraDrawRectangleMode, TerraDrawCircleMode, TerraDrawPolygonMode } from "terra-draw";
import { TerraDrawMapLibreGLAdapter } from "terra-draw-maplibre-gl-adapter";
import maplibregl from "maplibre-gl";

// turf
import * as turf from "@turf/turf";
import { BsWrenchAdjustable } from "react-icons/bs";

/**
 * FormZone is a React component that provides an interface for selecting and drawing different types of
 * geographical zones on a map. It supports rectangle, circle, polygon, and geo-coordinate based zones.
 *
 * Props:
 * - map: The map object to which the drawing functionality is applied.
 * - hideZone: boolean, if true, hide the selected zone.
 * - setZoneDrawing: A function to update the drawing state :
 *    - called with {true} parameter when the user start drawinging a new zone
 *    - called with {false} paramter when the user end drawing a zone
 * - initialZone: a GeoJSON object with the initial zone
 * - onZoneChange: a callback function with zone GeoJSON object as parameter
 *
 * It initializes and destroys the drawing tool as needed and updates the zone type based on user
 * interaction with the buttons.
 */
function FormZone({ map, hideZone, setZoneDrawing, initialZone, onZoneChange }) {
  const { t, i18n } = useTranslation();

  const [zoneStyle, setZoneStyle] = useState("none");
  const [feature, setFeature] = useState(null);
  const drawRef = useRef();
  const mapObject = map;
  setZoneDrawing = setZoneDrawing ? setZoneDrawing : () => {};

  const destroyDraw = () => {
    if (drawRef.current) {
      console.log("destroying draw...");
      drawRef.current.stop();
      drawRef.current = null;
    }
  };

  const initDraw = (mode) => {
    destroyDraw();

    console.log("creating draw draw...");
    // init TerraDraw if not already done
    // Create Terra Draw
    const draw = new TerraDraw({
      // Using the MapLibre Adapter
      adapter: new TerraDrawMapLibreGLAdapter({ map: mapObject, lib: maplibregl }),

      // Add the Rectangle Mode
      modes: [new TerraDrawRectangleMode(), new TerraDrawCircleMode(), new TerraDrawPolygonMode()],
    });

    draw.on("finish", (id, context) => {
      console.log("draw finish", id, context);
      //if (context.mode == "polygon") return;
      const snapshot = draw.getSnapshot();
      console.log("snapshot", snapshot);
      if (snapshot && snapshot.length == 1) {
        drawRef.current.setMode("static");
        setZoneDrawing(null);
        setFeature(snapshot[1]);
        if (onZoneChange) onZoneChange(snapshot[0]);
      } else {
        setZoneStyle("none");
        destroyDraw();
        setZoneDrawing(null);
        if (onZoneChange) onZoneChange(null);
      }
    });

    console.log("draw", draw);
    console.log("map", mapObject);

    drawRef.current = draw;
    drawRef.current.start();
    drawRef.current.setMode(mode);
  };

  useEffect(() => {
    if (initialZone) {
      console.log("initialZone: ", initialZone);
      if (initialZone && initialZone.geometry && initialZone.properties) {
        let r;
        switch (initialZone.properties.mode) {
          case "rectangle":
            setZoneStyle("rect");
            initDraw("static");
            r = drawRef.current.addFeatures([initialZone]);
            console.log("result: ", r[0]);
            break;
          case "circle":
            if (initialZone.properties.arkeomode === "geo") {
              setZoneStyle("geo");
            } else {
              setZoneStyle("disc");
            }
            initDraw("static");
            r = drawRef.current.addFeatures([initialZone]);
            console.log("result: ", r[0]);
            break;
          case "polygon":
            setZoneStyle("polygon");
            initDraw("static");
            r = drawRef.current.addFeatures([initialZone]);
            console.log("result: ", r[0]);
            break;
        }
      } else {
        setZoneStyle("none");
        initDraw("static");
      }
    }
  }, [initialZone]);

  useEffect(() => {}, [hideZone]);

  useEffect(() => {
    return destroyDraw;
  }, []);

  const manualValueUpdated = (newDisc) => {
    if (newDisc) {
      console.log("newdisc: ", newDisc);
      const center = turf.point([newDisc.lon, newDisc.lat]);
      const options = { steps: 64, units: "kilometers", properties: { mode: "circle", arkeomode: "geo" } };
      let circle;
      circle = turf.circle(center, newDisc.radius, options);

      // ensure each coordinates are float with not to many decimals, because terra don't like thems
      const fixedCoordinates = circle.geometry.coordinates[0].map((coord) => [
        parseFloat(coord[0].toFixed(8)),
        parseFloat(coord[1].toFixed(8)),
      ]);

      circle.geometry.coordinates[0] = fixedCoordinates;

      console.log("circle: ", circle);
      initDraw("static");
      const r = drawRef.current.addFeatures([circle]);
      console.log("result: ", r[0]);
      if (onZoneChange) onZoneChange(circle);
    } else {
      console.log("no full data");
      if (onZoneChange) onZoneChange(null);
    }
  };

  //const zoneStyle = useSearchStore((state) => state.zoneStyle);
  //const setZoneStyle = useSearchStore((state) => state.setZoneStyle);

  return (
    <div className="FormZone">
      <Form.Label>{t("components.FormZone.label")}</Form.Label>

      <div>
        <div className="zoneType">
          <ButtonImage
            id="button_zone_none"
            label={t("components.FormZone.none.label")}
            checked={zoneStyle === "none"}
            onClick={() => {
              setZoneStyle("none");
              destroyDraw();
              setZoneDrawing(null);
            }}
            image={<ZoneNone />}
          />

          <ButtonImage
            id="button_zone_rect"
            label={t("components.FormZone.rect.label")}
            checked={zoneStyle === "rect"}
            onClick={() => {
              setZoneStyle("rect");
              initDraw("rectangle");
              setZoneDrawing("rect");
            }}
            image={<ZoneRect />}
          />

          <ButtonImage
            id="button_zone_disc"
            label={t("components.FormZone.disc.label")}
            checked={zoneStyle === "disc"}
            onClick={() => {
              setZoneStyle("disc");
              initDraw("circle");
              setZoneDrawing("disc");
            }}
            image={<ZoneDisc />}
          />

          <ButtonImage
            id="button_zone_polygon"
            label={t("components.FormZone.polygon.label")}
            checked={zoneStyle === "polygon"}
            onClick={() => {
              setZoneStyle("polygon");
              initDraw("polygon");
              setZoneDrawing("polygon");
            }}
            image={<ZonePolygon />}
          />

          <ButtonImage
            id="button_zone_geo"
            label={t("components.FormZone.geo.label")}
            checked={zoneStyle === "geo"}
            onClick={() => {
              setZoneStyle("geo");
              initDraw("static");
              setZoneDrawing(null);
            }}
            image={<ZoneGeo />}
          />
        </div>

        <div className={"GeoCoordinateContainer " + (zoneStyle === "geo" ? "show" : "hide")}>
          <GeoCoordinateForm initialValue={null} valueUpdated={manualValueUpdated} />
        </div>
      </div>
    </div>
  );
}

const ButtonImage = (props) => {
  return (
    <button {...props} className="btn btn-secondary ButtonIcon">
      {props.image}
      {props.checked ? (
        <div className="ButtonIconSelected">
          <ZoneSelected />
        </div>
      ) : (
        <div></div>
      )}
    </button>
  );
};

export default FormZone;
