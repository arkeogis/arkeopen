import { render } from "@testing-library/react";

import BurgerModal from "./BurgerModal";
import styles from "./BurgerModal.module.scss";

import useStore from "../../lib/store";
import { BrowserRouter as Router } from "react-router-dom";

it("should not have show class by default", () => {
  useStore.setState({ showBurgerModal: false });

  const { container } = render(
    <Router>
      <BurgerModal />
    </Router>
  );

  expect(container.firstChild).not.toHaveClass(styles.show);
});

it("should have show class when showBurgerModal is set to true", () => {
  useStore.setState({ showBurgerModal: true });

  const { container } = render(
    <Router>
      <BurgerModal />
    </Router>
  );

  expect(container.firstChild).toHaveClass(styles.show);
});
