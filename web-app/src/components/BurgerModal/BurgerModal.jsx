import React from "react";
//import Button from 'react-bootstrap/Button';

// zustand
import useStore from "../../lib/store";
import { useAuthStore } from "../../lib/auth";
import { shallow } from "zustand/shallow";

// i18n
import { useTranslation } from "react-i18next";
//

import LangSelect from "../LangSelect/LangSelect";
import ArkeoLink from "../ArkeoUI/ArkeoLink";

import { CloseIcon } from "../Icons/Icons";

import styles from "./BurgerModal.module.scss";
import { Link } from "react-router-dom";

/**
 * This is the Modal Dialog that is displayed when you click on the burger menu
 * @returns The Modal Dialog Component to include somewhere
 */

const BurgerModal = () => {
  const [showBurgerModal, setShowBurgerModal] = useStore(
    (state) => [state.showBurgerModal, state.setShowBurgerModal],
    shallow
  );

  const handleClose = () => setShowBurgerModal(false);

  const { t } = useTranslation();

  return (
    <div className={`${styles.burgerModal} ${showBurgerModal ? styles.show : ""}`}>
      <img src="/logos/logo-arkeopen-white.svg" alt="ArkeoPen" title="Arkeopen" />

      <button className={styles.closeButton} onClick={handleClose}>
        <CloseIcon />
      </button>

      <div className={styles.linkGroup1}>
        <ArkeoLink projectPage="project">
          {t("components.BurgerModal.link.theproject1")}
          <span style={{ color: "white" }}>ArkeOpen</span>
          {t("components.BurgerModal.link.theproject2")}
        </ArkeoLink>
        <ArkeoLink sidePanelOpened="left">{t("components.BurgerModal.link.search")}</ArkeoLink>
        <ArkeoLink projectPage="partners">{t("components.BurgerModal.link.partners")}</ArkeoLink>
        <ArkeoLink to="/chronologies" className={styles.smallLink}>
          {t("components.BurgerModal.link.chronologies")}
        </ArkeoLink>
        <ArkeoLink to="/thesaurus" className={styles.smallLink}>
         {t("components.BurgerModal.link.thesaurus")}
        </ArkeoLink>
      </div>

      <div className={styles.linkGroup2}>
        <ArkeoLink helpPage="interface">{t("components.BurgerModal.link.inlinehelp")}</ArkeoLink>
        <div>
          <ArkeoLink helpPage="interface" className={styles.smallLink}>
            {t("components.BurgerModal.link.interface")}
          </ArkeoLink>
          <ArkeoLink helpPage="when" className={styles.smallLink}>
            {t("components.BurgerModal.link.chronology")}
          </ArkeoLink>
          <ArkeoLink helpPage="others" className={styles.smallLink}>
            {t("components.BurgerModal.link.filter")}
          </ArkeoLink>
          <ArkeoLink helpPage="map" className={styles.smallLink}>
            {t("components.BurgerModal.link.map")}
          </ArkeoLink>
        </div>
      </div>

      <div className={styles.linkGroup3}>
        <ArkeoLink projectPage="credits" className={styles.smallLink}>
          {t("components.BurgerModal.link.credits")}
        </ArkeoLink>
        <ArkeoLink projectPage="cgu" className={styles.smallLink}>
          {t("components.BurgerModal.link.cgu")}
        </ArkeoLink>
        <ArkeoLink projectPage="legals" className={styles.smallLink}>
          {t("components.BurgerModal.link.legals")}
        </ArkeoLink>
        <ArkeoLink className={styles.smallLink} projectPage="contact">
          {t("components.BurgerModal.link.contact")}
        </ArkeoLink>
        {__ARKEOGIS__ && (
          <ArkeoLink logout className={styles.smallLink}>
            {t("components.BurgerModal.link.logout")}
          </ArkeoLink>
        )}
        <div className={styles.lang}>
          <LangSelect />
        </div>
      </div>
    </div>
  );
};

export default BurgerModal;
