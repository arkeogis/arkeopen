import React, { useState, useRef, useEffect } from "react";
import styles from "./Popover.module.scss";

const Popover = ({ position = "top", children, content, triggerType = "hover", hoverDelay = 0 }) => {
  const [isVisible, setIsVisible] = useState(false);
  const wrapperRef = useRef(null);
  let timeoutId = null;

  // Capitalize position for class names
  const capitalizedPosition = position.charAt(0).toUpperCase() + position.toLocaleLowerCase().slice(1);

  // Handle clicks outside the popover
  useEffect(() => {
    const handleClickOutside = (event) => {
      if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
        setIsVisible(false);
      }
    };

    if (triggerType === "click") {
      document.addEventListener("mousedown", handleClickOutside);
    }
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [triggerType]);

  // Event handlers
  // const handleToggle = () => triggerType === "click" && setIsVisible(!isVisible);
  const handleMouseEnter = () => triggerType === "hover" && setIsVisible(true);
  const handleMouseLeave = () => {
    if (triggerType === "hover") {
      timeoutId = setTimeout(() => setIsVisible(false), hoverDelay);
    }
  };
  const cancelMouseLeave = () => clearTimeout(timeoutId);

  const mergeEventHandlers = (originalHandler, newHandler) => (e) => {
    originalHandler?.(e);
    if (!e.defaultPrevented) {
      newHandler(e);
    }
  };

  const getTriggerElement = () => {
    // Wrap primitive children in a span
    const childElement = React.isValidElement(children) ? children : <span>{children}</span>;

    // Get existing props from the child element
    const childProps = childElement.props || {};

    // Merge event handlers
    const mergedProps = {
      ...childProps,
      ...(triggerType === "click" && {
        onClick: mergeEventHandlers(childProps.onClick, () => setIsVisible(!isVisible)),
      }),
      onMouseEnter: mergeEventHandlers(childProps.onMouseEnter, handleMouseEnter),
      onMouseLeave: mergeEventHandlers(childProps.onMouseLeave, handleMouseLeave),
      onMouseOver: mergeEventHandlers(childProps.onMouseOver, cancelMouseLeave),
    };

    return React.cloneElement(childElement, mergedProps);
  };

  return (
    <div className={styles.popoverWrapper} ref={wrapperRef}>
      {getTriggerElement()}
      <div
        className={`${styles.popover} ${styles[`popover${capitalizedPosition}`]} ${isVisible ? styles.visible : ""}`}
      >
        <div className={`${styles.popoverArrow} ${styles[`popoverArrow${capitalizedPosition}`]}`}></div>
        <div className={styles.popoverContent}>{content}</div>
      </div>
    </div>
  );
};

export default Popover;
