import React, { useEffect, useState } from "react";
import { useQuery } from "@apollo/client";
import { DataType, SortDirection } from "ka-table";
import gql from "graphql-tag";
import { useTranslation } from "react-i18next";
import akprefix from "../../lib/akprefix";
import { getInLangWKey } from "../../lib/translate";
import { yearToHumanNumber } from "../../lib/year";
import ArkeoLink from "../ArkeoUI/ArkeoLink";
import Index from "../Index/Index";

import "./IndexThesaurus.scss";

// GraphQL query to fetch chronology data
const GET_DATA = gql`
  query MyQuery {
    akg_charac_root {
      root_charac_id
      cached_langs
      charac {
        order
        updated_at
        created_at
        charac_trs {
          lang_isocode
          name
        }
      }
    }
  }
`;

// Custom cell component for rendering chronology links
const ArkeoLinkCustomCell = ({ column, rowKeyValue, value }) => {
  return <ArkeoLink rightPanel={{ type: "chronology", id: rowKeyValue }}>{value}</ArkeoLink>;
};

const IndexThesaurus = () => {
  const { t, i18n } = useTranslation();
  const { loading, error, data } = useQuery(GET_DATA);
  const [thesaurus, setThesaurus] = useState([]);
  const [columns, setColumns] = useState([]);

  // Process data when it's loaded or language changes
  useEffect(() => {
    if (loading || error) {
      setThesaurus([]);
      return;
    }

    // Transform API data to table rows
    const _thesaurus = data[`${akprefix}charac_root`].map((charac) => {
      return {
        id: charac.root_charac_id,
        order: charac.charac.order,
        name: getInLangWKey(charac.charac.charac_trs, "name", i18n.language),
        langs: charac.cached_langs.toUpperCase(),
        shared: "-",
        created_at: new Date(charac.charac.created_at),
        updated_at: new Date(charac.charac.updated_at),
      };
    });

    setThesaurus(_thesaurus);
  }, [loading, error, data, i18n.language]);

  // Setup columns when language changes
  useEffect(() => {
    const _columns = [
      {
        key: "order",
        title: t("components.IndexThesaurus.index.order"),
        dataType: DataType.Number,
        sortDirection: SortDirection.Ascend,
        width: "90px",
      },
      {
        key: "name",
        title: t("components.IndexThesaurus.index.name"),
        dataType: DataType.String,
        customCellComponent: ArkeoLinkCustomCell,
      },
      {
        key: "langs",
        title: t("components.IndexThesaurus.index.langs"),
        dataType: DataType.String,
        width: "110px",
      },
      {
        key: "shared",
        title: t("components.IndexThesaurus.index.shared"),
        isFilterable: false,
        width: "100px",
      },
      {
        key: "created_at",
        title: t("components.IndexThesaurus.index.createdAt"),
        dataType: DataType.Date,
        isFilterable: false,
        width: "90px",
      },
      {
        key: "updated_at",
        title: t("components.IndexThesaurus.index.updatedAt"),
        dataType: DataType.Date,
        isFilterable: false,
        width: "90px",
      },
    ];

    setColumns(_columns);
  }, [i18n.language, t]);

  // Loading states
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error.message}</p>;

  // Format function for formatting cell values
  const format = ({ column, value }) => {
    if (column.dataType === DataType.Date) {
      return (
        value &&
        new Date(value).toLocaleDateString(i18n.language, { month: "2-digit", day: "2-digit", year: "numeric" })
      );
    }
  };

  return (
    <Index
      data={thesaurus}
      columns={columns}
      rowKeyField="id"
      format={format}
      className="IndexThesaurus"
      textSearch={true}
      title={
        <>
          <div>{t`components.IndexThesaurus.title1`}</div>
          <div>{t`components.IndexThesaurus.title2`}</div>
        </>
      }
    />
  );
};

export default IndexThesaurus;
