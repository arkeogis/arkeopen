import React from 'react'

/**
 * About Component, display an about
 * @returns The About component
 */

export const About = () => {
    return (
        <div>
            <p>Arkeopen is a prototype for future arkeogis developpements</p>
            <p>Arkeopen is based on react.js for frontend (client), hasura and postgresql database for backend</p>
        </div>
    )
}

export default About
