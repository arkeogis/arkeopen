// Ensure the About component renders correctly and contains the expected text
import { About } from "./About";

it("should render About component without crashing", () => {
  const { getByText } = render(<About />);
  expect(getByText("Arkeopen is a prototype for future arkeogis developpements")).toBeInTheDocument();
  expect(
    getByText("Arkeopen is based on react.js for frontend (client), hasura and postgresql database for backend")
  ).toBeInTheDocument();
});

it("should display two paragraphs of text when rendered", () => {
  const { getByText } = render(<About />);
  expect(getByText("Arkeopen is a prototype for future arkeogis developpements")).toBeInTheDocument();
  expect(
    getByText("Arkeopen is based on react.js for frontend (client), hasura and postgresql database for backend")
  ).toBeInTheDocument();
});
