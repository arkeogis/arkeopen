import React from "react";
import { Navigate, Outlet, useLocation } from "react-router-dom";
import { useAuthStore } from "../../lib/auth";

const ProtectedRoute = ({ children, redirectPath = "/login" }) => {
  const isAuthenticated = useAuthStore(state => state.isAuthenticated)
  const location = useLocation();
  if (!isAuthenticated) {
    return <Navigate to={redirectPath} replace state={{ from: location.pathname + location.search }} />;
  }
  return children ? children : <Outlet />;
};

export default ProtectedRoute;
