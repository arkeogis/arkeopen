import React, { useEffect, useState, useMemo, useRef } from "react";
import { debounce } from "lodash";
import { toast } from "sonner";
import { useAuthStore } from "../../lib/auth";
import { useTranslation } from "react-i18next";

const InactivityMonitor = () => {
  const { t } = useTranslation();
  const [isDialogVisible, setIsDialogVisible] = useState(false);
  const activityTimer = useRef(null);
  const logoutTimer = useRef(null);
  const isAuthenticated = useAuthStore((state) => state.isAuthenticated);
  const setLoggedOut = useAuthStore((state) => state.setLoggedOut);

  const activityEvents = useMemo(() => ["mousemove", "keydown", "mousedown", "scroll"], []);

  useEffect(() => {
    const INACTIVITY_TIMEOUT = 60 * 60 * 1000; // 1 hour in milliseconds
    const LOGOUT_TIMEOUT = 120 * 1000; // 2 minutes in milliseconds

    // Handle user logout
    const handleLogout = () => {
      setIsDialogVisible(false); // Hide the dialog
      clearTimeout(activityTimer.current);
      clearTimeout(logoutTimer.current);
      toast.dismiss();
      setLoggedOut();
    };

    // Start the 2-minute logout timer
    const startLogoutTimer = () => {
      logoutTimer.current = setTimeout(handleLogout, LOGOUT_TIMEOUT);
    };

    // Reset all timers and hide the dialog
    const resetTimer = () => {
      if (isDialogVisible) {
        toast.dismiss();
      }
      setIsDialogVisible(false); // Hide the dialog
      clearTimeout(activityTimer.current);
      clearTimeout(logoutTimer.current);
      startActivityTimer(); // Restart the main timer
    };

    // Start the inactivity timer
    const startActivityTimer = () => {
      activityTimer.current = setTimeout(() => {
        setIsDialogVisible(true);
        startLogoutTimer();
      }, INACTIVITY_TIMEOUT);
    };

    // Debounced activity handler
    const debouncedActivityHandler = debounce(() => {
      resetTimer(); // Reset everything if the user interacts during the logout countdown
    }, 100); // 100ms debounce delay

    if (isAuthenticated) {
      // Add event listeners to monitor activity
      activityEvents.forEach((event) => {
        window.addEventListener(event, debouncedActivityHandler);
      });
      // Start the activity timer initially
      startActivityTimer();
    }

    // Cleanup on unmount or when user logs out
    return () => {
      clearTimeout(activityTimer.current);
      clearTimeout(logoutTimer.current);
      debouncedActivityHandler.cancel(); // Cancel debounce
      // Remove the event listeners
      activityEvents.forEach((event) => {
        window.removeEventListener(event, debouncedActivityHandler);
      });
    };
  }, [isAuthenticated, setLoggedOut, isDialogVisible, activityEvents]);

  return <>{isAuthenticated && isDialogVisible && toast.error(t("components.InactivityMonitor.toast.label"), { duration: Infinity })}</>;
};

export default InactivityMonitor;
