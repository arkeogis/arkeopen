import React from 'react'
//import PropTypes from 'prop-types'

import { initClusters, loadClusters, unloadCluster, loadSpiderfy } from '../../lib/map/clusters';

const MapButtonHack = (props) => {
  const { map, loader } = props;
  return (
    <button
      onClick={() => {
        console.log(map.getStyle());
      }}
    >{loader ? "1" : "0"}</button>
  )
}

MapButtonHack.propTypes = {}

export default MapButtonHack
