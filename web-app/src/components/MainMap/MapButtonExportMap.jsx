import React from "react";
//import PropTypes from 'prop-types'

// i18n
import { useTranslation } from "react-i18next";

import useStore from "../../lib/store";
//import {shallow} from 'zustand/shallow'

import domtoimage from "dom-to-image-more";

const getCanvasImage = async (map) => {
  const canvas = map.getCanvas();
  const data = canvas.toDataURL("image/png");

  // return the image after it is loaded
  return new Promise((resolve, reject) => {
    const img = new Image();
    img.src = data;

    img.onload = () => {
      resolve(img);
    };
    img.onerror = (e) => {
      reject(e);
    };
  });
};

const getHtmlImage = async () => {
  const node = document.getElementById("MainArkeoMap");
  const data = await domtoimage.toSvg(node);
  const img = new Image();
  img.src = data;

  // return the image after it is loaded
  return new Promise((resolve, reject) => {
    img.onload = () => {
      resolve(img);
    };
    img.onerror = (e) => {
      reject(e);
    };
  });
};

const handleDownloadImage = async (map) => {
  const srccanvas = map.getCanvas(document.querySelector(".mapboxgl-canvas"));
  const size = { width: srccanvas.width, height: srccanvas.height };

  console.log("size", size);

  const canvasImage = await getCanvasImage(map);
  const htmlImage = await getHtmlImage();

  console.log("canvasImage", canvasImage);
  //console.log("htmlImage", htmlImage);

  const canvas = document.createElement("canvas");
  canvas.width = size.width;
  canvas.height = size.height;
  const ctx = canvas.getContext("2d");
  ctx.drawImage(canvasImage, 0, 0);
  ctx.drawImage(htmlImage, 0, 0);
  const data = canvas.toDataURL();

  const link = document.createElement("a");
  link.href = data;
  link.download = "downloaded-image.jpg";

  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
};

const MapButtonExportMap = (props) => {
  const mapObject = useStore((state) => state.mapObject);

  const { t } = useTranslation();
  return (
    <button
      className="MapButtonExportMap"
      onClick={(e) => {
        handleDownloadImage(mapObject);
        e.preventDefault();
      }}
    >
      <img alt="MapButtonExportMap" src="/icons-maps/MapButtonExportMap.svg" />
      <div className="mapbuttontext">
        {t("components.MainMap.MapButtonExportMap.label")}
      </div>
    </button>
  );
};

MapButtonExportMap.propTypes = {};

export default MapButtonExportMap;
