import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { MockedProvider } from "@apollo/client/testing";
import MainMap from "./MainMap";
import { GET_SITES_QUERY } from "../../lib/queries/sites";

jest.mock("../../lib/store", () => ({
  useStore: (passedFunction) => {
    const data = {
      basemap: "Positron",
      sidePanelOpened: false,

      setSidePanelOpened: jest.fn(),
      setRightPanelOpened: jest.fn(),
      setLng: jest.fn(),
      setLat: jest.fn(),
      setZoom: jest.fn(),
      setMapObject: jest.fn(),
      setMapUpdated: jest.fn(),
      setAllMapDrawed: jest.fn(),
      setFlyTo: jest.fn(),
    };

    return passedFunction(data);
  },
  useValidatedSearchStore: (passedFunction) => {
    const data = {
      geojsonResult: null,
      setGeojsonResult: jest.fn(),
      selectedChronologyId: 1,
      characSelectionCompacted: [],
      chronologySelectionCompacted: [],
      selectedShapefiles: [],
      knowledgeTypes: [],
      occupations: [],
      datasetTypes: [],
      scaleResolutions: [],
      exceptional: null,
      illustrated: null,
      centroid: null,
      editors: [],
      authors: [],
      databases: [],
      textual: "",
      textualOn: "all",
      chronologyStartDate: -2147483648,
      chronologyEndDate: 2147483647,
      chronologyFindIncludeUndetermined: false,
      chronologyFindOnlyInside: false,
    };

    return passedFunction(data);
  },
}));

// Mock the useTranslation hook
jest.mock("react-i18next", () => ({
  useTranslation: jest.fn(),
}));

// Mock the maplibre-gl library
jest.mock("maplibre-gl", () => ({
  Map: jest.fn(() => ({
    on: jest.fn(),
    addControl: jest.fn(),
    setStyle: jest.fn(),
    getCenter: jest.fn(() => ({ lng: 0, lat: 0 })),
    getZoom: jest.fn(() => 0),
    flyTo: jest.fn(),
    remove: jest.fn(),
  })),
  NavigationControl: jest.fn(),
  ScaleControl: jest.fn(),
  AttributionControl: jest.fn(),
  GeolocateControl: jest.fn(),
  Marker: jest.fn(() => ({
    setLngLat: jest.fn().mockReturnThis(),
    addTo: jest.fn().mockReturnThis(),
    remove: jest.fn(),
  })),
  Popup: jest.fn(() => ({
    setLngLat: jest.fn().mockReturnThis(),
    setHTML: jest.fn().mockReturnThis(),
    setOffset: jest.fn().mockReturnThis(),
    addTo: jest.fn().mockReturnThis(),
    remove: jest.fn(),
  })),
}));

const mocks = [
  {
    request: {
      query: GET_SITES_QUERY,
    },
    result: {
      data: {
        ako_site: [],
      },
    },
  },
];

it("renders without crashing", async () => {
  render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <MainMap />
    </MockedProvider>
  );

  await waitFor(() => {
    expect(screen.getByTestId("MainArkeoMap")).toBeInTheDocument();
  });
});

it("displays the loader when data is loading", async () => {
  render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <MainMap />
    </MockedProvider>
  );

  await waitFor(() => {
    expect(screen.getByTestId("loader")).toBeInTheDocument();
  });
});

it("does not display the loader when data is loaded", async () => {
  render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <MainMap />
    </MockedProvider>
  );

  await waitFor(() => {
    expect(screen.queryByTestId("loader")).not.toBeInTheDocument();
  });
});

it("displays map controls", async () => {
  render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <MainMap />
    </MockedProvider>
  );

  await waitFor(() => {
    expect(screen.getByTestId("map-controls")).toBeInTheDocument();
  });
});

it("renders markers on the map", async () => {
  const siteMocks = [
    {
      request: {
        query: GET_SITES_QUERY,
      },
      result: {
        data: {
          ako_site: [
            { id: 1, name: "Site 1", location: { lat: 10, lng: 10 } },
            { id: 2, name: "Site 2", location: { lat: 20, lng: 20 } },
          ],
        },
      },
    },
  ];

  render(
    <MockedProvider mocks={siteMocks} addTypename={false}>
      <MainMap />
    </MockedProvider>
  );

  await waitFor(() => {
    expect(screen.getByTestId("marker-1")).toBeInTheDocument();
  });
});

it("handles map interactions", async () => {
  render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <MainMap />
    </MockedProvider>
  );

  await waitFor(() => {
    const mapElement = screen.getByTestId("MainArkeoMap");
    expect(mapElement).toBeInTheDocument();

    // Simulate map interaction
    mapElement.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    //expect(screen.getByTestId("map-interaction")).toBeInTheDocument();
  });
});
