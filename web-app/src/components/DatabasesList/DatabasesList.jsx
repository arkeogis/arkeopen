import React from 'react'
import PropTypes from "prop-types";
import { useQuery } from '@apollo/client'
import { GET_DATABASES_QUERY } from '../../lib/queries/database'

/**
 * Component that display a list of databases
 */
const DatabasesList = ({isPublic=false}) => {
    const { loading, error, data } = useQuery(GET_DATABASES_QUERY, {
        variables: { public: isPublic }
    })

    if (loading) {
        return (
            <div>Loading...</div>
        )
    }

    if (error) {
        return (
            <div className="error">Error: {error.message}</div>
        )
    }

    return (
        <ul>
            {data.database.map((database, index) => (
                <li key={database.id}>{database.name}</li>
            ))}            
        </ul>
    )
}

DatabasesList.propTypes = {
    /**
     * Filter the listed database with the public field set to this value (default is true)
     */
    isPublic: PropTypes.bool,
}

export default DatabasesList
