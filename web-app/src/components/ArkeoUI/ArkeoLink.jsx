import React from "react";
import useStore from "../../lib/store";
import { shallow } from "zustand/shallow";
import { useAuthStore } from "../../lib/auth";
import { useNavigate } from "react-router-dom";

export const ArkeoLink = (props) => {
  const { children, rightPanel, leftPanel, projectPage, helpPage, sidePanelOpened, logout, showBurgerModal=false, onClick, to, ...restProps } = props;
  const [setShowBurgerModal, setSidePanelOpened, setRightPanel, setLeftPanel, setProjectPage, setHelpPage] = useStore(
    (state) => [
      state.setShowBurgerModal,
      state.setSidePanelOpened,
      state.setRightPanel,
      state.setLeftPanel,
      state.setProjectPage,
      state.setHelpPage,
    ],
    shallow
  );
  const setLoggedOut = useAuthStore((state) => state.setLoggedOut);
  const navigate = useNavigate();

  const onClicked = e => {
    e.preventDefault();
    if (rightPanel !== undefined) setRightPanel(rightPanel, true);
    if (leftPanel !== undefined) {
      console.log("leftPanel: ", leftPanel);
      setLeftPanel(leftPanel);
      if (sidePanelOpened === undefined) setSidePanelOpened("left");
      if (document.location.pathname !== "/") {
        setTimeout(() => {
          console.log("navigate...", "/"+document.location.search);
          navigate("/"+document.location.search);
        }, 0);
      }
    }
    if (projectPage !== undefined) setProjectPage(projectPage);
    if (helpPage !== undefined) setHelpPage(helpPage);
    if (logout !== undefined) {
      setLoggedOut();
    }
    if (sidePanelOpened !== undefined) {
      setSidePanelOpened(sidePanelOpened);
    }
    console.log("show burger modal :", showBurgerModal)
    setShowBurgerModal(showBurgerModal);
    if (to !== undefined) {
      setTimeout(() => {
        navigate(to);
      }, 0);
    }
    if (onClick !== undefined) onClick(e);
  }

  return (
    <a
      onClick={onClicked}
      href={ to ? to : "/#"}
      rel="noreferrer"
      {...restProps}
    >
      {children}
    </a>
  );

};

export default ArkeoLink;
