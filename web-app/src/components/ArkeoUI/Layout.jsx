import React from "react";

import styles from "./Layout.module.scss";

const Grid = ({ areas, columns = "auto", rows = "auto", gap = "0", style, children }) => {
  return (
    <div
      style={{
        display: "grid",
        gridTemplateAreas: areas,
        gridTemplateColumns: columns,
        gridTemplateRows: rows,
        gap,
        ...style,
      }}
    >
      {children}
    </div>
  );
};

Grid.Item = ({ children, area, as: Component = 'div', style, ...props }) => {
  return (
    <Component
      {...props}
      style={{
        ...style,
        gridArea: area,
      }}
    >
      {children}
    </Component>
  );
};

export { Grid };
