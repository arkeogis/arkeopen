import React from "react";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import { default as BootstrapTooltip } from "react-bootstrap/Tooltip";

import styles from "./Overlay.module.scss";

export const Tooltip = ({ children, content, placement = "top", ...rest }) => {
  return (
    <OverlayTrigger
      placement={placement}
      overlay={<BootstrapTooltip className={styles.tooltip}>{content}</BootstrapTooltip>}
      {...rest}
    >
      <div>{children}</div>
    </OverlayTrigger>
  );
};
