import { render } from "@testing-library/react";
import i18n from "../../i18n";
import { ContentWithLabel, Align } from "./ArkeoUI";

it("should render label with French colon separator and children content when given default props", () => {
  const { container } = render(
    <ContentWithLabel label="Test Label">
      <span>Test Content</span>
    </ContentWithLabel>
  );

  expect(container.querySelector("div:first-child")).toHaveTextContent("Test Label : ");
  expect(container.querySelector("div:last-child")).toHaveTextContent("Test Content");
});

it("should render label with English colon separator and children content when given default props", () => {
  const { container } = render(
    <ContentWithLabel label="Test Label" lang="en">
      <span>Test Content</span>
    </ContentWithLabel>
  );

  expect(container.querySelector("div:first-child")).toHaveTextContent("Test Label:");
  expect(container.querySelector("div:last-child")).toHaveTextContent("Test Content");
});

it("should render children elements inside div container", () => {
  const children = <span>Test content</span>;

  const { container } = render(<Align>{children}</Align>);

  const divElement = container.firstChild;
  expect(divElement.tagName).toBe("DIV");
  expect(divElement).toContainElement(container.querySelector("span"));
  expect(divElement.textContent).toBe("Test content");
});
