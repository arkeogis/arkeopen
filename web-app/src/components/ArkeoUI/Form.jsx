import React, { useCallback, useEffect, useRef, useState } from "react";
import { debounce } from "lodash";
import { useQuery } from "@apollo/client";
import { default as ReactSelect } from "react-select";
import { default as ReactAsyncSelect } from "react-select/async";
import classNames from "classnames";
import { Tooltip } from "./Overlay";
import DatePicker from "react-date-picker";
import { InfoIcon } from "../Icons/Icons";
import { getApolloClient } from "../ApolloSetup/ApolloSetup";

import "react-date-picker/dist/DatePicker.css";
import "react-calendar/dist/Calendar.css";
import styles from "./Form.module.scss";

export const FormFieldContainer = ({ title, infoText, className, children, mandatory }) => {
  return (
    <div className={`${styles.fieldContainer} ${className || ""}`}>
      <div className={styles.header}>
        <div className={`${styles.title} ${mandatory ? styles.mandatory : ""}`}>{title}</div>
        <Tooltip content={infoText}>
          <InfoIcon className={styles.infoIcon} />
        </Tooltip>
      </div>
      {children}
    </div>
  );
};

export const FormError = ({ errors, field }) => {
  return errors && errors[field] ? <div className={styles.formError}>{errors[field]}</div> : null;
};

export const TextInput = ({ maxLength = -1, className, onChange, value, inputRef = null, ...rest }) => {
  const [internalValue, setInternalValue] = useState(value);

  const handleChange = (e) => {
    const inputValue = e.target.value;
    if (maxLength < 0 || inputValue.length <= maxLength) {
      setInternalValue(inputValue);
      if (typeof onChange === "function") {
        onChange(e);
      }
    }
  };

  const remainingChars = internalValue ? maxLength - internalValue.length : maxLength;

  return (
    <div className={classNames(styles.textInput, className)}>
      <input ref={inputRef} value={internalValue} type="text" onChange={handleChange} {...rest} />
      {maxLength > 0 && (
        <div className={`${styles.remainingChars} ${remainingChars === 0 ? styles.warning : ""}`}>{remainingChars}</div>
      )}
    </div>
  );
};

export const TextArea = ({ maxLength = 0, className, value = "", height = 80, onChange, ...rest }) => {
  const [internalValue, setInternalValue] = useState(value);
  const ref = useRef(null);

  const handleChange = (e) => {
    const inputValue = e.target.value;
    if (maxLength > 0 && inputValue.length <= maxLength) {
      setInternalValue(e.target.value);
      if (typeof onChange === "function") {
        onChange(e);
      }
    }
  };

  const remainingChars = internalValue ? maxLength - internalValue.length : maxLength;

  return (
    <div
      onClick={() => {
        ref.current.focus();
      }}
      style={{ height }}
      className={`${styles.textArea} ${className ? className : ""}`}
    >
      <textarea ref={ref} value={internalValue} onChange={handleChange} maxLength={maxLength} {...rest} />
      {maxLength > 0 && (
        <div className={`${styles.remainingChars} ${remainingChars === 0 ? styles.warning : ""}`}>{remainingChars}</div>
      )}
    </div>
  );
};

export const Select = ({ options, onChange, value, ...rest }) => {
  const isMulti = rest.isMulti;

  const getInitialValue = (val, multi) => {
    if (multi) {
      return Array.isArray(val) ? options.filter((opt) => val.includes(opt.value)) : [];
    }
    return options.find((opt) => opt.value === val) || null;
  };

  const [selectedValue, setSelectedValue] = useState(getInitialValue(value, isMulti));

  useEffect(() => {
    setSelectedValue(getInitialValue(value, isMulti));
  }, [value, isMulti]);

  const handleChange = (selectedOption) => {
    const newValue = isMulti ? (selectedOption || []).map((opt) => opt.value) : selectedOption?.value ?? null;
    setSelectedValue(selectedOption || (isMulti ? [] : null));
    onChange?.(newValue);
  };

  return (
    <ReactSelect
      classNames={{
        control: () => styles.selectControl,
        menu: () => styles.selectMenu,
        option: ({ isFocused, isSelected }) =>
          isSelected ? styles.selectOptionSelected : isFocused ? styles.selectOptionFocused : styles.selectOption,
        multiValue: () => styles.multiValue,
        multiValueRemove: () => styles.multiValueRemove,
        indicatorsContainer: () => styles.indicatorContainer,
      }}
      options={options}
      onChange={handleChange}
      isOptionSelected={(option, selectedValue) => {
        if (!selectedValue) return false;
        return isMulti
          ? Array.isArray(selectedValue) && selectedValue.some((v) => v.value === option.value)
          : option.value === selectedValue?.value;
      }}
      value={selectedValue}
      {...rest}
    />
  );
};

export const AsyncSelect = ({
  query,
  queryByKey,
  labelField,
  valueField,
  onChange,
  transformData,
  loadOnInit = false,
  value,
  ...rest
}) => {
  const client = getApolloClient();
  const isMulti = rest.isMulti;

  // Fallback for initial state: just a string conversion of the value(s)
  const getInitialValue = (val, multi) => {
    if (multi) {
      const values = Array.isArray(val) ? val : [];
      return values.map((v) => ({ value: v, label: String(v) }));
    }
    return val != null ? { value: val, label: String(val) } : null;
  };

  const [selectedValue, setSelectedValue] = useState(getInitialValue(value, isMulti));

  // On mount only, if a value is provided, fetch the corresponding options
  useEffect(() => {
    async function loadInitialOptions() {
      if (value == null || (isMulti && (!Array.isArray(value) || value.length === 0))) return;

      try {
        let fetchedData;
        if (queryByKey) {
          // Use the queryByKey if provided; adjust variable names as needed.
          fetchedData = await client.query({
            query: queryByKey,
            variables: { search: isMulti ? value : value },
          });
        } else {
          // Fallback: use the default query with a wildcard search.
          fetchedData = await client.query({
            query,
            variables: { search: "%" },
          });
        }
        const rawData = fetchedData.data?.[Object.keys(fetchedData.data)[0]] || [];
        const options = (transformData ? rawData.map(transformData) : rawData).map((item) => ({
          value: item[valueField],
          label: item[labelField],
        }));
        const values = isMulti ? value : [value];
        const selectedOptions = values.map(
          (val) => options.find((o) => o.value === val) || { value: val, label: String(val) }
        );
        setSelectedValue(isMulti ? selectedOptions : selectedOptions[0]);
      } catch (error) {
        console.error("Error fetching initial options:", error);
      }
    }

    loadInitialOptions();
    // Empty dependency array ensures this runs only on mount.
  }, []);

  // Function to load options as the user types.
  const loadOptions = async (inputValue) => {
    const effectiveInput =
      inputValue && inputValue.trim() !== ""
        ? `%${inputValue}%`
        : loadOnInit
        ? "%"
        : "";
    if (!effectiveInput) return [];

    try {
      const { data } = await client.query({
        query,
        variables: { search: effectiveInput },
      });
      const rawData = data?.[Object.keys(data)[0]] || [];
      return (transformData ? rawData.map(transformData) : rawData).map((item) => ({
        value: item[valueField],
        label: item[labelField],
      }));
    } catch (error) {
      console.error("Error fetching options:", error);
      return [];
    }
  };

  const debouncedLoadOptions = useCallback(
    debounce((inputValue, callback) => {
      loadOptions(inputValue).then(callback);
    }, 500),
    [client, query, labelField, valueField, transformData, loadOnInit]
  );

  const handleChange = (selectedOption) => {
    const newValue = isMulti
      ? (selectedOption || []).map((opt) => opt.value)
      : selectedOption?.value ?? null;

    setSelectedValue(selectedOption || (isMulti ? [] : null));
    onChange?.(newValue);
  };

  useEffect(() => {
    return () => debouncedLoadOptions.cancel();
  }, [debouncedLoadOptions]);

  return (
    <ReactAsyncSelect
      classNames={{
        control: () => styles.selectControl,
        menu: () => styles.selectMenu,
        option: ({ isFocused, isSelected }) =>
          isSelected
            ? styles.selectOptionSelected
            : isFocused
            ? styles.selectOptionFocused
            : styles.selectOption,
        multiValue: () => styles.multiValue,
        multiValueRemove: () => styles.multiValueRemove,
        indicatorsContainer: () => styles.indicatorContainer,
      }}
      isOptionSelected={(option, selectedValue) => {
        if (!selectedValue) return false;
        return isMulti
          ? Array.isArray(selectedValue) && selectedValue.some((v) => v.value === option.value)
          : option.value === selectedValue?.value;
      }}
      value={selectedValue}
      loadOptions={debouncedLoadOptions}
      defaultOptions={loadOnInit ? true : undefined}
      onChange={handleChange}
      {...rest}
    />
  );
};


export const DateInput = ({ onChange, value, className, ...rest }) => {
  const [internalValue, setInternalValue] = useState(value);
  const handleChange = (newValue) => {
    setInternalValue(newValue);
    onChange?.(newValue);
  };
  return (
    <DatePicker
      className={classNames(styles.datePicker, className)}
      calendarProps={{
        className: styles.calendar,
        tileClassName: [styles.tile],
      }}
      onChange={handleChange}
      value={internalValue}
      {...rest}
    />
  );
};

export const Button = ({ children, className, variant, type = "button", ...rest }) => {
  let styleClass = null;
  switch (variant) {
    case "primary":
      styleClass = styles.primary;
      break;
    case "accent":
      styleClass = styles.accent;
      break;
    default:
      styleClass = styles.default;
  }
  return (
    <button type={type} className={classNames(styles.button, styleClass, className)} {...rest}>
      {children}
    </button>
  );
};
