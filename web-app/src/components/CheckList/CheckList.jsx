import React from "react";

import styles from "./CheckList.module.scss";

const CheckList = ({items, className}) => {
  return (
    <div className={`${styles.checkList} ${className ? className : ""}`}>
      {items.map((item) => (
        <div key={item.label} className={styles.item}>
          <div className={styles.label}>{item.label}</div>
          <div className={`${styles.number} ${item.accent ? styles.accent : ""}`}>{item.num}</div>
        </div>
      ))}
    </div>
  )

}

export default CheckList
