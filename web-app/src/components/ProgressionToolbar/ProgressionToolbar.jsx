import React, { useEffect, useState } from "react";
import classNames from "classnames";
import { useTranslation } from "react-i18next";
import Popover from "../Popover/Popover";
import styles from "./ProgressionToolbar.module.scss";

const ProgressionToolbar = ({ children }) => {
  return <div className={styles.progressionToolbar}>{children}</div>;
};

ProgressionToolbar.Steps = ({
  labels,
  activeStepIndex = 0,
  popOverProps,
  disableFromIndex,
  className,
  onClick,
  ...rest
}) => {
  const { t } = useTranslation();
  const [activeNum, setActiveNum] = useState(activeStepIndex);

  useEffect(() => {
    setActiveNum(activeStepIndex);
  }, [activeStepIndex]);

  return (
    <div className={classNames(styles.progressionToolbarSteps, className)} {...rest}>
      <div className={styles.label}>{t("components.ProgressionSteps.label")}</div>
      {labels.map((label, index) => (
        <Popover position="top" triggerType="hover" content={label} key={`step${index}`} {...popOverProps}>
          <div
            className={classNames(styles.number, {
              [styles.active]: activeNum === index,
              [styles.disabled]: index >= disableFromIndex - 1,
            })}
            onClick={() => {
              if (index >= disableFromIndex - 1) {
                return;
              }
              setActiveNum(index);
              onClick(index);
            }}
          >
            {index + 1}
          </div>
        </Popover>
      ))}
    </div>
  );
};
ProgressionToolbar.ButtonContainer = ({ children }) => {
  return <div className={styles.buttonContainer}>{children}</div>;
};

export default ProgressionToolbar;
