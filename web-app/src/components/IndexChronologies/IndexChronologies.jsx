import React, { useEffect, useState } from "react";
import { useQuery } from "@apollo/client";
import { DataType, SortDirection } from "ka-table";
import gql from "graphql-tag";
import { useTranslation } from "react-i18next";
import akprefix from "../../lib/akprefix";
import { getInLangWKey } from "../../lib/translate";
import { yearToHumanNumber } from "../../lib/year";
import ArkeoLink from "../ArkeoUI/ArkeoLink";
import Index from "../Index/Index";

import "./IndexChronologies.scss";

// GraphQL query to fetch chronology data
const GET_DATA = gql`
  query GetRootChronologies {
    ${akprefix}chronology_root(where: { active: { _eq: true } }) {
      chronology_root_trs {
        lang_isocode
        geographical_covering
      }
      chronology {
        chronology_trs {
          lang_isocode
          name
          description
        }
        id
        end_date
        start_date
        created_at
        updated_at
      }
      chronology_collection {
        name
      }
      editor
      editor_uri
      cached_langs
      active
      opened
    }
  }
`;

// Custom cell component for rendering chronology links
const ArkeoLinkCustomCell = ({ column, rowKeyValue, value }) => {
  return <ArkeoLink rightPanel={{ type: "chronology", id: rowKeyValue }}>{value}</ArkeoLink>;
};

const IndexChronologies = () => {
  const { t, i18n } = useTranslation();
  const { loading, error, data } = useQuery(GET_DATA);
  const [IndexChronologies, setIndexChronologies] = useState([]);
  const [columns, setColumns] = useState([]);

  // Process data when it's loaded or language changes
  useEffect(() => {
    if (loading || error) {
      setIndexChronologies([]);
      return;
    }

    // Transform API data to table rows
    const _IndexChronologies = data[`${akprefix}chronology_root`].map((chr) => {
      return {
        id: chr.chronology.id,
        name: getInLangWKey(chr.chronology.chronology_trs, "name", i18n.language),
        collection: chr.chronology_collection.name,
        editor: chr.editor,
        langs: chr.cached_langs.toUpperCase(),
        start_date: yearToHumanNumber(chr.chronology.start_date),
        end_date: yearToHumanNumber(chr.chronology.end_date),
        shared: chr.active ? (chr.opened ? "ArkeOpen" : "ArkeoGIS") : t("components.IndexChronologies.not_active"),
        created_at: new Date(chr.chronology.created_at),
        updated_at: new Date(chr.chronology.updated_at),
      };
    });

    setIndexChronologies(_IndexChronologies);
  }, [loading, error, data, i18n.language]);

  // Setup columns when language changes
  useEffect(() => {
    const _columns = [
      {
        key: "id",
        title: t("components.IndexChronologies.index.id"),
        dataType: DataType.Number,
        sortDirection: SortDirection.Ascend,
        width: "50px",
      },
      {
        key: "name",
        title: t("components.IndexChronologies.index.name"),
        dataType: DataType.String,
        customCellComponent: ArkeoLinkCustomCell,
      },
      {
        key: "collection",
        title: t("components.IndexChronologies.index.collection"),
        dataType: DataType.String,
        width: "110px",
      },
      {
        key: "editor",
        title: t("components.IndexChronologies.index.editor"),
        dataType: DataType.String,
        width: "150px",
      },
      {
        key: "langs",
        title: t("components.IndexChronologies.index.langs"),
        dataType: DataType.String,
        width: "110px",
      },
      {
        key: "start_date",
        title: t("components.IndexChronologies.index.startDate"),
        dataType: DataType.Number,
        isFilterable: false,
        width: "90px",
      },
      {
        key: "end_date",
        title: t("components.IndexChronologies.index.endDate"),
        dataType: DataType.Number,
        isFilterable: false,
        width: "90px",
      },
      {
        key: "shared",
        title: t("components.IndexChronologies.index.shared"),
        isFilterable: false,
        width: "100px",
      },
      {
        key: "created_at",
        title: t("components.IndexChronologies.index.createdAt"),
        dataType: DataType.Date,
        isFilterable: false,
        width: "90px",
      },
      {
        key: "updated_at",
        title: t("components.IndexChronologies.index.updatedAt"),
        dataType: DataType.Date,
        isFilterable: false,
        width: "90px",
      },
    ];

    setColumns(_columns);
  }, [i18n.language, t]);

  // Loading states
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error.message}</p>;

  // Format function for formatting cell values
  const format = ({ column, value }) => {
    if (column.dataType === DataType.Date) {
      return (
        value &&
        new Date(value).toLocaleDateString(i18n.language, { month: "2-digit", day: "2-digit", year: "numeric" })
      );
    }
  };

  return (
    <Index
      data={IndexChronologies}
      columns={columns}
      rowKeyField="id"
      format={format}
      className="IndexChronologies"
      textSearch={true}
      title={
        <>
          <div>{t`components.IndexChronologies.title1`}</div>
          <div>{t`components.IndexChronologies.title2`}</div>
        </>
      }
    />
  );
};

export default IndexChronologies;
