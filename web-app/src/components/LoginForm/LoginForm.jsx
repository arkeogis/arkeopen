import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { authSchema } from "../../lib/schemas/auth";
import { useShallow } from "zustand/react/shallow";
import { checkAuth, useAuthStore } from "../../lib/auth";
import { useLocation, useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import FundersLogos from "../FundersLogos/FundersLogos";
import Header from "../Header/Header";
import styles from "./LoginForm.module.scss";
import { CrossedEye, Eye } from "../Icons/Icons";

const LoginForm = () => {
  const userauth = useAuthStore(
    useShallow((state) => ({
      setLoggedIn: state.setLoggedIn,
      isAuthenticated: state.isAuthenticated,
    }))
  );

  const location = useLocation();
  const navigate = useNavigate();
  const from = location.state?.from || "/";
  const [loginError, setLogginError] = useState(false);
  const { t } = useTranslation();
  const [passwordVisible, setPasswordVisible] = useState(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: zodResolver(authSchema),
  });

  // Submit handler
  const onSubmit = async (data) => {
    const { token, user } = await checkAuth(data.username, data.password);
    if (token) {
      console.log(`Welcome ${user.firstname} ${user.lastname}`, user.id);
      userauth.setLoggedIn(token, user);
      navigate(from || "/", { replace: true });
    } else {
      setLogginError(true);
    }
  };

  // If user is already authenticated, redirect to "/"
  useEffect(() => {
    if (userauth.isAuthenticated) {
      navigate("/");
    }
  }, []);

  return (
    <div className={styles.LoginForm}>
      <Header className={styles.disabled} />
      <div className={styles.loginForm}>
        <div className={styles.title} dangerouslySetInnerHTML={{ __html: t("components.LoginForm.title.label") }}></div>
        <div className={styles.cartouche}>
          {t("components.LoginForm.cartouche.label_start")}
          <a
            target="_blank"
            className={`${styles.link} no-icon`}
            href={t("components.LoginForm.cartouche.url.link")}
            rel="noreferrer"
          >
            {t("components.LoginForm.cartouche.url.label")}
          </a>
          {t("components.LoginForm.cartouche.label_end")}
        </div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div>
            <input
              className={styles.username}
              id="username"
              type="text"
              {...register("username")}
              placeholder={t("components.LoginForm.input.login.placeholder")}
            />
            {/* {errors.username && <p>{errors.username.message}</p>} */}
          </div>
          <div className={styles.passwordContainer}>
            <input
              className={styles.password}
              id="password"
              type={passwordVisible ? "text" : "password"}
              {...register("password")}
              placeholder={t("components.LoginForm.input.password.placeholder")}
            />
            <div
              className={styles.eye}
              onClick={() => {
                setPasswordVisible((state) => !state);
              }}
            >
              {passwordVisible ? <CrossedEye /> : <Eye />}
            </div>
            {/* {errors.password && <p>{errors.password.message}</p>} */}
          </div>
          {loginError && <div className={styles.loginError}>{t("components.LoginForm.error.loginfailed.label")}</div>}
          <button type="submit">{t("components.LoginForm.button.label")}</button>
          <a
            className={`${styles.link} no-icon`}
            target="_blank"
            href={t("components.LoginForm.forgotten_password.url")}
            rel="noreferrer"
          >
            {t("components.LoginForm.forgotten_password.label")}
          </a>
          <div className={styles.accessInfos}>
            {t("components.LoginForm.access_info.label")}
            <div>
              <a
                target="_blank"
                className={`${styles.link} ${styles.accessDemand} no-icon`}
                href="http://arkeogis.org/fr/demande-dacces/"
                rel="noreferrer"
              >
                {t("components.LoginForm.access_demand.label")}
              </a>
            </div>
          </div>
          <FundersLogos className={styles.funderLogos} />
        </form>
      </div>
      <div className={styles.footerGradient}></div>
    </div>
  );
};

export default LoginForm;
