// react
import React from "react";
//import PropTypes from 'prop-types';
//

// zustand
import useStore, { useSearchStore } from "../../lib/store";
import { useShallow } from "zustand/react/shallow";
//

// bootstrap
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
//

// i18n
import { useTranslation } from "react-i18next";
//

import ChronologySelector from "../ChronologySelector/ChronologySelector";
import ChronologyMenu from "./ChronologyMenu";

import "./FormSearchWhen.scss";

import { yearToHumanNumber, HumanNumberToComputerYear, UNDETERMINED_LEFT, UNDETERMINED_RIGHT } from "../../lib/year";
import { useAuthStore } from "../../lib/auth";

FormSearchWhen.propTypes = {};

/**
 * The "When" Tab in the Search Form (left side bar)
 * @param {*} props
 * @returns The 'When' Component
 */
function FormSearchWhen(props) {
  const { t, i18n } = useTranslation();
  // const selectedChronologyId = useSearchStore(state => state.selectedChronologyId);
  const [
    selectedChronologyId,
    chronologyStartDate,
    setChronologyStartDate,
    chronologyEndDate,
    setChronologyEndDate,
    chronologyFindIncludeUndetermined,
    setChronologyFindIncludeUndetermined,
    chronologyFindOnlyInside,
    setChronologyFindOnlyInside,
  ] = useSearchStore(
    useShallow((state) => [
      state.selectedChronologyId,
      state.chronologyStartDate,
      state.setChronologyStartDate,
      state.chronologyEndDate,
      state.setChronologyEndDate,
      state.chronologyFindIncludeUndetermined,
      state.setChronologyFindIncludeUndetermined,
      state.chronologyFindOnlyInside,
      state.setChronologyFindOnlyInside,
    ])
  );
  const chronologies = useStore((state) => state.chronologies);
  console.log("chronologies: ", chronologies);
  const project = useAuthStore((state) => state.project);

  const selectedChronology = chronologies.find((c) => c.id === selectedChronologyId);

  // tests
  //console.log("project", project);
  //project.start_date = -23;
  //project.end_date = 2500;

  return (
    <div className="FormSearchWhen">
      {__ARKEOPEN__ ? (
        <div>
          <Form.Label>{t("components.FormSearchWhen.label")}</Form.Label>

          <ChronologySelector />
        </div>
      ) : (
        <React.Fragment />
      )}

      <div>
        <Form.Label>{t("components.FormSearchWhen.chronology.period.label")}</Form.Label>

        {selectedChronology ? (
          <ChronologyMenu
            chronologies={chronologies}
            chronology={selectedChronology}
            lang={i18n.language}
            path={[selectedChronology.id]}
            //disabled={chronologyStartDate !== UNDETERMINED_LEFT || chronologyEndDate !== UNDETERMINED_RIGHT}
            project_start_date={__ARKEOGIS__ ? project.start_date : undefined}
            project_end_date={__ARKEOGIS__ ? project.end_date : undefined}
          />
        ) : (
          "Loading..."
        )}
      </div>

      <div>
        <Form.Label>{t("components.FormSearchWhen.period.label")}</Form.Label>

        <div className="periodselector">
          <FormControl
            type="number"
            placeholder={t("components.FormSearchWhen.period-start.label")}
            step={1}
            value={yearToHumanNumber(chronologyStartDate)}
            onChange={(e) => setChronologyStartDate(HumanNumberToComputerYear(e.target.value, UNDETERMINED_LEFT))}
          />
          <FormControl
            type="number"
            placeholder={t("components.FormSearchWhen.period-end.label")}
            step={1}
            value={yearToHumanNumber(chronologyEndDate)}
            onChange={(e) => setChronologyEndDate(HumanNumberToComputerYear(e.target.value, UNDETERMINED_RIGHT))}
          />
        </div>
      </div>

      <div>
        <Form.Label>{t("components.FormSearchWhen.chronology.include.label")}</Form.Label>

        <div className="switchsContainer">
          <Form.Check
            label={t("components.FormSearchWhen.chronology.include.undetermined.label")}
            type="switch"
            checked={chronologyFindIncludeUndetermined === true}
            onChange={(e) => setChronologyFindIncludeUndetermined(e.target.checked)}
          />

          <Form.Check
            label={t("components.FormSearchWhen.chronology.include.only-inside.label")}
            type="switch"
            checked={!!chronologyFindOnlyInside === true}
            onChange={(e) => setChronologyFindOnlyInside(e.target.checked)}
          />
        </div>
      </div>
    </div>
  );
}

export default FormSearchWhen;
