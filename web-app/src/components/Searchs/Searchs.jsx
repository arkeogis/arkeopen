import Button from "react-bootstrap/Button";

import React from "react";
import { CloseIcon, SearchIcon } from "../Icons/Icons";

import SlidableTabs from "../SlidableTabs/SlidableTabs";

import MySearchs from "./MySearchs";
import BookmarkedSearchs from "./BookmarkedSearchs";

// i18n
import { useTranslation } from "react-i18next";

// zustand
import useStore, { useSearchStore, useValidatedSearchStore } from "../../lib/store";

import "./Searchs.scss";

const SearchsButtons = (props) => {
  const { t /*, i18n*/ } = useTranslation();

  const setLeftPanel = useStore((state) => state.setLeftPanel);

  return (
    <div className="SearchButtons">
      <Button className="SearchButton big" variant="light" onClick={() => setLeftPanel("search")}>
        {t("components.Searchs.button-newSearch.label")}{" "}
        <span style={{ fontSize: 10 }}>
          <SearchIcon />
        </span>
      </Button>
    </div>
  );
};

const Searchs = (props) => {
  const { t /*, i18n*/ } = useTranslation();
  const setSidePanelOpened = useStore((state) => state.setSidePanelOpened);

  const tabs = [
    {
      code: "MySearchs",
      text: t("components.Searchs.MySearchs.title"),
      content: <MySearchs />,
    },
    {
      code: "BookmarkedSearchs",
      text: t("components.Searchs.BookmarkedSearchs.title"),
      content: <BookmarkedSearchs />,
    },
  ];

  return (
    <div className="Searchs" style={props.show ? {} : { display: "none" }}>
      <div className="SearchForm">
        <div className="SearchHeader">
          <div className="SaerchHeaderTitle">{t("components.Searchs.title")}</div>
          <div className="SaerchHeaderButtons">
            <button
              className="CloseButton"
              onClick={() => {
                setSidePanelOpened("none");
              }}
            >
              <CloseIcon />
            </button>
          </div>
        </div>
        <SlidableTabs className="SlidableTabs" id="SearchsTabs" tabs={tabs} hideNavigationOnBigScreen></SlidableTabs>
      </div>
      <SearchsButtons />
    </div>
  );
};

export default Searchs;
