import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import _ from "underscore";
import { useTranslation } from "react-i18next";

// zustand
import useStore from "../../lib/store";

// Icons
import {
  QueryAddBookmark,
  QueryDownload,
  QueryEdit,
  QueryEditBig,
  QueryExcel,
  QueryEye,
  QueryEyeHide,
  QueryInfo,
  QueryLetter,
  QueryOpened,
  QueryTrash,
} from "../Icons/Icons";
import { GetDatabaseName } from "../../lib/queries/database";
import { useQuery } from "@apollo/client";

const DatabaseLine = ({ dbid, sitescount, hide, onToggleHide }) => {
  const { loading, error, data } = useQuery(GetDatabaseName, { variables: { id: dbid } });
  const setRightPanel = useStore((state) => state.setRightPanel);
  return (
    <div className="database">
      <div className="showhide">
        <button onClick={() => onToggleHide()}>{hide ? <QueryEyeHide /> : <QueryEye />}</button>
      </div>
      <div className="dbname">
        {loading || error ? (
          "?"
        ) : (
          <a
            href="/#"
            rel="noreferrer"
            onClick={(e) => {
              e.preventDefault();
              setRightPanel({ type: "db", id: dbid });
            }}
          >
            {data.akg_database_by_pk.name}
          </a>
        )}
      </div>
      <div className="sitescount">{sitescount}</div>
    </div>
  );
};

const ButtonsRow = ({ arkeoLayer, resultsCount }) => {
  const hided_dbs = arkeoLayer.useStore((state) => state.hide_database_ids);
  const removeArkeoLayerByInstance = useStore((state) => state.removeArkeoLayerByInstance);
  const { t /*, i18n*/ } = useTranslation();

  const onToggleHide = () => {
    if (hided_dbs.length > 0) arkeoLayer.showAllDatabases();
    else arkeoLayer.hideAllDatabases();
  };

  const queryTrash = () => {
    removeArkeoLayerByInstance(arkeoLayer);
    arkeoLayer.removeFromMap();
    arkeoLayer.dispose();
  };

  return (
    <div className="ButtonsRow">
      <button onClick={() => onToggleHide()}>{hided_dbs.length > 0 ? <QueryEyeHide /> : <QueryEye />}</button>
      <button onClick={() => queryEdit()}>{hided_dbs.length > 0 ? <QueryEdit /> : <QueryEdit />}</button>
      <button onClick={() => queryInfo()}>{hided_dbs.length > 0 ? <QueryInfo /> : <QueryInfo />}</button>
      <button onClick={() => queryExcel()}>{hided_dbs.length > 0 ? <QueryExcel /> : <QueryExcel />}</button>
      <button onClick={() => queryAddBookmark()}>
        {hided_dbs.length > 0 ? <QueryAddBookmark /> : <QueryAddBookmark />}
      </button>
      <button onClick={() => queryDownload()}>{hided_dbs.length > 0 ? <QueryDownload /> : <QueryDownload />}</button>
      <button onClick={() => queryTrash()}>{hided_dbs.length > 0 ? <QueryTrash /> : <QueryTrash />}</button>
      <div className="resultsCount">
        <span className="title">{t`components.Searchs.MySearchs.MySearch.resultsCount.title`}</span>
        {resultsCount}
      </div>
    </div>
  );
};

const MySearch = ({ arkeoLayer }) => {
  // sort all databases
  //console.log("geojson: ", arkeoLayer.geojson);

  const hided_dbs = arkeoLayer.useStore((state) => state.hide_database_ids);
  const setHided_dbs = arkeoLayer.useStore((state) => state.setHideDatabaseIds);
  const ungrouped = arkeoLayer.useStore((state) => state.ungrouped);
  const name = arkeoLayer.useStore((state) => state.name);

  const [show, setShow] = useState(true);

  const toggleHide = (database_id) => {
    const hide_database_ids = [...hided_dbs];
    const idx = hide_database_ids.indexOf(database_id);
    idx !== -1 ? hide_database_ids.splice(idx, 1) : hide_database_ids.push(database_id);

    setHided_dbs(hide_database_ids);
  };

  let sitescountbydb = {};
  let resultsCount = arkeoLayer.orig_geojson.features.length;
  for (const feature of arkeoLayer.orig_geojson.features) {
    if (sitescountbydb[feature.properties.database_id]) sitescountbydb[feature.properties.database_id].count++;
    else sitescountbydb[feature.properties.database_id] = { database_id: feature.properties.database_id, count: 1 };
  }

  sitescountbydb = _.sortBy(sitescountbydb, (v) => -v.count);
  //console.log("sitescountbydb", sitescountbydb);

  const letter = arkeoLayer._getLetter();

  return (
    <div className="MySearch">
      <div className="letterCol">
        <QueryLetter letter={letter} />
      </div>
      <div className="RightBloc">
        <div className="HeaderRow">
          <div className="title"> {name}</div>
          <div className="buttons">
            <button>
              <QueryEditBig />
            </button>
            <button onClick={() => setShow(!show)} className={show ? "showing" : "hidding"}>
              <QueryOpened />
            </button>
          </div>
        </div>
        <div className={"SearchDetails" + (show ? " showing" : " hidding")}>
          <ButtonsRow arkeoLayer={arkeoLayer} resultsCount={resultsCount} />
          <div className="databases">
            {sitescountbydb.map((sitescount) => (
              <DatabaseLine
                key={sitescount.database_id}
                dbid={sitescount.database_id}
                sitescount={sitescount.count}
                hide={hided_dbs.includes(sitescount.database_id)}
                onToggleHide={() => toggleHide(sitescount.database_id)}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

const MySearchs = (props) => {
  const arkeoLayers = useStore((state) => state.arkeoLayers);

  return (
    <div className="MySearchs">
      {arkeoLayers.map((arkeoLayer) => (
        <MySearch key={arkeoLayer.id} arkeoLayer={arkeoLayer} />
      ))}
    </div>
  );
};

export default MySearchs;
