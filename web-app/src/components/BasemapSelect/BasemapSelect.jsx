// react
import React from 'react'
import PropTypes from 'prop-types'
//

import useStore from '../../lib/store'
import {shallow} from 'zustand/shallow'

// bootstrap
import Form from 'react-bootstrap/Form';
//

// i18n
import { useTranslation } from 'react-i18next';
//

function BasemapSelect(props) {
    const { t, i18n } = useTranslation();
    const [ basemap, setBasemap ] = useStore(state => [ state.basemap, state.setBasemap], shallow)
    const handleChange = (event) => {
        setBasemap(event.target.value)
        props.onChange && props.onChange(event.target.value)
    }

    return (
      <Form.Select
        id="basemal-select"
        value={basemap}
        placeholder={t('components.BasemapSelect.select.label')}
        onChange={handleChange}
        size={"sm"}
      >
        <option value={'mapbox://styles/arkeofi/cl9wf7qdx009z14qtf61wxfwj/draft oupk.eyJ1IjoiYXJrZW9maSIsImEiOiJja2dvcTFranUwNG5yMnlvaG45bzFsNmtwIn0.NJRCLNOJTIFpUH-pq6ew3g'}>Mapbox / Arkeopen</option>
        <option value={"https://api.maptiler.com/maps/hybrid/style.json?key=k0tbxqAqkpwuJyvZEucC"}>MapTiler / Satellite Hybrid</option>
        <option value={"https://api.maptiler.com/maps/topographique/style.json?key=k0tbxqAqkpwuJyvZEucC"}>MapTiler / Topographique</option>
        <option value={"https://api.maptiler.com/maps/topo/style.json?key=k0tbxqAqkpwuJyvZEucC"}>MapTiler / Topo</option>
        <option value={"https://api.maptiler.com/maps/outdoor/style.json?key=k0tbxqAqkpwuJyvZEucC"}>MapTiler / Outdoor</option>
        <option value={"https://api.maptiler.com/maps/basic-4326/style.json?key=k0tbxqAqkpwuJyvZEucC"}>MapTiler / Basic WGS84</option>
        <option value={"https://api.maptiler.com/maps/streets/style.json?key=k0tbxqAqkpwuJyvZEucC"}>MapTiler / Street</option>
        <option disabled value={"https://api.maptiler.com/maps/fr-streets-2154/tiles.json?key=k0tbxqAqkpwuJyvZEucC"}>MapTiler / FR Sreets</option>
        <option value={"https://basemaps.cartocdn.com/gl/positron-gl-style/style.json"}>Positron</option>
        <option value={"https://openmaptiles.geo.data.gouv.fr/styles/osm-bright/style.json"}>Etalab</option>
        <option disabled value={"https://openmaptiles.geo.data.gouv.fr/data/france-vector.json"}>OSM / France Vector</option>
        <option disabled value={"https://openmaptiles.geo.data.gouv.fr/data/decoupage-administratif.json"}>OSM / Découpage administratif</option>
        <option disabled value={"https://openmaptiles.geo.data.gouv.fr/data/cadastre.json"}>OSM / cadastre</option>
        <option disabled value={"https://vectortiles.ign.fr/demonstrateur/styles/planign.json"}>IGN</option>
        <option value={"https://wxs.ign.fr/essentiels/static/vectorTiles/styles/PLAN.IGN/standard.json"}>IGN plan</option>
        <option value={"https://geoserveis.icgc.cat/contextmaps/icgc.json"}>Institut Cartogràfic i Geològic de Catalunya</option>
      </Form.Select>
    )
}

BasemapSelect.propTypes = {
    onChange: PropTypes.object
}

export default BasemapSelect

