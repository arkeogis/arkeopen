import React, { useEffect, useState } from "react";
import { gql, useQuery } from "@apollo/client";
import { useTranslation } from "react-i18next";
import ak from "../../lib/akprefix";
import styles from "./HeaderStats.module.scss";

const HeaderStats = ({className = ""}) => {

  const { t } = useTranslation();

  const GetAkgStats = gql`
  query GetAkgStats {
    databases: ${ak}database_aggregate {
      aggregate {
        count
      }
    }
    sites: ${ak}site_aggregate {
      aggregate {
        count
      }
    }
  }`;

  const [queryData, setQueryData] = useState(false);

  const { data } = useQuery(GetAkgStats, {
    fetchPolicy: "no-cache",
    skip: !!queryData,
  });

  useEffect(() => {
    if (data) {
      setQueryData(data);
    }
  }, [data]);

  const date = new Date();

  // Get the browser's language
  const userLang = navigator.language || "en-US"; // Fallback if unavailable

  // Format the date
  const formattedDate = new Intl.DateTimeFormat(userLang, {
    day: "2-digit",
    month: "2-digit",
    year: "numeric",
  })
    .format(date)
    .replaceAll("/", ".");

  return (
    <div className={`${styles.HeaderStats} ${className}`}>
      <div>
        {formattedDate} - <span>{queryData?.sites?.aggregate?.count}</span>&nbsp;{t("components.HeaderStats.sites.label")}
      </div>
      <div>
        <span>{queryData?.databases?.aggregate?.count}</span>&nbsp;{t("components.HeaderStats.databases.label")}
      </div>
    </div>
  );
};

export default HeaderStats;
