import { React } from "react";
import styles from "./Header.module.scss";

// zustand
import useStore from "../../lib/store";
import { shallow } from "zustand/shallow";

// icons
import { HelpIcon, FullScreenIcon, SearchIcon, BurgerIcon } from "../Icons/Icons";

// i18n
import { useTranslation } from "react-i18next";

// fullscreen
import BurgerModal from "../BurgerModal/BurgerModal";
import { useFullScreen } from "../../lib/hook";
import HeaderStats from "../HeaderStats/HeaderStats";
import ArkeoLink from "../ArkeoUI/ArkeoLink";

const Header = ({ className = "" }) => {
  const { t } = useTranslation();
  const [
    setRightPanel,
    setLeftPanel,
    setSidePanelOpened,
    setShowBurgerModal,
    showBurgerModal,
    sidePanelOpened,
    rightPanel,
    leftPanel,
  ] = useStore(
    (state) => [
      state.setRightPanel,
      state.setLeftPanel,
      state.setSidePanelOpened,
      state.setShowBurgerModal,
      state.showBurgerModal,
      state.sidePanelOpened,
      state.rightPanel,
      state.leftPanel,
    ],
    shallow
  );

  const [fullscreenEnabled, toggleFullscreen, fullscreenAvailable] = useFullScreen();

  return (
    <>
      <div className={`${styles.componentHeader} ${className}`}>
        <header>
          {/* Logo */}
          {__ARKEOPEN__ && (
            <div className={styles.arkeopenLogo}>
              <img src="/logos/logo-arkeopen-white.svg" alt="ArkeoPen" title="Arkeopen" />
            </div>
          )}
          {__ARKEOGIS__ && (
            <div className={styles.arkeogisLogo}>
              <img src="/logos/logo-arkeogis-white.svg" alt="ArkeoGIS" title="Arkeogis" />
              <HeaderStats className={`${styles.headerStats} ${styles.visibleOnDesktopOnly}`} />
            </div>
          )}
          {/* Links and Buttons */}
          <div className={styles.linksAndIcons}>
            <div className={`${styles.links} ${styles.visibleOnDesktopOnly}`}>
              {/* Search */}
              <div>
                <ArkeoLink
                  leftPanel="search"
                  className={`${sidePanelOpened === "left" && leftPanel === "search" ? styles.active : ""}`}
                  dangerouslySetInnerHTML={{
                    __html: __ARKEOGIS__
                      ? t("components.Header.Link.NewRequest.label")
                      : t("components.Header.Link.Search.label"),
                  }}
                />
              </div>
              {/* Legend */}
              {__ARKEOPEN__ && (
                <div>
                  <ArkeoLink
                    rightPanel={{type: "legend"}}
                    className={`${sidePanelOpened === "right" && rightPanel.type === "legend" ? styles.active : ""}`}
                    dangerouslySetInnerHTML={{
                      __html: t("components.Header.Link.MyMapIndex.label"),
                    }}
                  />
                </div>
              )}
              {__ARKEOGIS__ && (
                <>
                  {/* My Requests */}
                  <div>
                    <ArkeoLink
                      className={`${sidePanelOpened === "left" && leftPanel === "requests" ? styles.active : ""}`}
                      leftPanel="requests"
                      dangerouslySetInnerHTML={{
                        __html: t("components.Header.Link.MyRequests.label"),
                      }}
                    />
                  </div>
                  {/* Exports */}
                  <div>
                    <ArkeoLink
                      rightPanel={{ type: "export" }}
                      className={`${sidePanelOpened === "right" && rightPanel.type === "legend" ? styles.active : ""}`}
                      dangerouslySetInnerHTML={{
                        __html: t("components.Header.Link.export.label"),
                      }}
                    />
                  </div>
                </>
              )}
              {/* Project */}
              {__ARKEOPEN__ && (
                <div>
                  <ArkeoLink
                    rightPanel={{ type: "project", id: "project" }}
                    className={`${sidePanelOpened === "right" && rightPanel.type === "project" ? styles.active : ""}`}
                    dangerouslySetInnerHTML={{
                      __html: t("components.Header.Link.Project.label"),
                    }}
                  />
                </div>
              )}
            </div>
            {/* Help */}
            <div className={styles.icons}>
              <ArkeoLink
                rightPanel={{ type: "help" }}
                className={`${styles.visibleOnDesktopOnly} ${
                  sidePanelOpened === "right" && rightPanel.type === "help" ? styles.active : ""
                }`}
              >
                <HelpIcon />
              </ArkeoLink>
              {/* Fullscreen */}
              {fullscreenAvailable && (
                <ArkeoLink
                  className={`${fullscreenEnabled ? styles.active : ""}`}
                  onClick={(e) => {
                    toggleFullscreen();
                    e.preventDefault();
                  }}
                >
                  <FullScreenIcon />
                </ArkeoLink>
              )}
              {/* Mobile search */}
              <ArkeoLink
                leftPanel={{ type: "search" }}
                className={`${styles.mobileSearch} ${styles.visibleOnPhoneOnly} ${
                  sidePanelOpened === "left" && leftPanel == "search" ? styles.active : ""
                }`}
              >
                <SearchIcon />
              </ArkeoLink>
              {/* Burger */}
              <ArkeoLink
                showBurgerModal
                className={`${sidePanelOpened === "right" && showBurgerModal ? styles.active : ""}`}
              >
                <BurgerIcon />
              </ArkeoLink>
            </div>
          </div>
        </header>
      </div>
      <BurgerModal />
    </>
  );
};

export default Header;
