import React, { useEffect } from "react";
import { H1 } from "../ArkeoUI/ArkeoUI";
import { useNavigate } from "react-router-dom";

import "./SubMainPanel.scss";
import CloseButton from "../CloseButton/CloseButton";
import useStore from "../../lib/store";
import ArkeoLink from "../ArkeoUI/ArkeoLink";

const SubMainPanel = ({ title, subTitleElement, children, onClose, className }) => {
  const navigate = useNavigate();
  const setHideRightSidePanelHandle = useStore((state) => state.setHideRightSidePanelHandle);

  if (!onClose) onClose = () => navigate("/");

  useEffect(() => {
    setHideRightSidePanelHandle(true);
    return () => {
      setHideRightSidePanelHandle(false);
    };
  }, []);

  return (
    <div className={"SubMainPanel" + (className ? " " + className : "")}>
      <div className="titleRow">
        <H1>{title}</H1>
        <ArkeoLink to="/"><CloseButton className="closeButton"/></ArkeoLink>
        {subTitleElement}
      </div>
      <div className="SubMainPanelContent">{children}</div>
    </div>
  );
};

export default SubMainPanel;
