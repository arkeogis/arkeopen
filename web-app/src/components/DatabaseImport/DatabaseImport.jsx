import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useShallow } from "zustand/react/shallow";
import FullscreenComponent from "../FullscreenComponent/FullscreenComponent";
import ProgressionToolbar from "../ProgressionToolbar/ProgressionToolbar";
import DatabaseImportStep1 from "./DatabaseImportStep1";
import DatabaseImportStep2 from "./DatabaseImportStep2";
import DatabaseImportStep3 from "./DatabaseImportStep3";
import DatabaseImportStep4 from "./DatabaseImportStep4";
import DatabaseImportStep5 from "./DatabaseImportStep5";
import { databaseImportSchema, databaseImportStep3Schema, databaseImportStep4Schema, databaseImportStep5Schema } from "../../lib/schemas/databaseImport";
import { Button } from "../ArkeoUI/Form";
import { H1 } from "../ArkeoUI/ArkeoUI";

import { useValidatedFormStore } from "../../lib/hook";

const DatabaseImport = () => {
  const { t } = useTranslation();

  const [stepIndex, setStepIndex] = useState(2);
  const { register, validatePartialSchema, useFormStore } = useValidatedFormStore(databaseImportSchema);

  const [submitButtonEnabled] = useFormStore(
    useShallow((state) => [state.submitButtonEnabled, state.validateSchema])
  );

  const steps = [
    {
      title: t(""),
      component: DatabaseImportStep1 /* Warning, no JSX brackets ! */,
    },
    { title: t(""), component: DatabaseImportStep2 },
    { title: t(""), component: DatabaseImportStep3, schema: databaseImportStep3Schema },
    { title: t(""), component: DatabaseImportStep4, schema: databaseImportStep4Schema },
    { title: t(""), component: DatabaseImportStep5, schema: databaseImportStep5Schema },
  ];

  const ActiveComponent = stepIndex >= 0 && steps[stepIndex].component;

  const handleSubmit = async () => {
    const schema = steps[stepIndex].schema
    if (schema) {
      const validation = await validatePartialSchema(schema);
      if (!validation) {
        return false;
      }
    }
    setStepIndex(stepIndex + 1);
  };

  return (
    <FullscreenComponent>
      <H1>
        Importer
        <br />
        un jeu de données
      </H1>
      {stepIndex >= 0 && <ActiveComponent register={register} useFormStore={useFormStore} />}
      <ProgressionToolbar>
        <ProgressionToolbar.Steps
          activeStepIndex={stepIndex}
          labels={steps.map((s) => s.title)}
          onClick={(stepNum) => setStepIndex(stepNum)}
        />
        <ProgressionToolbar.ButtonContainer>
          <Button
            onClick={() => {
              setStepIndex(stepIndex - 1);
            }}
            disabled={stepIndex === 0}
          >
            {t("Étape précédente")}
          </Button>
          <Button type="button" variant="primary" disabled={!submitButtonEnabled} onClick={handleSubmit}>
            {stepIndex !== steps.length - 1 ? t("Étape suivante") : t("Enregister")}
          </Button>
        </ProgressionToolbar.ButtonContainer>
      </ProgressionToolbar>
    </FullscreenComponent>
  );
};

export default DatabaseImport;
