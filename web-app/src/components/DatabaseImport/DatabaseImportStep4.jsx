import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { FormFieldContainer, FormError, TextInput, TextArea, Select } from "../ArkeoUI/Form";
import { Grid } from "../ArkeoUI/Layout";
import { H2 } from "../ArkeoUI/ArkeoUI";

export const DatabaseImportStep4 = ({ useFormStore, register }) => {
  const { t } = useTranslation();
  const [formErrors, default_language] = useFormStore((state) => [state.formErrors, state.default_language]);

  const typeList = [
    { value: "undefined", label: t("arkeo.database.type.undefined") },
    { value: "inventory", label: t("arkeo.database.type.inventory") },
    { value: "research", label: t("arkeo.database.type.research") },
    { value: "literary-work", label: t("arkeo.database.type.literary-work") },
  ];

  const scaleList = [
    { value: "undefined", label: t("arkeo.database.scale_resolution.undefined") },
    { value: "object", label: t("arkeo.database.scale_resolution.object") },
    { value: "site", label: t("arkeo.database.scale_resolution.site") },
    { value: "watershed", label: t("arkeo.database.scale_resolution.watershed") },
    { value: "micro-region", label: t("arkeo.database.scale_resolution.micro-region") },
    { value: "region", label: t("arkeo.database.scale_resolution.region") },
    { value: "country", label: t("arkeo.database.scale_resolution.country") },
    { value: "continent", label: t("arkeo.database.scale_resolution.continent") },
    { value: "world", label: t("arkeo.database.scale_resolution.world") },
  ];

  const stateList = [
    { value: "undefined", label: t("arkeo.database.state.undefined") },
    { value: "in-progress", label: t("arkeo.database.state.in-progress") },
    { value: "finished", label: t("arkeo.database.state.finished") },
  ];

  const geographicalExtentList = [
    { value: "undefined", label: t("arkeo.database.geographical_extent.undefined") },
    { value: "country", label: t("arkeo.database.geographical_extent.country") },
    { value: "continent", label: t("arkeo.database.geographical_extent.continent") },
    { value: "international_waters", label: t("arkeo.database.geographical_extent.international_waters") },
    { value: "world", label: t("arkeo.database.geographical_extent.world") },
  ];

  const langList = [
    { value: "fr", label: t("arkeo.lang.fr") },
    { value: "en", label: t("arkeo.lang.en") },
    { value: "de", label: t("arkeo.lang.de") },
    { value: "es", label: t("arkeo.lang.es") },
  ];

  return (
    <div>
      <H2 number={4}>{t("Informations pour partage ArkeoGIS")}</H2>
      <Grid areas="'zone1 zone2' 'zone3 zone3' 'zone4 zone5' 'zone6 zone6'" gap={34} style={{ gridRowGap: 0 }}>
        <Grid.Item area="zone1">
          <FormFieldContainer infoText={t("")} title={t("Type")} mandatory>
            <Select options={typeList} {...register("type")} />
            <FormError errors={formErrors} field="context" />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("État de réalisation")} mandatory>
            <Select options={stateList} {...register("state")} />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Chronologie associée")}>
            <Select options={[]} />
          </FormFieldContainer>
        </Grid.Item>
        <Grid.Item area="zone2">
          <FormFieldContainer infoText={t("")} title={t("Échelle des points")} mandatory>
            <Select options={scaleList} {...register("scale_resolution")} />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Collection")} mandatory>
            <Select options={[]}  />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Étendue géographique la plus large")}>
            <Select options={geographicalExtentList} {...register("geographical_extent")} />
          </FormFieldContainer>
        </Grid.Item>
        <Grid.Item area="zone3">
          <FormFieldContainer infoText={t("")} title={t("Étendue géographique plus précise")} mandatory>
            <Select isMulti options={[]}  />
          </FormFieldContainer>
        </Grid.Item>
        <Grid.Item area="zone4">
          <FormFieldContainer infoText={t("")} title={t("Langue d'importation")}>
            <TextInput value={langList.find((lang) => lang.value === default_language).label} disabled></TextInput>
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Précision géographique")}>
            <TextArea height={114} />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Mots clés")} mandatory>
            <TextArea height={114} />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Description")} mandatory>
            <TextArea height={114} />
          </FormFieldContainer>
        </Grid.Item>
        <Grid.Item area="zone5">
          <FormFieldContainer infoText={t("")} title={t("Langue de traduction")} mandatory>
            <Select options={langList} {...register("translation_lang")} />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Précision géographique traduction")}>
            <TextArea height={114} />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Mots clés traduction")} mandatory>
            <TextArea height={114} />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Description traduction")} mandatory>
            <TextArea height={114} />
          </FormFieldContainer>
        </Grid.Item>
        <Grid.Item area="zone6">
          <FormFieldContainer infoText={t("")} title={t("Illustrations")} mandatory>
            <TextArea height={200} />
          </FormFieldContainer>
        </Grid.Item>
      </Grid>
    </div>
  );
};

export default DatabaseImportStep4;
