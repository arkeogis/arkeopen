import React, { useEffect, useState } from "react";
import { useShallow } from "zustand/react/shallow";
import { useTranslation } from "react-i18next";
import { toast } from "sonner";
import FileDropzone from "../FileDropzone/FileDropzone";
import { useCSVParser } from "../../lib/hook";
import { importCsvLineSchema } from "../../lib/schemas/databaseImport";
import { FormFieldContainer, TextInput, Select, FormError } from "../ArkeoUI/Form";
import { Grid } from "../ArkeoUI/Layout";
import { H2 } from "../ArkeoUI/ArkeoUI";

export const DatabaseImportStep1 = ({ useFormStore, register }) => {
  const { parsedData, error: csvFormatError, processing, asyncParseCSV } = useCSVParser();
  const { t } = useTranslation();

  const [name, default_language, csvLines, formErrors, setSubmitButtonEnabled, validateFields] = useFormStore(
    useShallow((state) => [
      state.name,
      state.default_language,
      state.csvLines,
      state.formErrors,
      state.setSubmitButtonEnabled,
      state.validateFields,
    ])
  );

  const langList = [
    { value: "fr", label: t("arkeo.lang.fr") },
    { value: "en", label: t("arkeo.lang.en") },
    { value: "de", label: t("arkeo.lang.de") },
    { value: "es", label: t("arkeo.lang.es") },
  ];

  useEffect(() => {
    if (!parsedData) return;
    useFormStore.setState({ csvLines: parsedData });
  }, [parsedData]);

  useEffect(() => {
    if (!csvFormatError) return;
    toast.error(csvFormatError);
  }, [csvFormatError]);

  useEffect(() => {
    const validate = async () => {
      const result = await validateFields(["name", "default_language"]);
      if (result && result.success && csvLines && csvLines.length > 0) {
        setSubmitButtonEnabled(true);
      } else {
        setSubmitButtonEnabled(false);
      }
    };
    validate();
  }, [name, default_language, csvLines]);

  return (
    <div className="databaseImportStep1">
      <H2 number={1}>{t("Informations générales")}</H2>
      <div className="formContainer">
        <Grid areas="'zone1 zone2 zone2'" gap={30}>
          <Grid.Item area="zone1">
            <FormFieldContainer infoText={t("")} title={t("Nom-Titre")} mandatory>
              <TextInput {...register("name")} />
              <FormError errors={formErrors} field="name" />
            </FormFieldContainer>
            <FormFieldContainer infoText={t("")} title={t("Langue utilisée")} mandatory>
              <Select options={langList} {...register("default_language")} />
            </FormFieldContainer>
          </Grid.Item>
          <Grid.Item area="zone2">
            <FileDropzone
              withIcon
              withBorder
              onSuccess={async (files) => {
                useFormStore.setState({ csvLines: [], csvErrors: [], csvValidatedLines: [] });
                await asyncParseCSV(files[0], Object.keys(importCsvLineSchema.shape), {
                  header: true,
                  transform: (value) => value.trim(),
                  preview: 20, // REMOVE ME
                });
              }}
              onError={(errors) => {
                toast(errors);
              }}
            />
          </Grid.Item>
        </Grid>
      </div>
    </div>
  );
};

export default DatabaseImportStep1;
