import React, { useEffect, useState } from "react";
import { useShallow } from "zustand/react/shallow";
import { useTranslation } from "react-i18next";
import { toast } from "sonner";
import FileDropzone from "../FileDropzone/FileDropzone";
import { useCSVParser } from "../../lib/hook";
import { buildImportCsvFileSchemaFromLang, importCsvLineSchema } from "../../lib/schemas/databaseImport";
import { FormFieldContainer, TextInput, Select } from "../ArkeoUI/Form";
import { Grid } from "../ArkeoUI/Layout";
import CheckList from "../CheckList/CheckList";
import CircularProgress from "../CircularProgress/CircularProgress";
import { H2 } from "../ArkeoUI/ArkeoUI";

export const DatabaseImportStep2 = ({ useFormStore }) => {
  const { t } = useTranslation();
  const { parsedData, error: csvFormatError, processing, asyncParseCSV } = useCSVParser();
  const [checkListItems, setCheckListItems] = useState([
    { label: "Nombre de lignes détectées dans le fichier", num: 0 },
    { label: "Nombre de sites détectés dans le fichier", num: 0 },
    { label: "Nombre de sites valides", num: 0 },
    { label: "Nombre de sites en erreur", num: 0, accent: true },
    { label: "Nombre total d’erreurs", num: 0, accent: true },
  ]);
  const [percentage, setPercentage] = useState(0);

  const [default_language, csvLines, csvErrors, setSubmitButtonEnabled] = useFormStore(
    useShallow((state) => [state.default_language, state.csvLines, state.csvErrors, state.setSubmitButtonEnabled])
  );

  useEffect(() => {
    if (!parsedData) return;
    useFormStore.setState({ csvLines: parsedData });
  }, [parsedData]);

  useEffect(() => {
    if (!csvLines) return;
    const sortErrors = (errors) => {
      return errors.sort((a, b) => {
        const lineNumA = a.meta?.lineNum ?? Number.MAX_SAFE_INTEGER;
        const lineNumB = b.meta?.lineNum ?? Number.MAX_SAFE_INTEGER;
        return lineNumA - lineNumB;
      });
    };
    const validate = async () => {
      const schema = buildImportCsvFileSchemaFromLang(default_language);
      const validation = await schema.safeParseAsync(csvLines);
      if (!validation.success) {
        useFormStore.setState({ csvErrors: sortErrors(validation.error.issues), csvValidatedLines: [] });
        setSubmitButtonEnabled(false);
      } else {
        useFormStore.setState({ csvErrors: [], csvValidatedLines: csvLines });
        setSubmitButtonEnabled(true);
      }
    };
    validate();
  }, [csvLines]);

  useEffect(() => {
    updateChecklist(csvLines, csvErrors);
  }, [csvErrors]);

  useEffect(() => {
    if (!csvFormatError) return;
    toast.error(csvFormatError);
  }, [csvFormatError]);

  const updateChecklist = (csvLines, csvErrors) => {
    const numberOfLines = csvLines.length;
    const numberOfErrors = csvErrors.length;
    const numberOfSites = new Set(
      csvLines
        .map((line) => {
          return line.SITE_SOURCE_ID;
        })
        .filter((n) => n)
    ).size;
    const numberOfSitesInError = new Set(
      csvErrors
        .map((error) => {
          return error.meta.siteSourceId;
        })
        .filter((n) => n)
    ).size;
    const numberOfSitesValid = numberOfSites - numberOfSitesInError;
    setCheckListItems((prevItems) => [
      { ...prevItems[0], num: numberOfLines },
      { ...prevItems[1], num: numberOfSites },
      { ...prevItems[2], num: numberOfSitesValid },
      { ...prevItems[3], num: numberOfSitesInError },
      { ...prevItems[4], num: numberOfErrors },
    ]);
    setPercentage(100 - Math.round((numberOfSitesInError * 100) / numberOfSites));
  };

  return (
    <div>
      <H2 number={2}>{t("Rapport")}</H2>
      <Grid areas="'zone1 zone2 zone3' 'zone4 zone4 zone4'" gap={34}>
        <Grid.Item area="zone1">
          <CheckList items={checkListItems} />
        </Grid.Item>
        <Grid.Item area="zone2">
          <FileDropzone
            withIcon
            withBorder
            onSuccess={async (files) => {
              await asyncParseCSV(files[0], Object.keys(importCsvLineSchema.shape), {
                header: true,
                transform: (value) => value.trim(),
                preview: 20, // REMOVE ME
              });
            }}
            onError={(errors) => {
              console.error(errors);
            }}
          />
        </Grid.Item>
        <Grid.Item area="zone3" style={{ display: "flex", alignItems: "center" }}>
          <CircularProgress percentage={percentage} />
        </Grid.Item>
        <Grid.Item area="zone4">
          <div style={{ border: "1px solid grey", width: "100%", height: 150 }}></div>
        </Grid.Item>
      </Grid>
    </div>
  );
};

export default DatabaseImportStep2;
