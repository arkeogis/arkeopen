import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { DateInput, FormFieldContainer, TextInput, TextArea, Select, AsyncSelect, FormError } from "../ArkeoUI/Form";
import { Grid } from "../ArkeoUI/Layout";
import { H2 } from "../ArkeoUI/ArkeoUI";
import { GetUserByNameQuery, GetUserByIdQuery } from "../../lib/queries/user";
import { GetLicenseQuery } from "../../lib/queries/license";

export const DatabaseImportStep3 = ({ useFormStore, register }) => {
  const [formErrors] = useFormStore((state) => [state.formErrors]);
  const { t } = useTranslation();

  const contextList = [
    { value: "undefined", label: t("arkeo.database.context.undefined") },
    { value: "academic-work", label: t("arkeo.database.context.academic-work") },
    { value: "contract", label: t("arkeo.database.context.contract") },
    { value: "research_team", label: t("arkeo.database.context.research_team") },
    { value: "other", label: t("arkeo.database.context.other") },
  ];


  return (
    <div>
      <H2 number={3}>{t("Informations administratives")}</H2>
      <Grid areas="'zone1 zone2'" gap={34}>
        <Grid.Item area="zone1">
          <FormFieldContainer infoText={t("")} title={t("Auteur(s)")} mandatory>
            <AsyncSelect
              query={GetUserByNameQuery}
              queryByKey={GetUserByIdQuery}
              labelField="fullname"
              valueField="id"
              isMulti={true}
              transformData={(user) => ({
                ...user,
                fullname: `${user.firstname} ${user.lastname}`,
              })}
              {...register("authors")}
            />
            <FormError errors={formErrors} field="authors" />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Licence")} mandatory>
            <AsyncSelect
              query={GetLicenseQuery}
              labelField="name"
              valueField="id"
              {...register("license")}
              loadOnInit
            />
            <FormError errors={formErrors} field="license" />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Éditeur")} mandatory>
            <TextInput {...register("editor")} />
            <FormError errors={formErrors} field="editor" />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Cadre(s) de réalisation")} mandatory>
            <Select isMulti options={contextList} {...register("context")} />
            <FormError errors={formErrors} field="context" />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Date de création")}>
            <DateInput {...register("declared_creation_date")} />
            <FormError errors={formErrors} field="declared_creation_date" />
          </FormFieldContainer>
        </Grid.Item>
        <Grid.Item area="zone2">
          <FormFieldContainer infoText={t("")} title={t("Contributeur(s)")}>
            <TextArea height={116} {...register("contributor")} />
            <FormError errors={formErrors} field="contributor" />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Adresse de l'éditeur")} mandatory>
            <TextInput {...register("editor_url")} />
            <FormError errors={formErrors} field="editor_url" />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Précision cadre(s) de réalisation(s)")}>
            <TextArea height={116} {...register("context_description")} />
            <FormError errors={formErrors} field="context_description" />
          </FormFieldContainer>
        </Grid.Item>
      </Grid>
    </div>
  );
};

export default DatabaseImportStep3;
