import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { DateInput, FormFieldContainer, TextInput, TextArea, Select } from "../ArkeoUI/Form";
import { Grid } from "../ArkeoUI/Layout";
import CheckList from "../CheckList/CheckList";
import { H2 } from "../ArkeoUI/ArkeoUI";

export const DatabaseImportStep5 = () => {
  const { t } = useTranslation();
  return (
    <div>
      <H2 number={5}>{t("Informations pour partage ArkeOpen")}</H2>
      <Grid areas="'zone1 zone2'" gap={34}>
        <Grid.Item area="zone1">
          <FormFieldContainer infoText={t("")} title={t("Identifiant pérenne")} mandatory>
            <TextArea height={114} />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Adresse de téléchargement")} mandatory>
            <TextInput onChange={(val) => console.log(val.target.value)}></TextInput>
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Réutilisation")}>
            <TextArea height={114} />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Adresse de la source")}>
            <TextInput></TextInput>
          </FormFieldContainer>
        </Grid.Item>
        <Grid.Item area="zone2">
          <FormFieldContainer infoText={t("")} title={t("Citation")} mandatory>
            <TextArea height={114} />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Réutilisation traduction")}>
            <TextArea height={114} />
          </FormFieldContainer>
          <FormFieldContainer infoText={t("")} title={t("Lien avec d'autres ressources")}>
            <TextArea height={114} />
          </FormFieldContainer>
        </Grid.Item>
      </Grid>
    </div>
  );
};

export default DatabaseImportStep5;
