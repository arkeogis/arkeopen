import React, { useCallback, useMemo } from "react";
import { useDropzone } from "react-dropzone";
import { useTranslation } from "react-i18next";
import { UploadIcon, UploadErrorIcon } from "../Icons/Icons";
import { toast } from "sonner";

import styles from "./FileDropzone.module.scss";

function FileDropzone({
  className = "",
  onSuccess = (_) => {},
  onError = (_) => {},
  maxFiles = 1,
  acceptedTypes = { "text/csv": [".csv"] },
  withIcon = false,
  withBorder = false,
}) {
  const { t } = useTranslation();
  const errorLabel = {
    "too-many-files": t("components.FileDropzone.error.tooManyFiles"),
    "file-invalid-type": t("components.FileDropzone.error.fileInvalidType"),
  };

  const onDrop = useCallback((acceptedFiles, fileRejections) => {
    const errors = new Set();
    if (fileRejections && fileRejections.length > 0) {
      fileRejections.map((rejection) =>
        rejection.errors.map((e) => {
          errors.add(errorLabel[e.code] || t("components.FileDropzone.error.default"));
        })
      );
    }
    if (errors.size === 0) {
      onSuccess(acceptedFiles);
    } else {
      onError(Array.from(errors));
      for (const error of errors) {
        toast.error(error);
      }
    }
  }, []);

  const { getRootProps, getInputProps, isDragAccept, isDragReject, isDragActive } = useDropzone({
    onDrop,
    maxFiles,
    accept: acceptedTypes,
  });

  const hoverClassName = useMemo(() => {
    return [
      isDragActive ? styles.dragActive : "",
      isDragAccept ? styles.noError : isDragReject ? styles.hasError : "",
    ].join(" ");
  }, [isDragAccept, isDragReject, isDragActive]);

  return (
    <div
      {...getRootProps()}
      className={`${styles.dropZone} ${withBorder ? styles.withBorder : ""} ${hoverClassName} ${className}`}
    >
      {withIcon && (isDragReject ? <UploadErrorIcon className={styles.uploadErrorIcon} /> : <UploadIcon className={styles.uploadIcon} />)}
      <input {...getInputProps()} />
      <button>{t("components.FileDropzone.button.label")}</button>
    </div>
  );
}

export default FileDropzone;
