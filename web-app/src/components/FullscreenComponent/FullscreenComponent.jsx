import React, { useState, useEffect } from "react";
import { CloseButton } from "../CloseButton/CloseButton";
import Header from "../Header/Header";

import styles from "./FullscreenComponent.module.scss";
import { useNavigate } from "react-router-dom";

const FullscreenComponent = ({ className, children, onClose, title }) => {
  const navigate = useNavigate();

  return (
    <>
      <Header className={styles.header} />
      <div className={`${styles.fullscreenComponent} ${className || ""}`}>
        <CloseButton
          className={styles.closeButton}
          onClick={() => (typeof onClose === "function" ? onClose() : navigate("/"))}
        />
        {children}
      </div>
    </>
  );
};

export default FullscreenComponent;
