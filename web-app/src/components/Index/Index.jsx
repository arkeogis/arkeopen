import React, { useEffect, useState } from "react";
import { DataType, FilteringMode, SortingMode, Table } from "ka-table";
import { useTranslation } from "react-i18next";
import { SearchIcon } from "../Icons/Icons";
import SubMainPanel from "../SubMainPanel/SubMainPanel";
import _ from "underscore";

import "./Index.scss";


/**
 * Generic Index component that can be used to display tabular data
 *
 * @param {Object} props Component props
 * @param {Array} props.data Data to display in the table
 * @param {Array} props.columns Column definitions for the table
 * @param {string} props.rowKeyField Field to use as key for rows
 * @param {Function} props.format Optional function to format cell values
 * @param {Object} props.childComponents Optional custom components for cells
 * @param {Function} props.customCellComponent Optional function to render custom cells https://ka-table.com/docs_props.html#childcomponents
 * @param {string|React.ReactNode} props.title Main title for the component
 * @param {string} props.className Additional CSS class name
 * @param {boolean} props.textSearch True if textSearch component should be rendered
 * @param {React.ReactNode} props.buttons Optional additional buttons that will be show on the top right
 * @returns {React.ReactElement} Rendered component
 */
const Index = ({
  data,
  columns,
  rowKeyField = "id",
  format,
  childComponents,
  title,
  className = "",
  textSearch = false,
  buttons,
}) => {
  const { t, i18n } = useTranslation();
  const [searchText, setSearchText] = useState("");
  const [rerender, setRerender] = useState(0);

  // Build child components if customCellComponent is provided
  const finalChildComponents = _.extend({
    cellText: {
      content: (props) => {
        if (props.column.customCellComponent !== undefined) {
          return props.column.customCellComponent(props);
        }
      }
    }
  }, childComponents);

  useEffect(() => {
    setRerender(rerender + 1);
  }, [data, columns, rowKeyField, childComponents, format, searchText, i18n.language]);

  return (
    <SubMainPanel
      className={`Index ${className}`}
      title={title}
      subTitleElement={
        <HeaderRightButtons>
          { buttons !== undefined && buttons }
          { textSearch === true && <SearchComponent/> }
        </HeaderRightButtons>
      }
    >
      <Table
        key={"table-" + rerender}
        className="IndexTable"
        data={data}
        columns={columns}
        rowKeyField={rowKeyField}
        sortingMode={SortingMode.Single}
        filteringMode={FilteringMode.HeaderFilter}
        format={format}
        searchText={textSearch === true ? searchText : undefined}
        childComponents={finalChildComponents}
      />
    </SubMainPanel>
  );
};

/**
 * HeaderRightButtons is the top right section of the Index component. It can contains buttons and a search input.
 *
 * @param {Object} props Component props
 * @param {React.ReactNode} props.children The button elements to be displayed on the right-hand side
 * @returns {React.ReactNode} The rendered HeaderRightButtons component
 */
const HeaderRightButtons = ({children}) => {
  return (
    <div className="HeaderRightButtons">
      {children}
    </div>
  );
};

/**
 * SearchComponent is a component that allows for searching within the table.
 *
 * @returns {React.ReactNode} The rendered SearchComponent component
 */
const SearchComponent = () => {
  const { t, i18n } = useTranslation();
  return (
    <div className="SearchComponent">
      <input
        type="text"
        onChange={(e) => setSearchText(e.target.value)}
        placeholder={t("components.Index.textSearch.placeholder")}
      />
      <SearchIcon />
    </div>
  )
};

export default Index;
