import Header from "../../Header/Header";
import BurgerModal from "../../BurgerModal/BurgerModal";
import WelcomeModal from "../WelcomeModal/WelcomeModal";

const Layout = ({children}) => {
  return (
    <>
      {children}
      <WelcomeModal />
      <Header />
    </>
  );
};

export default Layout;
