import { render } from "@testing-library/react";

import i18n from "../../i18n";
import Loader from "./Loader";

describe("Loader component", () => {
  it("renders Loader component", () => {
    const { container } = render(<Loader />);
    const loaderElement = container.querySelector(".Loader");
    expect(loaderElement).toBeInTheDocument();
  });
});
