import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { act } from "react-dom/test-utils";
import StoreRoute from "./StoreRoute";
import useStore, { useValidatedSearchStore, useSearchStore } from "../../lib/store";

it("should not have rp param in url", () => {
  const { setRightPanel } = useStore.getState();

  const pushStateSpy = vi.spyOn(window.history, "pushState");

  setRightPanel({ type: "site", id: 42 });
  render(
    <MemoryRouter>
      <StoreRoute />
    </MemoryRouter>
  );

  act(() => {
    setRightPanel({ type: "site", id: 43 });
  });

  expect(pushStateSpy).toHaveBeenCalledWith({}, "", "/search?query=test");

  console.log("window.history", window.history);
});

it("should have rp param set with left in param", () => {
  useStore.setState({ rightPanel: { type: "site", id: 42 } });

  const { container } = render(
    <MemoryRouter>
      <StoreRoute />
    </MemoryRouter>
  );
});
