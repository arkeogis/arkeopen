import React from "react";
import styles from "./CircularProgress.module.scss";

const CircularProgress = ({ className, percentage }) => {
  const radius = 45;
  const circumference = 2 * Math.PI * radius;
  const progressOffset = circumference * (1 - percentage / 100);

  return (
    <div className={`${styles.circularProgress} ${className ? className : ""}`}>
      <svg viewBox="0 0 100 100" style={{ width: "100%", height: "100%" }}>
        {/* Background circle */}
        <circle cx="50" cy="50" r={radius} fill="none" stroke="none" strokeWidth="10" />
        {/* Progress circle */}
        <circle
          cx="50"
          cy="50"
          r={radius}
          fill="none"
          stroke="#ff0000"
          strokeWidth="10"
          strokeDasharray={circumference}
          strokeDashoffset={progressOffset}
          transform="rotate(-90 50 50)"
        />
      </svg>
      {/* Percentage text */}
      <div className={styles.label}>
        {percentage}%
      </div>
    </div>
  );
};

export default CircularProgress;
