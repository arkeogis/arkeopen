import React, { useRef, useState } from 'react'
import { Button, Stack, Badge, Offcanvas, Form } from 'react-bootstrap';

import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import Popover from 'react-bootstrap/Popover';
import { buildHtmlMarker } from '../../lib/map/ArkeoMarkerBuilder';

import FlatSelect from '../FlatSelect/FlatSelect';

export const ScrollTest = () => {
  return (
    <div style={{height: 200, width: 200, background: "#ffffff", overflowY: "scroll"}}>
      <div style={{height: 300, width: 150, background: "#3f3f3f"}}></div>
    </div>
  )
}

export const LeftSideBar = (props) => {
  const [show, setShow] = useState(false);
    return (
    <div>
      <Offcanvas show={show} scroll={true} backdrop={false} container={props.container}>
        <p>Coucou</p>
      </Offcanvas>
      <button onClick={() => setShow(!show)}>Toggle</button>
    </div>
  )
}

function SelectBasicExample() {
  return (
    <Form.Select aria-label="Default select example">
      <option variant="primary">Open this select menu</option>
      <option value="1">One</option>
      <option value="2">Two</option>
      <option value="3">Three</option>
    </Form.Select>
  );
}

function TooltipPositionedExample() {
  return (
    <>
      {['top', 'right', 'bottom', 'left'].map((placement) => (
        <OverlayTrigger
          key={placement}
          placement={placement}
          overlay={
            <Tooltip id={`tooltip-${placement}`}>
                Tooltip on <strong>{placement}</strong>.
            </Tooltip>
          }
        >
          <Button variant="secondary">Tooltip on {placement}</Button>
        </OverlayTrigger>
      ))}
    </>
  );
}

function PopoverPositionedExample() {
  return (
    <>
      {['top', 'right', 'bottom', 'left'].map((placement) => (
        <OverlayTrigger
          trigger="focus"
          key={placement}
          placement={placement}
          overlay={
            <Popover id={`popover-positioned-${placement}`}>
              <Popover.Header as="h3">{`Popover ${placement}`}</Popover.Header>
              <Popover.Body>
                <strong>Holy guacamole!</strong> Check this info.
              </Popover.Body>
            </Popover>
          }
        >
          <Button variant="secondary">Popover on {placement}</Button>
        </OverlayTrigger>
      ))}
    </>
  );
}

function MarkersRow(props) {
  const {exceptional, centroid, selected} = props;
  return (
    <div className="hstack gap-2">
      {[5,4,3,2,1,0].map((knowledge) => (
        <div  style={{"margin": "30px"}} ref={ ref => ref.appendChild(buildHtmlMarker([0,0], exceptional, "#BFA66A", knowledge, centroid, selected)) }></div>
      ))}
    </div>
  );
}

function MarkersExample() {
  return (
    <div>
      <MarkersRow exceptional={false} centroid={0} selected={1}/>
      <MarkersRow exceptional={false} centroid={0} selected={2}/>
      <MarkersRow exceptional={"yes"} centroid={0} selected={1}/>
      <MarkersRow exceptional={"yes"} centroid={0} selected={2}/>
      <MarkersRow exceptional={false} centroid={1} selected={1}/>
      <MarkersRow exceptional={false} centroid={1} selected={2}/>
      <MarkersRow exceptional={"yes"} centroid={1} selected={1}/>
      <MarkersRow exceptional={"yes"} centroid={1} selected={2}/>
    </div>
  )
}

export const Boot = () => {
  const refSideBar = useRef();
  return (
    <div>
      <div ref={refSideBar} className="MonSideBarContainer"></div>
      <div>
        <Stack direction="horizontal" gap={2}>
          <Button as="a" variant="primary">
            Button as link
          </Button>
          <Button as="a" variant="success">
            Button as link
          </Button>
          <Button>A button</Button>
        </Stack>
      </div>

      <div>
        <Badge bg="primary">Primary</Badge>{' '}
        <Badge bg="secondary">Secondary</Badge>{' '}
        <Badge bg="success">Success</Badge> <Badge bg="danger">Danger</Badge>{' '}
        <Badge bg="warning" text="dark">
          Warning
        </Badge>{' '}
        <Badge bg="info">Info</Badge>{' '}
        <Badge bg="light" text="dark">
          Light
        </Badge>{' '}
        <Badge bg="dark">Dark</Badge>
      </div>

      <div>
        <Button variant="primary">Primary</Button>{' '}
        <Button variant="secondary">Secondary</Button>{' '}
        <Button variant="success">Success</Button>{' '}
        <Button variant="warning">Warning</Button>{' '}
        <Button variant="danger">Danger</Button>{' '}
        <Button variant="info">Info</Button>{' '}
        <Button variant="light">Light</Button>{' '}
        <Button variant="dark">Dark</Button> <Button variant="link">Link</Button>
      </div>

      <div style={{margin: "10px 0"}}>
        <Button className="big" variant="primary">Big button for big stuffs</Button>
      </div>

      <ScrollTest/>

      <FlatSelect label="Open Me" leaf={false}>
        <FlatSelect label="Option 1" leaf={true}/>
        <FlatSelect label="Option 2" leaf={false}>
          <FlatSelect label="SubOption 1" leaf={true}/>
          <FlatSelect label="SubOption 2" leaf={true}/>
        </FlatSelect>
        <FlatSelect label="Option 3" leaf={true}/>
      </FlatSelect>

      <SelectBasicExample/>

      <LeftSideBar container={refSideBar}/>

      <TooltipPositionedExample/>

      <PopoverPositionedExample/>

      <MarkersExample/>

    </div>
  )
}


export default Boot
