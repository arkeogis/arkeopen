import { ApolloClient, ApolloProvider, InMemoryCache, createHttpLink } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { onError } from "@apollo/client/link/error";
import { IS_ARKEOGIS } from "../../lib/utils";
import { useAuthStore } from "../../lib/auth";

let apolloClient = null;

export const getApolloClient = () => {
  // This setup is only needed once per application;
  if (apolloClient === null) {
    // Apollo Client (GRAPHQL) SETUP
    const httpLink = createHttpLink({
      uri: import.meta.env.VITE_GRAPHQL_URI,
      fetchOptions: {
        //mode: "no-cors",
        //credentials: "include",
      },
    });

    const authLink = setContext((_, { headers, role = "user" }) => {
      const token = useAuthStore.getState().token;
      return __ARKEOGIS__
        ? {
            headers: {
              ...headers,
              authorization: token ? `Bearer ${token}` : "",
              "x-hasura-role": role,
            },
          }
        : headers;
    });

    const errorLink = onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors) {
        for (const err of graphQLErrors) {
          if (
            err.extensions.code === "UNAUTHENTICATED" ||
            err.extensions.code === "invalid-jwt" ||
            err.extensions.code === "invalid-headers"
          ) {
            useAuthStore.getState().setLoggedOut();
          }
        }
      }
      if (networkError) console.error(`[Network error]: ${networkError}`);
    });

    apolloClient = new ApolloClient({
      cache: new InMemoryCache({
        resultCacheMaxSize: Math.pow(2, 17), // 2^17 is ok with nocache on MainMap query off. If we remove nocache on MainMap : 2^17 was insuffisient. 2^18 was ok. So 2^19 is large. (2^19=512kB)
      }),
      link: errorLink.concat(authLink).concat(httpLink),
    });
  }
  return apolloClient;
};

const ApolloSetup = ({ children }) => {
  return <ApolloProvider client={getApolloClient()}>{children}</ApolloProvider>;
};

export default ApolloSetup;
