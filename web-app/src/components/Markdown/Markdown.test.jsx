import { render } from "@testing-library/react";
import Markdown from "./Markdown";

// Renders markdown text content correctly with basic markdown syntax
it("should render markdown text with basic syntax correctly", () => {
  const markdownText = "# Title\n**bold text**\n_italic text_";
  const { container } = render(<Markdown>{markdownText}</Markdown>);

  expect(container.querySelector("h1")).toHaveTextContent("Title");
  expect(container.querySelector("strong")).toHaveTextContent("bold text");
  expect(container.querySelector("em")).toHaveTextContent("italic text");
});
