import "./TestInit.js";
import { render, screen } from "@testing-library/react";
import App from "./App";

test("checks for body tag", () => {
  render(<App />);
  const bodyElement = document.body;
  expect(bodyElement).toBeInTheDocument();
});
