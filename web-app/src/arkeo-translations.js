/**
 * This file is not included anywhere.
 * It's purpose is only to be parsed by "npm run extract-translations"
 * so; translations json file contain them.
 * We need this, because most of theses keys are not present in source files, but are generated from database content
 */

if (false) {
  t=null;
  t`arkeo.database.type.undefined`
  t`arkeo.database.type.inventory`
  t`arkeo.database.type.research`
  t`arkeo.database.type.literary-work`

  t`arkeo.database.geographical_extent.undefined`
  t`arkeo.database.geographical_extent.country`
  t`arkeo.database.geographical_extent.continent`
  t`arkeo.database.geographical_extent.international_waters`
  t`arkeo.database.geographical_extent.world`

  t`arkeo.database.state.undefined`
  t`arkeo.database.state.in-progress`
  t`arkeo.database.state.finished`

  t`arkeo.database.context.undefined`
  t`arkeo.database.context.academic-work`
  t`arkeo.database.context.contract`
  t`arkeo.database.context.research_team`
  t`arkeo.database.context.other`

  t`arkeo.database.scale_resolution.undefined`
  t`arkeo.database.scale_resolution.object`
  t`arkeo.database.scale_resolution.site`
  t`arkeo.database.scale_resolution.watershed`
  t`arkeo.database.scale_resolution.micro-region`
  t`arkeo.database.scale_resolution.region`
  t`arkeo.database.scale_resolution.country`
  t`arkeo.database.scale_resolution.continent`
  t`arkeo.database.scale_resolution.world`

}
