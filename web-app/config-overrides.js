// Overrides create-react-app webpack configs without ejecting
// https://github.com/timarney/react-app-rewired

const { addBabelPlugins, override } = require("customize-cra");
const path = require("path");

module.exports = override(
  ...addBabelPlugins(
    [
      "transform-inline-environment-variables",
      {
        include: ["REACT_APP_ARKEOGIS", "REACT_APP_ARKEOPEN", "REACT_APP_GRAPHQL_URI", "REACT_APP_SERVER_URI"],
      },
    ]

    /* Add plug-in names here (separate each value by a comma) */
  ),

  (config) => {
    // Check environment variables
    const isArkeoGis = process.env.REACT_APP_ARKEOGIS === "1";
    const isArkeoOpen = process.env.REACT_APP_ARKEOPEN === "1";

    // Define cache directory based on environment variables
    let cacheDirectory, babelCacheDirectory;
    if (isArkeoGis) {
      cacheDirectory = path.resolve(__dirname, "node_modules/.cache/arkeogis");
      babelCacheDirectory = path.resolve(__dirname, "node_modules/.cache/arkeogis/babel-loader");
    } else if (isArkeoOpen) {
      cacheDirectory = path.resolve(__dirname, "node_modules/.cache/arkeopen");
      babelCacheDirectory = path.resolve(__dirname, "node_modules/.cache/arkeopen/babel-loader");
    } else {
      cacheDirectory = path.resolve(__dirname, "node_modules/.cache");
      babelCacheDirectory = path.resolve(__dirname, "node_modules/.cache/babel-loader");
    }

    // Set Webpack cache options
    config.cache = {
      type: "filesystem",
      cacheDirectory: cacheDirectory,
    };

    // Customize Babel loader options
    config.module.rules.forEach((rule) => {
      if (rule.oneOf) {
        rule.oneOf.forEach((oneOfRule) => {
          if (oneOfRule.loader && oneOfRule.loader.includes("babel-loader") && oneOfRule.options) {
            oneOfRule.options.cacheDirectory = babelCacheDirectory;
          }
        });
      }
    });

    return config;
  }
);
