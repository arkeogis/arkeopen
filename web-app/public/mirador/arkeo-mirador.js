/* global Mirador */

var mir = null;
var mirconfig = {
  id: "viewer",
  window: {
    allowClose: false,
    allowMaximize: false,
    allowFullscreen: false,
    allowTopMenuButton: false,
    views: [
      {
        key: "single",
        behaviors: ["individuals"],
      },
      {
        key: "book",
        behaviors: ["paged"],
      },
      {
        key: "scroll",
        behaviors: ["continuous"],
      },
    ],
    defaultSidebarPanelWidth: parseInt(window.innerWidth / 3),
  },
  catalog: [],
  windows: [],
  workspace: {
    type: "none", // elsaic, mosaic
    isWorkspaceAddVisible: false,
    allowNewWindows: false,
    showZoomControls: true,
    workspaceControlPanel: {
      enabled: false,
    },
  },
  language: "fr",
  selectedTheme: "light",
  themes: {
    light: {
      palette: {
        primary: {
          main: "#BFA66A",
        },
        secondary: {
          main: "#00FF00",
        },
        shades: {
          dark: "#FFFFFF",
          main: "#FFFFFF",
          light: "#FFFFFF",
        },
      },
      typography: {
        fontFamily: ["Inter"],
      },
    },
  },
  thumbnailNavigation: {
    defaultPosition: "off", // Which position for the thumbnail navigation to be be displayed. possible values are "off", "far-bottom" or "far-right"
  },
  workspaceControlPanel: {
    enabled: false, // Configure if the control panel should be rendered.  Useful if you want to lock the viewer down to only the configured manifests
  },
};

/**
 * Initialize the Mirador viewer
 */
function initMirador(iiifresources, language) {
  initMiradorConfigDatasFromArkeo(iiifresources);
  mir = Mirador.viewer({ ...mirconfig, language });
  initMiradorNoWindowHack();
  mir.store.subscribe(() =>
    window.parent.postMessage(
      {
        miradorMessage: "isWorkspaceAddVisible",
        status: mir.store.getState().workspace.isWorkspaceAddVisible,
      },
      "*"
    )
  );
}

/**
 * Initialize the "no window" hack for Mirador
 */
var latestWindowId = null;
function initMiradorNoWindowHack() {
  mir.store.subscribe(() => {
    var s = mir.store.getState();
    var found = false;
    for (var w in s.windows) {
      if (found && latestWindowId !== w) {
        console.log("closing: " + latestWindowId);
        mir.store.dispatch(Mirador.actions.removeWindow(latestWindowId));
        break;
      }
      if (latestWindowId === w) found = true;
      latestWindowId = w;
    }
  });
}

/**
 * When datas to view are transmited from Arkeo, add them to the mirador instance
 */
function initMiradorConfigDatasFromArkeo(iiifresources) {
  iiifresources.forEach((r) => {
    mirconfig.catalog.push({
      manifestId: r,
    });
  });
  if (iiifresources.length > 1) {
    mirconfig.workspace.isWorkspaceAddVisible = true;
  } else if (iiifresources.length === 1) {
    mirconfig.windows.push(mirconfig.catalog[0]);
  }
}
window.addEventListener(
  "message",
  function (event) {
    if (
      !event.data.arkeopenMessage ||
      event.origin !== window.location.origin
    ) {
      return;
    }

    switch (event.data.arkeopenMessage) {
      case "ready":
        initMirador(event.data.iiifresources, event.data.language);
        break;
      case "switchLang":
        var action = Mirador.actions.updateConfig({
          language: event.data.language,
          themes: {
            light: {
              palette: {
                type: "light",
                primary: {
                  main: "#ba55d3",
                },
              },
            },
          },
        });
        mir.store.dispatch(action);
        break;
      case "showResourceList":
        mir.store.dispatch(Mirador.actions.setWorkspaceAddVisibility(true));
        break;
      default:
        console.log("No valid arkeopenMessage received");
    }
  },
  false
);
