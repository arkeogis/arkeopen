## Presentation of the choice of thematic layers shared on the ArkeOpen platform.

ArkeOpen offers thematic geographic layers produced as part of research projects and shared by their authors, enabling different interpretations of the same place to be discovered.

## 1. Choose one or more layers

- They are listed in **alphabetical order**.
- When hovering over the mouse on this list, a **bubble of information** displays the start and end dates and a short presentation of each layer.

## 2.  Result of choices

The **validation of the choices at the bottom of the screen** has the following consequences.

- The **layers and credits visible on the map** are updated.
- The **My Map index is updated** to show the elements selected and access is given to the full presentation sheet for the layers used (authors, licence, citation, etc.).
