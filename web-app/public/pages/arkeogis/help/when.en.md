## Presentation and use of the temporal research on the ArkeOpen platform.

ArkeOpen offers you in the **WHEN** tab of the research section to consult and manipulate different approaches to time and dates.

- Use **thematic chronologies** produced in the framework of research work and shared by their authors to discover different readings of the same period.
- **Explore and choose the periods** of the chosen chronology.
- **Define yourself the period** of research of your choice.
- **Refine, if necessary**, your intentions for taking into account resources with indeterminate dates and inclusion limits.

## 1. The chronologies are proposed as desired

You can first modify the **Gregorian Calendar** timeline presented by default.

- Timelines are ranked according to **popularity**.
- When hovering over the mouse of this list, a **bubble of information** displays the start and end dates, a short presentation of the time period, the geographical area and the origin of each chronology.
- When clicking on a chronology, the following menu in this pane is updated. It allows you to **consult the proposed periods** for this chronology and **select some in particular**.

ArkeOpen uses original chronologies based on a hierarchical ontology resulting from a double alignment on shared online repositories. This choice to guarantee the data a perennial interoperability.

- The **[PACTOLS thesaurus](https://pactols.frantiq.fr/opentheso/)** produced by **Frantiq** - Fédération et Ressources sur l'Antiquité (Groupement de service n°3378 du CNRS).
- **[PeriodO](http://n2t.net/ark:/99152/p09hq4n)**, a directory of definitions of historical, archaeological and art historical periods in the public domain, produced and used by various international institutions

## 2. Choose one or more periods from the chosen chronology

You can then consult and retain the particular periods of the active chronology. This menu is automatically updated when a chronology is selected.

- The list of the most extensive successive **main periods** is presented, from the oldest (top) to the most recent.
- If these periods offer more detailed **sub-periods**, the chevron on the right invites you to discover the following ones, and so on, on 4 possible levels.
- When hovering over the mouse of this list, a **bubble of information** displays the start and end dates, and the colour that represents the resources of this period on the map opposite.
- When clicking on a period, it is **selected** and then **unselected** to be visible on the map.
- Clicking on a period automatically selects all its sub-periods.

## 3. Select particular dates.

Or, to refine the display of resources on the map, it is possible to specify in the input fields particular dates outside the periodisations proposed by the available chronologies.

- Thus, if you wish, you can ask for the sites that exist between such and such a date, by indicating a start and end date in each of the input fields.
  * For example, we wish to define a period between -212 and 357.

## 4. Choose particular details.

Whether you use pre-defined periods, or a period that you have defined, for these particular cases, ArkeOpen uses the following settings, which you can modify.

- Geolocated resources whose dates are still unknown (start-finish-two) are qualified as **undetermined** (some resources have still unknown dates), and are **included by default in the search**. They are represented on the map by grey icons. You can exclude them here to display only resources with known start and end dates.
- Resources with **start and end dates including the selected start and end dates**. This is the widest choice when specifying start and end dates. This choice includes all resources whose date range is wider than that specified but which must still include the retained date range. **This is the default choice.
- It is possible to choose to **display only resources dated between the defined or chosen start and end dates**.

## 5. Result of the choices

The **validation of the choices at the bottom of the screen** and therefore of the timeline has the following consequences

- The **loading and display of the timeline** at the bottom of the screen.
- The period selection is presented **hidden** in this frieze.
- Updating the **colours of the sites on the map** in accordance with the periodisations used.
- The **change of viewpoint of the map**. The map is now centred on the chronology's area of coverage, increased by 30% to maintain visibility of the immediate surrounding resources available.
- The **update of the My map index** to present the selected elements and access to the complete presentation sheet of the chronology used (authors, licence, citation, dataset using this chronology etc).
