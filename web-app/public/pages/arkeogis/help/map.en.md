## Map tools and the My Map index on the ArkeOpen platform.

ArkeOpen also offers cartographic tools and a dynamic index of resources visible on the on-screen map.

## 1. Map tools

The various tools are available at the bottom right of the main screen.

- **Locate me** on the map.
  * Very useful in mobile mode for discovering sites close to where you are.
- The **zoom-in and zoom-out** functions.
  * The ‘+’ or ‘-’ icon is greyed out if the selected background map does not have an extra level.
- **The compass** indicates the orientation of the map.
  * By default, north is located at the top of the screen. You can change the orientation of the map by changing the orientation of the compass in this button.
  * Clicking on the compass returns north to the top of the screen.
- Different **background maps** each offer a different way of reading the terrain to suit your needs.
  * When you hover the mouse over these icons, an **information bubble** displays the name of the background map.
  * Simply click on it to update the background map.
  * The credits for the background map and the layers used are visible at the bottom of the map.
- **Symbols used** button
  * A reminder of the different **symbols** used on the map to locate sites/objects is available here.
- **Group/Ungroup** icons on the map.
  * By default, and for reasons of map legibility due to the large number of resources available, the icons are presented grouped together.
- ArkeOpen offers **scales** in imperial and metric units.

## 2. Dynamic index

At the user's request and depending on the choices made in the search pane, **ArkeOpen generates an index** of the resources visible on My map.
This search result is organised as follows:

### **Title banner**

Some information and action are proposed.
- The date of the query, and the **total number of resources matching** the search criteria.
- You can also **share** your choices and the results of your search with other readers.

### **WHEN?**

- A presentation of the **chronology used**, as well as a link to a full presentation of the associated metadata and the shared resource (in CSV format).
- A list of the **periods** used. The number in brackets indicates the number of occurrences of that period on the map. The colour used to illustrate resources from this period, followed by the start and end dates. Links to shared definitions are also provided. This list is organised according to the earliest date in the current period.
- If geo-located resources whose dates are still unknown (undetermined) are included. And the treatment used for the defined bounds.

### **WHAT?**

- Lists of **the characterisations** used. The number in brackets indicates the number of occurrences of that characterisation on the map. They also contain links to the shared definitions of the concepts. These thematic lists (real estate, furniture, etc.) are organised according to **the ArkeoGIS-ArkeOpen thesaurus**.

### **WHERE?**

- The **coordinates** of the search area and the references of the active base map.
- A presentation, if any, of the **thematic layers** used, as well as links to a complete presentation sheet of the associated metadata and to the shared resource (in Shape format). They are listed in alphabetical order.

### **OTHER FILTERS**

- A presentation of the **other filters selected** in the left-hand pane to build this map (**Status of knowledge**, **Location accuracy**, **Exceptional**, **Publishers**, **Authors**).
- A presentation of the **datasets used**, as well as links to a complete presentation sheet of associated metadata and to the shared resource (CSV).
- They are listed in descending order of the number of occurrences on the map.

### **CITATIONS**

- This tab displays the citations **to be copied and pasted**, in the event of re-use, from the open resources used by ArkeOpen following your selection to construct the map visible on the screen.
- These citations show the **authors, title, date, type** (dataset or map) and **permanent address** of each of these resources.
- For the **background map**, there is one exception due to the specific nature of the resource: a single citation of the credits is required when the resource is re-used.
