## ArkeOpen vous propose les outils suivants dans son interface principale.

## 1. En haut

- **RECHERCHER** permet d'ouvrir le volet de gauche (recherche) et d'utiliser différents filtres pour explorer les ressources partagées.
- **MA CARTE** permet d'ouvrir le volet de droite (information) pour consulter le détail de l'index des ressources visibles sur la carte. L'index est généré dynamiquement lors de la validation des choix dans le volet de recherche.
- **LE PROJET** permet d'ouvrir le volet de droite et de consulter la présentation du projet.
- Cliquer sur le **?** pour accéder à cette aide en ligne.
- Cliquer sur l'icône pour afficher en **plein écran**.
- Ouvrir le menu des **informations disponibles** pour le projet (crédits, partenaires, CGU, etc).

## 2. Écran principal

- Sur la gauche le volet recherche permet de **choisir une chronologie et de filtrer les ressources** disponibles visibles sur la carte.
  * Des **bulles d'informations** présentent des détails sur les chronologies, les périodes et les calques.
  * En bas de ce volet se trouve le bouton **Reset** qui permet d'annuler tous les choix effectués.
  * Il faut **valider les choix** pour actualiser la carte.
- Sur la carte **une icône localise** les sites/objets.
  * **La couleur** correspond à la période de fin du site/objet. Elle est actualisée en fonction de sa datation absolue, en tenant compte de la chronologie choisie (datation relative).
  * **La forme** indique que la localisation est précise ou imprécise.
  * **La taille** indique que l'état des connaissances est plus ou moins précis.
  * **Au survol** une bulle d'information affiche le nom du site/objet.
  * **Au clic** les informations du site/objet sont consultables dans le volet information.
  * **Une étoile** visible au centre d'une icône indique un site ou un objet qualifié comme **exceptionnel** par l'auteur.
- Si plusieurs sites/objets sont proche géographiquement **ils sont présentés groupés**.
  * Au centre le nombre indique combien ils sont.
  * Les portions colorées de la couronne autour du nombre sont proportionnelles aux périodes représentées dans ce regroupement.
  * Il est possible de demander à **dégrouper** ces icônes.
- En bas à droite se trouvent **les outils de cartographie** ainsi que l'**échelle** métrique et impériale de la carte.

## 3. En bas

**La frise chronologique** affiche par défaut le calendrier grégorien. Elle est mise à jour en fonction de la sélection de l'utilisateur dans le volet de recherche de l'interface (onglet QUAND).

- **Les années** sont présentées de la plus ancienne à gauche vers la plus récente à droite.
- **Les couleurs** vont des plus froides vers les plus chaudes et sont reprises sur la carte pour illustrer les datations de fin des sites.
- **Les périodes** principales sont dans la partie supérieure de la frise et peuvent avoir des sous-périodes plus précises qui sont présentées en dessous.
- Au survol de la souris des **bulles-informations** présentent les noms les dates de début et de fin et une courte présentation ou des évènements pour chacune des périodes dans cette aire géographique.
- Suite à la **validation de la sélection** de certaines périodes dans le volet de gauche, elles sont présentées hachurées dans cette frise comme rappel de la sélection faite.

**Les crédits des fonds de carte et calque(s) thématique(s) visible(s)** à l'écran.

- Ils sont générés dynamiquement lors de la validation des choix par l'utilisateur dans le volet recherche de l'interface.
