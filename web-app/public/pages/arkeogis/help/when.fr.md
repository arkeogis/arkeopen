## Présentation et utilisation de la recherche temporelle sur la plateforme ArkeOpen.

ArkeOpen vous propose dans l'onglet **QUAND** du volet de recherche de consulter et de manipuler différentes approches du temps et des dates.

- Utilisez des **chronologies thématiques** réalisées dans le cadre de travaux de recherche et partagées par leurs auteurs pour découvrir les lectures différentes d'une même époque.
- **Explorez et sélectionnez les périodes** de la chronologie choisie.
- **Définissez vous-même la période** de recherche de votre choix.
- **Affinez, si besoin**, vos intentions pour la prise en compte des ressources dont les dates sont indéterminées et des bornes d'inclusion.

## 1. Les chronologies sont proposées au choix

Vous pouvez en premier lieu modifier la chronologie du **calendrier grégorien** présentée par défaut.

- Elles sont classées en fonction de leur **popularité**.
- Au survol de la sourie de cette liste une **bulle information** affiche les dates de début, de fin, une courte présentation de l’ère temporelle, de l'aire géographique et de l'origine de chacune des chronologies.
- Au clic sur une chronologie le menu suivant, dans ce volet, est mis à jour. Il permet de **consulter les périodes proposées** pour cette chronologie et **d'en choisir certaines en particulier**.

ArkeOpen utilise des chronologies originales basées sur une ontologie hiérarchisée issue d’un double alignement sur des référentiels mutualisés en ligne. Ce choix permet de garantir l’interopérabilité pérenne des données.

- Le **[thésaurus PACTOLS](https://pactols.frantiq.fr/opentheso/)** réalisé par **Frantiq** – Fédération et Ressources sur l’Antiquité (Groupement de service n°3378 du CNRS).
- **[PeriodO](http://n2t.net/ark:/99152/p09hq4n)**, un répertoire de définitions de périodes historiques, archéologiques et d'histoire de l'art dans le domaine public, réalisé et utilisé par diverses institutions internationales

## 2. Choisissez une ou plusieurs périodes dans la chronologie sélectionnée.

Vous pouvez ensuite consulter et retenir les périodes particulières de la chronologie active. Ce menu est automatiquement mis à jour lors de la sélection d'une chronologie.

- La liste des **périodes principales** successives les plus étendues est présentée, de la plus ancienne (en haut) à la plus récente.
- Si ces périodes proposent des **sous-périodes** plus fines le chevron de droite vous invite à découvrir les suivantes et ainsi de suite sur 4 niveaux possibles.
- Au survol de la souris de cette liste une **bulle information** affiche les dates de début, de fin, et la couleur qui représente les ressources de cette période sur la carte ci-contre.
- Le clic sur une période permet de la **sélectionner** ou de la **désélectionner** pour qu'elle soit visible sur la carte.
- Le clic sur une période sélectionne automatiquement toutes ses sous-périodes.

## 3. Définissez des dates particulières.

Pour affiner l’affichage des ressources sur la carte, il est possible de préciser, dans les champs de saisie des dates, des périodes différentes de celles proposées par les chronologies disponibles.
- Ainsi, Si l'on le souhaite on peut demander les sites ayant une existence entre telle date et telle date, en indiquant dans chacun des champs de saisie une date de début et une date de fin.
  * Par exemple on souhaite définir une période comprise entre -212 et 357.

## 4. Choisir des précisions particulières.

Que l'on utilise des périodes pré-définies ou une période que l'on a définie soi-même, ArkeOpen utilise les réglages suivants, que vous pouvez modifier.

- Les ressources géolocalisées dont les datations sont encore inconnues (début-fin-les deux) sont qualifiées de **indéterminé** (certaines ressources ont des datations encore inconnues), et sont **incluses par défaut dans la recherche**. Elles sont représentées sur la carte par des icônes de couleur grise. Vous pouvez les exclure ici pour afficher uniquement les ressources dont les dates de début et de fin sont connues.
- Les ressources ayant des **dates de début et de fin incluant les dates de début et de fin retenues**. C'est le choix le plus large quand on précise des bornes de début et de fin. Il inclut toutes les ressources dont la datation est plus large que celle précisée, mais qui doit quand même inclure la datation retenue. **C’est le choix par défaut**.
- Il est possible de choisir d'**afficher uniquement les ressources datées entre les dates de début et fin définies ou choisies**.

## 5. Résultat des choix

La **validation des choix en bas d'écran** et donc de la chronologie a pour conséquences :

- Le **chargement et l’affichage de la frise chronologique** en bas de l’écran.
- La sélection de période est présentée(s) **hachurée(s)** dans cette frise.
- L'actualisation des **couleurs des sites sur la carte** en accord avec les périodisations utilisées.
- Le **changement de point de vue de la carte**. Celle-ci est centrée sur la zone d’emprise de la chronologie augmentée de 30% pour garder une visibilité sur les ressources voisines immédiates disponibles.
- La **mise à jour de l'index de Ma carte** pour présenter les éléments retenus et l'accès à la fiche de présentation complète de la chronologie utilisée (auteurs, licence, citation, jeux de données utilisant cette chronologie etc).
