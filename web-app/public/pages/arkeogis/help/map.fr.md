## Les outils cartographiques et l'index de Ma carte sur la plateforme ArkeOpen.

ArkeOpen propose également des outils cartographiques et un index dynamique des ressources visibles sur la carte à l'écran.

## 1. Outils cartographiques

Les différents outils sont disponibles en bas à droite de l'écran principal.

- **Se localiser** sur la carte.
  * Très utile en mode mobile pour découvrir des sites à proximité de l'endroit où on se trouve.
- Les **fonctions de zoom** avant et zoom arrière.
  * L'icône « + » ou « - » est grisée si le fond de carte sélectionné ne dispose pas de niveau supplémentaire.
- La **boussole** indique l'orientation de la carte.
  * Par défaut, le nord est situé en haut de l'écran. Il est possible de modifier l'orientation de la carte en agissant sur l'orientation de la boussole dans ce bouton.
  * Un clic sur la boussole permet de remettre le nord en haut de l'écran.
- Des **fonds de cartes différents** proposent chacun une lecture particulière du relief en fonction de vos usages.
  * Au survol de la souris de ces icônes une **bulle information** affiche le nom du fond de carte.
  * Il suffit de cliquer dessus pour mettre à jour le fond de carte.
  * Les crédits du fond de carte et des calques utilisés sont visibles en bas de la carte.
- Bouton **symboles utilisés**
  * Un rappel des différents **symboles utilisés** sur la carte pour localiser les sites/objets est accessible ici.
- Les fonctions **Grouper/Dégrouper** les icônes sur la carte.
  * Par défaut, et pour des raisons de lisibilité de la carte en raison du grand nombre de ressources disponibles, les icônes sont présentées groupées.
- ArkeOpen propose des **échelles** suivant les systèmes d'unités impériales et métriques.

## 2. Index dynamique

A la demande de l'utilisateur et suite au choix fait dans le volet recherche, **ArkeOpen génère un index** des ressources visibles sur Ma carte.
Cet Index est organisé comme suit :

### **Bandeau titre**

Certaines informations et actions sont proposées.
- La date de la requête et donc des données disponibles suivit du **nombres total de ressources répondant aux critères** retenus dans la recherche.
- La possibilité de **partager** le résultat de ces choix et donc cette légende avec d'autres lecteurs.

### **QUAND?**

- Une présentation de **la chronologie utilisée**, ainsi qu'un lien vers une fiche de présentation complète des métadonnées associées et la ressource partagée (au format CSV).
- Une liste **des périodes** utilisées. Le nombre entre parenthèses indique le nombre d’occurrences de cette période présentes sur la carte. La couleur utilisée pour illustrer les ressources de cette période suivie des dates de début et de fin. Ainsi que des liens vers les définitions partagée. Cette liste est organisé d'après la date la plus ancienne de la période présente.
- Si les ressources géolocalisées dont les datations sont encore inconnues (indéterminé) sont incluses. Et le traitement utilisé pour les bornes définies.

### **QUOI?**

- Des listes des **caractérisations** utilisées. Le nombre entre parenthèses indique le nombre d'occurrences de cette caractérisation présentes sur la carte. Elles contiennent également des liens vers les définitions partagées des concepts. Ces listes thématiques (immobilier, mobilier, etc.) sont organisées selon **le thésaurus ArkeoGIS-ArkeOpen**.

### **OÙ?**

- Les **coordonnées** de la zone de recherche et les références du fond de carte actif.
- Une présentation, si il y en a, des **calques thématiques** utilisés, ainsi que des liens vers une fiche de présentation complète des métadonnées associées (et du fichier Shape partagé). Ils sont listés par ordre alphabétique.

### **AUTRES FILTRES**

- Une présentation des **autres filtres retenus** dans le volet de gauche pour construire cette carte (**État des connaissances**, **Précision de la localisation**, **Exceptionnel**, **Éditeurs**, **Auteurs**).
- Une présentation des **jeux de données utilisés**, ainsi que des liens vers la ressource partagée (CSV) et vers une fiche de présentation complète des métadonnées associées.
- Ils sont listés par ordre décroissant du nombre d’occurrences présentes sur la carte.

### **CITATIONS**

- Cet onglet affiche les citations à **copier et coller**, en cas de ré-emplois, des ressources ouvertes utilisées par ArkeOpen suite à votre sélection pour construire la carte visible à l'écran.
- Ces citations présentent les **auteurs**, **titre**, **date**, **type** (dataset ou map) et l'**adresse pérenne** de chacune de ces ressources.
- Pour le **fond de carte**, il y a une exception en raison de la nature spécifique de la ressource, l'unique citation des crédits est requise lors d'un réemplois.
