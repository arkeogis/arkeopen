# Mentions Légales


**Url du site**: **http://arkeopen.org**

- **Propriétaire du site**: Loup BERNARD
- **Statut du propriétaire du site**: Maître de Conférences, Faculté de Sciences Historiques, Antiquités nationales.
Université de Strasbourg
Membre titulaire CNRS UMR 7044 ArcHiMedE
- **Numéro siret**: 130 005 457 000 10
- **Adresse postale du propriétaire du site**: Maison Inter-universitaire des Sciences de l'Homme - Alsace
5, allée du Général Rouvillois
CS 50008
67083 Strasbourg cedex
- **Créateur du site**: Nicolas DIMITRIJEVIC, Philippe JULLIEN, Pedro VITORINO
- **Url du créateur du site**: https://unistra.fr/
- **Nom du responsable de publication**: Loup BERNARD
- **Contact du responsable de publication**: loup.bernard(@)unistra.fr
- **Nom du WebMaster**: Philippe JULLIEN
- **Contact du WebMaster**:	admin(@)arkeogis.org
- **Droits images**: Photos des objets et sites © indiqué sur chaque par leurs auteurs, pour les autres illustrations © arkeopen.org
- **Hébergeur du site**: La grille de services de la TGIR Huma-Num / TGIR http://www.huma-num.fr/
- **Adresse postale de l'hébergeur**: TGIR Huma-Num - UMS 3598 - 4, rue Lhomond - F - 75005 Paris
- **Stockage des données**: Les données sont stockées sur la grille de services de la TGIR Huma-Num.
- **Archivage des données**: Les données sont archivées dans le cadre du service d'archivage à long terme Huma-Num/CINES.

**CNIL**: Le président de l’Université de Strasbourg a décidé la mise en place du système d’information géographique ArkeOpen permettant aux chercheurs de déposer et de diffuser vers le grand public des données de recherches.
Les informations recueillies font l’objet d’un traitement informatique destiné à la gestion des données de recherche déposées sur ce SIG. Les destinataires des données sont : le service qui gère ce SIG et le grand public. Conformément à la loi « Informatique et Libertés » du 6 janvier 1978 modifiée, vous bénéficiez d’un droit d’accès et de rectification aux informations qui vous concernent, que vous pouvez exercer en vous adressant à Monsieur Loup Bernard loup.bernard(@)unistra.fr
Vous pouvez également, pour des motifs légitimes, vous opposer au traitement des données vous concernant.

