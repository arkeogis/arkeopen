# Project

## 1. The open platform

The open platform **ArkeOpen**, this open access interface, allows you to consult, query and cross-reference open data (OpenData) from the research and teaching platform **[ArkeoGIS](https://arkeogis.org/)** research and teaching platform.

- The left pane (search) allows you to **change the chronology and filter the results visible on the map** according to different criteria (Thesaurus, Periods, Authors etc...).
- The **detailed information** will be presented in this right-hand pane (information).

Creation of the tools under the supervision of **[University of Strasbourg](https://www.unistra.fr/)** and under Free Software license following a funding from the **French Ministry of [Higher Education, Research and Innovation](https://www.enseignementsup-recherche.gouv.fr/fr)** (2022).

The platform is managed and administered by the **[University of Strasbourg](https://www.unistra.fr/)** and the **Maison Interuniverstaire des Sciences de l'Homme - Alsace [(MISHA)](https://www.misha.fr/)** on the infrastructures of the **Infrastructure de Recherche des humanités numériques [Huma-Num](https://www.huma-num.fr/)**, by a multi-disciplinary team.

## 2. Shared data

The accessible data come from research works, theses, inventories, books transcript, in the fields of humanities and social sciences (archaeology, history) and environment.

Following an alignment carried out on the research platform **[ArkeoGIS](https://arkeogis.org/)** accessible only to professionals, these data are presented here to all readers by their authors and publishers. This sharing is done following **the recommendations of [open science](https://www.ouvrirlascience.fr/accueil/)** which consists of "**making research results, mostly from public funds, accessible as much as possible and closed as much as necessary**".

In the framework of the **ArkeOpen** project, they are made available to all, in English and French, to participate in the promotion, dissemination and unhindered reuse of research data with a view to generalizing access to knowledge.

Following the principles of **[F.A.I.R data](https://fr.wikipedia.org/wiki/Fair_data)**, they are also respectful of the logic of open and linked data **[(LOD)](https://fr.wikipedia.org/wiki/Linked_open_data)**

- **F** : **Findable** with their rich metadata and unique identifier.
- **A** : **Accessible** as they are stored in certified open repositories.
- **I** : **Interoperable** because of the standard concepts used and the formats offered.
- **R** : **Reusable** through the use of common standards, licensing and citation of authors...

The repositories (chronologies-periods, thesaurus, geography) implemented by **ArkeOpen** are established in collaboration with various partners.

## 3. Illustrated data

Some of the resources are enriched with open documentation from research projects, archives, digital libraries or other projects to enhance the value of heritage documents. To this end, **ArkeOpen** uses the **[International Image Interoperability Framework (IIIF)](https://iiif.io/)**, a set of standards designed to define an international framework for the interoperability of images distributed on the Web.
