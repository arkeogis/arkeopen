## Présentation du choix des calques thématiques partagés sur la plateforme ArkeOpen.

ArkeOpen propose des calques géographiques thématiques réalisés dans le cadre de travaux de recherche et partagés par leurs auteurs pour découvrir des lectures différentes d'un même lieu.

## 1. Choisir un ou des calque(s)

- Ils sont listés par **ordre alphabétique**.
- Au survol de la souris de cette liste, une **bulle d'information** affiche les dates de début et de fin, ainsi qu'une courte présentation de chacun des calques.

## 2. Résultat des choix

La **validation des choix en bas d'écran** a pour conséquences :

- L'actualisation des **calques et crédits visibles sur la carte**.
- La **mise à jour de l'index de Ma carte** pour présenter les éléments retenus et l'accès à la fiche de présentation complète des calques utilisés (auteurs, licence, citation, etc).
