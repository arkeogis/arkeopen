## ArkeOpen offers the following tools in its main interface.

## 1. Top

- **SEARCH** opens the left-hand pane (search) and allows you to use various filters to explore shared resources.
- **MY MAP** opens the right-hand pane (information) to view the details of the index of resources visible on the map. The index is generated dynamically when you validate your choices in the search pane.
- **THE PROJECT** opens the right-hand pane and displays the presentation of the project.
- Click on the **?** to access this online help.
- Click on the icon to display the **full screen**.
- Open the menu of **available information** for the project (Credits, Partners, etc).

## 2. Main screen

- On the left, the search pane allows you to **choose a timeline and filter the available resources** visible on the map.
  * **Information bubbles** provide details on chronologies, periods and layers.
  * At the bottom of this pane, the **reset** button is used to cancel all choices made.
  * You must **validate choices** to update the map.
- On the map **an icon locates** the sites/objects.
  * **The colou**r is that of the end period of the site/object. It is updated according to its absolute dating based on the chronology chosen (relative dating).
  * **The shape** indicates whether the location is precise or imprecise.
  * **The size** indicates whether the state of knowledge is more or less precise.
  * **On moussover** an information bubble displays the name of the site/object.
  * **When clicked** information about the site/object can be consulted in the information pane.
  * **A star** visible in the centre of an icon indicates a site/object described as **exceptional** by the author.
- If several sites/objects are geographically close **they are presented grouped together**. The number in the centre indicates how many there are. The coloured portions of the crown around the number are proportional to the periods represented in this grouping.
  * It is possible to **ungroup** these icons.
- At the bottom right are the **mapping tools** and the map's metric and imperial **scales**.

## 3. Bottom

The **Timeline** displays the Gregorian calendar by default. It is updated according to the user's choice of **WHEN** in the search section of the interface.

- **The years** are displayed from the oldest on the left to the most recent on the right.
- **The colours** run from coldest to warmest and are used on the map to illustrate the end dates of the sites.
- **The main periods** are at the top of the frieze and may have more detailed sub-periods, which are shown below.
- Moving the mouse over the **information bubbles** will display the name, start and end dates and a short presentation or events for each of the periods in that geographical area.
- Once you have **confirmed** the selection of certain periods in the search window, they will appear hatched in this frieze as a reminder of the selection you have made.

**The credits of the background map and the thematic layer(s) are visible** on the screen.

- They are generated dynamically when the user confirms their choices in the search section of the interface.
