## Présentation et utilisation de la recherche thématique sur la plateforme ArkeOpen.

Les auteurs ArkeOpen organisent les informations relatives aux sites et aux objet en utilisant des concepts et des attributs partagés. Il est possible de filtrer ces éléments en fonction des critères suivants.

## 1. Choisir des descripteurs (concept) en particulier

ArkeOpen propose un **thésaurus ouvert, dans une ontologie hiérarchisée issue d’un double alignement** sur des référentiels mutualisés en ligne. Ce choix permet de garantir une interopérabilité pérenne aux données.

- Le [**thésaurus PACTOLS**](https://pactols.frantiq.fr/opentheso/) a été réalisé par **Frantiq** – Fédération et Ressources sur l’Antiquité (groupement de service n°3378 du CNRS).
- Le [**Art & Architecture Thesaurus® (AAT)**](https://www.getty.edu/research/tools/vocabularies/aat/) proposé par le **Getty Research Institute** - GRI

Les **descripteurs** (concept) du thésaurus proposés sont organisés d’après ces cinq grandes thématiques, avec quatre niveaux de précision possibles. Le chevron de droite vous invite à découvrir les suivantes, et ainsi de suite.

- **Immobilier** : Les édifices, leur fonction, ce qui sert à les construire et comment y accéder (ouvrage routier) et les approvisionner (adduction, mines, etc).
- **Mobilier** : Leur matériaux les objets, les outils et les moyens de la mobilité.
- **Production** : Les lieux de production, de stockage, les sites d’extraction.
- **Analyse** : Le type d’analyse, le site de prélèvement de l’échantillon ou de la référence.
- **Paysage** : Les structures agraires et autres formations géologiques qui dessinent le paysage.

Il est possible de **croiser les informations** pour demander, par exemple, les embarcations et les amphores, et de préciser une période, afin d'obtenir les ressources disponibles qui sont qualifiées d'**embarcation** et l'emplacement des **amphores** de telle **période**.

## 2. Choisir certaines spécificités

Certains auteurs peuvent qualifier des sites ou des objets d'**exceptionnels**. Ils peuvent y joindre des **illustrations** et donner une **localisation précise** de leur emplacement. Vous pouvez choisir d'inclure uniquement les sites/objets répondant à un ou plusieurs de ces critères.

* **Exceptionnel**
  * **Oui,** c’est à dire les ressources dont au moins une des caractérisations (immobilier, mobilier, production, analyse paysage,) est qualifiée au moins une fois " Exceptionnel " par son auteur. L’icône sur la carte dispose d'une étoile en son centre.
* **Illustration(s)**
  * **Oui,** un logo "images" sur la fiche d'information du site/objet permet d'utiliser la visionneuse Mirador pour accéder aux images (photos, dessins, représentations 3D, plans, etc) et à leur description.
* **Précision de la localisation**
  * La localisation du site est **précise**. L'icône est une goutte inversée. La pointe en bas de l'icône se situe aux coordonnées latitude et longitude renseignées par l'auteur lors de l'importation du site.
  * La localisation du site est **imprécise**. L'icône est un disque. Le centre de ce disque se situe aux coordonnées indiquées ou calculées. En effet, lors de l'importation sur ArkeoGIS le site est déclaré par son auteur soit en centroïde (centre géographique de la commune ou du site de fouille) soit en rapport avec un Geoname (http://geonames.org).

## 3. Affiner votre sélection

Pour restreindre le nombre de sites/objets visibles sur la carte, vous pouvez utiliser les attributs suivants.

* **Occupation.** Ce que l’ont sait de la présence humaine à cet endroit.
  * A un seul moment avec le même occupant (**Unique**)
  * Pendant une période sans interruption (**Continue**).
  * Pendant une période avec interruption(s) (**Multiple**).
  * On ne sait la qualifier à ce jour (**Non renseigné**).
* **État des connaissances.** Ce que l’ont sait sur cette ressource est provient de:
  * On ne sait pas (**Non renseigné**).
  * On en parle dans un ou des ouvrages (**Littérature**).
  * D’une lecture d’images satellites, de photographies aériennes, de Lidar (**Prospecté aérien**).
  * D’une visite sur site (**Prospecté pédestre**).
  * D’une intervention de sondage (**Sondé**).
  * D’une fouille (**Fouillé**).
  Sur la carte la taille de l’icône est indexée sur la valeur "**état des connaissances**" la plus précise de la ou les caractéristique(s) de la ressource. De la plus petite à la plus grande, de **Non renseigné** à **Fouillé**, Ils traduisent en les rendant plus visibles les sites dont la connaissance est la plus aboutie.

## 4. Résultat des choix

La **validation des choix en bas d'écran** a pour conséquences :

- L'actualisation des **ressources visibles sur la carte**.
- La **mise à jour de l'index de Ma carte** pour présenter les éléments retenus.
