## Presentation and use of the thematic search on the ArkeOpen platform.

ArkeOpen's Authors organise information about sites and objects using shared concepts and attributes. These elements can be filtered according to the following criteria.

## 1. choose descriptors (concepts) in particular

ArkeOpen offers an **open thesaurus, in a hierarchical ontology derived from a double alignment** with shared online repositories. This choice guarantees the long-term interoperability of the data.

- The [**PACTOLS**] thesaurus (https://pactols.frantiq.fr/opentheso/) was produced by **Frantiq** - Fédération et Ressources sur l'Antiquité (CNRS service group no. 3378).
- The [**Art & Architecture Thesaurus® (AAT)**](https://www.getty.edu/research/tools/vocabularies/aat/) proposed by the **Getty Research Institute** - GRI

The **descriptors** (concept) of the proposed thesaurus are organised according to these five main themes, with four possible levels of detail. The chevron on the right invites you to discover the next ones, and so on.

- **Property**: Buildings, their function, what was used to construct them and how to access them (roads) and supply them (water supply, mines, etc.).
- **Furniture**: Their materials objects, tools and means of mobility.
- **Production**: Places of production, storage, extraction sites.
- **Analysis**: The type of analysis, the site where the sample or reference was taken.
- **Landscape**: The agrarian structures and other geological formations that make up the landscape.

It is possible to **cross-reference information** to request, for example, boats and amphorae, and to specify a period, in order to obtain the resources available which are qualified as **boats** and the location of **amphorae** from a given **period**.

## 2. Choose certain specific features

Some authors may describe sites or objects as **exceptional**. They may attach **illustrations** and give a **precise location** of their location. You can choose to include only sites/objects that meet one or more of these criteria.

- **Exceptional**
  * **Yes**, i.e. resources for which at least one of the characteristics (real estate, furnishings, production, landscape analysis, etc.) has been described as ‘Exceptional’ by its author at least once. The icon on the map has a star in its centre.
- **Illustration(s)**
  * **Yes**, an ‘images’ logo on the site/object information sheet allows you to use the Mirador viewer to access images (photos, drawings, 3D representations, plans, etc) and their descriptions.
- **Accuracy of location**
  * The location of the site is **precise**. The icon is an inverted teardrop. The point at the bottom of the icon is located at the latitude and longitude coordinates entered by the author when importing the site.
  * The location of the site is **imprecise**. The icon is a disc. The centre of this disc is located at the coordinates indicated or calculated. When the site is imported into ArkeoGIS, it is declared by its author either as a centroid (the geographical centre of the municipality or excavation site) or in relation to a Geoname (http://geonames.org).

  ## 3. Refine your selection

To restrict the number of sites/objects visible on the map, you can use the following attributes.

* What is known about the human presence at this location.
  * At a single time with the same occupant (**Unique**)
  * For an uninterrupted period (**Continuous**).
  * During a period with interruption(s) (**Multiple**).
  * Not known at present (**Not documented**).
* What is known about this resource is derived from:
  * We do not know (**Not documented**).
  * It is mentioned in one or more works (**Literature**).
  * A reading of satellite images, aerial photographs, Lidar (**Aerial survey**).
  * A site visit (**Pedestrian survey**).
  * A sounding (**Sounded**).
  * An excavation (**Excavated**).
  On the map, the size of the icon is indexed to the most accurate ‘state of knowledge’ value for the resource characteristic(s). From the smallest to the largest, from **No information** to **Excavated**, they reflect and make more visible the sites for which knowledge is most complete.

## 4. Result of choices

Validating the choices at the bottom of the screen** has the following consequences:

- Updating the **resources visible on the map**.
- The **My Map index** is updated to show the elements selected.
