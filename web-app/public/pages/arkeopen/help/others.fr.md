## Présentation des données thématiques partagées sur la plateforme ArkeOpen.

ArkeOpen propose des filtres relatifs aux jeux de données thématiques partagés par les éditeurs et leurs auteurs.

## 1. Choisissez parmi les données thématiques

Il est possible de sélectionner un ou plusieurs jeux de données en fonction de différents critères.

- Les thèmes des différents **jeux de données** partagés par leurs auteurs sur la plateforme ArkeOpen.
  - Ils sont classés suivant l'**ordre alphabétique** de leur nom (titre).
- Le jeu de données est le résultat d'un travail de **type** :
  * Un **Inventaire**
  * Une **Recherche**
  * Un **Ouvrage** (retranscription d'un travail disponible sous un autre format, livre, publication, etc...)
- Le jeu de données présente des informations à l'**échelle** de :
  * **Site**
  * **Objet**

## 2. Recherche Textuelle

ArkeOpen permet de faire des recherches textuelles dans les champs suivants des jeux de données :

- Le **Nom** du jeu de données, le **Nom de la ressource** (site/objet), le **Nom de la commune**, les **Commentaires** et la **Bibliographie** de la ressource (le site ou l'objet).
- **Par défaut** la recherche se fait dans **Tout**, vous pouvez en choisir certains.

## 3. Attributs

ArkeOpen propose de mettre en avant les éditeurs et les auteurs participant au F.A.I.R data et la diffusion ouverte de la recherche.

* **Éditeurs** : Nom de l'organisation ou du service responsable de la mise à disposition ou de la diffusion des jeux de données partagés sur la plateforme ArkeOpen.
  - Ils sont classés par **ordre alphabétique** de leur nom.
* **Auteurs** : Prénom et Nom des auteurs des différents jeux de données partagés sur la plateforme ArkeOpen.
  * L'auteur d'un jeu de données est la personne qui réalise l'alignement et la normalisation au format ArkeOpen d'une information provenant d'une autre source dans un format différent (base de données de recherche, d'inventaire, ouvrage).
  * Il peut s'agir de la personne à l'origine de l'information source (base de données, ouvrage) c'est-à-dire un chercheur, ou pas.
  * Ils sont listés suivant l'**ordre alphabétique** de leur nom.

## 4. Résultat des choix

La **validation des choix en bas d'écran** a pour conséquences :

- L'actualisation des **ressources visibles sur la carte**.
- La **mise à jour de la l'index de Ma carte** pour présenter les éléments retenus et l'accès à la fiche de présentation complète des jeux de données utilisés (auteurs, licence, citation, etc).
