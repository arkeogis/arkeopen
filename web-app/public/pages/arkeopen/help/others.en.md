## Presentation of thematic data shared on the ArkeOpen platform.

ArkeOpen offers filters for thematic datasets shared by publishers and their authors.

## 1. choose from thematic data sets

You can select one or more datasets according to various criteria.

- The themes of the various **datasets** shared by their authors on the ArkeOpen platform.
  - They are classified according to the **alphabetical order** of their name (title).
- The dataset is the result of a **type** of work:
  * A **Inventory**
  * A **Research**
  * A **Book** (transcription of a work available in another format, book, publication, etc.)
- The dataset presents information at the **scale** of :
  * **Site**
  * **Object**

## 2. Text search

ArkeOpen allows you to perform text searches in the following fields of datasets:

- The **Name** of the dataset, the **Name of the resource** (site/object), the **Name of the municipality**, the **Comments** and the **Bibliography** of the resource (site or object).
- **By default** the search is done in **All**, you can choose some of them.

## 3. Attributes

ArkeOpen proposes to highlight publishers and authors participating in F.A.I.R data and the open dissemination of research.

* **Publishers**: Name of the organisation or department responsible for making available or distributing the datasets shared on the ArkeOpen platform.
  - They are listed in **alphabetical order** by name.
* **Authors**: First name and surname of the authors of the various datasets shared on the ArkeOpen platform.
  * The author of a dataset is the person who aligns and standardises information from another source in a different format (research database, inventory, work) in ArkeOpen format.
  * This may or may not be the person who created the source information (database, work), i.e. a researcher.
  * They are listed in the **alphabetical order** of their name.

## 4. Selection results

The **validation of choices at the bottom of the screen** has the following consequences:

- The **resources visible on the map** are updated.
- The **My Map index** is updated to show the elements selected and access is given to the full presentation sheet for the datasets used (authors, licence, citation, etc).