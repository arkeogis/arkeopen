# Contacts

Pour toutes questions vous pouvez prendre contact avec l'équipe

## Par courrier électronique à cette adresse

- admin(@)arkeogis.org

## Par courrier postal à cette adresse

- **ArkeoGIS/ArkeOpen**
- Maison Inter-universitaire des Sciences de l'Homme - Alsace
- 5, allée du Général Rouvillois
- CS 50008
- 67083 Strasbourg cedex
