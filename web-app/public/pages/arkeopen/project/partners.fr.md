# Partenaires

Création des outils Logiciel Libre suite à un financement du **Ministère français de lʼ[Enseignement supérieur, de la Recherche et de lʼInnovation](https://www.enseignementsup-recherche.gouv.fr/fr)**, et avec le soutien à la mise en œuvre et à la coordination des partenaires de l’**[Université de Strasbourg](https://www.unistra.fr/)**.
Le Projet **ArkeOpen** collabore avec les différents intervenants dans la cadre de la **[science ouverte](https://www.ouvrirlascience.fr/accueil/)** pour atteindre les objectifs d'Open data du **[F.A.I.R data](https://fr.wikipedia.org/wiki/Fair_data)**.

## F : Faciles à retrouver avec leurs riches métadonnées et un identifiant unique.

- Les données sont accompagnées de **métadonnées de description** au format **[Dublin Core™ étendu](https://fr.wikipedia.org/wiki/Dublin_Core)**.
- Un identifiant unique stable et pérenne (**Digital Object Identifier- DOI**) leur est attribué notamment par **[NAKALA](https://nakala.fr/)**, une réalisation de l'infrastructure de recherche **[IR Huma-Num](https://www.huma-num.fr/)** (CNRS, Aix-Marseille Université, Campus Condorcet).

## A : Accessibles car elles sont entreposées dans des dépôts certifiés.

- Toutes les données présentées sont accessibles au moins dans **[la collection ouverte ArkeOpen de l’entrepôt de données NAKALA](https://nakala.fr/search/?q=ArkeOpen)**, ou dans un autre entrepôt ouvert.
- Cette collection **ArkeOpen** est moissonnée par **[ISIDORE](https://isidore.science/)**, un moteur de recherche permettant l'accès aux données numériques des Sciences Humaines et Sociales (SHS). Il est ouvert à tous les lecteurs et en particulier aux enseignants, chercheurs, doctorants et étudiants.

## I : Interopérables par les concepts normés utilisés et les formats proposés.

- **ArkeOpen/ArkeoGIS** est aligné sur ces référentiels disposant d’identifiants uniques pour garantir leur inter-opérabilité dans différentes langues.
- Le **[thésaurus PACTOLS](https://pactols.frantiq.fr/opentheso/)** accessible sur la plateforme **[Opentheso](https://opentheso.huma-num.fr/opentheso/)** réalisée par **[FrantiQ](https://catalogue.frantiq.fr/)** la Fédération et ressources sur l’Antiquité constituée en Groupement de Service* (GDS 3378) du **[CNRS](https://www.cnrs.fr/fr)** le Centre National de la Recherche Scientifique française.
- Le **[Art & Architecture Thesaurus](https://www.getty.edu/research/tools/vocabularies/aat/)** du (Getty Research Institute), un vocabulaire évolutif, qui grandit et change grâce aux contributions des projets Getty et d'autres institutions.
- **[PeriodO](https://client.perio.do/?page=authority-view&backendID=web-https%3A%2F%2Fdata.perio.do%2F&authorityID=p09hq4n)** un répertoire de définitions de périodes historiques, archéologiques et d'histoire de l'art dans le domaine public. Il facilite l'établissement de liens entre les ensembles de données qui définissent les périodes différemment. Il aide également les chercheurs et les étudiants à voir où les définitions des périodes se chevauchent ou divergent.
- La norme géographique **WGS84** et **Geonames** une base de données géographiques modifiable par l'utilisateur.
- Les données sont disponibles dans les entrepôts sous format standard **CSV**, **XML**, **Shape**, etc.

## R : Réutilisables par l'utilisation de standards communs, d’une licence et de la citation des auteurs...

- **ArkeOpen/ArkeoGIS** fait partie de l'écosystème du **consortium Mémoires des archéologues et des sites archéologiques** (**[MASA](https://masa.hypotheses.org/)**) et sert de solution comme d'objet d'études dans le cadre du consortium ayant pour objectif la diffusion et la mise en œuvre des principes FAIR dans la communauté archéologique.
- Une exploration de ces données est possible sur la plateforme web sémantique **[OpenArchaeo](http://openarchaeo.huma-num.fr/explorateur/home)**.
- Les auteurs des bases de données, des calques cartographiques et leurs éditeurs ont choisi différentes licences pour partager, tout en étant cités, les résultats de leurs travaux.
