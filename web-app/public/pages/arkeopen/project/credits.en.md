# Credits

## 1. Design and development of the application

- **[University of Strasbourg](https://www.unistra.fr/)** / **[CNRS UMR 7044 - ARCHIMÈDE](https://archimede.unistra.fr/)** - Loup BERNARD, Lizzie SCHOLTUS
- Lead Developers - **[Nicolas DIMITRIJEVIC - Christophe BEVERAGGI](https://arkeogis.org/contact-2/)**
- Project Management - **[Philippe JULLIEN](https://arkeogis.org/contact-2/)**
- UI/UX Design - **[Pedro VITORINO](https://pedrovitorino.com/)**
- Writing of the platform texts: **ArkeOpen Team**
- Translation of the texts: **[DeepL AI](https://www.deepl.com/translator)**

## 2. Animation and data administration

By a multidisciplinary team composed of archaeologists, historians, paleo-environmentalists and cartographers.

- **[University of Strasbourg](https://www.unistra.fr/)** Loup BERNARD, Jean-Philippe DROUX, Lizzie SCHOLTUS
- **[CNRS UMR 7044 - ARCHIMÈDE](https://archimede.unistra.fr/)** - Archaeology and Ancient History: Mediterranean Europe Loup BERNARD, Jean-Philippe DROUX
- **[Institut für Ur- und Frühgeschichte, Christian-Albrechts-Universität zu Kiel](https://www.ufg.uni-kiel.de/de)** Lizzie SCHOLTUS
- **[PHUN / MISHA](https://www.misha.fr/plateformes/phun) - Maison Inter-universitaire des Sciences de l'Homme - Alsace**
- on the infrastructures of the **[Très Grande Infrastructure de Recherche (TGIR) des humanités numériques Huma-Num](https://www.huma-num.fr/)**

## 3. Maintenance and support

The technical maintenance of the platform and the assistance to authors and users are taken care of by :

- Lead Developer - **Nicolas DIMITRIJEVIC** and **Christophe BEVERAGGI**
- Project Management - **Philippe JULLIEN**
- **[CNRS UMR 7044 - ARCHIMÈDE](https://archimede.unistra.fr/)** Mohammed BENKHALID, Jean-Philippe DROUX
- **[PHUN / MISHA](https://www.misha.fr/plateformes/phun) - Maison Inter-universitaire des Sciences de l'Homme - Alsace**


## 4. Source code and license

The data presented is open and the application allowing access to it is also open. ArkeOpen uses and adapts open tools ([React](https://react.dev/), [Maplibre](https://maplibre.org/), [Mirador](https://projectmirador.org/) ...) and respects the expectations of Free Software.

- The license chosen is version 3 of the GNU General Public License (**[GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.fr.html)**).
- The source code of the **ArkeOpen** application and its documentation are available on the **[Gitlab Huma-num](https://gitlab.huma-num.fr/arkeogis/arkeopen)**.
- **The current online version is 1.0, since 15 October 2024**.
  * You can consult the log of changes made since version 0.1 of ArkeOpen went online in April 2023 on the **[Gitlab Huma-num of the ArkeoGIS-ArkeOpen project](https://gitlab.huma-num.fr/arkeogis/arkeogis/-/issues/?sort=created_date&state=closed&first_page_size=50)**.
