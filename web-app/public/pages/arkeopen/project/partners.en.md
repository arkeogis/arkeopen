# Partners

Creation of the Free Software tools following funding from the **[French Ministry of Higher Education, Research and Innovation](https://www.enseignementsup-recherche.gouv.fr/fr)**, and support to the implementation and coordination of the partners of the **[University of Strasbourg](https://www.unistra.fr/)**. The ArkeOpen Project collaborates with its various partners in the framework of **[open science](https://www.ouvrirlascience.fr/accueil/)** to achieve the objectives of **[F.A.I.R data](https://fr.wikipedia.org/wiki/Fair_data)**.

## F: Findable with rich metadata and a unique identifier.

- Data is accompanied by **description metadata** in **[Extended Dublin Core™](https://fr.wikipedia.org/wiki/Dublin_Core)** format.
- A stable and durable unique identifier (**Digital Object Identifier- DOI**) is assigned to them, for example by **[NAKALA](https://nakala.fr/)**, a product of the **[Huma-Num](https://www.huma-num.fr/)** research infrastructure (CNRS, Aix-Marseille University, Campus Condorcet).

## A: Accessible because they are stored in certified repositories.

- All the data presented are accessible at least in **[the open collection ArkeOpen of the NAKALA data warehouse](https://nakala.fr/search/?q=ArkeOpen)**, or in another open warehouse.
- This **ArkeOpen** collection is harvested by **[ISIDORE](https://isidore.science/)**, a search engine allowing access to digital data in the Humanities and Social Sciences (SHS). Open to all readers and in particular to teachers, researchers, PhD students and students.

## I: Interoperable through the standard concepts used and the formats proposed.

- **ArkeOpen/ArkeoGIS** is aligned with these repositories with unique identifiers to ensure interoperability in different languages.
- The **[PACTOLS Thesaurus](https://pactols.frantiq.fr/opentheso/)** accessible on the **[Opentheso](https://opentheso.huma-num.fr/opentheso/)** platform created by **[FrantiQ](https://catalogue.frantiq.fr/)** the Federation and Resources on Antiquity constituted as a Service Grouping* (GDS 3378) of the **[CNRS](https://www.cnrs.fr/fr)** the French National Centre for Scientific Research.
- The **[Art & Architecture Thesaurus](https://www.getty.edu/research/tools/vocabularies/aat/)** of the Getty Research Institute, an evolving vocabulary, which grows and changes thanks to contributions from Getty projects and other institutions.
- **[PeriodO](https://client.perio.do/?page=authority-view&backendID=web-https%3A%2F%2Fdata.perio.do%2F&authorityID=p09hq4n)** a repository of historical, archaeological, and art historical period definitions in the public domain. It facilitates the linking of datasets that define periods differently. It also helps researchers and students see where period definitions overlap or diverge.
- The geographic standard **WGS84** and **Geonames** a user-modifiable geographic database.
- Data is available in the repositories in standard **CSV**, **XML**, **Shape**, etc.

## R: Reusable through the use of common standards, licensing and citation of authors...

- **ArkeOpen/ArkeoGIS** is part of the ecosystem of the **consortium Memories of Archaeologists and Archaeological Sites** (**[MASA](https://masa.hypotheses.org/)**) and serves as a solution as well as an object of study in the framework of the consortium aiming to disseminate and implement the FAIR principles in the archaeological community.
- An exploration of these data is possible on the semantic web platform **[OpenArchaeo](http://openarchaeo.huma-num.fr/explorateur/home)**.
- The authors of the databases, map layers and their publishers have chosen different licenses to share, while being quoted, the results of their work.
