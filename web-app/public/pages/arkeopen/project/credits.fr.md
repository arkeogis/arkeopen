# Crédits

## 1. Conception et développement de l’application

- **[Université de Strasbourg](https://www.unistra.fr/)** / **[CNRS UMR 7044 - ARCHIMÈDE](https://archimede.unistra.fr/)** - Loup BERNARD, Lizzie SCHOLTUS
- Lead Developers - **[Nicolas DIMITRIJEVIC - Christophe BEVERAGGI](https://arkeogis.org/contact/)**
- Gestion de projet - **[Philippe JULLIEN](https://arkeogis.org/contact/)**
- UI/UX Design - **[Pedro VITORINO](https://pedrovitorino.com/)**
- Rédaction des textes de la plateforme - **Équipe ArkeOpen**
- Traduction des textes - **[DeepL IA](https://www.deepl.com/translator)**

## 2. Animation et administration des données

Par une équipe pluridisciplinaire composée d’archéologues, d’historiens, de paléo-environnementalistes et de cartographes.

- **[Université de Strasbourg](https://www.unistra.fr/)** - Loup BERNARD, Jean-Philippe DROUX, Lizzie SCHOLTUS
- **[CNRS UMR 7044 - ARCHIMÈDE](https://archimede.unistra.fr/) - Archéologie et histoire ancienne : Méditerranée Europe** - Loup BERNARD, Jean-Philippe DROUX
- [**Institut für Ur- und Frühgeschichte, Christian-Albrechts-Universität zu Kiel**](https://www.ufg.uni-kiel.de/de) - Lizzie SCHOLTUS
- **[PHUN / MISHA](https://www.misha.fr/plateformes/phun) - Maison Inter-universitaire des Sciences de l’Homme – Alsace**
- sur les infrastructures de l'**[Infrastructure de Recherche (IR) des humanités numériques Huma-Num](https://www.huma-num.fr/)**

## 3. Maintenance et support

La maintenance technique de la plateforme et l’assistance auprès des auteurs et des utilisateurs sont pris en charge par :

- Lead Developer - **Nicolas DIMITRIJEVIC** et **Christophe BEVERAGGI**
- Gestion de projet - **Philippe JULLIEN**
- **[CNRS UMR 7044 - ARCHIMÈDE](https://archimede.unistra.fr/)** - Mohammed BENKHALID, Jean-Philippe DROUX
- **[PHUN / MISHA](https://www.misha.fr/plateformes/phun) - Maison Inter-universitaire des Sciences de l’Homme – Alsace**


## 4. Code source et licence

Les données présentées étant ouvertes l’application permettant d’y accéder est aussi ouverte. ArkeOpen utilise et adapte des outils ouverts ([React](https://react.dev/), [Maplibre](https://maplibre.org/), [Mirador](https://projectmirador.org/) ...) et respecte les principes du Logiciel Libre.

- La licence retenue est la version 3 de la GNU General Public License **[GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.fr.html)**.
- Le code source de l’application **ArkeOpen** et sa documentation sont accessibles sur le **[Gitlab Huma-num ArkeOpen](https://gitlab.huma-num.fr/arkeogis/arkeopen)**.
- **La version actuelle en ligne est la 1.0, depuis le 15 octobre 2024**.
  * Vous pouvez consulter le journal des changements réalisés depuis la mise en ligne de la version 0.1 d'ArkeOpen en avril 2023 sur le **[Gitlab Huma-num du projet ArkeoGIS-ArkeOpen](https://gitlab.huma-num.fr/arkeogis/arkeogis/-/issues/?sort=created_date&state=closed&first_page_size=50)**.
