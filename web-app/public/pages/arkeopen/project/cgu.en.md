# Terms and Conditions of Use

## A. Administrative structure

- The **ArkeOpen steering committee** is composed of an elected president, the spokespersons of the ArkeOpen support groups and the ArkeOpen database manager.
- The ArkeOpen president coordinates the activity of the support groups and represents ArkeOpen.
- The Chair is elected by ArkeOpen contributors and scientists with a bona fide interest in ArkeOpen, who must register in advance with the database manager.
- Candidates for the position of President of ArkeOpen must be proposed before the elections by one proposer and one seconder. The term of office is four years and can be re-elected.
- The **ArkeOpen support groups** support the database manager. The spokespersons of the support groups are chosen by the groups themselves. Interested scientists are encouraged to join the support groups. The vice-presidency of the Board of Directors is rotated among the spokespersons of the support groups.

## B. Data

- Data are **original or re-used** (cited).
- The content of the data is **the responsibility of the author(s)** associated with the data.
- Data is classified in an unrestricted form, as being for **access under a defined licence**.

## C. Authors

- Provides data in unrestricted form to allow widespread use.
- May request to verify that the data contained in the database is correct.
- In cases where the central database manually enters data, the database must systematically return a copy of the data, as entered into the database, to the author of the data for optional verification by the author.

## D. License

- The data shared by their authors within the framework of the **ArkeOpen** project are protected by Licenses. 
- The conditions of re-use are indicated in the notices of each license.  

## E. Users

- Must, as a courtesy, inform the authors of the data of the use made of their data. 
If the author so desires, shall show them the results of analyses and manuscripts to be published for critical comment. 
- Shall cite, in any publication using data from the database, the contributors' original publications describing their data. 
- Must send reprints of publications that use **ArkeOpen** data to the database centre.
- Authors should be thanked for the use of unpublished data and for any advice they may have provided. 
- Normal ethics apply to co-ownership of publications. The author should be invited to be a co-author if a user makes significant use of an individual author's data, or if an individual author's data form a substantial part of a larger dataset being analysed, or if an author makes a significant contribution to the analysis of the data or the interpretation of the results. This guideline applies to unrestricted data. In the case of synoptic studies that use pollen data from many sites, it may not be practical, possible or reasonable to contact all contributors or to make them co-authors of the publication. It may not even be possible to cite all the publications that have been used. Nevertheless, in all cases, authors must inform ArkeOpen of their use of the data and must include the following text in their acknowledgements: the data was extracted from ArkeOpen (http://arkeopen.org). In no case, the authorship of the data should be attributed to the authors, individually or collectively, without their explicit consent.
- The data are only accessible to non-profit organisations and research.
- Profit-making organisations may only use the data, even for legitimate uses, with the written consent of the ArkeOpen Board of Directors, which will determine or negotiate the payment of any required royalties.
- This website may, without its knowledge, have been linked to other websites through hyperlinks in the resources shared by the authors. The University of Strasbourg declines all responsibility for the information presented on these other sites.
- The user acknowledges that the use of the website http://arkeopen.org is governed by French law.

## F. Sites, respect and access

- **ArkeOpen** shares information about the past and aims to bring together researchers and archaeology enthusiasts. As a reminder, on French territory, it is formally [forbidden to use a metal detector](https://www.legifrance.gouv.fr/codes/id/LEGISCTA000006159957/) or [to carry out excavations or collections](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006074236/LEGISCTA000006129164/#LEGISCTA000006129164) on archaeological sites without authorisation from the competent services. For other territories, contact the local authorities. 
- The sites presented by the authors remain limited in their physical access by the authorisations and conditions of the owners and/or operators.


## G. Audience measurement and confidentiality 

- The **ArkeOpen** platform uses **Matomo**, an open source software for audience measurement (number of visits). It is configured to respect the conditions of exemption of the consent of the Internet user, in application of the recommendations of the **CNIL**, which advises this application. The processing of Matomo logs and the use of this tool is based on article 6 paragraph 1 letter f of the RGPD, which validates that the website publisher has a legitimate interest in analysing the behaviour of users in an anonymous way, in order to maintain and improve its Internet service and thus to obtain anonymous audiences.
- The anonymous information provided by Matomo from the server logs on the use of the website is not passed on to third parties.

Date of last update: 05 July 2024
