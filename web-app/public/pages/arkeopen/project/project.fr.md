# Projet

## 1. La plate-forme ouverte

La plate-forme ouverte **ArkeOpen**, cette interface en libre accès, vous permet de consulter, d’interroger et de croiser les données ouvertes (OpenData) issues de la plateforme de recherche et d'enseignement **[ArkeoGIS](https://arkeogis.org/)**.

- Le volet de gauche (recherche) permet de **changer de chronologie et de filtrer les résultats** visibles sur la carte en fonction de différents critères (périodes, thésaurus, auteurs, etc.).
- Les **informations détaillées** sont présentées dans ce volet de droite (information).

Création des outils sous la supervision de **[Université de Strasbourg](https://www.unistra.fr/)** et sous licence Logiciel Libre suite à un financement du **Ministère français de lʼ[Enseignement supérieur, de la Recherche et de lʼInnovation](https://www.enseignementsup-recherche.gouv.fr/fr)** (2022).

La plateforme est animée et administrée par l’**[Université de Strasbourg](https://www.unistra.fr/)** et la **Maison Inter-universitaire des Sciences de l'Homme – Alsace [(MISHA)](https://www.misha.fr/)** sur les infrastructures de la **IR des humanités numériques [Huma-Num](https://www.huma-num.fr/)**, par **une équipe pluridisciplinaire**.

## 2. Les données partagées

Les données accessibles sont issues de travaux de recherche, de thèses, d’inventaires, d’ouvrages, dans les domaines des sciences humaines et sociales (archéologie, histoire) et environnementales.

Suite à un alignement réalisé sur la plateforme de recherche **[ArkeoGIS](https://arkeogis.org/)** accessible uniquement aux professionnels, ces données sont présentées ici à tous les lecteurs par leurs auteurs et éditeurs. Ce partage est réalisé en suivant **les recommandations de la [science ouverte (open science)](https://www.ouvrirlascience.fr/accueil/)** qui consistent **«à rendre accessible autant que possible et fermé autant que nécessaire»** les résultats de la recherche, issus en majorité des fonds publics.

Dans le cadre du projet **ArkeOpen**, elles sont mises à disposition de tous, en anglais et en français, pour participer à la promotion, la diffusion et la réutilisation sans entrave des données de la recherche en vue de généraliser l’accès aux savoirs.

En suivant les principes du **[F.A.I.R data](https://fr.wikipedia.org/wiki/Fair_data)**, elles sont également respectueuses de la logique des **données ouvertes et liées [LOD](https://fr.wikipedia.org/wiki/Linked_open_data)**

- **F** : **Faciles** à retrouver avec leurs riches métadonnées et un identifiant unique.
- **A** : **Accessibles** car elles sont entreposées dans des dépôts ouverts certifiés.
- **I** : **Interopérables** grâce aux concepts normalisés utilisés et aux formats proposés.
- **R** : **Réutilisables** par l'utilisation de standards communs, d’une licence et de la mention des auteurs.

Les référentiels (chronologies, thésaurus, géographie) mis en œuvre par **ArkeOpen** sont établis en collaboration avec différents partenaires.

## 3. Les données illustrées

Certaines des ressources sont enrichies de documentations ouvertes issues de travaux de recherche, de services d'archives, de bibliothèques numériques ou d’autres projets de valorisation de documents patrimoniaux. À cette fin, **ArkeOpen** utilise l'environnement **[International Image Interoperability Framework (IIIF)](https://iiif.io/)**, un ensemble de normes visant à définir un cadre international d’interopérabilité des images diffusées sur le Web.
