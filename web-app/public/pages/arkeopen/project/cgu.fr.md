# Condition Générales d’Utilisation

## A. Structure administrative

- Le **comité de pilotage d’ArkeOpen** est composé d’un président élu, des porte-parole des groupes de soutien d’ArkeOpen et du gestionnaire de la base de données d’ArkeOpen.
- Le président d’ArkeOpen coordonne l’activité des groupes de soutien et représente ArkeOpen.
- Le président est élu par les contributeurs d’ArkeOpen et les scientifiques ayant un intérêt de bonne foi dans ArkeOpen, qui doivent s’inscrire à l’avance auprès du gestionnaire de la base de données.
- Les candidats au poste de président d’ArkeOpen doivent être proposés avant les élections par un proposant et un second. Le mandat de quatre ans, rééligible, est possible.
- Les **groupes de soutien d’ArkeOpen** soutiennent le gestionnaire de la base de données. Les porte-parole des groupes de soutien sont choisis par les groupes eux-mêmes. Les scientifiques intéressés sont encouragés à rejoindre les groupes de soutien. La vice-présidence du conseil d’administration est assurée à tour de rôle par les porte-parole des groupes de soutien.

## B. Données

- Les données sont **originales ou de ré-emplois** (citées).
- Le contenu des données est sous **la responsabilité du ou des auteur(s)** associé(s) à ces données.
- Les données sont classées sous une forme non restreinte, comme étant **à accès suivant licence** définie.

## C. Auteurs

- Fournit les données sous une forme non restreinte afin de permettre une utilisation généralisée.
- Peut demander à vérifier que les données contenues dans la base de données sont correctes.
- Dans les cas où la base de données centrale saisit manuellement les données, la base de données doit systématiquement renvoyer à l’auteur des données une copie des données, telles qu’elles sont saisies dans la base de données, pour vérification facultative par l’auteur.

## D. Licence

- Les données partagées par leurs auteurs dans le cadre du projet **ArkeOpen** sont protégées par des Licences. 
- Les conditions de ré-emplois sont indiqués dans les notices de chacune des licences.  

## E. Utilisateurs

- Doit, par courtoisie, informer les auteurs des données de l’utilisation qui est faite de leurs données. 
Si l’auteur le souhaite, doit lui montrer les résultats d’analyses et les manuscrits à publier pour qu’il puisse les commenter de manière critique. 
- Doit citer, dans toute publication utilisant des données de la base de données, les publications originales des contributeurs décrivant leurs données. 
- Doit envoyer des réimpressions des publications qui utilisent les données d’**ArkeOpen** au centre de la base de données.
- Les auteurs doivent être remerciés pour l’utilisation de données non publiées et pour tout conseil qu’ils ont pu fournir. 
- L’éthique normale s’applique à la co-propriété des publications. L’auteur doit être invité à être coauteur si un utilisateur fait un usage significatif des données d’un auteur individuel, ou si les données d’un auteur individuel constituent une partie substantielle d’un ensemble de données plus important analysé, ou si un auteur apporte une contribution significative à l’analyse des données ou à l’interprétation des résultats. Cette ligne directrice s’applique aux données non restreintes. Dans le cas d’études à grande échelle qui utilisent par exemple des données sur le pollen provenant de nombreux sites, il peut être ni pratique, ni possible, ni raisonnable de contacter tous les contributeurs ou de faire d’eux les co-auteurs de la publication. Il peut même ne pas être possible de citer toutes les publications qui ont été utilisées. Néanmoins, dans tous les cas, les auteurs doivent informer ArkeOpen de leur utilisation des données et doivent inclure le texte suivant dans leurs remerciements : les données ont été extraites d’ArkeOpen (http://arkeopen.org). En aucun cas, la paternité des données ne doit être attribuée aux auteurs, individuellement ou collectivement, sans leur consentement explicite.
- Les données ne sont accessibles qu’aux organisations à but non lucratif et à la recherche.
- Les organisations à but lucratif ne peuvent utiliser les données, même pour des utilisations légitimes, qu’avec le consentement écrit du conseil d’administration d’ArkeOpen, qui déterminera ou négociera le paiement de toute redevance requise.
- Ce site internet peut, à son insu, avoir été relié à d'autres sites internet par le biais de liens hypermédia présents dans les ressources partagées par les auteurs. L'université de Strasbourg décline toute responsabilité pour les informations présentées sur ces autres sites.
- L'utilisateur reconnaît que l'utilisation du site internet http://arkeopen.org est régie par le droit français.

## F. Sites, respect et accès

- **ArkeOpen** partage des informations sur le passé et a vocation à mettre en relation des chercheurs et des passionnés d'archéologie. Pour rappel, sur le territoire français, il est formellement [interdit d'utiliser un détecteur de métaux](https://www.legifrance.gouv.fr/codes/id/LEGISCTA000006159957/) ou [de procéder à des fouilles ou des ramassages](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006074236/LEGISCTA000006129164/#LEGISCTA000006129164) sur des sites archéologiques sans autorisation des services compétents. Pour les autres territoires il faut prendre contact avec les autorités locales. 
- Les sites présentés par les auteurs restent limités dans leur accès physique par les autorisations et conditions des propriétaires et/ou exploitants.

## G. Mesure d'audience et confidentialité 

- La plateforme **ArkeOpen** utilise **Matomo**, logiciel Open Source de mesure d'audience (nombre de visites). Il est paramétré pour respecter les conditions d’exemption du consentement de l’internaute, en application des recommandations de la **CNIL**, qui conseille cet application. Le traitement des logs Matomo et l'utilisation de cet outil se font sur la base de l'article 6 alinéa 1 lettre f du RGPD, qui valide que l'éditeur du site Internet a un intérêt légitime à analyser de manière anonyme le comportement des utilisateurs, afin de maintenir et d'améliorer son service Internet et ainsi d'obtenir des audiences anonymes.
- Les informations anonymes fournies par Matomo à partir des logs du serveur sur l'utilisation du site Internet ne sont pas transmises à des tiers.

Date de la dernière mise à jour : 05 juillet 2024