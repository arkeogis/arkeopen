# Legal Notice

**Site URL**: **http://arkeopen.org**

- **Site owner**: Loup BERNARD
- **Site owner's status**: Senior Lecturer, Faculty of Historical Sciences, National Antiquities.
University of Strasbourg
Full member CNRS UMR 7044 ArcHiMedE
- **Siret number**: 130 005 457 000 10
- **Postal address of the site owner**: Maison Interuniversitaire des Sciences de l'Homme - Alsace
5, allée du Général Rouvillois
CS 50008
67083 Strasbourg cedex
- **Site creator**: Nicolas DIMITRIJEVIC, Philippe JULLIEN, Pedro VITORINO
- **Site creator URL**:	https://unistra.fr/
- **Name of the publication manager**: Loup BERNARD
- **Contact of the publication manager**: loup.bernard(@)unistra.fr
- **Name of the Webmaster**: Philippe JULLIEN
- **Webmaster contact**: admin(@)arkeogis.org
- **Image rights**: Photos of objects and sites © indicated on each by their authors, for other illustrations © arkeopen.org
- **Site host**: The TGIR Huma-Num / TGIR service grid http://www.huma-num.fr/
- **Postal address of the host**: TGIR Huma-Num - UMS 3598 - 4, rue Lhomond - F - 75005 Paris
- **Data storage**: The data are stored on the TGIR Huma-Num service grid.
- **Data archiving**: Data are archived in the framework of the Huma-Num/CINES long-term archiving service.

**CNIL**: The President of the University of Strasbourg has decided to set up the ArkeOpen geographic information system allowing researchers to deposit and disseminate research data to the general public.
The information collected is subject to computer processing for the management of research data deposited on this GIS. The recipients of the data are: the service that manages this GIS and the general public. In accordance with the amended "Informatique et Libertés" law of 6 January 1978, you have the right to access and rectify information concerning you, which you can exercise by contacting Mr Loup Bernard loup.bernard(@)unistra.fr
You may also, for legitimate reasons, object to the processing of data concerning you.
