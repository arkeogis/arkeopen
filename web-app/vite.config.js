import { defineConfig, loadEnv } from "vite";
import react from "@vitejs/plugin-react-swc";

export default defineConfig(({ mode, port }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };
  return {
    plugins: [react()],
    resolve: {
      mainFields: [],
    },
    define: {
      __ARKEOGIS__: mode === "arkeogis",
      __ARKEOPEN__: mode === "arkeopen",
    },
    build: {
      outDir: `dist/${mode}`, // Compiler chaque mode dans un dossier distinct
    },
    server: {
      port: parseInt(process.env.VITE_PORT),
      strictPort: true, // Ne change pas de port si le port est occupé
    },
    preview: {
      port: parseInt(process.env.VITE_PORT),
      strictPort: true, // Ne change pas de port si le port est occupé
    },
    hmr: {
      //host: "ws://localhost:40110/socket.io/",
      //strictPort: true,
      //clientPort: 443, //parseInt(process.env.VITE_PORT),
      //port: parseInt(process.env.VITE_PORT),
    },
    css: {
      preprocessorOptions: {
        scss: {
          quietDeps: true, // Désactive les avertissements liés aux dépendances
          logLevel: "silent", // Réduit les logs
        },
      },
    },
    test: {
      globals: true, // Active des variables globales pour éviter d'importer `describe`, `it`, etc.
      environment: "jsdom", // Simule un environnement de navigateur
      setupFiles: "./setupTests.js", // Fichier de configuration des tests
    },
  };
});
