#!/bin/bash

if [[ $(basename $PWD) != "web-app" ]]; then
    echo "This script should be run from the web-app directory"
    exit 1
fi

[ -f env/.env.arkeogis.development ] && . .env/env.arkeogis.development
[ -f env/.env.arkeogis.development.local ] && . env/.env.arkeogis.development.local

[ -f env/.env.arkeogis.production ] && . env/.env.arkeogis.production
[ -f env/.env.arkeogis.production.local ] && . env/.env.arkeogis.production.local

export REACT_APP_GRAPHQL_URI

npm run build

echo "For production, it should be served with apache/nginx."
echo "however, to serve it for resting, run : ./node_modules/.bin/serve -p $PORT -s build"
