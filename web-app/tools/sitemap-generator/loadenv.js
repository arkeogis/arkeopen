// loadEnv.js
import dotenvx from "@dotenvx/dotenvx";
import fs from "node:fs";
import path from "node:path";

const envFile = path.join(process.cwd(), "../../.env");
let errorFound = false;
if (!fs.existsSync(envFile)) {
  console.error(`Env file ${envFile} not found.`);
  process.exit(1);
}

dotenvx.config({ path: envFile });

for (const envVar of ["REACT_APP_GRAPHQL_URI"]) {
  if (!process.env[envVar]) {
    console.error(
      `Environment variable ${envVar} not found. Please fill it in ${envFile}`
    );
    process.exit(1);
  }
}
