# Arkeopen sitemap generator

### General purpose

- Generate a sitemap.xml file into the public directory listing all pages, databases and chronologies available.
- Add an entry in the robots.txt file located in the public directory to inform crawlers to check the sitemap.

### Usage
run the ./sitemap-generator.js script

It should output the number of chronologie, database and shapefile links added to the sitemap.





