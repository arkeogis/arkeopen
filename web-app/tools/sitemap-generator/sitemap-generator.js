#!/usr/bin/env node
import "./loadenv.js";
import fs from "node:fs";
import path from "node:path";
import { asyncPipe } from "./utils.js";
import xml from "xml";
import pkg from "@apollo/client";
const { gql, ApolloClient, InMemoryCache, HttpLink } = pkg;

const URL_SEPARATOR = "%C2%A7"; // "§";

const SITE_URL = "https://arkeopen.org";

const PUBLIC_DIRECTORY = path.join(
  path.dirname(process.argv[1]),
  "../../build"
);

const PAGES_DIRECTORY = path.join(PUBLIC_DIRECTORY, "pages");

const DEFAULT_LANG_FOR_FILES = "fr";

const ARKEO_URLS = {
  project: `${SITE_URL}?p=right&rp=project${URL_SEPARATOR}__PAGE__`,
  help: `${SITE_URL}?p=right&rp=help${URL_SEPARATOR}__PAGE__`,
  database: `${SITE_URL}?p=right&rp=db${URL_SEPARATOR}__ID__`,
  chronology: `${SITE_URL}?p=right&rp=chronology${URL_SEPARATOR}__ID__`,
  shapefile: `${SITE_URL}?p=right&rp=shp${URL_SEPARATOR}__ID__&shps=__ID__`,
};

const apolloClient = new ApolloClient({
  cache: new InMemoryCache({
    resultCacheMaxSize: Math.pow(2, 17),
  }),
  link: new HttpLink({
    uri: process.env.REACT_APP_GRAPHQL_URI,
    fetchOptions: {
      credentials: "include",
    },
  }),
});

/**
 * Returns the appropriate link based on the item type.
 * If the item type is 'project' or 'help', replaces '__PAGE__' in the link with the item's page.
 * If the item type is 'database', 'chronology', or 'shapefile', replaces '__ID__' in the link with the item's id.
 * If no matching link is found for the item type, logs an error and returns null.
 *
 * @param {Object} item - The item object containing type, page, and id properties.
 * @returns {string|null} The generated link based on the item type, or null if no matching link is found.
 */
const getLink = (item) => {
  const href = ARKEO_URLS[item.type];
  if (!href) {
    console.error("No link matches this type");
    return null;
  }
  return item.type === "project" || item.type === "help"
    ? href.replaceAll("__PAGE__", item.page)
    : href.replaceAll("__ID__", item.id);
};

/**
 * Performs a GraphQL query using the configured Apollo client.
 *
 * @param {Object} query - The GraphQL query object to be executed.
 * @returns {Object} The data returned from the GraphQL query.
 * @throws {Error} If an error occurs during the query execution.
 */
const doGraphqlQuery = async (query) => {
  try {
    const { data } = await apolloClient.query({ query });
    return data;
  } catch (error) {
    console.error(error.message);
  }
};

/**
 * Retrieves shapefile items from the GraphQL server and generates sitemap entries based on them.
 * @param {Array} sitemapItems - The existing sitemap items to be included in the generated sitemap.
 * @returns {Array} - The updated sitemap with the shapefile items included.
 */
const getSitemapEntriesFromShapefiles = async (sitemapItems) => {
  const resultItems = [];
  // where: { published: { _eq: true } }
  const query = gql`
    query GetShapefiles {
      items: ako_shapefile(order_by: { id: asc }) {
        id
        updated_at
      }
    }
  `;
  const result = await doGraphqlQuery(query);
  if (!result) return null;
  console.log(`Nombre de shapefiles: ${result.items.length}`);
  for (const items of result.items) {
    resultItems.push({
      updated_at: items.updated_at,
      loc: getLink({ type: "shapefile", id: items.id }),
    });
  }
  return [...sitemapItems, ...resultItems];
};

/**
 * Retrieves databases items from the GraphQL server and generates sitemap entries based on them.
 * @param {Array} sitemapItems - The existing sitemap items to be included in the generated sitemap.
 * @returns {Array} - The updated sitemap with the shapefile items included.
 */
const getSitemapEntriesFromDatabases = async (sitemapItems) => {
  const resultItems = [];
  const query = gql`
    query GetDatabases {
      items: ako_database(where: { published: { _eq: true } }) {
        id
        updated_at
      }
    }
  `;
  const result = await doGraphqlQuery(query);
  if (!result) {
    console.warn("No entries found for databases.");
    return null;
  }
  console.log(`Nombre de bases: ${result.items.length}`);
  for (const items of result.items) {
    resultItems.push({
      updated_at: items.updated_at,
      loc: getLink({ type: "database", id: items.id }),
    });
  }
  return [...sitemapItems, ...resultItems];
};

/**
 * Retrieves chronologies items from the GraphQL server and generates sitemap entries based on them.
 * @param {Array} sitemapItems - The existing sitemap items to be included in the generated sitemap.
 * @returns {Array} - The updated sitemap with the shapefile items included.
 */
const getSitemapEntriesFromChronologies = async (sitemapItems) => {
  const resultItems = [];
  const query = gql`
    query GetChronologies {
      items: ako_chronology(
        order_by: { start_date: asc }
        where: { _and: { parent_id: { _eq: 0 }, id: { _neq: 0 } } }
      ) {
        id
        updated_at
      }
    }
  `;
  const result = await doGraphqlQuery(query);
  console.log(`Nombre de chronologies: ${result.items.length}`);
  if (!result) {
    console.warn("No entries found for chronologies.");
    return null;
  }
  for (const items of result.items) {
    resultItems.push({
      updated_at: items.updated_at,
      loc: getLink({ type: "chronology", id: items.id }),
    });
  }
  return [...sitemapItems, ...resultItems];
};

/**
 * Recursively generates sitemap entries URLs from pages in a given directory.
 *
 * @param {string} directory - The directory path to read the content from.
 * @returns {Array} An array of sitemap entries with 'updated_at' and 'loc' properties.
 */
const getSitemapEntriesUrlsFromPages = (directory) => (sitemapItems) => {
  // Read the content of the directory
  const filesOrDirectory = fs.readdirSync(directory);
  let newItems = [];

  // Loop through each item in the directory
  for (const item of filesOrDirectory) {
    const fullPath = path.join(directory, item);
    const stat = fs.statSync(fullPath);

    // If the item is a directory, recurse into it
    if (stat.isDirectory()) {
      newItems = [
        ...sitemapItems,
        ...getSitemapEntriesUrlsFromPages(fullPath)(newItems),
      ];
    } else if (stat.isFile() && path.extname(item) === ".md") {
      // Extract ISO code from filename (assumes filename contains "-XX" where XX is the ISO code)
      const isoCodeMatch = fullPath.match(
        new RegExp(`pages/(.*)/(.*).${DEFAULT_LANG_FOR_FILES}.md$`)
      );
      if (!isoCodeMatch) {
        continue;
      }
      const type = isoCodeMatch[1];
      const page = isoCodeMatch[2];
      if (type && page) {
        newItems.push({
          updated_at: stat.mtime.toISOString(),
          loc: getLink({ type, page }),
        });
      }
    }
  }

  return [...sitemapItems, ...newItems];
};

/**
 * Writes a sitemap XML file to the specified output directory based on the provided URL collection.
 *
 * @param {string} outputDirectory - The directory where the sitemap file will be saved.
 * @returns {Function} A function that takes a URL collection and generates the sitemap XML file.
 */
const writeSitemap = (outputDirectory) => async (urlCollection) => {
  // Construct the XML object
  const xmlObject = {
    urlset: [
      {
        _attr: {
          xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9",
        },
      },
      ...urlCollection.map((url) => {
        return {
          url: [{ loc: url.loc }, { lastmod: url.updated_at }],
        };
      }),
    ],
  };

  // Generate the XML markup
  const xmlString = xml(xmlObject, {
    declaration: { version: "1.0", encoding: "UTF-8" },
  });

  // Write the file to disk
  try {
    fs.writeFileSync(`${outputDirectory}/sitemap.xml`, xmlString);
  } catch (e) {
    console.error(`Unable to write ${outputDirectory}/sitemap.xml`);
    return null;
  }
};

/**
 * Updates the robots.txt file with the sitemap URL if not already present.
 *
 * @param {string} outputDirectory - The directory where the robots.txt file is located.
 * @param {string} siteUrl - The base URL of the site.
 * @returns {Promise<null>} A Promise that resolves to null if the update is successful, otherwise an error is logged.
 */
const updateRobotsTxt = (outputDirectory, siteUrl) => () => {
  let fileContent = [];

  // Check if robots.txt exists
  const rotbotsFilePath = path.join(outputDirectory, "robots.txt");
  const sitemapFileUrl = `${siteUrl}/sitemap.xml`;
  if (fs.existsSync(rotbotsFilePath)) {
    fileContent = fs.readFileSync(rotbotsFilePath, "utf-8").split("\n");
  }

  let sitemapLineFound = false;

  // Search for the Sitemap line
  fileContent = fileContent.map((line) => {
    if (line.startsWith("Sitemap:")) {
      sitemapLineFound = true;
      return `Sitemap: ${sitemapFileUrl}`;
    }
    return line;
  });

  // If no Sitemap line was found, add it
  if (!sitemapLineFound) {
    fileContent.push(`Sitemap: ${sitemapFileUrl}`);
  }

  // Write back to robots.txt (create or update)
  try {
    fs.writeFileSync(rotbotsFilePath, fileContent.join("\n"));
  } catch (e) {
    console.error(`Unable to write ${rotbotsFilePath}`);
    return null;
  }
};

await asyncPipe(
  getSitemapEntriesUrlsFromPages(PAGES_DIRECTORY),
  getSitemapEntriesFromChronologies,
  getSitemapEntriesFromShapefiles,
  getSitemapEntriesFromDatabases,
  writeSitemap(PUBLIC_DIRECTORY),
  updateRobotsTxt(PUBLIC_DIRECTORY, SITE_URL)
)([]);
