/**
 * Applies multiple functions in sequence to a value.
 *
 * @param {...Function} fns - Functions to be applied in sequence.
 * @param {*} value - The initial value to be processed by the functions.
 * @returns {*} - The final result after applying all functions in sequence.
 * @throws {Error} - If any of the passed arguments is not a function.
 */
export const pipe =
  (...fns) =>
  (value) => {
    for (const fn of fns) {
      if (typeof fn !== "function") {
        throw Error("pipe(): passed argument is not a function");
      }
      const result = fn(value);
      if (result === null) return null;
      value = result;
    }
    return value;
  };

/**
 * Applies multiple asynchronous functions in sequence to a value.
 *
 * @param {...Function} fns - Functions to be applied in sequence.
 * @param {*} value - The initial value to be processed by the functions.
 * @returns {Promise<*>} - A promise that resolves to the final result after applying all functions in sequence.
 * @throws {Error} - If any of the passed arguments is not a function.
 */
export const asyncPipe =
  (...fns) =>
  async (value) => {
    for (const fn of fns) {
      if (typeof fn !== "function") {
        throw Error("asyncPipe(): passed argument is not a function");
      }
      const result = await fn(value);
      if (result === null) return null;
      value = result;
    }
    return value;
  };
