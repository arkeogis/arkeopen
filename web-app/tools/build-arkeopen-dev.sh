#!/bin/bash

if [[ $(basename $PWD) != "web-app" ]]; then
    echo "This script should be run from the web-app directory"
    exit 1
fi

[ -f env/.env.arkeopen.development ] && . env/.env.arkeopen.development
[ -f env/.env.arkeopen.development.local ] && . env/.env.arkeopen.development.local

export REACT_APP_GRAPHQL_URI

npm run build

echo "to serve it, run : ./node_modules/.bin/serve -p $PORT -s build"
