# SQL Schema, hasura, graphql, postgresql

## SQL Schema

Schemas are here : https://sqldesigner.dev.arkeogis.org/
This is the beggining of all ! We use sqldesigner to design the database. Then we have scripts for converting the sqldesigner schema to arkeogis and arkeopen.
Actually, arkeopen use exactly the same schema of arkeogis.

If it is not done already, you should have a look to this documentaiton : [Database / SQLDesigner - Manual](sqldesigner.md) It also explain how to **upgrade the sql shematics on web-admin**, which is **required** if you update the sql design.

## Updating the Schema

Here is the process :

1. update the schema on sqldesigner
2. export the xml schema to a file
3. copy this file in arkeopen web-admin source folder `web-admin/arkeogis.xml`
4. copy this file in arkeogis source folder : `src/server/src/gitlab.huma-num.fr/arkeogis/arkeogis-server/db-schema.xml`
5. For updating arkeogis go model, run :

```shell
cd arkeogis/dist
./tools/xmltogo > ../src/server/src/gitlab.huma-num.fr/arkeogis/arkeogis-server/model/model.go
```

6. Write migrations scripts (SQL), we'll use hasura migrations for this, and for both arkeopen and arkeogis. See next chapter for this.

## Create a new migration

We use hasura for managing migrations of databases
For theses examples, we will add two columns to chronology table : id_ark_periodo and id_ark_pactols.

1. Create the migration (we are doing the one for arkeogis, we will copy it for arkeopen later)

```shell
cd arkeopen/server/hasura
./hasura migrate create 4.7.1.0_periodo_and_pactols --database-name arkeogis
```

2. A folder was created : `./migrations/arkeogis/1667223631186_4.7.1.0_periodo_and_pactols`. There are two empty files in this folder :

- up.sql # this file is for upgrade
- down.sql # this file is for downgrade

3. Update theses files like this :

- up.sql

```sql
alter table chronology add column id_ark_periodo character varying(255) not null default '';
alter table chronology add column id_ark_pactols character varying(255) not null default '';
```

- down.sql

```sql
alter table chronology drop column id_ark_periodo;
alter table chronology drop column id_ark_pactols;
```

4. Apply the migration

```shell
hasura migrate apply --database-name arkeogis
```

5. Check the migration

```shell
./hasura migrate status
? Select a database to use All (all available databases)

Database: arkeogis
VERSION        NAME                 SOURCE STATUS  DATABASE STATUS
1667223468212  init                 Present        Present
1667223631186  periodo_and_pactols  Present        Present

Database: arkeopen
VERSION        NAME                 SOURCE STATUS  DATABASE STATUS
1667223389020  init                 Present        Present
```

6. Update the hasura metadata :

- For this, we go on the hasura web admin interface, DATA section, search for the `chronology` table (in `arkeogis` database) and in the "Modify" tab, click on "Untrack Table".
- Go to "public" of `arkeogis`, you should see `chronology` table listed in "`Untracked tables or views`", click on the Track button.
- Next, you have to track all foreign-key.

7. export metadata

```shell
./hasura metadata export
```

You can now check using git for differences

```shell
git diff
```

8. Commit the diffs

```shell
git commit ...
```

9. Now that everything is done, you can copy the folder `./migrations/arkeogis/1667223631186_periodo_and_pactols` to `./migrations/arkeopen/1667223631186_periodo_and_pactols` and START AGAIN with arkeopen !
