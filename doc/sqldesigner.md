# Database / SQLDesigner - Manual

Arkeogis uses SQLDesigner for its SQL schema.

Here is some littles explanations of how to use it, and next, how to integrate it in ArkeOpen / ArkeoGIS v5.

## Open a schema

From the SQLDesigner web interface:

- Click on the "Save / Load" button in the top right menu
- In the newly opened window, click on the "LIST ITEMS" button
- A list will appear in the "Input / Output" section with, for example:
  - ArkeoGIS_v4.6
  - default
  - test
- Select the latest version of Arkeogis, for example "ArkeoGIS_v4.7.1" (text selection) and press ctrl-c (copy)
- Click on the "LOAD" button
- In the newly opened window, paste "ArkeoGIS_v4.7.1", then click on the "OK" button
- That's it, the schema should be loaded

# Save a schema

- Click on the "Save / Load" button in the top right menu
- In the newly opened window, click on the "SAVE" button
- In the newly opened window, paste "ArkeoGIS_v4.7.01" (increment the version number), then click on the "OK" button

# Integration into ArkeOpen / Arkeogis v5

- Click on the "Save / Load" button in the top right menu
- In the newly opened window, click on the "SAVE XML" button
- Copy the generated XML text in the "Input / Output" section
- Paste the entire copied content into the file `web-admin/arkeogis.xml`
- Edit the file `web-admin/src/lib/db/index.js` at the beggining :

```js
const xml = raw("../../../arkeogis.xml"); // 42 <= when changing arkeogis.xml file, increment this comment number, to invalidate cache
```

and increment the commented number, here is 42, you should change it to 43. This will update the cache and load again arkeogis.xml

- In some cases, depend of what you've done on the sql schematic, you'll may need to update the files :
  - `web-admin/src/lib/queryToMutation.js`
  - `web-admin/src/lib/queries/*.js`
- and less probably in :
  - `web-admin/src/lib/transfertCharac.js`
  - `web-admin/src/lib/transfertChrono.js`
  - `web-admin/src/lib/transfertDatabase.js`
  - `web-admin/src/lib/transfertShapefile.js`

Then you should try the web-admin app, transfert some stuffs, to check that everything work fine.
