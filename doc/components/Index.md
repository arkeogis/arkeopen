**Documentation: Using the `Index` Component**

The `Index` component is a generic table component that can be used to display tabular data. It is designed to be customizable and flexible, making it easy to use in various contexts.

This component is already contained within a `SubMainPanel`, which is designed to display over the main map by hiding the main panel and left sliding panel. The `SubMainPanel` will not hide the right sliding panel, allowing users to access necessary tools and features.

### Props

The `Index` component accepts several props that can be customized to suit your needs:

*   **data**: The array of data to be displayed in the table.
*   **columns**: An array of column definitions for the table.
*   **rowKeyField**: The field to use as the key for rows (default: "id").
*   **format**: An optional function to format cell values.
*   **childComponents**: Optional custom components for cells.
*   **customCellComponent**: An optional function to render custom cells.
*   **title**: The main title for the component.
*   **className**: Additional CSS class name for styling.
*   **textSearch**: A boolean indicating whether the text search component should be rendered (default: false).
*   **buttons**: Optional additional buttons that will be shown on the top right.

### Using the `Index` Component

To use the `Index` component, you need to import it and pass the required props. Here's an example:

```jsx
import React from 'react';
import Index from './Index';

const data = [
    { id: 1, name: 'John Doe', age: 30 },
    { id: 2, name: 'Jane Doe', age: 25 }
];

const columns = [
    {
        key: 'id',
        title: 'ID',
        dataType: DataType.Number
    },
    {
        key: 'name',
        title: 'Name',
        dataType: DataType.String
    },
    {
        key: 'age',
        title: 'Age',
        dataType: DataType.Number
    }
];

const IndexExample = () => {
    return (
        <Index
            data={data}
            columns={columns}
            rowKeyField="id"
            format={(column, value) => {
                if (column.dataType === DataType.Date) {
                    return new Date(value).toLocaleDateString();
                }
            }}
            className="custom-class"
            textSearch={true}
            title="Example Table"
        />
    );
};

export default IndexExample;
```

In this example, we're displaying a simple table with three columns and two rows. We're also using the `format` prop to format the date values in the "Age" column.

### Customization

The `Index` component is highly customizable. You can use the following props to customize its behavior:

*   **childComponents**: Pass an object with custom components for cells.
*   **customCellComponent**: Pass a function to render custom cells.
*   **format**: Pass a function to format cell values.

You can also use CSS classes to style the table. The `className` prop accepts a string value, which will be appended to the default class names.

### Text Search

The `Index` component includes a text search feature that can be enabled by setting the `textSearch` prop to `true`. This will render an input field at the top of the table, allowing users to filter data based on their searches.

```jsx
<Index
    data={data}
    columns={columns}
    rowKeyField="id"
    textSearch={true}
    title="Example Table with Text Search"
/>
```

This is a basic example of how you can use the `Index` component. You can customize it further by passing additional props and using CSS classes to style the table.

### Example using custom buttons in header
```jsx
import React from 'react';
import Index from './Index';

const data = [
    { id: 1, name: 'John Doe', age: 30 },
    { id: 2, name: 'Jane Doe', age: 25 }
];

const columns = [
    {
        key: 'id',
        title: 'ID',
        dataType: DataType.Number
    },
    {
        key: 'name',
        title: 'Name',
        dataType: DataType.String
    },
    {
        key: 'age',
        title: 'Age',
        dataType: DataType.Number
    }
];

const IndexExample = () => {
    return (
        <Index
            data={data}
            columns={columns}
            rowKeyField="id"
            format={(column, value) => {
                if (column.dataType === DataType.Date) {
                    return new Date(value).toLocaleDateString();
                }
            }}
            className="custom-class"
            textSearch={true}
            title="Example Table"
            buttons={
                <button type="button" className="btn btn-primary" onClick={(e) => console.log('Export button clicked!')}>
                    Export
                </button>
            }
        />
    );
};

export default IndexExample;
```

### Example Use Cases

The `Index` component can be used in various contexts, such as:

*   Displaying lists of data in a table format
*   Creating interactive tables with filtering and sorting features
*   Building custom data visualizations using tables and charts

You can use the `Index` component as a starting point for building your own custom components or as a reusable piece of code to display tabular data in your application.

