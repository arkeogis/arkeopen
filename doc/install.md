# Arkeopen Install from Scratch

## Requirements

The arkeopen project was designed to run under unix (linux) server.
It will need :
 - docker installed and ready
 - a nginx proxy server (or anything else that you'll be able ton configure like required) in front
 - ArkeoGIS is required for admin / transfert of datas to ArkeOpen

This manual does not cover how to install linux, ArkeoGIS, softwares like docker, nginx, etc.

## Install from scratch

### Create a user, for exemple, `arkeopen`, and add it to docker group

```bash
adduser arkeopen
vi /etc/group # edit the docker line to add arkeopen
```

### Clone the repository

```bash
git clone https://gitlab.huma-num.fr/arkeogis/arkeopen.git
```

## Docker (hasura/graphql and postgresql)

### You'll need to define wich ports we will use for them. Here, we'll use :

 - 40022 : arkeopen-graphql
 - 40023 : arkeopen-postgresql

(note that all ports are described here: https://gitlab.huma-num.fr/arkeogis/vm-arkeo/-/blob/main/ports.md)

### Server / docker config

Go to `arkeopen/server` and copy/edit env.production

```bash
su - arkeopen
cd arkeopen/server
cp env.production .env   # (or use env.development)

# edit the .env file
vi .env
```

You'll have something like this :
```bash
COMPOSE_PROJECT_NAME="arkeopen"

HASURA_GRAPHQL_ADMIN_SECRET="plop"
ARKEOPEN_POSTGRESQL_PORT="40023"
ARKEOPEN_GRAPHQL_PORT="40022"
ARKEOPEN_DATABASE_VOLUME="/home/arkeopen/app-data/docker-volumes/arkeopen-postgres-data"

# used by hasura-cli
HASURA_GRAPHQL_ENDPOINT="http://localhost:${ARKEOPEN_GRAPHQL_PORT}/"
```

- The value for COMPOSE_PROJECT_NAME must be unique on you're server.
- Choose a different password for HASURA_GRAPHQL_ADMIN_SECRET, it should be long and complex.
- Choose differents ports for ARKEOPEN_POSTGRESQL_PORT and ARKEOPEN_GRAPHQL_PORT, and update https://gitlab.huma-num.fr/arkeogis/vm-arkeo/-/blob/main/ports.md with them
- Set the ARKEOPEN_DATABASE_VOLUME to a local directory where the databases files will be stored

### Web-admin config

Go to `arkeopen/web-admin` and copy/edit env.production (or env.development)
```bash
su - arkeopen
cd arkeopen/web-admin
cp env.production .env   # (or env.development, note that instead of copying it to .env, you can copy it to .env.development.local)

# edit the .env file
vi .env
```
You'll have something like this :
```bash
REACT_APP_GRAPHQL_URI="https://graphql.arkeopen.org/v1/graphql"
REACT_APP_GRAPHQL_PASS="pass"
```

- Put the same password in REACT_APP_GRAPHQL_PASS that the one choosen in the .env of the server (HASURA_GRAPHQL_ADMIN_SECRET)

### Web-app config

Go to `arkeopen/web-app` and copy/edit env.production
```bash
su - arkeopen
cd arkeopen/web-admin
cp env.production .env

# edit the .env.production file
vi .env
```

You'll have something like this :
```bash
REACT_APP_GRAPHQL_URI="https://graphql.arkeopen.org/v1/graphql"
```

### Build the web-admin

```bash
su - arkeopen
cd arkeopen/web-admin

# install the dependencies
npm install

# build the prod
./build-prod.sh # (or ./build-dev.sh)
```

### Build the web-app

```bash
su - arkeopen
cd arkeopen/web-app

# install the dependencies
npm install

# build the prod
./build-prod.sh # (or ./build-dev.sh)
```

### create the nginx domains, and certbotssl

Here, we'll do it for arkeopen.org, www.arkeopen.org, graphql.arkeopen.org, and admin.arkeopen.org. Based on the documentation here : https://gitlab.huma-num.fr/arkeogis/vm-arkeo/-/blob/main/web-reverse-proxy-and-ssl.md

(as root)
```bash
certbot --nginx -d arkeopen.org -d www.arkeopen.org -d graphql.arkeopen.org -d admin.arkeopen.org
```

All the domains will be created in the file `/etc/nginx/sites-enabled/default`.
We have to remove them from here, and put it in an another file, like `/etc/nginx/sites-available/arkeopen.org`
Use your vi/emacs/ed/... skills for this.

Certbot has generated only one server configuration for the 3 domains, but we'll have to move admin away in a separate server configuration (we'll use the same file here)

Enable the configuration

```bash
cd /etc/nginx/sites-enabled
ln -s ../sites-available/arkeopen.org .
/etc/init.d/nginx reload
```

### hasura metadata configuration
```shell
cd server/hasura
[ ! -f metadata/databases/databases.yaml ] && cp metadata/databases/databases.dist.yaml metadata/databases/databases.yaml

# edit databases.yaml to set the PostgreSQL for arkeogis and arkeopen
vi metadata/databases/databases.yaml
cd -
```

## Server initialization

### starting dockers

```bash
su - arkeopen
cd server

# Create a local network for docker. Only once.
docker network create arkeo

# ensure that hasura/postgresql docker is running
docker-compose up -d
```

### create the arkeopen database

```bash
# connect to sql
docker exec -it arkeopen-postgres psql -U postgres # <== Use the name of your docker arkeopen postgres instance !

# create arkeopen role
postgres=# create role arkeopen with login password 'chooseAPasswordHere';
postgres=# create database arkeopen owner arkeopen;
postgres=# \c arkeopen
arkeopen=# create extension postgis;
arkeopen=# create extension ltree;
(exit with ctrl-d)
```

### create the arkeogis database

```bash
# connect to sql
docker exec -it arkeopen-postgres psql -U postgres # <== Use the name of your docker arkeopen postgres instance !

# create arkeogis role
postgres=# create role arkeogis with login password 'chooseAPasswordHere';
postgres=# create database arkeogis owner arkeogis;
postgres=# \c arkeogis
arkeogis=# create extension postgis;
arkeogis=# create extension ltree;
(exit with ctrl-d)
```

### Do the ArkeOpen SQL migrations
```bash
./hasura migrate apply # select only arkeopen when prompted
```

### Do the ArkeoGIS SQL migrations
```bash
./hasura migrate apply # select only arkeogis when prompted
```

### apply metadata

```bash
cd server/hasura
./hasura metadata apply
```

### Add a prefix the your Hasura root fields
Following the documentation : https://hasura.io/blog/naming-convention-for-graphql-schema-snake-case-vs-camel-case

Using the Hasura web console, go to DATA -> Data Manager -> arkeopen -> Edit
Go to Connection Details -> GraphQL Customization -> Root fields -> Prefix ans set "ako_" as prefix.
Click on "Update connection".

### import arkeopen data (optional)

# Make a dump of an existing database. We get only the data, not the whole db structure.

```bash
docker exec -i arkeopen-prod-postgres pg_dump -Upostgres --data-only arkeopen > /tmp/arkeopendev-postgres-dataonly-20240725.sql # <== Use a valid postgres instance. 
```

for arkeogis:
```bash
docker exec -i arkeo4prod-postgres pg_dump -Upostgres --data-only arkeogis > /tmp/arkeogis-dataonly-20270208.sql
```


If you see some warnings about circular foreign-key constraints, add this line inside your dump:
set session_replication_role = replica;

# Import the dump in arkeopen:

```bash
docker exec -i arkeopen-postgres psql -Upostgres arkeopen < /tmp/arkeopendev-postgres-dataonly-20240725.sql # <== Use the name of your docker arkeopen postgres instance !
```

# Import the dump in arkeogis:
```bash
docker exec -i arkeopen-postgres psql -Upostgres arkeogis < /tmp/arkeogis-dataonly-20270208.sql # <== Use the name of your docker arkeopen postgres instance !
```
