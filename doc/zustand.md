# Zustand

A small, fast and scalable bearbones state-management solution using simplified flux principles. Has a comfy api based on hooks, isn't boilerplatey or opinionated.

## Implementation example

```jsx
import useStore from '../../store'

function BasemapSelect(props) {
    const [ basemap, setBasemap ] = useStore(state => [
        state.basemap, state.setBasemap
    ], shallow);

    return (
        <Select
            value={basemap}
            onChange={(event) => {setBasemap(event.target.value)}}
        >
        /*... some MenuItem ...*/
        </Select>
   ):
}
```

## get the value in another component

```jsx
import useStore from '../../store'

function Map(props) {
    const basemap = useStore(state => state.basemap)
    /*...*/
}
```

## Arkeopen implementations

There are 3 stores in arkeopen :

- `useStore`
  This store contain general usage of the application (is a side panel opened, etc.)
- `useSearchStore`
  This is the Search store, it contain all the current filter selection
- `useValidatedSearchStore`
  This is the Validated Search Store, it contain mostly a copy of the Search Store, but when it was validated (with the search button).
