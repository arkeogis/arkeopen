# React routing

We use [react-router-dom v6](https://github.com/remix-run/react-router)

## Example adding a page

Import a component (Here, "About") and add the root in App.js :
```jsx
import About from './components/About'

<Routes>
  <Route exact path="/" element={<Main/>}/>
  <Route path="/about" element={<About/>}/>
</Routes>
```

## ArkeoLink

Because ArkeOpen and ArkeoGIS also uses parameters in URL for routing, there is a mix between path and parameters. To help this, we use `ArkeoLink` which can be used to create links with parameters, and help in somes actions like closing panels or opening them.

### Examples of usage:

1. Simple navigation using helpPage
```jsx
<ArkeoLink helpPage="interface">
  {t("components.BurgerModal.link.interface")}
</ArkeoLink>
```

1. Navigation to specific routes using 'to' prop
```jsx
<ArkeoLink to="/chronologies" className={styles.smallLink}>
  {t("components.BurgerModal.link.chronologies")}
</ArkeoLink>
```
