# Multi-apps (ArkeOpen / ArkeoGIS) code / build

## Presentation

This repository contain the code for two applications in one :

- ArkeOpen
- ArkeoGIS

## How to choose / build ArkeOpen or ArkeoGIS

Build the ArkeOpen app :

```shell
npm run arkeopen-build
```

Build the ArkeoGIS app :

```shell
npm run arkeogis-build
```

## How to run the development server

Running the Arkeopen development server :

```shell
npm run arkeopen-start
```

Running the ArkeoGIS development server :

```shell
npm run arkeogis-start
```

## Every npm actions have a arkeopen- or arkeogis- version if needed

## coding for ArkeOpen or ArkeoGIS ?

You can add some directives in the code, for example :

```js
if (process.env.REACT_APP_ARKEOGIS) {
  console.log("Hello, i'm ArkeoGIS !");
}
if (process.env.REACT_APP_ARKEOPEN) {
  console.log("Hello, i'm ArkeOpen !");
}
```

To make an import conditional, you should not use "import", but "require", like this :

```js
const App =
  process.env.REACT_APP_ARKEOGIS || process.env.REACT_APP_ARKEOPEN
    ? require("./App").default
    : require("./ChooseApp").default;
```
